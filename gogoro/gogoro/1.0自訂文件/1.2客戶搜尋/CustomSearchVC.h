
#import <UIKit/UIKit.h>
#import "CustomDocumentVC.h"

@interface CustomSearchVC : TemplateVC
@property (nonatomic, strong) NSMutableArray *arrayStore,*arrayPerson,*arrayFunctions;
@property NSInteger selectedIndex_Store,selectedIndex_Person,selectedIndex_Function;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *orderNOTextField;

@property (nonatomic,weak) IBOutlet UIButton *ButtonStore;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPerson;
@property (nonatomic,weak) IBOutlet UIButton *ButtonFunction;
@property (nonatomic,weak) IBOutlet UITextField *TextStartDate;      //開始日期
@property (nonatomic,weak) IBOutlet UITextField *TextEndDate;        //結束日期
@property (nonatomic,strong) NSString *stringFunction;
@property (nonatomic,strong) NSString *stringStore;
@property (nonatomic,strong) NSString *stringPerson;
@property (nonatomic,strong) NSString *stringFirstName;
@property (nonatomic,strong) NSString *stringLastName;
@property (nonatomic,strong) NSString *stringEmail;
@property (nonatomic,strong) NSString *stringPhone;
@property (nonatomic,strong) NSString *stingOrderNO;

@property (nonatomic,weak) CustomDocumentVC *sourceVC;
@property NSInteger attachTypeId; //附件類型（首次參觀:101, 預約試乘: 102）
- (IBAction)submitButtonClick:(id)sender;

@end
