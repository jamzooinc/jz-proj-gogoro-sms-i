
#import "CustomSearchVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "RMDateSelectionViewController.h"
#import "RMActionController.h"

@interface CustomSearchVC () {
    UIButton *selectedButton;
    UITextField *selectedTextField;
}
@end

@implementation CustomSearchVC
@synthesize arrayPerson;
@synthesize arrayStore;
@synthesize arrayFunctions;
@synthesize selectedIndex_Person;
@synthesize selectedIndex_Store;
@synthesize selectedIndex_Function;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    selectedIndex_Person = -1;
    selectedIndex_Store = -1;
    selectedIndex_Function = -1;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //帶入前一頁的資料 "1.2客戶搜尋 & 2.1客戶搜尋服務項目、服務門市、服務人員，一進入時為空白(可以不選)"
    //[_ButtonFunction setTitle:_stringFunction forState:UIControlStateNormal];
    //[_ButtonStore setTitle:_stringStore forState:UIControlStateNormal];
    //[_ButtonPerson setTitle:_stringPerson forState:UIControlStateNormal];
    
    //1.2 客戶搜尋 和 2.1 客戶搜尋 幫他帶入預設一個服務項目
    if ([arrayFunctions count] > 0) {
        selectedIndex_Function = 0;
        NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
        NSString *title = [_dic objectForKey:@"Name"];
        [_ButtonFunction setTitle:title forState:UIControlStateNormal];
    }
    
    self.nameTextField.text = [NSString stringWithFormat:@"%@%@",_stringLastName,_stringFirstName];
    self.phoneTextField.text = _stringPhone;
    self.emailTextField.text = _stringEmail;
    self.orderNOTextField.text = _stingOrderNO;
    [self showOrderNOTextFieldWithSearchCondition];
}

#pragma mark - Call API
#pragma mark [011] 取得問卷門市及服務人員列表
- (void) callSurveyGetShopSalesList {
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //問卷ID
    NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
    [parameter setObject:[_dic objectForKey:@"SurveyId"] forKey:@"SurveyId"];
    
    [GatewayManager callSurveyGetShopSalesList:parameter Source:self];
}
- (void)callBackSurveyGetShopSalesList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        [arrayStore removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayStore addObjectsFromArray:data];
        
        /*//預設載入第一筆
        if ([arrayStore count] > 0) {
            selectedIndex_Store = 0;
            NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
            NSString *ShopName = [_dic objectForKey:@"ShopName"];
            [_ButtonStore setTitle:ShopName forState:UIControlStateNormal];
            
            //取出該店的人
            [arrayPerson removeAllObjects];
            NSArray *Accounts = [_dic objectForKey:@"Accounts"];
            [arrayPerson addObjectsFromArray:Accounts];
            
            //預設載入第一筆
            if ([arrayPerson count] > 0) {
                selectedIndex_Person = 0;
                NSString *name = [arrayPerson objectAtIndex:selectedIndex_Person];
                [_ButtonPerson setTitle:name forState:UIControlStateNormal];
            }
        }*/
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [003] 搜尋客戶
- (void)callCustomerFind
{
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_nameTextField.text forKey:@"name"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    [parameter setObject:_orderNOTextField.text forKey:@"orderNo"];
    
    //SurveyId 問卷ID
    if (selectedIndex_Function >= 0) {
        NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
        [parameter setObject:[_dic objectForKey:@"SurveyId"] forKey:@"SurveyId"];
    }
    
    //attachTypeId 同意書類型
    //null: 除試乘同意書外所有文件
    //102: 試乘同意書
    if (_attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
        [parameter setObject:[NSString stringWithFormat:@"%ld",_attachTypeId] forKey:@"attachTypeId"];
    }
    else {
        //[parameter setObject:@"" forKey:@"attachTypeId"];
    }
    
    //ShopName 服務門市
    if (selectedIndex_Store >= 0) {
        NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
        [parameter setObject:[_dic objectForKey:@"ShopName"] forKey:@"ShopName"];
    }
    
    //AccountName 服務人員
    if (selectedIndex_Person >= 0) {
        NSString *AccountName = [arrayPerson objectAtIndex:selectedIndex_Person];
        [parameter setObject:AccountName forKey:@"AccountName"];
    }
    
    //開始日期
    if ([_TextStartDate.text length] > 0) {
        [parameter setObject:_TextStartDate.text forKey:@"createStartDate"];
    }
    
    //結束日期
    if ([_TextEndDate.text length] > 0) {
        [parameter setObject:_TextEndDate.text forKey:@"createEndDate"];
    }
    
    [GatewayManager callCustomerFind:parameter Source:self];
}
- (void)callBackCustomerFind:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //判斷有沒有記錄
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        BOOL HasRecord = [[Data objectForKey:@"HasRecord"] boolValue];
        NSArray *Customers = [Data objectForKey:@"Customers"];
        if (HasRecord || [Customers count] > 0) { //有記錄
            [self dismissViewControllerAnimated:NO completion:^(void) {
                NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
                
                //組成搜尋的參數
                NSMutableDictionary *dataParameter = [[NSMutableDictionary alloc] init];
                [dataParameter setObject:@"" forKey:@"firstName"];
                [dataParameter setObject:_nameTextField.text forKey:@"lastName"];
                [dataParameter setObject:_phoneTextField.text forKey:@"phone"];
                [dataParameter setObject:_emailTextField.text forKey:@"email"];
                [dataParameter setObject:_orderNOTextField.text forKey:@"orderNo"];

                
                if ([_ButtonStore.titleLabel.text isEqualToString:@"門市"]) {
                    [dataParameter setObject:@"" forKey:@"shopName"];
                }
                else {
                    [dataParameter setObject:_ButtonStore.titleLabel.text forKey:@"shopName"];
                }
                
                if ([_ButtonPerson.titleLabel.text isEqualToString:@"人員"]) {
                    [dataParameter setObject:@"" forKey:@"accountName"];
                }
                else {
                    [dataParameter setObject:_ButtonPerson.titleLabel.text forKey:@"accountName"];
                }
                
                //開始日期
                if ([_TextStartDate.text length] > 0) {
                    [dataParameter setObject:_TextStartDate.text forKey:@"createStartDate"];
                }
                
                //結束日期
                if ([_TextEndDate.text length] > 0) {
                    [dataParameter setObject:_TextEndDate.text forKey:@"createEndDate"];
                }
                
                [dataParameter setObject:[_dic objectForKey:@"SurveyId"] forKey:@"SurveyId"];
                
                [_sourceVC GotoListPageWithData:dataParameter DidFromSearch:YES];
            }];
        }
        else { //沒記錄
            [self showAlertControllerWithMessage:@"查無條件相符資料！"];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - TextField Delegate
-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    //記錄做動的物件
    selectedTextField = textField;
    
    if (textField == _TextStartDate ||             //開始日期
        textField == _TextEndDate               //結束日期
        ) {
        [self ShowDateSelectionWithTextField:textField];
        
        return NO;
    }
    
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //限制輸入數字
    NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789#"] invertedSet];
    
    if (range.length == 1){
        return YES;
    }
    else{
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
    }
    
    return YES;
}
#pragma mark - 選擇日期
-(void) ShowDateSelectionWithTextField:(UITextField *)textField {
    NSString *message = nil;
    if (textField == _TextStartDate) { //開始日期
        message = @"開始日期";
    }
    else if (textField == _TextEndDate) { //結束日期
        message = @"結束日期";
    }
    
    //Create select action
    RMAction *selectAction = [RMAction actionWithTitle:@"選擇" style:RMActionStyleDone andHandler:^(RMActionController *controller) {
        NSLog(@"Successfully selected date: %@", ((UIDatePicker *)controller.contentView).date);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy/MM/dd";
        NSString *stringToDelegate = [formatter stringFromDate:((UIDatePicker *)controller.contentView).date];
        //NSLog(@"stringToDelegate:%@",stringToDelegate);
        
        selectedTextField.text = stringToDelegate;
    }];
    
    //Create cancel action
    RMAction *cancelAction = [RMAction actionWithTitle:@"取消" style:RMActionStyleCancel andHandler:^(RMActionController *controller) {
        NSLog(@"Date selection was canceled");
    }];
    
    message = [NSString stringWithFormat:@"請選擇%@",message];
    
    //Create date selection view controller
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleDefault selectAction:selectAction andCancelAction:cancelAction];
    dateSelectionController.title = message;
    dateSelectionController.message = @"";
    
    //Create now action and add it to date selection view controller
    RMAction *nowAction = [RMAction actionWithTitle:@"今天" style:RMActionStyleAdditional andHandler:^(RMActionController *controller) {
        ((UIDatePicker *)controller.contentView).date = [NSDate date];
        NSLog(@"Now button tapped");
    }];
    nowAction.dismissesActionController = NO;
    
    [dateSelectionController addAction:nowAction];
    
    //設定時區
    NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    dateSelectionController.datePicker.locale = datelocale;
    dateSelectionController.datePicker.timeZone = [NSTimeZone timeZoneWithName:@"GMT+8"];
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    
    //不能大於今天
    //dateSelectionController.datePicker.minimumDate = [NSDate date];
    
    //On the iPad we want to show the date selection view controller within a popover.
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        //First we set the modal presentation style to the popover style
        dateSelectionController.modalPresentationStyle = UIModalPresentationPopover;
        
        //Then we tell the popover presentation controller, where the popover should appear
        dateSelectionController.popoverPresentationController.sourceView = self.view;
        dateSelectionController.popoverPresentationController.sourceRect = selectedTextField.frame;
    }
    
    //Now just present the date selection controller using the standard iOS presentation method
    [self presentViewController:dateSelectionController animated:YES completion:nil];
}
#pragma mark - Button Action
- (IBAction)pressLeftBtn:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}
/* 送出 */
- (IBAction)submitButtonClick:(id)sender
{
    [self callCustomerFind];
}
-(IBAction)pressFunctionSelectBtn:(id)sender { //問券選擇
    selectedButton = _ButtonFunction;
    [self ShowFunctionSelect];
}
-(IBAction)pressStoreSelectBtn:(id)sender { //門市選擇
    selectedButton = _ButtonStore;
    [self ShowStoreSelect];
}
-(IBAction)pressStorePersonSelectBtn:(id)sender { //門市人員選擇
    //要先選門市，再選人員
    if (selectedIndex_Store < 0) {
        [self pressStoreSelectBtn:nil];
    }
    else {
        selectedButton = _ButtonPerson;
        [self ShowPersonSelect];
    }
}
#pragma mark - 選擇問券
-(void) ShowFunctionSelect { //選擇問券
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇項目" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayFunctions count]; i++) {
        NSDictionary *_dic = [arrayFunctions objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonFunction;
    popPresenter.sourceRect = _ButtonFunction.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - 選擇門市與人員
-(void) ShowStoreSelect { //選擇門市
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayStore count]; i++) {
        NSDictionary *_dic = [arrayStore objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"ShopName"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonStore;
    popPresenter.sourceRect = _ButtonStore.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
-(void) ShowPersonSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市人員" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayPerson count]; i++) {
        NSString *title = [arrayPerson objectAtIndex:i];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    //[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
    //style:UIAlertActionStyleDestructive
    //handler:^(UIAlertAction *action) {
    //}]];
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonPerson;
    popPresenter.sourceRect = _ButtonPerson.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    if (selectedButton == _ButtonFunction) {
        selectedIndex_Function = buttonIndex;
        
        NSDictionary *_dic = [arrayFunctions objectAtIndex:buttonIndex];
        [_ButtonFunction setTitle:[_dic objectForKey:@"Name"] forState:UIControlStateNormal];
        
        [self showOrderNOTextFieldWithSearchCondition];
        //抓取對應的門市人員
        [self callSurveyGetShopSalesList];
    }
    else if (selectedButton == _ButtonStore) {
        selectedIndex_Store = buttonIndex;
        
        NSDictionary *_dic = [arrayStore objectAtIndex:buttonIndex];
        [_ButtonStore setTitle:[_dic objectForKey:@"ShopName"] forState:UIControlStateNormal];
        
        //取出該店的人
        selectedIndex_Person = -1;
        [_ButtonPerson setTitle:@"" forState:UIControlStateNormal];
        [arrayPerson removeAllObjects];
        NSArray *Accounts = [_dic objectForKey:@"Accounts"];
        [arrayPerson addObjectsFromArray:Accounts];
        
        //選人
        //[self pressStorePersonSelectBtn:nil];
    }
    else if (selectedButton == _ButtonPerson) {
        selectedIndex_Person = buttonIndex;
        
        NSString *name = [arrayPerson objectAtIndex:selectedIndex_Person];
        [_ButtonPerson setTitle:name forState:UIControlStateNormal];
    }
}
#pragma mark - 換頁
-(IBAction)pressDismissBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^(void) {
        
    }];
}

#pragma mark - 根據所選查詢條件決定是否顯示
/**
 根據所選搜尋條件，決定是否顯示訂單編號查詢欄位
 <當選擇 Gogoro 2 交車 時顯示訂單編號查詢欄位>
 
 */
- (void)showOrderNOTextFieldWithSearchCondition
{
    if ([_ButtonFunction.titleLabel.text isEqualToString:@"Gogoro 2 交車"])
    {
        _orderNOTextField.hidden = NO;
    }
    else
    {
        _orderNOTextField.text = @"";
        _orderNOTextField.hidden = YES;
    }
}
@end
