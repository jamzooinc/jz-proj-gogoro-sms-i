
#import <UIKit/UIKit.h>

@interface CustomDocumentVC : TemplateVC

@property (nonatomic,strong) NSMutableDictionary *customerDataAfterCreateCustomer;
@property BOOL shouldGotoNextFunctionAfterCreateCustomer;
@property NSInteger attachTypeId; //附件類型（首次參觀:101, 預約試乘: 102）
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *orderNOTextField;


- (IBAction)submitButtonClick:(id)sender;
-(void) GotoListPageWithData:(NSMutableDictionary *)data DidFromSearch:(BOOL)didFromSearch; //1.1客戶資料比對
-(void) GotoCustomerDocumentSignVCWithCustomerID:(NSString *)customerID; //1.3選擇簽核文件
@end
