


#import "CustomerDocumentSignVCTitleView.h"

@implementation CustomerDocumentSignVCTitleView
@synthesize delegate;
#pragma mark - Delegate
-(void) CustomerDocumentSignVCTitleViewDidExpand:(BOOL)didExpand AtIndex:(NSInteger)index {
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCTitleViewDidExpand:AtIndex:)]) {
        [[self delegate] CustomerDocumentSignVCTitleViewDidExpand:didExpand AtIndex:index];
    }
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data {
    NSString *title = [data objectForKey:@"Category"];
    _LabelTitle.text = title;
}
-(IBAction)pressExpandOrCollapseBtn:(id)sender {
    _isExpand = !_isExpand;
    [self CustomerDocumentSignVCTitleViewDidExpand:_isExpand AtIndex:self.tag];
}
-(void) SetCollaspe { //關閉
    _isExpand = NO;
    _ImageArrow.image = [UIImage imageNamed:@"arrow_close.png"];
}
-(void) SetExpand { //展開
    _isExpand = YES;
    _ImageArrow.image = [UIImage imageNamed:@"arrow_open.png"];
}
@end
