
#import <UIKit/UIKit.h>
#import "CustomerDocumentSignVCView.h"
#import "CustomerDocumentSignVCTitleView.h"

@interface CustomerDocumentSignVC : TemplateVC
<
CustomerDocumentSignVCViewDelegate,
CustomerDocumentSignVCTitleViewDelegate
>

//客戶ID
@property (strong, nonatomic) NSString *customerID;

//附件類型（首次參觀:101, 預約試乘: 102）
@property NSInteger attachTypeId;

//問券ID
@property (strong, nonatomic) NSString *surverID;

 //1.6 問卷QR Code完成後下一步功能
-(void) GotoNextFunctionAfterQRCode;

@end
