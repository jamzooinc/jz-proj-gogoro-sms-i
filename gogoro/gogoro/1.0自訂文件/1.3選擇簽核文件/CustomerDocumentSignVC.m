#import "CustomerDocumentSignVC.h"
#import "UploadFileVC.h"
#import "PhotoViewerVC.h"
#import "ConsentVC.h"
#import "TransferConfirmDMSVC.h"
#import "TransferNewEmailVC.h"
#import "CustomerDocumentQRCodeVC.h"
#import "CustomerDocumentWebQuestionVC.h"

@interface CustomerDocumentSignVC ()
{
    NSDictionary *dicData;
    NSInteger indetityType;                 //身分別 0個人 1法人
    NSMutableArray *arrayData,
                   *arrayRawData;           //下方文件資料
    NSInteger selectedIndex;
    NSMutableArray *arrayModifiedAttachedID; //有異動的附件ID
    CGFloat totalHeight,gapBetweenView;
    NSString *ownerName,*buyerName;
}

@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UILabel *LabelName;
@property (nonatomic,weak) IBOutlet UILabel *LabelEmail;
@property (nonatomic,weak) IBOutlet UILabel *LabelPhone;
@property (nonatomic,weak) IBOutlet UILabel *LabelStore;
@property (nonatomic,weak) IBOutlet UILabel *LabelPerson;
@property (nonatomic,weak) IBOutlet UILabel *LabelDate;

@end

@implementation CustomerDocumentSignVC

#pragma mark -
#pragma mark - Life Circle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrayData = [NSMutableArray new];
    arrayRawData = [NSMutableArray new];
    arrayModifiedAttachedID = [NSMutableArray new];
    
    [self setupNavigationItemTitle:@"選擇簽核文件" Image:nil];
    [self LeftButtonBack];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self GetCustomerDocument];
}


#pragma mark -
#pragma mark -
#pragma mark - Call API


#pragma mark - [106] 取得客戶非訂單類文件
- (void) GetCustomerDocument
{
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [NSMutableDictionary new];
    
    //客戶ID
    [parameter setObject:_customerID forKey:@"customerId"];
    
    //（首次參觀:101, 預約試乘: 102）
    if (_attachTypeId == 102)
    {
        [parameter setObject:@"102" forKey:@"attachTypeId"];
    }
    
    //問券ID
    [parameter setObject:_surverID forKey:@"SurveyId"];
    
    [GatewayManager callContractGetCustomerDocument:parameter Source:self];
}

- (void) callBackContractGetCustomerDocument:(NSDictionary *)parameter
{
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess)
    {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //更新UI
        dicData = [[NSDictionary alloc] initWithDictionary:Data];
        
        //姓名
        _LabelName.text = [dicData objectForKey:@"Name"];
        
        //Email
        _LabelEmail.text = [dicData objectForKey:@"Email"];
        
        //電話
        _LabelPhone.text = [dicData objectForKey:@"Phone"];
        
        //服務門市
        _LabelStore.text = [dicData objectForKey:@"StoreName"];
        
        //服務人員
        _LabelPerson.text = [dicData objectForKey:@"SalesName"];

        //建立日期
        _LabelDate.text = [dicData objectForKey:@"CreateDate"];
        
        //文件類別與內容
        NSArray *Attachs = [Data objectForKey:@"CategoryDocuments"];
        [arrayRawData removeAllObjects];
        [arrayRawData addObjectsFromArray:Attachs];
        
        //組成資料
        [arrayData removeAllObjects];
        for (NSInteger i = 0; i < [arrayRawData count]; i++)
        {
            NSDictionary *_dic = [arrayRawData objectAtIndex:i];
            
            //類別標題
            NSMutableDictionary *dicToAdd = [[NSMutableDictionary alloc] initWithDictionary:_dic];
            
            //預設是關閉，所以先移除文件列表
            [dicToAdd removeObjectForKey:@"Documents"];
            
            [arrayData addObject:dicToAdd];
        }
        //產生項目
        [self GenerateSubview];
    }
    else
    {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}


#pragma mark -
#pragma mark - Private Function
#pragma mark -


#pragma mark - UI產生
-(void) GenerateSubview
{
    //移除現有的
    for (id _subView in _ScrollView.subviews) {
        if ([_subView isKindOfClass:[CustomerDocumentSignVCTitleView class]] ||
            [_subView isKindOfClass:[CustomerDocumentSignVCView class]]) {
            [_subView removeFromSuperview];
        }
    }
    
    gapBetweenView = 15;
    totalHeight = 20;
    
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *dic = [arrayData objectAtIndex:i];
        NSArray *Documents = [dic objectForKey:@"Documents"];
        
        //產生標題
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"CustomerDocumentSignVCTitleView" owner:self options:nil];
        CustomerDocumentSignVCTitleView *view = [toplevels objectAtIndex:0];
        view.tag = i;
        view.delegate = self;
        view.frame = CGRectMake(0, totalHeight, _ScrollView.frame.size.width, 40);
        [_ScrollView addSubview:view];
        [view UpdateUIWithData:dic];
        
        if ([Documents count] > 0) {
            [view SetExpand];
        }
        else {
            [view SetCollaspe];
        }
        
        totalHeight = view.frame.origin.y + view.frame.size.height + gapBetweenView;
        
        //產生文件
        for (NSInteger j = 0; j < [Documents count]; j++) {
            NSDictionary *dicDocument = [Documents objectAtIndex:j];
            NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"CustomerDocumentSignVCView" owner:self options:nil];
            CustomerDocumentSignVCView *view = [toplevels objectAtIndex:0];
            view.delegate = self;
            view.rawIndex = i;
            view.tag = j;
            view.frame = CGRectMake(0, totalHeight, _ScrollView.frame.size.width, 98);
            [_ScrollView addSubview:view];
            [view UpdateUIWithData:dicDocument];
            
            totalHeight = view.frame.origin.y + view.frame.size.height + gapBetweenView;
        }
    }
    
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}


#pragma mark - CustomerDocumentSignVCTitleViewDelegate
#pragma mark - 項目展開/關閉
-(void) CustomerDocumentSignVCTitleViewDidExpand:(BOOL)didExpand AtIndex:(NSInteger)index
{
    if (didExpand) {
        [self AddDocumentListAtRawDataIndex:index];
    }
    else {
        [self RemoveDocumentListAtRawDataIndex:index];
    }
}
-(void) AddDocumentListAtRawDataIndex:(NSInteger) index { // StartIndex:(NSInteger)startIndex
    //從原始資料取出第n筆的文件，加入列表資料中
    /*NSDictionary *_dic = [arrayRawData objectAtIndex:index];
     NSArray *Documents = [_dic objectForKey:@"Documents"];
     for (NSInteger i = 0; i < [Documents count]; i++) {
     NSDictionary *_dicDocument = [Documents objectAtIndex:i];
     NSMutableDictionary *dicToAdd = [[NSMutableDictionary alloc] initWithDictionary:_dicDocument];
     [dicToAdd setObject:[NSNumber numberWithBool:NO] forKey:@"isTitle"];
     [arrayData insertObject:dicToAdd atIndex:startIndex];
     
     //判斷該文件是否已勾
     BOOL IsChecked = [[_dicDocument objectForKey:@"IsChecked"] boolValue];
     if (IsChecked) { //已勾的文件，記起來等上傳API的時候使用
     NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
     [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
     }
     }*/
    
    //取出原始資料的文件
    NSDictionary *_dicRaw = [arrayRawData objectAtIndex:index];
    NSArray *Documents = [_dicRaw objectForKey:@"Documents"];
    
    //加入
    NSMutableDictionary *_dic = [arrayData objectAtIndex:index];
    [_dic setObject:Documents forKey:@"Documents"];
    
    //重新產生UI
    [self GenerateSubview];
}
-(void) RemoveDocumentListAtRawDataIndex:(NSInteger) index {
    NSMutableDictionary *_dic = [arrayData objectAtIndex:index];
    [_dic removeObjectForKey:@"Documents"];
    
    //重新產生UI
    [self GenerateSubview];
}

#pragma mark - CustomerDocumentSignVCViewDelegate
#pragma mark - 勾選/取消勾選
-(void) CustomerDocumentSignVCViewCheckAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex { //勾選
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];

    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}

-(void) CustomerDocumentSignVCViewUnCheckAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex { //取消勾選
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];

    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID removeObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}

#pragma mark - 重簽(重新套版)
-(void) CustomerDocumentSignVCViewReTemplateAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex {
    selectedIndex = index;
    
    /*NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];

    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [self ReAapplyDocumentWithAttachType:AttachTypeId];*/
    
    [self GotoConsentResignAtRawIndex:(NSInteger)atRawIndex];
}
#pragma mark - 上傳
-(void) CustomerDocumentSignVCViewUploadAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex { //上傳
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];

    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    NSString *memo = [_dic objectForKey:@"Memo"];
    [self GotoUploadFileVCWithType:AttachTypeId AttachName:AttachTypeName Memo:memo];
}
#pragma mark - FileUrl
-(void) CustomerDocumentSignVCViewFileUrldAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex { //fileUrl
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];
    
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    
    if ([FileUrl length] == 0) {
        [self showAlertControllerWithMessage:@"請先上傳電子檔！"];
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];
    
    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([FileUrl rangeOfString:@".jpg"].length > 0 || [FileUrl rangeOfString:@".png"].length > 0 || [FileUrl rangeOfString:@".JPG"].length > 0 || [FileUrl rangeOfString:@".PNG"].length > 0) {
        [self GotoPhotoViewerWithUrl:FileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoSignedDocumentWithData:_dic FromTempUrl:NO];
    }
}
#pragma mark - 預覽列印
-(void) CustomerDocumentSignVCViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl AtRawIndex:(NSInteger)atRawIndex { //預覽列印
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];
    
    if ([urlString length] == 0) {
        return;
    }
    
    //測試
    //TemplateFileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    //NSLog(@"TemplateFileUrl:%@",TemplateFileUrl);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([urlString rangeOfString:@".jpg"].length > 0 || [urlString rangeOfString:@".png"].length > 0 || [urlString rangeOfString:@".JPG"].length > 0 || [urlString rangeOfString:@".PNG"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:urlString Name:AttachTypeName];
    }
    else { //pdf
        [self GotoSignedDocumentWithData:_dic FromTempUrl:fromTempUrl];
    }
}
#pragma mark - 以客人手機掃描QR Code
-(void) CustomerDocumentSignVCViewQRCodeAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex {
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];
    NSString *urlString = [_dic objectForKey:@"SurveyUrl"];
    [self GotoCustomerDocumentQRCodeVCWithUrlstring:urlString];
}
#pragma mark - 在此平板裝置開啟問卷
-(void) CustomerDocumentSignVCViewOpenQuestionAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex {
    selectedIndex = index;
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];
    NSString *urlString = [_dic objectForKey:@"SurveyUrl"];
    [self GotoCustomerDocumentWebQuestionVCWithUrlstring:urlString];
}
#pragma mark - Button Action
-(IBAction)pressSendBtn:(id)sender { //送出
    //NSString *newOwnerEmail = _TextNewOwnerEmail.text;
    
    //確認車牌與身分證皆有填寫
    /*if ([carNo length] == 0 || [ID length] == 0) {
        [self showAlertControllerWithMessage:@"請確認車牌號碼與原車主身分證皆有填寫！"];
    }
    else {
        [self TransferGetDetail];
    }*/
}
-(IBAction)pressPreviousStepBtn:(id)sender { //上一步
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)pressNextStepBtn:(id)sender { //下一步
    //6.1.3-2 DMS過戶說明文件
    //[self GotoProcessDocument];
    //return;
    
    //文件完整（都勾選），取得取得過戶移轉案件詳細資料
    //[self TransferGet];
    //return;
    
    //2016.10.19 全勾才能申請
    NSInteger totalCount = 0;
    NSInteger needCount = [arrayData count];
    NSInteger noCehckIndex = -1; //哪一筆沒有打勾
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *_dic = [arrayData objectAtIndex:i];
        BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
        if (IsChecked) { //已勾
            totalCount++;
        }
        else { //未勾，判斷有沒有在手動勾選清單裡面
            BOOL didExist = NO;
            NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
            for (NSString *_attachID in arrayModifiedAttachedID) {
                NSInteger attachID = [_attachID integerValue];
                if (AttachTypeId == attachID) {
                    didExist = YES;
                    totalCount++;
                }
            }
            if (!didExist) {
                noCehckIndex = i;
                break;
            }
        }
    }
    
    if (totalCount != needCount) {
        if (noCehckIndex >= 0) { //防呆
            NSDictionary *_dic = [arrayData objectAtIndex:noCehckIndex];
            NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
            NSString *message = [NSString stringWithFormat:@"請確認「%@」已勾選！",AttachTypeName];
            [self showAlertControllerWithMessage:message];
        }
        
        return;
    }
    
    //文件完整（都勾選），取得取得過戶移轉案件詳細資料
    //[self TransferGet];
    
    //6.1.3-2 DMS過戶說明文件
    //[self GotoProcessDocument];
}
#pragma mark - 換頁
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName Memo:(NSString *)memo { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    
    NSMutableDictionary *documentInfo = [NSMutableDictionary new];
    [documentInfo setObject:_LabelName.text forKey:@"Name"];
    [documentInfo setObject:_LabelEmail.text forKey:@"Email"];
    [documentInfo setObject:_LabelPhone.text forKey:@"Phone"];
    [documentInfo setObject:_LabelStore.text forKey:@"Store"];
    [documentInfo setObject:_LabelPerson.text forKey:@"Person"];
    
    destVC.documentInfo = documentInfo;
    destVC.attachTypeId = _attachTypeId;
    destVC.type = attachType;
    destVC.attachName = attachName;
    destVC.didFromCustomer = YES;
    destVC.customerID = _customerID;
    destVC.memo = memo;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentResignAtRawIndex:(NSInteger)atRawIndex { //重簽
    NSArray *Documents = [[arrayData objectAtIndex:atRawIndex] objectForKey:@"Documents"];
    NSDictionary *_dic = [Documents objectAtIndex:selectedIndex];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_PERSONAL_Data;
//    destVC.enumCONSENT =CONSENT_GOGORO2_CONTRACT_SUPPLEMENTARY_PROVISIONS;
    destVC.newAttachType = AttachTypeId;
    destVC.customerID = _customerID;
    destVC.stringTitle = [_dic objectForKey:@"AttachTypeName"];
    [self.navigationController pushViewController:destVC animated:YES];
}
/*-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //API 309 有 Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = ownerName;
    NSString *BuyName = buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = YES;
    destVC.hideEmailButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    //destVC.OrderNo = _OrderNo;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoProcessDocument { //6.1.3-2 DMS過戶說明文件
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_TRANSFER_DOC;
    destVC.dicTranfer = dicTransfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTransferConfirmDMSVC { //6.1.4確認DMS過戶資料
    TransferConfirmDMSVC *destVC = [[TransferConfirmDMSVC alloc] initWithNibName:@"TransferConfirmDMSVC" bundle:nil];
    destVC.stringCarNumber = _stringCarNumber;
    //destVC.stringCarNumberOld = _stringCarNumberOld;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) GotoTransferNewEmailVC { //退回6.1.1 輸入新車主Email
    //到補助選擇
    NSArray *array = self.navigationController.viewControllers;
    TransferNewEmailVC *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[TransferNewEmailVC class]]) {
            destVC = (TransferNewEmailVC *)vc;
            
        }
    }
    NSLog(@"destVC:%@",destVC);
    if (destVC) { //退回6.1.5 輸入租約轉讓資料
        destVC.stringNewOwnerEmail = _LabelEmail.text;
        [self.navigationController popToViewController:destVC animated:NO];
    }
    else { //6.1.5 輸入租約轉讓資料
        TransferNewEmailVC *destVC = [[TransferNewEmailVC alloc] initWithNibName:@"TransferNewEmailVC" bundle:nil];
        destVC.stringCarNumber = _stringCarNumber;
        destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
        destVC.stringNewOwnerEmail = _LabelEmail.text;
        [self.navigationController pushViewController:destVC animated:YES];
    }
}*/
-(void) GotoCustomerDocumentQRCodeVCWithUrlstring:(NSString *)urlString { //1.6問券QR Code
    CustomerDocumentQRCodeVC *destVC = [[CustomerDocumentQRCodeVC alloc] initWithNibName:@"CustomerDocumentQRCodeVC" bundle:nil];
    destVC.sourceVC = self;
    destVC.urlString = urlString;
    //[self.navigationController pushViewController:destVC animated:YES];
    [self.navigationController presentViewController:destVC animated:YES completion:nil];
}
-(void) GotoCustomerDocumentWebQuestionVCWithUrlstring:(NSString *)urlString { //1.7填寫問券
    CustomerDocumentWebQuestionVC *destVC = [[CustomerDocumentWebQuestionVC alloc] initWithNibName:@"CustomerDocumentWebQuestionVC" bundle:nil];
    //[self.navigationController pushViewController:destVC animated:YES];
    destVC.attachTypeId = _attachTypeId;
    destVC.urlString = urlString;
    [self.navigationController presentViewController:destVC animated:YES completion:nil];
}
-(void) GotoNextFunctionAfterQRCode { //1.6 問卷QR Code完成後下一步功能
    if (_attachTypeId == 102) { //預約試乘-->1.1客戶資料比對
        
    }
    else { //1.1客戶資料比對
        
    }
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //API 309 有 Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = ownerName;
    NSString *BuyName = buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = NO;
    destVC.showFinishButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoSignedDocumentWithData:(NSDictionary *)data FromTempUrl:(BOOL)fromTempUrl { //1.5預覽已簽核文件
    NSInteger AttachTypeId = [[data objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [data objectForKey:@"AttachTypeName"];
    
    NSString *FileUrl = [data objectForKey:@"FileUrl"];
    if (fromTempUrl) {
        FileUrl = [data objectForKey:@"TemplateFileUrl"];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = FileUrl;
    destVC.stringTitle = AttachTypeName;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = NO;
    destVC.showFinishButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = nil;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
