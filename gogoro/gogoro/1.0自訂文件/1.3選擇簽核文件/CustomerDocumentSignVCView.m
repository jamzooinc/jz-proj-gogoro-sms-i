


#import "CustomerDocumentSignVCView.h"

@implementation CustomerDocumentSignVCView

@synthesize delegate;
#pragma mark - Delegate
-(void) CustomerDocumentSignVCViewCheckAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex{ //勾選
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewCheckAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewCheckAtIndex:index AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewUnCheckAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex{ //取消勾選
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewUnCheckAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewUnCheckAtIndex:index AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewUploadAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex{ //上傳
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewUploadAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewUploadAtIndex:index AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewReTemplateAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex{ //重新套版
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewReTemplateAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewReTemplateAtIndex:index AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewFileUrldAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex{ //fileUrl
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewFileUrldAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewFileUrldAtIndex:index AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl AtRawIndex:(NSInteger)atRawIndex{ //預覽列印
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewPreviewPrintAtIndex:Rect:UrlString:FromTempUrl:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewPreviewPrintAtIndex:index Rect:rect UrlString:urlString FromTempUrl:fromTempUrl AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewQRCodeAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex {
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewQRCodeAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewQRCodeAtIndex:index AtRawIndex:atRawIndex];
    }
}
-(void) CustomerDocumentSignVCViewOpenQuestionAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex {
    if ([[self delegate] respondsToSelector:@selector(CustomerDocumentSignVCViewOpenQuestionAtIndex:AtRawIndex:)]) {
        [[self delegate] CustomerDocumentSignVCViewOpenQuestionAtIndex:index AtRawIndex:atRawIndex];
    }
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data {
    NSMutableArray *arrayButtons = [[NSMutableArray alloc] init];
    arrayPreviewPrint = [[NSMutableArray alloc] init];
    arrayQuestion = [[NSMutableArray alloc] init];
    
    _LabelTitle.text = [data objectForKey:@"AttachTypeName"];
    NSLog(@"文件名稱:%@",_LabelTitle.text);
    
    //IsOriginal 正本文件
    BOOL IsOriginal = [[data objectForKey:@"IsOriginal"] boolValue];
    //IsOriginal = YES;
    if (IsOriginal) {
        //NSLog(@"正本文件:%@",_LabelTitle.text);
    }
    
    //是否已勾
    IsChecked = [[data objectForKey:@"IsChecked"] boolValue];
    if (IsChecked) { //已勾的文件，不能取消已勾
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_y.png"];
        _ButtonCheck.hidden = YES; //隱藏勾選按鈕
    }
    else { //未勾的文件
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_n.png"];
        
        //正本文件：如果沒勾的話要可以勾，（上傳後）已勾的話不能取消
        if (IsOriginal) {
            _ButtonCheck.hidden = NO;
        }
        else { //其他文件不能手動打勾
            _ButtonCheck.hidden = YES;
        }
    }
    
    //是否重套版
    //2016.12.13 改為重簽
    BOOL NeedSign = [[data objectForKey:@"NeedPrintSign"] boolValue]; //NeedSign
    //NeedSign = NO;
    if (NeedSign) {
        [arrayButtons addObject:_ButtonReSign];
        NSLog(@"NeedPrintSign:YES");
    }
    else {
        NSLog(@"NeedPrintSign:NO");
    }
    
    //是否需要上傳檔案
    BOOL IsUploadFile = [[data objectForKey:@"IsUploadFile"] boolValue];
    //IsUploadFile = YES;
    if (IsUploadFile) {
        [arrayButtons addObject:_ButtonUpload];
        NSLog(@"IsUploadFile:YES");
    }
    else {
        NSLog(@"IsUploadFile:NO");
    }
    
    //是否可以列印
    //2016.10.13 改為預覽列印
    BOOL IsOutPaper = [[data objectForKey:@"IsOutPaper"] boolValue];
    //IsOutPaper = YES;
    if (IsOutPaper) {
        //[arrayButtons addObject:_ButtonPrint];
        [arrayButtons addObject:_ButtonPreviewPrint];
    }
    else {
    }
    
    //判斷要不要顯示相機按鈕
    //if (IsUploadFile || UseTemplate) {
        _ImageCamera.hidden = NO;
        _ButtonCamera.hidden = NO;
    //}
    
    NSString *TemplateFileUrl = [data objectForKey:@"TemplateFileUrl"];
    BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //hasTemplateFileUrl = YES;
    if (hasTemplateFileUrl) {
        NSDictionary *_dic = [NSDictionary dictionaryWithObjectsAndKeys:@"尚未簽名的空白文件",@"title",TemplateFileUrl,@"url", nil];
        [arrayPreviewPrint addObject:_dic];
    }
    
    NSString *FileUrl = [data objectForKey:@"FileUrl"];
    BOOL hasFileUle = [FileUrl length] > 0;
    //hasFileUle = YES;
    if (hasFileUle) {
        NSDictionary *_dic = [NSDictionary dictionaryWithObjectsAndKeys:@"電子檔文件",@"title",FileUrl,@"url", nil];
        [arrayPreviewPrint addObject:_dic];
    }
    
    //只有【首次參觀 - 個資同意書】、【預約試乘 - 個資同意書】需要出現寫問卷按鈕
    if ([[data objectForKey:@"AttachTypeName"] rangeOfString:@"個資同意書"].length > 0) {
        [arrayButtons addObject:_ButtonQuestion];
        
        //加入選項
        [arrayQuestion addObject:@"以客人手機掃描QR Code"];
        [arrayQuestion addObject:@"在此平板裝置開啟問卷"];
    }
    
    //判斷有沒有按鈕，及按鈕位置
    //NSLog(@"arrayButtons:%ld",[arrayButtons count]);
    if ([arrayButtons count] > 0 && !_didFromECDocumentListVC) { //有按鈕 且 從5.1.1電子文件清單（功能按鈕不需作用）
        //調整自己的高度
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 108);
        
        //調整按鈕位置
        CGFloat startX = 121;
        CGFloat gap = 8;
        for (NSInteger i = 0; i < [arrayButtons count]; i++) {
            UIButton *Button = [arrayButtons objectAtIndex:i];
            /*CGFloat x = 0;
            switch (i) {
                case 0:
                    x = 121; //164
                    break;
                case 1:
                    x = 255; //298
                    break;
                case 2:
                    x = 389; //432
                    break;
                case 3:
                    x = 556;
                    break;
                default:
                    break;
            }*/
            Button.hidden = NO;
            Button.frame = CGRectMake(startX, Button.frame.origin.y, Button.frame.size.width, Button.frame.size.height);
            
            startX = startX + Button.frame.size.width + gap;
        }
    }
    else { //沒有按鈕
        
    }
}
#pragma mark - Button
-(IBAction) pressCheckBtn:(UIButton *)sender { //勾選按鈕
    if (sender.tag == 0) { //未勾-->已勾
        sender.tag = 1;
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_y.png"];
        [self CustomerDocumentSignVCViewCheckAtIndex:self.tag AtRawIndex:_rawIndex];
    }
    else if (sender.tag == 1) { //已勾-->未勾
        sender.tag = 0;
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_n.png"];
        [self CustomerDocumentSignVCViewUnCheckAtIndex:self.tag AtRawIndex:_rawIndex];
    }
}
-(IBAction) pressUploadBtn:(id)sender { //上傳
    [self CustomerDocumentSignVCViewUploadAtIndex:self.tag AtRawIndex:_rawIndex];
}
-(IBAction) pressReSignBtn:(id)sender { //重簽
    [self CustomerDocumentSignVCViewReTemplateAtIndex:self.tag AtRawIndex:_rawIndex];
}
-(IBAction) pressFileUrlBtn:(id)sender { //FileUrl
    [self CustomerDocumentSignVCViewFileUrldAtIndex:self.tag AtRawIndex:_rawIndex];
}
-(IBAction) pressPreviewPrintBtn:(id)sender { //預覽列印
    if ([arrayPreviewPrint count] == 0) return; //防呆
    [self ShowPreviewPrintSelect];
}
-(IBAction) pressQuetionBtn:(id)sender { //問券
    if ([arrayQuestion count] == 0) return; //防呆
    [self ShowQuestionSelect];
}
#pragma mark - UIAlertController Handle
-(void) ShowPreviewPrintSelect {
    didSelectQuestion = NO;
    NSString *message = [NSString stringWithFormat:@"請選擇"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayPreviewPrint count]; i++) {
        NSDictionary *_dic = [arrayPreviewPrint objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"title"];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonPreviewPrint;
    popPresenter.sourceRect = _ButtonPreviewPrint.bounds;
    
    UIViewController *sourceVC = [ObjectManager viewController:self];
    [sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) ShowQuestionSelect {
    didSelectQuestion = YES;
    NSString *message = [NSString stringWithFormat:@"請選擇"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayQuestion count]; i++) {
        NSString *title = [arrayQuestion objectAtIndex:i];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonQuestion;
    popPresenter.sourceRect = _ButtonQuestion.bounds;
    
    UIViewController *sourceVC = [ObjectManager viewController:self];
    [sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);

    if (didSelectQuestion) { //問券
        NSString *title = [arrayQuestion objectAtIndex:buttonIndex];
        if ([title isEqualToString:@"以客人手機掃描QR Code"]) {
            [self CustomerDocumentSignVCViewQRCodeAtIndex:self.tag AtRawIndex:_rawIndex];
        }
        else if ([title isEqualToString:@"在此平板裝置開啟問卷"]) {
            [self CustomerDocumentSignVCViewOpenQuestionAtIndex:self.tag AtRawIndex:_rawIndex];
        }
    }
    else { //預覽列印
        NSDictionary *_dic = [arrayPreviewPrint objectAtIndex:buttonIndex];
        
        NSString *urlString = [_dic objectForKey:@"url"];
        NSLog(@"urlString:%@",urlString);
        
        NSString *title = [_dic objectForKey:@"title"];
        
        BOOL FromTempUrl = NO;
        if ([title isEqualToString:@"尚未簽名的空白文件"]) {
            FromTempUrl = YES;
        }
        [self CustomerDocumentSignVCViewPreviewPrintAtIndex:self.tag Rect:_ButtonPreviewPrint.frame UrlString:urlString FromTempUrl:FromTempUrl AtRawIndex:_rawIndex];
    }
}
@end
