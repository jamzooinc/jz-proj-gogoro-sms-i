
#import <UIKit/UIKit.h>

@protocol CustomerDocumentSignVCTitleViewDelegate <NSObject>
@optional
-(void) CustomerDocumentSignVCTitleViewDidExpand:(BOOL)didExpand AtIndex:(NSInteger)index;
@end

@interface CustomerDocumentSignVCTitleView : UIView {
    __weak id <CustomerDocumentSignVCTitleViewDelegate> delegate;
}
@property (weak) id delegate;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIImageView *ImageArrow;
@property (nonatomic,weak) IBOutlet UIButton *ButtonArrow;
@property BOOL isExpand;
-(void) UpdateUIWithData:(NSDictionary *)data;
-(void) SetCollaspe; //關閉
-(void) SetExpand; //展開
@end
