
#import <UIKit/UIKit.h>

@protocol CustomerDocumentSignVCViewDelegate <NSObject>
@optional
-(void) CustomerDocumentSignVCViewCheckAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //勾選
-(void) CustomerDocumentSignVCViewUnCheckAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //取消勾選
-(void) CustomerDocumentSignVCViewUploadAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //上傳
-(void) CustomerDocumentSignVCViewReTemplateAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //重新套版
-(void) CustomerDocumentSignVCViewFileUrldAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //fileUrl
-(void) CustomerDocumentSignVCViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl AtRawIndex:(NSInteger)atRawIndex; //預覽列印
-(void) CustomerDocumentSignVCViewQRCodeAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //以客人手機掃描QR Code
-(void) CustomerDocumentSignVCViewOpenQuestionAtIndex:(NSInteger)index AtRawIndex:(NSInteger)atRawIndex; //在此平板裝置開啟問卷
@end

@interface CustomerDocumentSignVCView : UIView {
    __weak id <CustomerDocumentSignVCViewDelegate> delegate;
    BOOL IsChecked; //文件是否已勾選
    NSMutableArray *arrayPreviewPrint; //預覽列印的文件
    NSMutableArray *arrayQuestion; //問券的選項
    BOOL didSelectQuestion;
}
@property (weak) id delegate;
@property NSInteger rawIndex; 
@property BOOL didFromECDocumentListVC; //從5.1.1電子文件清單（功能按鈕不需作用）
@property (nonatomic,weak) IBOutlet UIImageView *ImageCamera;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCamera;
@property (nonatomic,weak) IBOutlet UIButton *ButtonUpload;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCheck;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPreviewPrint; //預覽列印
@property (nonatomic,weak) IBOutlet UIImageView *ImageCheckBox;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIButton *ButtonReSign; //重簽
@property (nonatomic,weak) IBOutlet UIButton *ButtonQuestion; //問券
-(void) UpdateUIWithData:(NSDictionary *)data;
@property BOOL didDocumentList; //YES:6.3電子文件清單 NO:6.1.8確認文件清單
@end
