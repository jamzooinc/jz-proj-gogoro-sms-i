
#import "CustomDocumentVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "CustomSearchVC.h"
#import "CustomDocumentListVC.h"
#import "CustomerDocumentSignVC.h"
#import "PhotographVC.h"

#define GOGORO2_DELIVERY 5

@interface CustomDocumentVC ()
{
    NSMutableArray *arrayStore,
                   *arrayPerson,
                   *arrayFunctions;
    
    NSInteger selectedIndex_Store,
              selectedIndex_Person,
              selectedIndex_Function;
    
    UIButton *selectedButton;
    
    UITextField *selectedTextField;
}

@property (nonatomic,weak) IBOutlet UIButton *ButtonStore;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPerson;
@property (nonatomic,weak) IBOutlet UIButton *ButtonFunction;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@end

@implementation CustomDocumentVC
@synthesize attachTypeId;

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
        _LabelTitle.text = @"預約試乘";
    }
    else {
        _LabelTitle.text = @"自訂文件";
    }
    
    arrayPerson = [[NSMutableArray alloc] init];
    arrayStore = [[NSMutableArray alloc] init];
    arrayFunctions = [[NSMutableArray alloc] init];
    selectedIndex_Person = -1;
    selectedIndex_Store = -1;
    selectedIndex_Function = -1;
    
#if TARGET_IPHONE_SIMULATOR
    if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
        _nameTextField.text = @"張";
        _firstNameTextField.text = @"小姐";
        _emailTextField.text = @"tina@gamil.com"; //Aaron1@123.com
        _phoneTextField.text = @"0982211930"; //0982123111
    }
    else {
//        _nameTextField.text = @"吳";
//        _firstNameTextField.text = @"先生";
//        _emailTextField.text = @"aaron@gamil.com"; //Aaron1@123.com
//        _phoneTextField.text = @"0982211931"; //0982123111
        _orderNOTextField.text = @"P1195";
    }
#endif
    
    //取得服務項目問卷列表
    [self callSurveyGetList];
    [self showOrderNOTextFieldWithSearchCondition];

}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self registerForKeyboardNotifications];
    
    if (_shouldGotoNextFunctionAfterCreateCustomer)
    {
        _shouldGotoNextFunctionAfterCreateCustomer = NO;
        if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
            [self callCustomerFind];
        }
        else
        {
            NSString *customerID = [_customerDataAfterCreateCustomer objectForKey:@"Id"];
            [self GotoCustomerDocumentSignVCWithCustomerID:customerID];
        }
    }
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self removeForKeyboardNotifications];
}
#pragma mark - Call API
#pragma mark [010] 取得服務項目問卷列表
- (void) callSurveyGetList {
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //附件類型（首次參觀:101, 預約試乘: 102）
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachTypeId] forKey:@"attachTypeId"];
    
    [GatewayManager callSurveyGetList:parameter Source:self];
}
- (void)callBackSurveyGetList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        [arrayFunctions removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayFunctions addObjectsFromArray:data];
        
        if ([arrayFunctions count] > 0) {
            selectedIndex_Function = 0;
            NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
            [_ButtonFunction setTitle:[_dic objectForKey:@"Name"] forState:UIControlStateNormal];
            
            //[011] 取得問卷門市及服務人員列表
            [self callSurveyGetShopSalesList];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [011] 取得問卷門市及服務人員列表
- (void) callSurveyGetShopSalesList {
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //問卷ID
    NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
    [parameter setObject:[_dic objectForKey:@"SurveyId"] forKey:@"SurveyId"];
    
    [GatewayManager callSurveyGetShopSalesList:parameter Source:self];
}
- (void)callBackSurveyGetShopSalesList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        [arrayStore removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayStore addObjectsFromArray:data];
        
        //預設載入第一筆
        if ([arrayStore count] > 0) {
            selectedIndex_Store = 0;
            NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
            NSString *ShopName = [_dic objectForKey:@"ShopName"];
            [_ButtonStore setTitle:ShopName forState:UIControlStateNormal];
            
            //取出該店的人
            [arrayPerson removeAllObjects];
            NSArray *Accounts = [_dic objectForKey:@"Accounts"];
            [arrayPerson addObjectsFromArray:Accounts];
            
            //預設載入第一筆
            if ([arrayPerson count] > 0) {
                selectedIndex_Person = 0;
                NSString *name = [arrayPerson objectAtIndex:selectedIndex_Person];
                [_ButtonPerson setTitle:name forState:UIControlStateNormal];
            }
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [003] 搜尋客戶
- (void)callCustomerFind {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSString *name = [NSString stringWithFormat:@"%@%@",_firstNameTextField.text,_nameTextField.text];
    [parameter setObject:name forKey:@"name"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    [parameter setObject:_orderNOTextField.text forKey:@"orderNo"];
    
    //SurveyId 問卷ID
    if (selectedIndex_Function >= 0) {
        NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
        [parameter setObject:[_dic objectForKey:@"SurveyId"] forKey:@"SurveyId"];
    }
    
    //attachTypeId 同意書類型
    //null: 除試乘同意書外所有文件
    //102: 試乘同意書
    if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
        [parameter setObject:[NSString stringWithFormat:@"%ld",attachTypeId] forKey:@"attachTypeId"];
    }
    else {
        //[parameter setObject:@"" forKey:@"attachTypeId"];
    }
    
    //ShopName 服務門市
    if (selectedIndex_Store >= 0) {
        NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
        [parameter setObject:[_dic objectForKey:@"ShopName"] forKey:@"ShopName"];
    }
    
    //AccountName 服務人員
    if (selectedIndex_Person >= 0) {
        NSString *AccountName = [arrayPerson objectAtIndex:selectedIndex_Person];
        [parameter setObject:AccountName forKey:@"AccountName"];
    }
    
    [GatewayManager callCustomerFind:parameter Source:self];
}

- (void)callBackCustomerFind:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //判斷有沒有記錄
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        BOOL HasRecord = [[Data objectForKey:@"HasRecord"] boolValue];
        NSArray *Customers = [Data objectForKey:@"Customers"];
        NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];
        if (HasRecord || [Customers count] > 0) { //有記錄
            
            
            //組成搜尋的參數
            NSMutableDictionary *dataParameter = [[NSMutableDictionary alloc] init];
            [dataParameter setObject:_firstNameTextField.text forKey:@"firstName"];
            [dataParameter setObject:_nameTextField.text forKey:@"lastName"];
            [dataParameter setObject:_phoneTextField.text forKey:@"phone"];
            [dataParameter setObject:_emailTextField.text forKey:@"email"];
            [dataParameter setObject:_orderNOTextField.text forKey:@"orderNo"];
            [dataParameter setObject:_ButtonStore.titleLabel.text forKey:@"shopName"];
            [dataParameter setObject:_ButtonPerson.titleLabel.text forKey:@"accountName"];
            [dataParameter setObject:[_dic objectForKey:@"SurveyId"] forKey:@"SurveyId"];
            
            [self GotoListPageWithData:dataParameter DidFromSearch:NO];
        }
        else { //沒記錄
            
            NSString *message;
            int survayId = [[_dic objectForKey:@"SurveyId"] intValue];
            
            if (survayId == GOGORO2_DELIVERY)
            {
                message = @"查無相似資料，請在DMS重新確認！";
            }
            else
            {
                message = @"查無相似資料，系統將新增一筆客戶！";
            }
            
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                if (survayId == GOGORO2_DELIVERY)
                {
                    return;
                }
                else
                {
                    //呼叫API新增資料
                    [self callCustomerData];
                }
            }];
            
            [alertController addAction:submitAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    else
    {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [004] 新增或修改客戶
- (void)callCustomerData {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:@"" forKey:@"id"];
    
    [parameter setObject:_firstNameTextField.text forKey:@"FirstName"];
    [parameter setObject:_nameTextField.text forKey:@"LastName"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    
    //ShopName 服務門市
    if (selectedIndex_Store >= 0) {
        NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
        [parameter setObject:[_dic objectForKey:@"ShopName"] forKey:@"ShopName"];
    }
    
    //AccountName 服務人員
    if (selectedIndex_Person >= 0) {
        NSString *AccountName = [arrayPerson objectAtIndex:selectedIndex_Person];
        [parameter setObject:AccountName forKey:@"AccountName"];
    }
    
    //SurveyId 問卷ID
    if (selectedIndex_Function >= 0) {
        NSDictionary *_dic2 = [arrayFunctions objectAtIndex:selectedIndex_Function];
        [parameter setObject:[_dic2 objectForKey:@"SurveyId"] forKey:@"SurveyId"];
    }
    
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        
        if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
            NSMutableDictionary *customerData = [[NSMutableDictionary alloc] init];
            [customerData setObject:[parameter objectForKey:@"Data"] forKey:@"Id"];
            [customerData setObject:_nameTextField.text forKey:@"Name"];
            [customerData setObject:_emailTextField.text forKey:@"Email"];
            [customerData setObject:_phoneTextField.text forKey:@"Phone"];
            [customerData setObject:_ButtonStore.titleLabel.text forKey:@"StoreName"];
            [customerData setObject:_ButtonPerson.titleLabel.text forKey:@"SalesName"];
            [customerData setObject:@"" forKey:@"DriverLicense"];
            
            //預約試乘-->1.8 拍攝駕照
            NSString *customerID = [parameter objectForKey:@"Data"];
            [self GotoTakePhotoWithCustomerID:customerID CustomerData:customerData]; //到2.2拍照上傳
        }
        else {
            //自訂文件-->1.3 選擇簽核文件
            NSString *customerID = [parameter objectForKey:@"Data"];
            [self GotoCustomerDocumentSignVCWithCustomerID:customerID];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}


#pragma mark - TextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //手機號碼僅可輸入數字
    if (textField == _phoneTextField)
    {
        NSCharacterSet *nonNumberSet;
        nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789#"] invertedSet];
        
        if (range.length == 1)
        {
            return YES;
        }
        else
        {
            return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
        }
    }
    return YES;
}
#pragma mark - Button Action
- (IBAction)pressSearchBtn:(id)sender { //1.2 客戶搜尋
    [self GotoCustomerSearchVC];
}

- (IBAction)pressLeftBtn:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}
/* 送出 */
- (IBAction)submitButtonClick:(id)sender
{
    //訂單編號有輸入則跳過檢查
    if ([_orderNOTextField.text isEqualToString:@""])
    {
        //檢查email格式
        if ([ObjectManager isValidateEmail:_emailTextField.text] == NO) {
            [self showAlertControllerWithMessage:@"請確認Email格式是否正確"];
            return;
        }
        
        //檢查有無填寫
        if ([_firstNameTextField.text isEqualToString:@""] ||
            [_nameTextField.text isEqualToString:@""] ||
            [_emailTextField.text isEqualToString:@""] ||
            [_phoneTextField.text isEqualToString:@""])
        {
            [self showAlertControllerWithMessage:@"請確認姓名、Email及電話皆有填寫！"];
            return;
        }
    }
    
    //關閉鍵盤
    [_firstNameTextField resignFirstResponder];
    [_nameTextField resignFirstResponder];
    [_emailTextField resignFirstResponder];
    [_phoneTextField resignFirstResponder];
    [_orderNOTextField resignFirstResponder];
    
    //檢查AppServer是否有類似資料
    [self callCustomerFind];
}
-(IBAction)pressFunctionSelectBtn:(id)sender { //問券選擇
    selectedButton = _ButtonFunction;
    [self ShowFunctionSelect];
}
-(IBAction)pressStoreSelectBtn:(id)sender { //門市選擇
    selectedButton = _ButtonStore;
    [self ShowStoreSelect];
}
-(IBAction)pressStorePersonSelectBtn:(id)sender { //門市人員選擇
    //要先選門市，再選人員
    if (selectedIndex_Store < 0) {
        [self pressStoreSelectBtn:nil];
    }
    else {
        selectedButton = _ButtonPerson;
        [self ShowPersonSelect];
    }
}
-(IBAction)pressCopyBtn:(id)sender { //複製
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:_emailTextField.text];
    
    [self showAlertControllerWithMessage:@"email複製成功！"];
}
#pragma mark - 選擇問券
-(void) ShowFunctionSelect { //選擇問券
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇項目" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayFunctions count]; i++) {
        NSDictionary *_dic = [arrayFunctions objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonFunction;
    popPresenter.sourceRect = _ButtonFunction.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - 選擇門市與人員
-(void) ShowStoreSelect { //選擇門市
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayStore count]; i++) {
        NSDictionary *_dic = [arrayStore objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"ShopName"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonStore;
    popPresenter.sourceRect = _ButtonStore.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
-(void) ShowPersonSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市人員" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayPerson count]; i++) {
        NSString *title = [arrayPerson objectAtIndex:i];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    //[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
    //style:UIAlertActionStyleDestructive
    //handler:^(UIAlertAction *action) {
    //}]];
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonPerson;
    popPresenter.sourceRect = _ButtonPerson.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    if (selectedButton == _ButtonFunction) {
        selectedIndex_Function = buttonIndex;
        
        NSDictionary *_dic = [arrayFunctions objectAtIndex:buttonIndex];
        [_ButtonFunction setTitle:[_dic objectForKey:@"Name"] forState:UIControlStateNormal];
        
        [self showOrderNOTextFieldWithSearchCondition];
        //抓取對應的門市人員
        [self callSurveyGetShopSalesList];
    }
    else if (selectedButton == _ButtonStore) {
        selectedIndex_Store = buttonIndex;
        
        NSDictionary *_dic = [arrayStore objectAtIndex:buttonIndex];
        [_ButtonStore setTitle:[_dic objectForKey:@"ShopName"] forState:UIControlStateNormal];
        
        //取出該店的人
        [arrayPerson removeAllObjects];
        NSArray *Accounts = [_dic objectForKey:@"Accounts"];
        [arrayPerson addObjectsFromArray:Accounts];
        
        //選人
        [self pressStorePersonSelectBtn:nil];
    }
    else if (selectedButton == _ButtonPerson) {
        selectedIndex_Person = buttonIndex;
        
        NSString *name = [arrayPerson objectAtIndex:selectedIndex_Person];
        [_ButtonPerson setTitle:name forState:UIControlStateNormal];
    }
}
#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    //記錄做動的物件
    selectedTextField = textField;
    
    return YES;
}
#pragma mark - 換頁
-(void) GotoListPageWithData:(NSMutableDictionary *)data DidFromSearch:(BOOL)didFromSearch { //1.1客戶資料比對
    CustomDocumentListVC *destVC = [[CustomDocumentListVC alloc] initWithNibName:@"CustomDocumentListVC" bundle:nil];
    destVC.stringFirstName = [data objectForKey:@"firstName"];
    destVC.stringLastName = [data objectForKey:@"lastName"];
    destVC.stringPhone = [data objectForKey:@"phone"];
    destVC.stringEmail = [data objectForKey:@"email"];
    destVC.stringOrderNo = [data objectForKey:@"orderNo"];
    destVC.stringShopName = [data objectForKey:@"shopName"];
    destVC.stringAccountName = [data objectForKey:@"accountName"];
    destVC.attachTypeId = attachTypeId;
    NSString *surveyID = [NSString stringWithFormat:@"%@",[data objectForKey:@"SurveyId"]];
    destVC.SurveyID = surveyID;
    destVC.createStartDate = [data objectForKey:@"createStartDate"];
    destVC.createEndDate = [data objectForKey:@"createEndDate"];
    destVC.didFromSearch = didFromSearch;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoCustomerSearchVC { //1.2客戶搜尋
    CustomSearchVC *destVC = [[CustomSearchVC alloc] initWithNibName:@"CustomSearchVC" bundle:nil];
    destVC.arrayFunctions = arrayFunctions;
    destVC.arrayStore = arrayStore;
    destVC.arrayPerson = arrayPerson;
    destVC.selectedIndex_Function = selectedIndex_Function;
    destVC.selectedIndex_Store = selectedIndex_Store;
    destVC.selectedIndex_Person = selectedIndex_Person;
    destVC.stringFirstName = self.firstNameTextField.text;
    destVC.stringLastName = self.nameTextField.text;
    destVC.stringPhone = self.phoneTextField.text;
    destVC.stringEmail = self.emailTextField.text;
    destVC.stringFunction = _ButtonFunction.titleLabel.text;
    destVC.stringStore = _ButtonStore.titleLabel.text;
    destVC.stringPerson = _ButtonPerson.titleLabel.text;
    destVC.sourceVC = self;
    destVC.attachTypeId = attachTypeId;
    [self.navigationController presentViewController:destVC animated:YES completion:^(void) {
    }];
}
-(void) GotoCustomerDocumentSignVCWithCustomerID:(NSString *)customerID { //1.3選擇簽核文件
    //問卷ID
    NSDictionary *_dic = [arrayFunctions objectAtIndex:selectedIndex_Function];

    CustomerDocumentSignVC *destVC = [[CustomerDocumentSignVC alloc] initWithNibName:@"CustomerDocumentSignVC" bundle:nil];
    destVC.customerID = customerID;
    destVC.attachTypeId = attachTypeId;
    destVC.surverID = [_dic objectForKey:@"SurveyId"];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTakePhotoWithCustomerID:(NSString *)customerID CustomerData:(NSMutableDictionary *)customerData { //拍照上傳
    PhotographVC *destVC = [[PhotographVC alloc] initWithNibName:@"PhotographVC" bundle:nil];
    destVC.customerID = customerID;
    destVC.customerData = customerData;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 鍵盤
- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHidden:) name:UIKeyboardDidHideNotification object:nil];
}
- (void)removeForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}
//selectedTextField
// Called when the UIKeyboardDidShowNotification is sent.
-(void)keyboardWasShown:(NSNotification*)aNotification {
    /*NSDictionary *info = [aNotification userInfo];
     CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
     
     // If active text field is hidden by keyboard, scroll it so it's visible
     // Your app might not need or want this behavior.
     CGRect aRect = self.view.frame;
     aRect.size.height -= kbSize.height;
     if (!CGRectContainsPoint(aRect, selectedTextField.frame.origin) ) {
     self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - kbSize.height, self.view.frame.size.width, self.view.frame.size.height);
     }*/
    
    CGRect keyboardBounds;
    [[aNotification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSLog(@"keyboardBounds:%@",NSStringFromCGRect(keyboardBounds));
    
    CGRect kRectTextField = selectedTextField.frame;
    
    //NSLog(@"kRect:%@",NSStringFromCGRect(kRect));
    //NSLog(@"kRectTextField:%@",NSStringFromCGRect(kRectTextField));
    
    //NSLog(@"self.view.frame:%@",NSStringFromCGRect(self.view.frame));
    
    CGFloat yToBe = keyboardBounds.origin.y - kRectTextField.size.height;
    //NSLog(@"yToBe:%lf",yToBe);
    
    CGFloat currentY = kRectTextField.origin.y + kRectTextField.size.height/2;
    
    CGFloat offset = currentY - yToBe;
    
    if (offset > 0) {
        CGRect rectToBe = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        rectToBe.origin.y = rectToBe.origin.y - offset;
        
        [self.view setFrame:rectToBe];
        //NSLog(@"_TextOrderContent:%@",NSStringFromCGRect(_TextOrderContent.frame));
    }
    else {
        
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    //
    
    //    scrollView.contentInset = contentInsets;
    //    scrollView.scrollIndicatorInsets = contentInsets;
}
-(void) keyboardDidHidden:(NSNotification*)aNotification {
    //[_TextOrderContent resignFirstResponder];
    
}

#pragma mark - 根據所選查詢條件決定是否顯示
/**
 根據所選搜尋條件，決定是否顯示訂單編號查詢欄位
 <當選擇 Gogoro 2 交車 時顯示訂單編號查詢欄位>
 
 */
- (void)showOrderNOTextFieldWithSearchCondition
{
    if ([_ButtonFunction.titleLabel.text isEqualToString:@"Gogoro 2 交車"])
    {
        
        _orderNOTextField.hidden = NO;
    }
    else
    {
        _orderNOTextField.text = @"";
        
#if TARGET_IPHONE_SIMULATOR
        _orderNOTextField.text = @"P1195";
#endif
        _orderNOTextField.hidden = YES;
    }
}
@end
