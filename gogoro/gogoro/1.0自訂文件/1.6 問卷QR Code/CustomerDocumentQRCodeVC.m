//

#import "CustomerDocumentQRCodeVC.h"
#import <iOS-QR-Code-Encoder/QRCodeGenerator.h>

@interface CustomerDocumentQRCodeVC () {
}
@property (nonatomic,weak) IBOutlet UILabel *LabelUrl;
@property (nonatomic,weak) IBOutlet UIImageView *ImageQRCode;
@end

@implementation CustomerDocumentQRCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _LabelUrl.text = _urlString;
    
    _ImageQRCode.image = [QRCodeGenerator qrImageForString:_urlString imageSize:_ImageQRCode.frame.size.width];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

}
#pragma mark - Button Action
-(IBAction)pressFinishBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^(void) {
        //[_sourceVC GotoNextFunctionAfterQRCode];
    }];
}

#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
