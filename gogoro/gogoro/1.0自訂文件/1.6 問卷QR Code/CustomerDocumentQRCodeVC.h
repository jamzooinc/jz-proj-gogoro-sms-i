
#import <UIKit/UIKit.h>
#import "CustomerDocumentSignVC.h"

@interface CustomerDocumentQRCodeVC : TemplateVC
@property NSInteger attachTypeId; //附件類型（首次參觀:101, 預約試乘: 102）
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,weak) id sourceVC;
@end
