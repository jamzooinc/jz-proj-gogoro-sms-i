//

#import "CustomerDocumentWebQuestionVC.h"

@interface CustomerDocumentWebQuestionVC () {
}
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *act;
@property (nonatomic,weak) IBOutlet UIWebView *WebView;
@end

@implementation CustomerDocumentWebQuestionVC
@synthesize urlString;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /*內嵌web view，帶入
    (1) 客戶姓名
    (2) Email
    (3) 電話
    (4) 服務人員
    (5) 服務門市
    帶入首次參觀/預約試乘問卷
    
    首次參觀
https://zh.surveymonkey.com/r/2THRBBR
    
    預約試乘
https://zh.surveymonkey.com/r/DLCDVPV*/
    
    //NSString *urlString = nil;
    /*if ([urlString length] > 0) {
        
    }
    else if (_attachTypeId == 102) { //預約試乘
        urlString = @"https://zh.surveymonkey.com/r/DLCDVPV";
    }
    else { //首次參觀
        urlString = @"https://zh.surveymonkey.com/r/2THRBBR";
    }*/

    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    [_WebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [_act startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_act stopAnimating];
}
#pragma mark - Button Action
-(IBAction)pressFinishBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^(void) {
        //[_sourceVC GotoNextFunctionAfterQRCode];
    }];
}

#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
