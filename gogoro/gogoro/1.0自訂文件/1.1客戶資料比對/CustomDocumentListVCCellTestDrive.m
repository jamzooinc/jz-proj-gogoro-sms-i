

#import "CustomDocumentListVCCellTestDrive.h"

@implementation CustomDocumentListVCCellTestDrive
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setEditing:YES animated:YES];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected) {
        //_ImageBG.image = [UIImage imageNamed:@"list03_press.png"];
    }
    else {
        //_ImageBG.image = [UIImage imageNamed:@"list03.png"];
    }
}
-(IBAction) pressModifyBtn:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Modify_Customer" object:[NSNumber numberWithInteger:self.tag]];
}
-(IBAction) pressQuestionBtn:(id)sender { //問券
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Question_Customer" object:[NSNumber numberWithInteger:self.tag]];
}
-(IBAction) pressDocumentBtn:(id)sender { //文件
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Document_Customer" object:[NSNumber numberWithInteger:self.tag]];
}
-(IBAction) pressCloseBtn:(id)sender { //結案
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Close_Customer" object:[NSNumber numberWithInteger:self.tag]];
}
- (void)dealloc {
}
@end
