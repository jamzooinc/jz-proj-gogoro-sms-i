

#import <UIKit/UIKit.h>

@interface CustomDocumentListVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageBG;
@property (nonatomic, weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderNo;

@property (nonatomic, weak) IBOutlet UILabel *LabelEmail;
@property (nonatomic, weak) IBOutlet UILabel *LabelPhone;
@property (nonatomic, weak) IBOutlet UILabel *LabelStore;
@property (nonatomic, weak) IBOutlet UILabel *LabelPerson;
@property (nonatomic, weak) IBOutlet UIButton *ButtonSelect;
@property (nonatomic, weak) IBOutlet UIButton *ButtonUpdate;
@property (nonatomic, weak) IBOutlet UILabel *LabelDate;
@property (nonatomic, weak) IBOutlet UIButton *ButtonDocument;//文件
@property (nonatomic, weak) IBOutlet UIButton *ButtonQuestionnaire; //問券
@property (nonatomic, weak) IBOutlet UIButton *ButtonClose; //結案
@end
