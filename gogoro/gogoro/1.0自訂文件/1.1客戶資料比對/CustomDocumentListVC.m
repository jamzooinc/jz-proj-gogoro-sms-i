

#import "CustomDocumentListVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "CustomDocumentListVCCell.h"
#import "CustomDocumentListVCCellTestDrive.h"
#import "PhotographVC.h"
#import "CustomerDocumentSignVC.h"
#import "PhotoViewerVC.h"
#import "CustomerDocumentQRCodeVC.h"
#import "CustomerDocumentWebQuestionVC.h"

#define GOGORO2_DELIVERY @"5"


@interface CustomDocumentListVC () {
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
    NSMutableArray *arrayQuestion; //問券的選項
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UIButton *ButtonAdd;
@end

@implementation CustomDocumentListVC
@synthesize attachTypeId;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //"由 1.2 客戶搜尋 -> 1.1客戶資料比對
    //下方不可顯示""新增客戶""的按鍵"
    if (_didFromSearch)
    {
        _ButtonAdd.hidden = YES;
        _TableView.frame = CGRectMake(_TableView.frame.origin.x, _TableView.frame.origin.y, _TableView.frame.size.width, _TableView.frame.size.height + _ButtonAdd.frame.size.height);
    }
    else
    {
        if ([_SurveyID isEqualToString:GOGORO2_DELIVERY])
        {
            _ButtonAdd.hidden = YES;
        }
        else
        {
            _ButtonAdd.hidden = NO;
        }
    }
    
    arrayQuestion = [[NSMutableArray alloc] init];
    //加入選項
    [arrayQuestion addObject:@"以客人手機掃描QR Code"];
    [arrayQuestion addObject:@"在此平板裝置開啟問卷"];
    
    arrayTableView = [[NSMutableArray alloc] init];
}
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self AddNotification];
    [self callCustomerFind];
}
-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
#pragma mark - IBAction

/* 送出 */
- (IBAction)submitButtonClick:(id)sender {
    //新增
    [self callCustomerData];
}

#pragma mark - tableView 相關
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //附件類型（首次參觀:101, 預約試乘: 102）
    if (attachTypeId == 102) {
        return 279+50;
    }
    else {
        return 279;
    }
}
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomDocumentListVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = nil;
        //附件類型（首次參觀:101, 預約試乘: 102）
        if (attachTypeId == 102) {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomDocumentListVCCellTestDrive" owner:self options:nil];
        }
        else {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomDocumentListVCCell" owner:self options:nil];
        }
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    CustomDocumentListVCCell *myCell = (CustomDocumentListVCCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //Title
    myCell.LabelTitle.text = [NSString stringWithFormat:@"姓名：%@",[_dic objectForKey:@"Name"]];
    
    //OrderNo
    if ([_SurveyID isEqualToString:GOGORO2_DELIVERY])
    {
        myCell.LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",[_dic objectForKey:@"OrderNo"]];
    }
    else
    {
        myCell.LabelOrderNo.hidden = YES;
        
        NSArray *objectArray = @[myCell.LabelStore,
                                 myCell.LabelPhone,
                                 myCell.LabelEmail];
        
        for (int magnification = 1; magnification <= objectArray.count; magnification++)
        {
            if (attachTypeId == 102)
            {
                continue;
            }
            float adjust = 11;
            UILabel *label = objectArray[magnification-1];
            CGRect labelFrame = label.frame;
            labelFrame.origin.y -= adjust*magnification;
            label.frame = labelFrame;
        }
    }

    
    //Email
    myCell.LabelEmail.text = [NSString stringWithFormat:@"Email：%@",[_dic objectForKey:@"Email"]];
    
    //Phone
    myCell.LabelPhone.text = [NSString stringWithFormat:@"電話：%@",[_dic objectForKey:@"Phone"]];
    
    //服務門市
    NSString *StoreName = [_dic objectForKey:@"StoreName"];
    if (!StoreName) StoreName = @"";
    myCell.LabelStore.text = [NSString stringWithFormat:@"服務門市：%@",StoreName];
    
    //人員
    NSString *SalesName = [_dic objectForKey:@"SalesName"];
    if (!SalesName) SalesName = @"";
    myCell.LabelPerson.text = [NSString stringWithFormat:@"服務人員：%@",SalesName];
    
    if (_didFromSearch) //”客戶搜尋“進入此頁
    {
        myCell.ButtonSelect.hidden = NO;
        myCell.ButtonUpdate.hidden = YES;
    }
    else //“自訂文件”進入此頁
    {
        if ([_SurveyID isEqualToString:GOGORO2_DELIVERY])
        {
            myCell.ButtonSelect.hidden = NO;
            myCell.ButtonUpdate.hidden = YES;
        }
        else
        {
            myCell.ButtonSelect.hidden = YES;
            myCell.ButtonUpdate.hidden = NO;
        }
    }
    
    //附件類型（首次參觀:101, 預約試乘: 102）
    if (attachTypeId == 102) { //預約試乘
        //"2.0 預約試乘 - 1.1客戶資料比對""結案""按鍵隱藏，不必顯示
        myCell.ButtonClose.hidden = YES;
        
        //2.0 預約試乘 - 1.1 客戶資料比對 若SignFileUrl無資料-> 隱藏"文件"按鍵
        NSString *SignFileUrl = [_dic objectForKey:@"SignFileUrl"];
        if ([SignFileUrl length] > 0) {
            myCell.ButtonDocument.hidden = NO;
        }
        else {
            myCell.ButtonDocument.hidden = YES;
        }
        
        NSString *date = [_dic objectForKey:@"CreateDate"];
        if (!date) date = @"";
        
        CustomDocumentListVCCellTestDrive *myCellTestDrive = (CustomDocumentListVCCellTestDrive *)cell;
        myCellTestDrive.LabelDate.text = [NSString
                                                stringWithFormat:@"文件建立日期：%@",date];
    }
    else { //首次參觀
        myCell.ButtonClose.hidden = NO;
    }

    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - Cell上的按鈕
#pragma mark 變更
-(void) ModifyCustomAtIndex:(NSNotification *)notif
{
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:index];
    NSString *customerID = [_dic objectForKey:@"Id"];
    
    if (_didFromSearch) {
        if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
            //預約試乘-->1.8 拍攝駕照
            [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
        }
        else
        {
            //自訂文件-->1.3 選擇簽核文件
            [self GotoCustomerDocumentSignVCWithCustomerID:customerID];
        }
    }
    else { //用選到的ID+前一頁輸入的資料跟後台update
        if ([_SurveyID isEqualToString:GOGORO2_DELIVERY])
        {
            [self GotoCustomerDocumentSignVCWithCustomerID:customerID];
        }
        else
        {
            [self callCustomerDataUpdate];
        }
    }
}
#pragma mark 問券
-(void) QuestionCustomAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    //NSDictionary *_dic = [arrayTableView objectAtIndex:index];
    
    //取出該cell的問券按鈕
    CustomDocumentListVCCellTestDrive *cell = [_TableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndex inSection:0]];
    UIButton *button = cell.ButtonQuestionnaire;
    if ([arrayQuestion count] == 0) return; //防呆
    [self ShowQuestionSelectWithSender:button];
}
#pragma mark 文件
-(void) DocumentCustomAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:index];
    
    NSString *FileUrl = [_dic objectForKey:@"SignFileUrl"];
    
    if ([FileUrl length] == 0) {
        [self showAlertControllerWithMessage:@"請先上傳電子檔！"];
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];
    
    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([FileUrl rangeOfString:@".jpg"].length > 0 || [FileUrl rangeOfString:@".png"].length > 0 || [FileUrl rangeOfString:@".JPG"].length > 0 || [FileUrl rangeOfString:@".PNG"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:FileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:FileUrl Name:AttachTypeName FromTempUrl:NO];
    }
}
#pragma mark 結案
-(void) CloseCustomAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:index];
}
#pragma mark - Call API
#pragma mark [003] 搜尋客戶
- (void)callCustomerFind {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSString *name = [NSString stringWithFormat:@"%@%@",_stringFirstName,_stringLastName];
    [parameter setObject:name forKey:@"name"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
    [parameter setObject:_stringOrderNo forKey:@"orderNo"];


    //attachTypeId 同意書類型
    //null: 除試乘同意書外所有文件
    //102: 試乘同意書
    if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
        [parameter setObject:[NSString stringWithFormat:@"%ld",attachTypeId] forKey:@"attachTypeId"];
    }
    else {
        //[parameter setObject:@"" forKey:@"attachTypeId"];
    }
    
    //ShopName 服務門市
    if ([_stringShopName length] > 0) {
        [parameter setObject:_stringShopName forKey:@"ShopName"];
    }
    
    //AccountName 服務人員
    if ([_stringAccountName length] > 0) {
        [parameter setObject:_stringAccountName forKey:@"AccountName"];
    }
    
    //SurveyId 問卷ID
    [parameter setObject:_SurveyID forKey:@"SurveyId"];
    
    //開始日期
    if ([_createStartDate length] > 0) {
        [parameter setObject:_createStartDate forKey:@"createStartDate"];
    }
    
    //結束日期
    if ([_createEndDate length] > 0) {
        [parameter setObject:_createEndDate forKey:@"createEndDate"];
    }
    
    [GatewayManager callCustomerFind:parameter Source:self];
}
- (void)callBackCustomerFind:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        
        //判斷有沒有記錄
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        BOOL HasRecord = [[Data objectForKey:@"HasRecord"] boolValue];
        NSArray *Customers = [Data objectForKey:@"Customers"];
        
        [arrayTableView removeAllObjects];
        [arrayTableView addObjectsFromArray:Customers];
        [_TableView reloadData];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [004] 新增或修改客戶
- (void)callCustomerData { //新增
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:@"" forKey:@"id"];
    
    [parameter setObject:_stringFirstName forKey:@"FirstName"];
    [parameter setObject:_stringLastName forKey:@"LastName"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
//    [parameter setObject:_stringOrderNo forKey:@"orderNo"];

    
    //ShopName 服務門市
    if ([_stringShopName length] > 0) {
        [parameter setObject:_stringShopName forKey:@"ShopName"];
    }
    
    //AccountName 服務人員
    if ([_stringAccountName length] > 0) {
        [parameter setObject:_stringAccountName forKey:@"AccountName"];
    }
    
    //SurveyId 問卷ID
    [parameter setObject:_SurveyID forKey:@"SurveyId"];
    
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callCustomerDataUpdate { //更新
    [ObjectManager showLodingHUDinView:self.view];
    
    //用選到的ID+前一頁輸入的資料跟後台update
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *customerId = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Id"]];
 
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:customerId forKey:@"id"];
    [parameter setObject:_stringFirstName forKey:@"FirstName"];
    [parameter setObject:_stringLastName forKey:@"LastName"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
//    [parameter setObject:_stringOrderNo forKey:@"orderNo"];

    
    //ShopName 服務門市
    if ([_stringShopName length] > 0) {
        [parameter setObject:_stringShopName forKey:@"ShopName"];
    }
    
    //AccountName 服務人員
    if ([_stringAccountName length] > 0) {
        [parameter setObject:_stringAccountName forKey:@"AccountName"];
    }
    
    //SurveyId 問卷ID
    [parameter setObject:_SurveyID forKey:@"SurveyId"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *customerID = [parameter objectForKey:@"Data"];
        
        if (attachTypeId == 102) { //（首次參觀:101, 預約試乘: 102）
            //預約試乘-->1.8 拍攝駕照
            [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
        }
        else {
            //自訂文件-->1.3 選擇簽核文件
            [self GotoCustomerDocumentSignVCWithCustomerID:customerID];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
-(void) ShowQuestionSelectWithSender:(UIButton *)sender {
    NSString *message = [NSString stringWithFormat:@"請選擇"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayQuestion count]; i++) {
        NSString *title = [arrayQuestion objectAtIndex:i];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = sender;
    popPresenter.sourceRect = sender.bounds;

    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - 問券或QrCode選擇
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    NSString *title = [arrayQuestion objectAtIndex:buttonIndex];
    if ([title isEqualToString:@"以客人手機掃描QR Code"]) {
        NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
        NSString *urlString = [_dic objectForKey:@"SurveyUrl"];
        [self GotoCustomerDocumentQRCodeVCWithUrlstring:urlString];
    }
    else if ([title isEqualToString:@"在此平板裝置開啟問卷"]) {
        NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
        NSString *urlString = [_dic objectForKey:@"SurveyUrl"];
        [self GotoCustomerDocumentWebQuestionVCWithUrlstring:urlString];
    }
}
#pragma mark - 換頁
-(void) GotoConsentPageWithCustomerID:(NSString *)customerID { //個資同意書
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_PERSONAL_Data;
    consentVC.customerID = customerID;
    [self.navigationController pushViewController:consentVC animated:YES];
}
-(void) GotoTakePhotoWithCustomerID:(NSString *)customerID { //拍照上傳
    PhotographVC *destVC = [[PhotographVC alloc] initWithNibName:@"PhotographVC" bundle:nil];
    destVC.customerID = customerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoCustomerDocumentSignVCWithCustomerID:(NSString *)customerID { //1.3選擇簽核文件
    CustomerDocumentSignVC *destVC = [[CustomerDocumentSignVC alloc] initWithNibName:@"CustomerDocumentSignVC" bundle:nil];
    destVC.customerID = customerID;
    destVC.attachTypeId = attachTypeId;
    destVC.surverID =_SurveyID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    //API 309 有 Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = [_dic objectForKey:@"Name"];//ownerName;
    NSString *BuyName = [_dic objectForKey:@"Name"];//buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = NO;
    destVC.showFinishButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoCustomerDocumentQRCodeVCWithUrlstring:(NSString *)urlString { //1.6問券QR Code
    CustomerDocumentQRCodeVC *destVC = [[CustomerDocumentQRCodeVC alloc] initWithNibName:@"CustomerDocumentQRCodeVC" bundle:nil];
    destVC.sourceVC = self;
    destVC.urlString = urlString;
    //[self.navigationController pushViewController:destVC animated:YES];
    [self.navigationController presentViewController:destVC animated:YES completion:nil];
}
-(void) GotoCustomerDocumentWebQuestionVCWithUrlstring:(NSString *)urlString { //1.7填寫問券
    CustomerDocumentWebQuestionVC *destVC = [[CustomerDocumentWebQuestionVC alloc] initWithNibName:@"CustomerDocumentWebQuestionVC" bundle:nil];
    //[self.navigationController pushViewController:destVC animated:YES];
    destVC.attachTypeId = attachTypeId;
    destVC.urlString = urlString;
    [self.navigationController presentViewController:destVC animated:YES completion:nil];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ModifyCustomAtIndex:) name:@"kNotification_Modify_Customer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(QuestionCustomAtIndex:) name:@"kNotification_Question_Customer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DocumentCustomAtIndex:) name:@"kNotification_Document_Customer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CloseCustomAtIndex:) name:@"kNotification_Close_Customer" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Modify_Customer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Question_Customer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Document_Customer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Close_Customer" object:nil];
}
@end
