

#import <UIKit/UIKit.h>

@interface CustomDocumentListVC : TemplateVC
@property NSInteger attachTypeId; //附件類型（首次參觀:101, 預約試乘: 102）
@property BOOL didFromSearch; //YES:從1.2 NO:從1.0



@property (strong, nonatomic) NSString *stringFirstName;
@property (strong, nonatomic) NSString *stringLastName;
@property (strong, nonatomic) NSString *stringEmail;
@property (strong, nonatomic) NSString *stringPhone;
@property (strong, nonatomic) NSString *stringOrderNo;
@property (strong, nonatomic) NSString *stringShopName; //服務門市
@property (strong, nonatomic) NSString *stringAccountName; //服務人員
@property (strong, nonatomic) NSString *SurveyID;
@property (strong, nonatomic) NSString *createStartDate;
@property (strong, nonatomic) NSString *createEndDate;

- (IBAction)submitButtonClick:(id)sender;

@end
