//
//  SelectSubsidyVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubsidyBasicView.h"
#import "SubsidyInfoView.h"
#import "SubsidyLicenseView.h"
#import "SubsidyUserView.h"

@interface SelectSubsidyVC : TemplateVC <SubsidyBasicViewDelegate,SubsidyInfoViewDelegate,SubsidyLicenseViewDelegate,SubsidyUserViewDelegate>
@property (nonatomic,strong) NSDictionary *dicSubsidyData; //後台傳的補助資料
@property (nonatomic,strong) NSDictionary *dicOrderDetail; //後台傳的訂單詳細資料（用來判斷是不是法人）
@property (nonatomic,strong) NSString *stringOrderNo;
@property (nonatomic,strong) NSString *CustomerName;
@property BOOL didFromSupplement; //是不是選擇補單後過來的
@end
