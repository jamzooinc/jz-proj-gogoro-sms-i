


#import "SubsidyBasicView.h"

#define LIST_GAP 7

@implementation SubsidyBasicView

@synthesize delegate;

#pragma mark - Delegate
-(void) SubsidyBasicViewType:(NSString *)type{
    if ([[self delegate] respondsToSelector:@selector(SubsidyBasicViewType:)]) {
        [[self delegate] SubsidyBasicViewType:type];
    }
}
-(void) SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:(BOOL)type{
    if ([[self delegate] respondsToSelector:@selector(SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:)]) {
        [[self delegate] SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:type];
    }
}
-(void) SubsidyBasicViewServantsType:(BOOL)type{
    if ([[self delegate] respondsToSelector:@selector(SubsidyBasicViewServantsType:)]) {
        [[self delegate] SubsidyBasicViewServantsType:type];
    }
}
-(void) SubsidyBasicViewGrantor:(BOOL)hasGrantor {
    if ([[self delegate] respondsToSelector:@selector(SubsidyBasicViewGrantor:)]) {
        [[self delegate] SubsidyBasicViewGrantor:hasGrantor];
    }
}
-(void) SubsidyBasicViewDistributorsStore:(NSString *)name{
    if ([[self delegate] respondsToSelector:@selector(SubsidyBasicViewDistributorsStore:)]) {
        [[self delegate] SubsidyBasicViewDistributorsStore:name];
    }
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}

-(void) UpdateUIWithData:(NSDictionary *)data {
    [super UpdateUIWithData:data];
    
    arrayDistributorsStoreName = [[NSMutableArray alloc] init];
    
    //預設值
    [self pressPersonalBtn];                                                            //個人
    [self pressNoJuridicalPersonsPresentAndPersonalGetBtn];                             //法人贈車個人領牌 N
    [self pressNoServantsBtn];                                                          //公務員 N
    [self pressGrantorNoBtn];                                                           //無保人
    _LabelSalesType.text = @"";                                                         //銷售類別 從DMS資料讀取不可修改
    [_ButtonDistributorsStore setTitle:@"請選擇通路店別" forState:UIControlStateNormal];   //通路店別
    
    
    //依據資料調整
    if (data) {
        dicData = [[NSDictionary alloc] initWithDictionary:data];
        
        //IdentityType 身份 個人0 法人1
        NSInteger IdentityType = [[data objectForKey:@"IdentityType"] integerValue];
        if (IdentityType == 0) { //個人0
            [self pressPersonalBtn];
        }
        else if (IdentityType == 1) { //法人1
            [self pressCorporateBtn];
    
        }
        
        //HasGuarantor 有無投保人 ● 無0 ● 有1
        NSInteger HasGuarantor = [[data objectForKey:@"HasGuarantor"] integerValue];
        if (HasGuarantor == 0) { //無0
            [self pressGrantorNoBtn];
        }
        else if (HasGuarantor == 1) { //有1
            [self pressGrantorYesBtn];
        }
        
        /*NSString *salesType = [_dicOrderDetail objectForKey:@"SalesTypeName"];
        if ([salesType isEqualToString:@"自用車"] ||
            [salesType isEqualToString:@"OS"] ||
            [salesType isEqualToString:@"Online"])
        {
            //銷售類別等於自用車不顯示通路店別選項
            //2016.10.17 新增:UAT#7_銷售類別為「OS」時，不需要顯示選擇通路類別的選項(同「自用車」)
            //2016.10.19 新增:UAT#7_銷售類別為「Online」時，不需要顯示選擇通路類別的選項(同「自用車」)
            _ButtonDistributorsStore.hidden = YES;
            self.LabelSalesType.text = salesType;
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 688, self.frame.size.height - 59);
            _ViewWhilehiddeViewServantsToUpView.frame = CGRectMake(_ViewWhilehiddeViewServantsToUpView.frame.origin.x, _ViewWhilehiddeViewServantsToUpView.frame.origin.y, _ViewWhilehiddeViewServantsToUpView.frame.size.width, _ViewWhilehiddeViewServantsToUpView.frame.size.height - 59);

        }
        else
        {
            if (salesType == nil || [salesType isEqualToString:@""])
            {
                self.LabelSalesType.text = @"尚無銷售類別";
                _ButtonDistributorsStore.hidden = NO;
            }
            else
            {
                _ButtonDistributorsStore.hidden = NO;
                self.LabelSalesType.text = salesType;
                self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 688, self.frame.size.height);

            }
        }*/
    }
    else { //沒補助資料表示是新建的單，用Gender判斷是法人還是個人
        NSString *gender = [_dicOrderDetail objectForKey:@"Gender"];
        if ([gender isEqualToString:@"法人"]) { //法人
            [self pressCorporateBtn];
        }
        else { //個人
            [self pressPersonalBtn];
            
        }
    }
   
    if (_dicOrderDetail) {
        NSString *salesType = [_dicOrderDetail objectForKey:@"SalesTypeName"];
        if ([salesType isEqualToString:@"自用車"] ||
            [salesType isEqualToString:@"OS"] ||
            [salesType isEqualToString:@"Online"])
        {
            //銷售類別等於自用車不顯示通路店別選項
            //2016.10.17 新增:UAT#7_銷售類別為「OS」時，不需要顯示選擇通路類別的選項(同「自用車」)
            //2016.10.19 新增:UAT#7_銷售類別為「Online」時，不需要顯示選擇通路類別的選項(同「自用車」)
            _ButtonDistributorsStore.hidden = YES;
            self.LabelSalesType.text = salesType;
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 688, self.frame.size.height - 59);
            _ViewWhilehiddeViewServantsToUpView.frame = CGRectMake(_ViewWhilehiddeViewServantsToUpView.frame.origin.x, _ViewWhilehiddeViewServantsToUpView.frame.origin.y, _ViewWhilehiddeViewServantsToUpView.frame.size.width, _ViewWhilehiddeViewServantsToUpView.frame.size.height - 59);
            
        }
        else
        {
            if (salesType == nil || [salesType isEqualToString:@""])
            {
                self.LabelSalesType.text = @"尚無銷售類別";
                _ButtonDistributorsStore.hidden = NO;
            }
            else
            {
                //2016.11.18 顯示通路店別
                NSString *PartnerStoreName = [_dicOrderDetail objectForKey:@"PartnerStoreName"];
                [_ButtonDistributorsStore setTitle:PartnerStoreName forState:UIControlStateNormal];
                [_ButtonDistributorsStore setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
                
                NSString *No = [_dicOrderDetail objectForKey:@"PartnerStoreNo"];
                
                //Delegate
                [self SubsidyBasicViewDistributorsStore:No];
                
                _ButtonDistributorsStore.hidden = NO;
                self.LabelSalesType.text = salesType;
                self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 688, self.frame.size.height);
                
            }
        }
        
        //2016.11.04 UAT#38_若非貸款件，則有無保人的選項須隱藏
        //貸款期數
        NSInteger CreditPeriod = [[_dicOrderDetail objectForKey:@"CreditPeriod"] integerValue];
        //CreditPeriod = 0;
        if (CreditPeriod <= 0) {
            _LabelGaurantyTitle.hidden = YES;
            _ViewGauranty.hidden = YES;
            
            CGFloat offset = _LabelSalesType.frame.origin.y - _LabelGaurantyTitle.frame.origin.y;
            _LabelSalesTypeTitle.center = CGPointMake(_LabelSalesTypeTitle.center.x, _LabelSalesTypeTitle.center.y - offset);
            _LabelSalesType.center = CGPointMake(_LabelSalesType.center.x, _LabelSalesType.center.y - offset);
            _ButtonDistributorsStore.center = CGPointMake(_ButtonDistributorsStore.center.x, _ButtonDistributorsStore.center.y - offset);
            _ViewWhilehiddeViewServantsToUpView.frame = CGRectMake(_ViewWhilehiddeViewServantsToUpView.frame.origin.x, _ViewWhilehiddeViewServantsToUpView.frame.origin.y, _ViewWhilehiddeViewServantsToUpView.frame.size.width, _ViewWhilehiddeViewServantsToUpView.frame.size.height - offset);
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height - offset);
        }
    }
    
}
#pragma mark - Button
-(IBAction) pressPersonalBtn { //個人
    
    _ViewServants.hidden = NO;
    _ViewWhilehiddeViewServantsToUpView.frame = CGRectMake(0, 221, 688, 178);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 688, 399);
    
    _LabelIdentity.text = @"個人";
    //[self SetButtonOnWithSender:_ButtonPerson];
    //[self SetButtonOffWithSender:_ButtonCorporate];
    
    [self SubsidyBasicViewType:@"個人"];
}
-(IBAction) pressCorporateBtn { //法人
    
    _ViewServants.hidden = YES;
    _ViewWhilehiddeViewServantsToUpView.frame = CGRectMake(0, 221 - _ViewServants.frame.size.height - LIST_GAP, 688, 178);
    self.frame = CGRectMake(self.frame.origin.x, 0, 688, 343);
    
    _LabelIdentity.text = @"法人";
    //[self SetButtonOnWithSender:_ButtonCorporate];
    //[self SetButtonOffWithSender:_ButtonPerson];
    
    [self SubsidyBasicViewType:@"法人"];
}
-(IBAction) pressGrantorNoBtn { //無保人
    [self SetButtonOnWithSender:_ButtonGrantorNo];
    [self SetButtonOffWithSender:_ButtonGrantorYes];
    
    [self SubsidyBasicViewGrantor:NO];
}
-(IBAction) pressGrantorYesBtn { //有保人
    [self SetButtonOnWithSender:_ButtonGrantorYes];
    [self SetButtonOffWithSender:_ButtonGrantorNo];
    
    [self SubsidyBasicViewGrantor:YES];
}
-(IBAction) pressNoJuridicalPersonsPresentAndPersonalGetBtn {
    [self SetButtonOnWithSender:_ButtonNoJuridicalPersonsPresentAndPersonalGet];
    [self SetButtonOffWithSender:_ButtonYesJuridicalPersonsPresentAndPersonalGet];
    
    [self SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:NO];
}
-(IBAction) pressYesJuridicalPersonsPresentAndPersonalGetBtn {
    //if (![self CheckPersonalAndCorporate]) {
        //return;
    //}
    [self SetButtonOnWithSender:_ButtonYesJuridicalPersonsPresentAndPersonalGet];
    [self SetButtonOffWithSender:_ButtonNoJuridicalPersonsPresentAndPersonalGet];
    
    [self SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:YES];
}
-(IBAction) pressNoServantsBtn;{
    [self SetButtonOnWithSender:_ButtonNoServants];
    [self SetButtonOffWithSender:_ButtonYesServants];
    
    [self SubsidyBasicViewServantsType:NO];
}
-(IBAction) pressYesServantsBtn{
    [self SetButtonOnWithSender:_ButtonYesServants];
    [self SetButtonOffWithSender:_ButtonNoServants];
    
    [self SubsidyBasicViewServantsType:YES];
}
-(IBAction) pressChooseDistributorsStore{
//    [self ShowDistributorsStoreSelect];
    //抓取該區域對應的補助
    [self GetPartnerStoreList];
}
-(BOOL) CheckPersonalAndCorporate {
    //2016.11.04 UAT#13_購買人與車主都是個人時，在APP端需擋住法人贈車的選擇，需跳出提示訊息"目前購買人身分為個人！請修改為法人相關資訊。"
    BOOL result = YES;
    
    NSString *BuyGender = [_dicOrderDetail objectForKey:@"BuyGender"];
    NSLog(@"BuyGender:%@",BuyGender);
    if ([BuyGender isEqualToString:@"個人"]) { //個人
        TemplateVC *sourceVC = (TemplateVC *)[ObjectManager viewController:self];
        [sourceVC showAlertControllerWithMessage:@"目前購買人身分為個人！請修改為法人相關資訊。"];
        result = NO;
    }

    return result;
}
#pragma mark [009]取得通路店別列表

- (void)GetPartnerStoreList {
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_LabelSalesType.text forKey:@"SalesTypeName"];
    [GatewayManager callGetPartnerStoreList:parameter Source:self];
}

- (void)callBackGetPartnerStoreList:(NSDictionary *)parameter {
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arrayDistributorsStoreName removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        
        [arrayDistributorsStoreName removeAllObjects];
        [arrayDistributorsStoreName addObjectsFromArray:data];
        
        [self ShowDistributorsStoreSelect];
    }
    else {
        [ObjectManager showErrorHUDWithStatus:Sysmsg duration:3.0f inView:self];
    }
}


#pragma mark popview
-(void) ShowDistributorsStoreSelect{
    //顯示通路店別
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇通路店別" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayDistributorsStoreName count]; i++) {
        NSDictionary *_dic = [arrayDistributorsStoreName objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonDistributorsStore;
    popPresenter.sourceRect = _ButtonDistributorsStore.bounds;
    
    [_sourceVC presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
        selectedIndex_DistributorsStore = buttonIndex;
    
        NSDictionary *_dic = [arrayDistributorsStoreName objectAtIndex:selectedIndex_DistributorsStore];
    
        NSString *title = [_dic objectForKey:@"Name"];
        [_ButtonDistributorsStore setTitle:title forState:UIControlStateNormal];
        [_ButtonDistributorsStore setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
    

        NSString *No = [_dic objectForKey:@"No"];
    
        //Delegate
        [self SubsidyBasicViewDistributorsStore:No];
}


@end
