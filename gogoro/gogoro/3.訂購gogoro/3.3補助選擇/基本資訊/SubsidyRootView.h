
#import <UIKit/UIKit.h>
#import "UIColor+Hexadecimal.h"

#define TEXT_COLOR_DIDSELECTED [UIColor colorWithHexString:@"#1d1d1d"]
#define TEXT_COLOR_UNSELECTED [UIColor colorWithHexString:@"#B3B3B3"]




@interface SubsidyRootView : UIView  {
}
@property (nonatomic,weak) IBOutlet UIImageView *ImageBG;
-(void) UpdateUIWithData:(NSDictionary *)data;
-(void) SetButtonOnWithSender:(UIButton *)sender;
-(void) SetButtonOffWithSender:(UIButton *)sender;
@end
