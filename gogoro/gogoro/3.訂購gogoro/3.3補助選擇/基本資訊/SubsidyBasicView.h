
#import <UIKit/UIKit.h>
#import "SubsidyRootView.h"

@protocol SubsidyBasicViewDelegate <NSObject>
@optional
-(void) SubsidyBasicViewType:(NSString *)type;
-(void) SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:(BOOL)type;
-(void) SubsidyBasicViewServantsType:(BOOL)type;
-(void) SubsidyBasicViewGrantor:(BOOL)hasGrantor;
-(void) SubsidyBasicViewSalesType:(NSString *)type;
-(void) SubsidyBasicViewDistributorsStore:(NSString *)name;
@end

@interface SubsidyBasicView : SubsidyRootView
{
    __weak id <SubsidyBasicViewDelegate> delegate;
    BOOL isPersonal;                                    //是否為個人
    NSMutableArray *arrayDistributorsStoreName;
    NSInteger selectedIndex_DistributorsStore;
    NSDictionary *dicData;
}

@property (nonatomic,weak) UIViewController *sourceVC;
@property (weak) id delegate;
@property (nonatomic,strong) NSDictionary *dicOrderDetail;                                      //後台傳的訂單詳細資料（用來判斷是不是法人）
@property (nonatomic,weak) IBOutlet UIButton *ButtonPerson;                                     //個人
@property (nonatomic,weak) IBOutlet UIButton *ButtonCorporate;                                  //法人
@property (nonatomic,weak) IBOutlet UILabel *LabelIdentity; //身分別：個人或法人
@property (nonatomic,weak) IBOutlet UIButton *ButtonNoJuridicalPersonsPresentAndPersonalGet;    //法人贈車個人領牌 N
@property (nonatomic,weak) IBOutlet UIButton *ButtonYesJuridicalPersonsPresentAndPersonalGet;   //法人贈車個人領牌 Y
@property (nonatomic,weak) IBOutlet UIButton *ButtonNoServants;                                 //公務員 N
@property (nonatomic,weak) IBOutlet UIButton *ButtonYesServants;                                //公務員 Y
@property (nonatomic,weak) IBOutlet UIButton *ButtonGrantorNo;                                  //保人 N
@property (nonatomic,weak) IBOutlet UIButton *ButtonGrantorYes;                                 //保人 Y
@property (nonatomic,weak) IBOutlet UILabel *LabelSalesType;                                    //銷售類別
@property (nonatomic,weak) IBOutlet UIButton *ButtonDistributorsStore;                          //通路門市
@property (nonatomic,weak) IBOutlet UIView *ViewGauranty;
@property (nonatomic,weak) IBOutlet UILabel *LabelGaurantyTitle;
@property (nonatomic,weak) IBOutlet UILabel *LabelSalesTypeTitle;

//Use at adjust UI
@property (weak, nonatomic) IBOutlet UIView * ViewServants;                                     
@property (weak, nonatomic) IBOutlet UIView * ViewWhilehiddeViewServantsToUpView;
@property (weak, nonatomic) IBOutlet UIView * ViewWhileOwnCarToShow;

-(IBAction) pressNoJuridicalPersonsPresentAndPersonalGetBtn;    //法人贈車個人領牌 N
-(IBAction) pressYesJuridicalPersonsPresentAndPersonalGetBtn;   //法人贈車個人領牌 Y
-(IBAction) pressNoServantsBtn;                                 //公務員 N
-(IBAction) pressYesServantsBtn;                                //公務員 Y
-(IBAction) pressGrantorNoBtn;                                  //保人 N
-(IBAction) pressGrantorYesBtn;                                 //保人 Y
-(IBAction) pressChooseDistributorsStore;                       //通路門市

@end
