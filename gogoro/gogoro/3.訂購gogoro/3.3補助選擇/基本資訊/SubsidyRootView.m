


#import "SubsidyRootView.h"


@implementation SubsidyRootView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data {
    
    
}
#pragma mark - Button
-(void) SetButtonOnWithSender:(UIButton *)sender {
    [sender setBackgroundImage:[UIImage imageNamed:@"btn_bz_blue.png"] forState:UIControlStateNormal];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
-(void) SetButtonOffWithSender:(UIButton *)sender {
    [sender setBackgroundImage:[UIImage imageNamed:@"btn_bz_grey.png"] forState:UIControlStateNormal];
    [sender setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
}
@end
