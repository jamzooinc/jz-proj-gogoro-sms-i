


#import "SubsidyUserView.h"

@implementation SubsidyUserView

@synthesize delegate;
#pragma mark - Delegate
-(void) SubsidyUserViewSigner:(NSString *)singer {
    if ([[self delegate] respondsToSelector:@selector(SubsidyUserViewSigner:)]) {
        [[self delegate] SubsidyUserViewSigner:singer];
    }
}
-(void) SubsidyUserViewSales:(NSString *)sales {
    if ([[self delegate] respondsToSelector:@selector(SubsidyUserViewSales:)]) {
        [[self delegate] SubsidyUserViewSales:sales];
    }
}
-(void) SubsidyUserViewLaw:(NSString *)law {
    if ([[self delegate] respondsToSelector:@selector(SubsidyUserViewLaw:)]) {
        [[self delegate] SubsidyUserViewLaw:law];
    }
}
-(void) SubsidyUserViewEditStatus:(BOOL)didBegin {
    if ([[self delegate] respondsToSelector:@selector(SubsidyUserViewEditStatus:)]) {
        [[self delegate] SubsidyUserViewEditStatus:didBegin];
    }
}
-(void) SubsidyUserViewIDDateLate:(BOOL)isLate {
    if ([[self delegate] respondsToSelector:@selector(SubsidyUserViewIDDateLate:)]) {
        [[self delegate] SubsidyUserViewIDDateLate:isLate];
    }
}
-(void) SubsidyUserViewIsOverSite:(BOOL)IsOverSite {
    if ([[self delegate] respondsToSelector:@selector(SubsidyUserViewIsOverSite:)]) {
        [[self delegate] SubsidyUserViewIsOverSite:IsOverSite];
    }
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data {
    [super UpdateUIWithData:data];
    
    //預設值
    //不勾選:身分證換發日期小於規範居住年限，故需備戶籍謄本
    [self pressOptionBtn:_ButtonOptionDocument];
    
    //不勾選:需跨店開立發票,無需檢附完整發票
    [self pressOptionBtn:_ButtonOptionInvoice];
    
    //依據資料調整
    if (data) {
        //MasterName 店長/授權簽署人
        NSString *MasterName = [data objectForKey:@"MasterName"];
        if ([MasterName length] > 0) {
            _TextSigner.text = MasterName;
            
            //Delegate
            [self SubsidyUserViewSigner:MasterName];
        }
        
        //SalesName 銷售人員姓名
        NSString *SalesName = [data objectForKey:@"SalesName"];
        if ([SalesName length] > 0) {
            _TextSales.text = SalesName;
            
            //Delegate
            [self SubsidyUserViewSales:SalesName];
        }
        
        //AgentName 法定代理人(父/母)或授權代 理人
        NSString *AgentName = [data objectForKey:@"AgentName"];
        if ([AgentName length] > 0) {
            _TextLaw.text = AgentName;
            
            //Delegate
            [self SubsidyUserViewLaw:AgentName];
        }
        
        //IsOverSite 需跨店開立發票,無需檢附 完整發票 0無1有
        NSInteger IsOverSite = [[data objectForKey:@"IsOverSite"] integerValue];
        if (IsOverSite == 0) { // 0無
            _ButtonOptionInvoice.tag = 1;
            [self pressOptionBtn:_ButtonOptionInvoice];
        }
        else if (IsOverSite == 1) { //1有
            _ButtonOptionInvoice.tag = 0;
            [self pressOptionBtn:_ButtonOptionInvoice];
        }
        
        //NeedCensus 需備戶籍謄本 ● 否0 ● 是1
        NSInteger NeedCensus = [[data objectForKey:@"NeedCensus"] integerValue];
        if (NeedCensus == 0) { // 否0
            _ButtonOptionDocument.tag = 1;
            [self pressOptionBtn:_ButtonOptionDocument];
        }
        else if (NeedCensus == 1) { //是1
            _ButtonOptionDocument.tag = 0;
            [self pressOptionBtn:_ButtonOptionDocument];
        }
    }
}
#pragma mark - Button
-(IBAction) pressOptionBtn:(UIButton *)sender {
    if (sender == _ButtonOptionDocument) { //文件
        if (sender.tag == 0) { //未勾-->已勾
            sender.tag = 1;
            _ImageOptionDocument.image = [UIImage imageNamed:@"checkbox_y.png"];
            [self SubsidyUserViewIDDateLate:YES];
        }
        else {
            sender.tag = 0;
            _ImageOptionDocument.image = [UIImage imageNamed:@"checkbox_white.png"];
            [self SubsidyUserViewIDDateLate:NO];
        }
    }
    else if (sender == _ButtonOptionInvoice) { //發票
        if (sender.tag == 0) { //未勾-->已勾
            sender.tag = 1;
            _ImageOptionInvoice.image = [UIImage imageNamed:@"checkbox_y.png"];
            [self SubsidyUserViewIsOverSite:YES];
        }
        else {
            sender.tag = 0;
            _ImageOptionInvoice.image = [UIImage imageNamed:@"checkbox_white.png"];
            [self SubsidyUserViewIsOverSite:NO];
        }
    }
}
#pragma mark - TextField Delegate
-(void) textFieldDidBeginEditing:(UITextField *)textField {
    [self SubsidyUserViewEditStatus:YES];
}
-(void) textFieldDidEndEditing:(UITextField *)textField {
    [self SubsidyUserViewEditStatus:NO];
    
    if (textField == _TextSigner) {
        [self SubsidyUserViewSigner:textField.text];
    }
    else if (textField == _TextSales) {
        [self SubsidyUserViewSales:textField.text];
    }
    else if (textField == _TextLaw) {
        [self SubsidyUserViewLaw:textField.text];
    }
}
@end
