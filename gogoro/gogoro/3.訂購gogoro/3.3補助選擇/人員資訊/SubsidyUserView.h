
#import <UIKit/UIKit.h>
#import "SubsidyRootView.h"

@protocol SubsidyUserViewDelegate <NSObject>
@optional
-(void) SubsidyUserViewSigner:(NSString *)singer;
-(void) SubsidyUserViewSales:(NSString *)sales;
-(void) SubsidyUserViewLaw:(NSString *)law;
-(void) SubsidyUserViewEditStatus:(BOOL)didBegin;
-(void) SubsidyUserViewIDDateLate:(BOOL)isLate;
-(void) SubsidyUserViewIsOverSite:(BOOL)IsOverSite;

@end

@interface SubsidyUserView : SubsidyRootView <UITextFieldDelegate>  {
    __weak id <SubsidyUserViewDelegate> delegate;
}
@property (weak) id delegate;
@property (nonatomic,weak) IBOutlet UITextField *TextSigner; //店長/授權簽署人
@property (nonatomic,weak) IBOutlet UITextField *TextSales; //銷售人員姓名
@property (nonatomic,weak) IBOutlet UITextField *TextLaw; //法定代理人(父/母)獲授權代理人
@property (nonatomic,weak) IBOutlet UIImageView *ImageOptionDocument;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOptionDocument;
@property (nonatomic,weak) IBOutlet UIImageView *ImageOptionInvoice;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOptionInvoice;
@end
