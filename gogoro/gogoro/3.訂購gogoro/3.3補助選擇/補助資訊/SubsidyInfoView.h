
#import <UIKit/UIKit.h>
#import "SubsidyRootView.h"

@protocol SubsidyInfoViewDelegate <NSObject>
@optional
-(void) SubsidyInfoViewSubsidyStatus:(NSString *)status;
-(void) SubsidyInfoViewSubsidyNoType:(NSString *)type;
-(void) SubsidyInfoViewSubsidyArea:(NSString *)area;
-(void) SubsidyInfoViewSubsidyType:(NSString *)type;
@end

@interface SubsidyInfoView : SubsidyRootView  {
    __weak id <SubsidyInfoViewDelegate> delegate;
    BOOL isSubsidyYes; //是否選擇補助
    BOOL isShowArea; //YES 正在顯示地區 NO正在顯示類型
    NSMutableArray *arraySubsidyArea,*arraySubsidyType;
    NSInteger selectedIndex_SubsidyArea,selectedIndex_SubsidyType;
}
@property (nonatomic,weak) UIViewController *sourceVC;
@property (weak) id delegate;
@property (nonatomic,strong) NSString *OrderNo;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSubsidyYes; //補助
@property (nonatomic,weak) IBOutlet UIButton *ButtonSubsidyNo; //放棄補助
@property (nonatomic,weak) IBOutlet UIView *ViewSubsidyNo;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSubsidyNoBoth; //放棄補助兩者
@property (nonatomic,weak) IBOutlet UIButton *ButtonSubsidyNoLocal; //放棄補助地方
//@property (nonatomic,weak) IBOutlet UIView *ViewSubsidyArea;
//@property (nonatomic,weak) IBOutlet UILabel *LabelSubsidyArea;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSubsidyArea;
//@property (nonatomic,weak) IBOutlet UIView *ViewSubsidyType;
//@property (nonatomic,weak) IBOutlet UILabel *LabelSubsidyType;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSubsidyType;
@property (weak, nonatomic) IBOutlet UIView *ViewEliminatingStatusType
;
//Add at version by Justin in 2016/9/21
@property (weak, nonatomic) IBOutlet UIButton *ButtonEliminatingStatusTypeSame;
@property (weak, nonatomic) IBOutlet UIButton *ButtonEliminatingStatusTypeDifferent;

-(IBAction)pressEliminatingStatusTypeSameBtn:(id)sender;
-(IBAction)pressSubsidyNoBtn; //放棄補助
-(IBAction)pressSubsidyNoLocalBtn; //放棄補助：地方
@end
