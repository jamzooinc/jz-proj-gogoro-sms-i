


#import "SubsidyInfoView.h"

@implementation SubsidyInfoView
@synthesize delegate;

#pragma mark - Delegate
-(void) SubsidyInfoViewSubsidyStatus:(NSString *)status {
    if ([[self delegate] respondsToSelector:@selector(SubsidyInfoViewSubsidyStatus:)]) {
        [[self delegate] SubsidyInfoViewSubsidyStatus:status];
    }
}
-(void) SubsidyInfoViewSubsidyNoType:(NSString *)type {
    if ([[self delegate] respondsToSelector:@selector(SubsidyInfoViewSubsidyNoType:)]) {
        [[self delegate] SubsidyInfoViewSubsidyNoType:type];
    }
}
-(void) SubsidyInfoViewSubsidyArea:(NSString *)area {
    if ([[self delegate] respondsToSelector:@selector(SubsidyInfoViewSubsidyArea:)]) {
        [[self delegate] SubsidyInfoViewSubsidyArea:area];
    }
}
-(void) SubsidyInfoViewSubsidyType:(NSString *)type {
    if ([[self delegate] respondsToSelector:@selector(SubsidyInfoViewSubsidyType:)]) {
        [[self delegate] SubsidyInfoViewSubsidyType:type];
    }
}
-(void) SubsidyInfoEliminatingStatusType:(NSString *)type {
    if ([[self delegate] respondsToSelector:@selector(SubsidyInfoEliminatingStatusType:)]) {
        [[self delegate] SubsidyInfoEliminatingStatusType:type];
    }
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}

-(void) UpdateUIWithData:(NSDictionary *)data {
    [super UpdateUIWithData:data];
    
    arraySubsidyArea = [[NSMutableArray alloc] init];
    arraySubsidyType = [[NSMutableArray alloc] init];
    
    //取得訂單補助地區選單
    [self GetSubsidyAreaList];
    
    //預設值
    [self pressSubsidyYesBtn]; //補助

    //依據資料調整
    if (data) {
        //SubsidyType    補助狀況 ● 放棄補助0 ● 放棄地方補助1 ● T E S 補 助 +  地  方 補 助 2
        NSInteger SubsidyType = [[data objectForKey:@"SubsidyType"] integerValue];
        if (SubsidyType == 2) { //T E S 補 助 +  地  方 補 助 2
            [self pressSubsidyYesBtn];
        }
        else if (SubsidyType == 1) { //放棄地方補助1
            [self pressSubsidyNoBtn];
            [self pressSubsidyNoLocalBtn];
        }
        else if (SubsidyType == 0) { //放棄補助0
            [self pressSubsidyNoBtn];
            [self pressSubsidyNoBothBtn];
        }
        
        //SubsidyArea 選擇補助地區
        NSString *SubsidyArea = [data objectForKey:@"SubsidyArea"];
        if ([SubsidyArea length] > 0) {
            NSString *title = SubsidyArea;
            
            [_ButtonSubsidyArea setTitle:SubsidyArea forState:UIControlStateNormal];
            [_ButtonSubsidyArea setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
            
            //抓取該區域對應的補助
            [self GetSubsidyTypeListWithArea:title];
            
            //Delegate
            [self SubsidyInfoViewSubsidyArea:title];
        }
        
        //LocalSubsidyType 選擇補助類型
        NSString *LocalSubsidyType = [data objectForKey:@"LocalSubsidyType"];
        if ([LocalSubsidyType length] > 0) {
            NSString *title = LocalSubsidyType;
            [_ButtonSubsidyType setTitle:title forState:UIControlStateNormal];
            [_ButtonSubsidyType setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
            
            //Delegate
            [self SubsidyInfoViewSubsidyType:title];
            
            //通知領牌地點畫面
            //NSArray *LicenseSite = [_dic objectForKey:@"LicenseSite"];
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_ChooseLicenseSite" object:LicenseSite];
        }
        
        //Add at version 2.0 by Justin in 2016/9/22
        //選擇汰購車與車主是否為同一人
        
        NSInteger EliminatingStatusType = [[data objectForKey:@"IsMySelfRenew"] integerValue];
        if ([[_ButtonSubsidyType.titleLabel.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"汰購"] ) {
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 282);
            _ViewEliminatingStatusType.hidden = NO;
            if (EliminatingStatusType == 0) {
                [self pressEliminatingStatusTypeDifferentBtn:nil];//不同人
            }else{
                [self pressEliminatingStatusTypeSameBtn:nil];//同一人
            }
        }else{
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 226);
            _ViewEliminatingStatusType.hidden = YES;
        }

        switch (EliminatingStatusType) {
            case 0:
                [self pressEliminatingStatusTypeDifferentBtn:nil];
                break;
            case 1:
                [self pressEliminatingStatusTypeSameBtn:nil];
                break;
            default:
                break;
        }
    }
}
#pragma mark - Button
-(IBAction)pressSubsidyYesBtn { //補助資訓＿補助狀況＿補助
    //防呆
    if (isSubsidyYes) {
        return;
    }
    isSubsidyYes = YES;
    
    //調整選取點擊按鈕ＵＩ
    [self SetButtonOnWithSender:_ButtonSubsidyYes];
    [self SetButtonOffWithSender:_ButtonSubsidyNo];
    
    //顯示地區即類型選項並調整ＵＩ
    //B3B3B3
    [_ButtonSubsidyArea setTitleColor:TEXT_COLOR_UNSELECTED forState:UIControlStateNormal];
    [_ButtonSubsidyType setTitleColor:TEXT_COLOR_UNSELECTED forState:UIControlStateNormal];
    [_ButtonSubsidyArea setTitle:@"請選擇補助地區" forState:UIControlStateNormal];
    [_ButtonSubsidyType setTitle:@"請選擇補助類型" forState:UIControlStateNormal];
    _ButtonSubsidyArea.hidden = NO;
    _ButtonSubsidyType.hidden = NO;
    _ViewEliminatingStatusType.hidden = YES;
    _ViewSubsidyNo.hidden = YES;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 225);
    
    [self SubsidyInfoViewSubsidyStatus:@"補助"];
}
-(IBAction)pressSubsidyNoBtn { //補助資訓＿補助狀況＿放棄補助
    if (!isSubsidyYes) {
        return;
    }
    isSubsidyYes = NO;
    
    //清掉補助類型array
    [arraySubsidyType removeAllObjects];
    
    //調整選取點擊按鈕ＵＩ
    [self SetButtonOnWithSender:_ButtonSubsidyNo];
    [self SetButtonOffWithSender:_ButtonSubsidyYes];
    
    //UI調整
    _ButtonSubsidyArea.hidden = YES;
    _ButtonSubsidyType.hidden = YES;
    _ViewEliminatingStatusType.hidden = YES;
    _ViewSubsidyNo.hidden = NO;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 169);
    
    [self SubsidyInfoViewSubsidyStatus:@"放棄補助"];
    
    //預設選項
    [self pressSubsidyNoBothBtn];
}

-(IBAction)pressSubsidyNoBothBtn { //放棄補助:兩者
    [self SetButtonOnWithSender:_ButtonSubsidyNoBoth];
    [self SetButtonOffWithSender:_ButtonSubsidyNoLocal];
    
    [self SubsidyInfoViewSubsidyNoType:@"兩者"];
}
-(IBAction)pressSubsidyNoLocalBtn { //放棄補助：地方
    [self SetButtonOnWithSender:_ButtonSubsidyNoLocal];
    [self SetButtonOffWithSender:_ButtonSubsidyNoBoth];
    
    [self SubsidyInfoViewSubsidyNoType:@"地方"];
}
-(IBAction)pressEliminatingStatusTypeSameBtn:(id)sender { //汰購車輛與車主：同一人
    [self SetButtonOnWithSender:_ButtonEliminatingStatusTypeSame];
    [self SetButtonOffWithSender:_ButtonEliminatingStatusTypeDifferent];
    [self SubsidyInfoEliminatingStatusType:@"同一人"];
}
-(IBAction)pressEliminatingStatusTypeDifferentBtn:(id)sender { //汰購車輛與車主：不同人
    [self SetButtonOnWithSender:_ButtonEliminatingStatusTypeDifferent];
    [self SetButtonOffWithSender:_ButtonEliminatingStatusTypeSame];
    [self SubsidyInfoEliminatingStatusType:@"不同人"];
}
-(IBAction)pressSubsidyAreaBtn { //補助地區
    [self ShowSubsidyAreaSelect];
}
-(IBAction)pressSubsidyTypeBtn { //補助類型
    [self ShowSubsidyTypeSelect];
}

#pragma mark - Call API
#pragma mark [302] 取得訂單補助地區選單
- (void) GetSubsidyAreaList {
    [ObjectManager showLodingHUDinView:self];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetSubsidyAreaList:parameter Source:self];
}
- (void) callBackGetSubsidyAreaList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arraySubsidyArea removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arraySubsidyArea addObjectsFromArray:data];
    }
    else {
        [ObjectManager showErrorHUDWithStatus:Sysmsg duration:3.0f inView:self];
    }
}
#pragma mark [303] 取得訂單地方補助類型選單
- (void) GetSubsidyTypeListWithArea:(NSString *)area {
    [ObjectManager showLodingHUDinView:self];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:area forKey:@"subsidyArea"];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetSubsidyTypeList:parameter Source:self];
}
- (void) callBackGetSubsidyTypeList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arraySubsidyType removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arraySubsidyType addObjectsFromArray:data];
    }
    else {
        [ObjectManager showErrorHUDWithStatus:Sysmsg duration:3.0f inView:self];
    }
}
#pragma mark - 輔助地區/類型選擇
-(void) ShowSubsidyAreaSelect{
    //顯示選擇補助區域
    isShowArea = YES;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇補助地區" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arraySubsidyArea count]; i++) {
        NSString *title = [arraySubsidyArea objectAtIndex:i];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonSubsidyArea;
    popPresenter.sourceRect = _ButtonSubsidyArea.bounds;
    
    [_sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) ShowSubsidyTypeSelect { //顯示選擇補助類型
    isShowArea = NO;
    if ([arraySubsidyType count] == 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請先選擇補助地區" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [self buildAlertContentList:alertController];

    }else if([arraySubsidyType count] >0){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇補助類型" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        [self buildAlertContentList:alertController];
        
    }

}
-(void)buildAlertContentList:(UIAlertController*) alertController{
    for (int i = 0; i < [arraySubsidyType count]; i++) {
        NSDictionary *_dic = [arraySubsidyType objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"SubsidyType"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonSubsidyType;
    popPresenter.sourceRect = _ButtonSubsidyType.bounds;
    
    [_sourceVC presentViewController:alertController animated:YES completion:NULL];
}
    

#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    if (isShowArea) { //選擇補助區域
        selectedIndex_SubsidyArea = buttonIndex;
        
        NSString *title = [arraySubsidyArea objectAtIndex:selectedIndex_SubsidyArea];
        [_ButtonSubsidyArea setTitle:title forState:UIControlStateNormal];
        [_ButtonSubsidyArea setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];

        
        //抓取該區域對應的補助
        [self GetSubsidyTypeListWithArea:title];
        
        //Delegate
        [self SubsidyInfoViewSubsidyArea:title];
    }
    else { //選擇補助類型
        selectedIndex_SubsidyType = buttonIndex;
        
        NSDictionary *_dic = [arraySubsidyType objectAtIndex:selectedIndex_SubsidyType];
        NSString *title = [_dic objectForKey:@"SubsidyType"];
        
        [_ButtonSubsidyType setTitle:title forState:UIControlStateNormal];
        [_ButtonSubsidyType setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];

        
        //通知領牌地點畫面
        NSArray *LicenseSite = [_dic objectForKey:@"LicenseSite"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_ChooseLicenseSite" object:LicenseSite];
        
//        _ButtonSubsidyType.titleLabel.textColor = [UIColor colorWithRed:85.0 green:85.0 blue:85.0 alpha:1];
        if ([[_ButtonSubsidyType.titleLabel.text substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"汰購"] ) {
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 282);
            _ViewEliminatingStatusType.hidden = NO;
            
        }else if ([_ButtonSubsidyType.titleLabel.text isEqualToString:@"新購"]){
            self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 226);
            _ViewEliminatingStatusType.hidden = YES;
        }

        //Delegate
        [self SubsidyInfoViewSubsidyType:title];
    }
}
@end
