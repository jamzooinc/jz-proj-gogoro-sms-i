//
//  SelectSubsidyVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "SelectSubsidyVC.h"
#import "GetOrderDetailVC.h"
#import "ConfirmInfoVC.h"
#import "SearchResultVC.h"

@interface SelectSubsidyVC () <UIScrollViewDelegate>
{
    CGFloat totalHeight;             //ScrollView總高度
    CGFloat gapBetweenView;
    SubsidyBasicView *ViewBasic;
    SubsidyLicenseView *ViewLicense;
    SubsidyInfoView *ViewInfo;
    SubsidyUserView *ViewUser;
    NSMutableDictionary *dicSubsidy; //要傳給後台的補助選擇資料
    
    BOOL didSelectNextWhenEditOrder; //YES:編輯訂單時，選擇下一步
    BOOL isCorporate;                //是不是法人訂單
    BOOL isAddingOrder;              //YES:正在新增訂單
}

@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@end

@implementation SelectSubsidyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    dicSubsidy = [[NSMutableDictionary alloc] init];
    
    //訂單編號
    NSLog(@"訂單編號:%@",_stringOrderNo);
    [dicSubsidy setObject:_stringOrderNo forKey:@"OrderNo"];
    
    gapBetweenView = 10;
    totalHeight = 0;
    
    NSLog(@"_dicSubsidyData:%@",_dicSubsidyData);
    NSLog(@"補助資料的領牌中心:%@",[_dicSubsidyData objectForKey:@"LicensingSite"]);
    
    //基本資訊
    [self UpdateUI_BasicInfo];
    //補助資訊
    [self UpdateUI_SubsidyInfo];
    //領牌資訊
    [self UpdateUI_LicenseInfo];
    //人員資訊
    [self UpdateUI_UserInfo];
    //上一步/下一步
    [self UpdateUI_Command];
    
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}
#pragma mark - 基本資訊
-(void) UpdateUI_BasicInfo {
    //Build face
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"SubsidyBasicView" owner:self options:nil];
    ViewBasic = (SubsidyBasicView *)[toplevels objectAtIndex:0];
    ViewBasic.delegate = self;
    ViewBasic.sourceVC = self;
    
    //Set UI
    ViewBasic.frame = CGRectMake(0, 0, 688, 399);
    ViewBasic.center = CGPointMake(ScreenWidth/2,ViewBasic.center.y);
    ViewBasic.dicOrderDetail = _dicOrderDetail;
    
    //show data content and adjustion UI
    ViewBasic.LabelSalesType.text = [_dicOrderDetail objectForKey:@"SalesTypeName"];
    [ViewBasic UpdateUIWithData:_dicSubsidyData];
    
    
    //Add to ScrollView
    [_ScrollView addSubview:ViewBasic];
    totalHeight = totalHeight + ViewBasic.frame.origin.y + ViewBasic.frame.size.height;
}

-(void) SubsidyBasicViewType:(NSString *)type { //身分
    if ([type isEqualToString:@"個人"]) {
        isCorporate = NO;
        [dicSubsidy setObject:@0 forKey:@"IdentityType"];
    }
    else if ([type isEqualToString:@"法人"]) {
        isCorporate = YES;
        [dicSubsidy removeObjectForKey:@"IsCivilService"];
        [ViewBasic pressNoServantsBtn];
        [dicSubsidy setObject:@1 forKey:@"IdentityType"];
    }
    
    //測試
    //isCorporate = YES;
    if (isCorporate) {
        //選擇法人時，自動強制選擇有保人
        [ViewBasic pressGrantorYesBtn];
    }
    
    //下面的View要調整位置
    [self updateFollowingViewWithLocationView:ViewBasic];
    
    //調整ScrollView
    totalHeight = _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height;
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
    //NSLog(@"選身分後:%@",dicSubsidy);
}
-(void) SubsidyBasicViewJuridicalPersonsPresentAndPersonalGetType:(BOOL)type{
    if (type) {
        [dicSubsidy setObject:@1 forKey:@"IsCorporateGiftCar"];
        
        //2016.11.19 UAT#80_成功選擇「法人贈車」時，下方補助選擇預設為放棄補助 - 放棄地方(請記得也要帶入領牌中心，同前一題)
        [ViewInfo pressSubsidyNoBtn];
        [ViewInfo pressSubsidyNoLocalBtn];

    }else if (!type) {
        [dicSubsidy setObject:@0 forKey:@"IsCorporateGiftCar"];
    }
}

-(void) SubsidyBasicViewServantsType:(BOOL)type{
    if (type) {
        [dicSubsidy setObject:@1 forKey:@"IsCivilService"];
        NSLog(@"yes");
        
    }else if (!type) {
        [dicSubsidy setObject:@0 forKey:@"IsCivilService"];
        NSLog(@"no");
    }
}
-(void) SubsidyBasicViewGrantor:(BOOL)hasGrantor { //有無保人
    //法人是規定一定要有保人的，當用戶選擇法人時，自動強制選擇有保人，
    //當使用者要選無的時候，彈出訊息提醒，法人添購必須有保證人之類的警語
    if (isCorporate && !hasGrantor) { //法人
        [self showAlertControllerWithMessage:@"法人添購必須有保證人"];
        return;
    }
    
    if (hasGrantor) {
        [dicSubsidy setObject:@1 forKey:@"HasGuarantor"];
    }
    else {
        [dicSubsidy setObject:@0 forKey:@"HasGuarantor"];
    }
    //NSLog(@"選有無保人後:%@",dicSubsidy);
}
-(void) SubsidyBasicViewDistributorsStore:(NSString *)name{
    if (name.length > 0) {
        [dicSubsidy setObject:name forKey:@"PartnerStoreNo"];
    }
}

#pragma mark - 補助資訊
-(void) UpdateUI_SubsidyInfo {
    //補助資訊：生成外觀
    
    //build face
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"SubsidyInfoView" owner:self options:nil];
    SubsidyInfoView *view1 = [toplevels objectAtIndex:0];
    view1.delegate = self;
    
    //set size and position
//    view1.frame = CGRectMake(0, totalHeight + gapBetweenView, 688, 282);
    view1.frame = CGRectMake(0, ViewBasic.frame.size.height + gapBetweenView, 688, 282);
    view1.center = CGPointMake(ScreenWidth/2, view1.center.y);
    
    //build data to show
    view1.OrderNo = _stringOrderNo;
    [view1 UpdateUIWithData:_dicSubsidyData];
    
    //add to scrollView
    [_ScrollView addSubview:view1];
    totalHeight = view1.frame.origin.y + view1.frame.size.height;
    ViewInfo = view1;
    ViewInfo.sourceVC = self;
}
-(void) SubsidyInfoViewSubsidyStatus:(NSString *)status {

    //補助資訊：補助狀況
    if ([status isEqualToString:@"補助"]) {
        [dicSubsidy setObject:@"2" forKey:@"SubsidyType"];
        
        //是否可以選擇領牌中心（只有在放棄補助時才可選）
        //ViewLicense.canShowLicensingSiteSelect = NO;
    }
    else if ([status isEqualToString:@"放棄補助"]) {
        [dicSubsidy setObject:@"0" forKey:@"SubsidyType"];
        
        //是否可以選擇領牌中心（只有在放棄補助時才可選）
        //ViewLicense.canShowLicensingSiteSelect = YES;
    }
    //NSLog(@"選補助狀況後:%@",dicSubsidy);
    
    //重設領牌中心資料跟UI
    [dicSubsidy removeObjectForKey:@"LicensingSite"];
    [ViewLicense ResetLicensingListUI];
    
    //輔助狀況下面的View要調整位置
    [self updateFollowingViewWithLocationView:ViewInfo];
    
    //調整ScrollView
    totalHeight = _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height;
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}
-(void) SubsidyInfoViewSubsidyNoType:(NSString *)type {
    //基本資訊：放棄補助
    //移除補助地區跟類型
    [dicSubsidy removeObjectForKey:@"SubsidyArea"];
    [dicSubsidy removeObjectForKey:@"LocalSubsidyType"];
    [dicSubsidy removeObjectForKey:@"LicensingSite"];
    [dicSubsidy removeObjectForKey:@"IsMySelfRenew"];
    
    if ([type isEqualToString:@"兩者"]) {
        [dicSubsidy setObject:@"0" forKey:@"SubsidyType"];
    }
    else if ([type isEqualToString:@"地方"]) {
        [dicSubsidy setObject:@"1" forKey:@"SubsidyType"];
    }
    //NSLog(@"選放棄補助後:%@",dicSubsidy);
    
    //取得領牌中心 需要用API [303]，subsidyArea帶入"放棄補助"，撈出領牌中心選項，並將下拉選單預設為API回傳的第一筆領牌中心
    ViewLicense.didGiveup = YES;
    [ViewLicense GetSubsidyTypeList];
}
-(void) SubsidyInfoViewSubsidyArea:(NSString *)area { //補助地區
    [dicSubsidy setObject:area forKey:@"SubsidyArea"];
   
    //移除補助類型跟地點，因要重選
    [dicSubsidy removeObjectForKey:@"SalesTypeName"];
    [dicSubsidy removeObjectForKey:@"LocalSubsidyType"];
    [dicSubsidy removeObjectForKey:@"LicensingSite"];
    [ViewInfo.ButtonSubsidyType setTitle:@"請選擇補助類型" forState:UIControlStateNormal];
    [ViewInfo.ButtonSubsidyType setTitleColor:TEXT_COLOR_UNSELECTED forState:UIControlStateNormal];
    
    
    ViewInfo.frame = CGRectMake(ViewInfo.frame.origin.x, ViewInfo.frame.origin.y, ViewInfo.frame.size.width, 226);
    ViewInfo.ViewEliminatingStatusType.hidden = YES;

    [ViewLicense.ButtonLicensePlace setTitle:@"請選擇領牌地點" forState:UIControlStateNormal];
    [ViewLicense.ButtonLicensePlace setTitleColor:TEXT_COLOR_UNSELECTED forState:UIControlStateNormal];
    ViewLicense.subsidyArea = area;
    [ViewLicense GetSubsidyTypeList];
    
    [ViewInfo pressEliminatingStatusTypeSameBtn:nil];
    
    [self updateFollowingViewWithLocationView:ViewInfo];

}
-(void) SubsidyInfoViewSubsidyType:(NSString *)type {
    //基本資廖：補助類型
    [dicSubsidy setObject:type forKey:@"LocalSubsidyType"];
    
    //
    [self updateFollowingViewWithLocationView:ViewInfo];
    
    //調整ScrollView
    totalHeight = _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height;
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];

    
    //NSLog(@"選補助類型後:%@",dicSubsidy);
}

-(void) SubsidyInfoEliminatingStatusType:(NSString *)type {
    if ([type isEqualToString:@"同一人"]) {
        [dicSubsidy setObject:@"1" forKey:@"IsMySelfRenew"];
    }else if ([type isEqualToString:@"不同人"]) {
        [dicSubsidy setObject:@"0" forKey:@"IsMySelfRenew"];
    }
}

#pragma mark - 領牌資訊
-(void) UpdateUI_LicenseInfo {
    //基本資訊
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"SubsidyLicenseView" owner:self options:nil];
    SubsidyLicenseView *view1 = [toplevels objectAtIndex:0];
    view1.delegate = self;
    view1.frame = CGRectMake(0, totalHeight + gapBetweenView, 688, 525);
    view1.center = CGPointMake(ScreenWidth/2, view1.center.y);
    view1.OrderNo = _stringOrderNo;
    view1.TextInfo.text = _dicOrderDetail[@"Memo"];
    [view1 UpdateUIWithData:_dicSubsidyData];
    [_ScrollView addSubview:view1];
    
    //2016.10.13 動態調整DMS備註的高度
    CGFloat originalHeight = view1.TextInfo.frame.size.height;
    [ObjectManager UpdateFrameWithStringLength:view1.TextInfo];
    CGFloat offset = view1.TextInfo.frame.size.height - originalHeight;
    if (offset > 0) { //超過原本高度，調整UIView的高度
        view1.frame = CGRectMake(view1.frame.origin.x,view1.frame.origin.y,view1.frame.size.width,view1.frame.size.height + offset);
    }
    
    totalHeight =  view1.frame.origin.y + view1.frame.size.height;
    
    ViewLicense = view1;
    ViewLicense.sourceVC = self;
}
-(void) SubsidyLicenseViewSite:(NSString *)site { //領牌地點
    [dicSubsidy setObject:site forKey:@"LicensingSite"];
    //NSLog(@"選領牌地點後:%@",dicSubsidy);
}
-(void) SubsidyLicenseViewStatus:(NSString *)status { //領牌狀況
    //移除選牌狀況
    [dicSubsidy removeObjectForKey:@"LicensingType"];
    
    if ([status isEqualToString:@"自領牌"]) {
        [dicSubsidy setObject:@"1" forKey:@"LicensingAgent"];
        
        //移除投保年限
        [dicSubsidy removeObjectForKey:@"InsuranceYear"];
    }
    else if ([status isEqualToString:@"代領牌"]) {
        [dicSubsidy setObject:@"0" forKey:@"LicensingAgent"];
    }
    //NSLog(@"選領牌狀況後:%@",dicSubsidy);
    
    //領牌資訊下面的View要調整位置
    [self updateFollowingViewWithLocationView:ViewLicense];
    
    //調整ScrollView
    totalHeight = _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height;
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}
-(void) SubsidyLicenseViewYear:(NSString *)year { //投保年限
    [dicSubsidy setObject:year forKey:@"InsuranceYear"];
    //NSLog(@"選投保年限後:%@",dicSubsidy);
    
}
-(void) SubsidyLicenseViewStamp:(NSString *)stamp { //印章
    if ([stamp isEqualToString:@"代刻"]) {
        [dicSubsidy setObject:@"1" forKey:@"SealType"];
    }
    else if ([stamp isEqualToString:@"自備"]) {
        [dicSubsidy setObject:@"0" forKey:@"SealType"];
    }
    //NSLog(@"選印章後:%@",dicSubsidy);
}
-(void) SubsidyLicenseViewChoose:(NSString *)choose { //選牌狀況
    if ([choose isEqualToString:@"選號"]) {
        [dicSubsidy setObject:@"1" forKey:@"LicensingType"];
    }
    else if ([choose isEqualToString:@"不選號"]) {
        [dicSubsidy setObject:@"0" forKey:@"LicensingType"];
    }
    else if ([choose isEqualToString:@"鐵支"]) {
        [dicSubsidy setObject:@"2" forKey:@"LicensingType"];
    }
    //NSLog(@"選選牌狀況後:%@",dicSubsidy);
}
-(void) SubsidyLicenseViewInfoEditStatus:(BOOL)didBegin { //領牌資訊備註編輯狀況
    if (didBegin) { //開始編輯
        [_ScrollView setContentOffset:CGPointMake(0, ScreenHeight*0.4) animated:YES];
    }
    else { //結束編輯
        [_ScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}
-(void) SubsidyLicenseViewInfo:(NSString *)info { //領牌資訊備註
    
    [dicSubsidy setObject:info forKey:@"Memo"];
    //NSLog(@"選領牌資訊備註後:%@",dicSubsidy);
}
#pragma mark - 人員資訊
-(void) UpdateUI_UserInfo {
    //基本資訊
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"SubsidyUserView" owner:self options:nil];
    SubsidyUserView *view1 = [toplevels objectAtIndex:0];
    view1.delegate = self;
    view1.frame = CGRectMake(0, totalHeight + gapBetweenView, 688, 333);
    view1.center = CGPointMake(ScreenWidth/2, view1.center.y);
    [view1 UpdateUIWithData:_dicSubsidyData];
    [_ScrollView addSubview:view1];
    totalHeight =  view1.frame.origin.y + view1.frame.size.height;
    
    ViewUser = view1;
}
-(void) SubsidyUserViewEditStatus:(BOOL)didBegin { //相關欄位編輯
    if (didBegin) { //開始編輯
        //scrollViewOffsetBeforeEdit = _ScrollView.contentOffset.y; //記錄原本位置
        [_ScrollView setContentOffset:CGPointMake(0, ScreenHeight*0.65) animated:YES];
    }
    else { //結束編輯
        //捲動到底
        CGPoint bottomOffset = CGPointMake(0, _ScrollView.contentSize.height - _ScrollView.bounds.size.height);
        [_ScrollView setContentOffset:bottomOffset animated:YES];
        //[_ScrollView setContentOffset:CGPointMake(0, scrollViewOffsetBeforeEdit) animated:YES];
    }
}
-(void) SubsidyUserViewSigner:(NSString *)singer { //店長/授權簽署人
    [dicSubsidy setObject:singer forKey:@"MasterName"];
    //NSLog(@"選店長/授權簽署人後:%@",dicSubsidy);
}
-(void) SubsidyUserViewSales:(NSString *)sales { //銷售人員姓名
    [dicSubsidy setObject:sales forKey:@"SalesName"];
    //NSLog(@"選銷售人員姓名後:%@",dicSubsidy);
}
-(void) SubsidyUserViewLaw:(NSString *)law { //法定代理人(父/母)獲授權代理
    [dicSubsidy setObject:law forKey:@"AgentName"];
    //NSLog(@"選法定代理人(父/母)獲授權代理後:%@",dicSubsidy);
}
-(void) SubsidyUserViewIDDateLate:(BOOL)isLate { //身分證換發日期小於規範居住年限，故需備戶籍謄本
    if (isLate) {
        [dicSubsidy setObject:@1 forKey:@"NeedCensus"];
    }
    else {
        [dicSubsidy setObject:@0 forKey:@"NeedCensus"];
    }
    //NSLog(@"選身分證換發日期小於規範居住年限後:%@",dicSubsidy);
}
-(void) SubsidyUserViewIsOverSite:(BOOL)IsOverSite { //需跨店開立發票,無需檢附完整發票
    if (IsOverSite) {
        [dicSubsidy setObject:@1 forKey:@"IsOverSite"];
    }
    else {
        [dicSubsidy setObject:@0 forKey:@"IsOverSite"];
    }
    //NSLog(@"選需跨店開立發票後:%@",dicSubsidy);
}
#pragma mark - 上一步/下一步
-(void) UpdateUI_Command {
    _ViewCommand.frame = CGRectMake(0, totalHeight + gapBetweenView, 768, 71);
    [_ScrollView addSubview:_ViewCommand];
    totalHeight =  _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height;
}
-(IBAction) pressPreviousBtn:(id)sender {
    if (_didFromSupplement) { //從補單來
        //更新完之後才知道更新前是不是選下一步
        didSelectNextWhenEditOrder = NO;
        
        //更新補助
        [self OrderAdd];
    }
    else { //回前頁
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(IBAction) pressNextBtn:(id)sender {
    if (_didFromSupplement) { //從補單來
        //更新完之後才知道更新前是不是選下一步
        didSelectNextWhenEditOrder = YES;
        
        //更新補助
        [self OrderAdd];
    }
    else { //新增訂單
        [self OrderAdd];
    }
}
#pragma mark - Call API
#pragma mark [305] 新增補助單(補助選擇完後呼叫)
- (void) OrderAdd {
    if (isAddingOrder) {
        return;
    }
    
    //關閉鍵盤，如果有的話
    [ViewLicense.TextInfo resignFirstResponder];
    [ViewUser.TextSigner resignFirstResponder];
    [ViewUser.TextSales resignFirstResponder];
    [ViewUser.TextLaw resignFirstResponder];
    
#if TARGET_IPHONE_SIMULATOR
    //測試資料
    /*[dicSubsidy removeAllObjects];
    [dicSubsidy setObject:@"法定代理人" forKey:@"AgentName"];
    [dicSubsidy setObject:@"0" forKey:@"HasGuarantor"];
    [dicSubsidy setObject:@"2" forKey:@"InsuranceYear"];
    [dicSubsidy setObject:@"1" forKey:@"IsOverSite"];
    [dicSubsidy setObject:@"0" forKey:@"LicensingAgent"];
    [dicSubsidy setObject:@"台北領牌中心" forKey:@"LicensingSite"];
    [dicSubsidy setObject:@"0" forKey:@"LicensingType"];
    [dicSubsidy setObject:@"新購" forKey:@"LocalSubsidyType"];
    [dicSubsidy setObject:@"店長" forKey:@"MasterName"];
    [dicSubsidy setObject:@"1" forKey:@"NeedCensus"];
    [dicSubsidy setObject:@"P1039" forKey:@"OrderNo"];
    [dicSubsidy setObject:@"銷售人員" forKey:@"SalesName"];
    [dicSubsidy setObject:@"1" forKey:@"SealType"];
    [dicSubsidy setObject:@"0" forKey:@"IdentityType"];
    [dicSubsidy setObject:@"台北市" forKey:@"SubsidyArea"];
    [dicSubsidy setObject:@"2" forKey:@"SubsidyType"];
    [dicSubsidy setObject:@"備註資訊" forKey:@"LicensingInfo"];*/
    
#else
#endif
    
    //檢查必要欄位
    //如果選擇補助，一定要有補助地區跟補助類型
    if ([[dicSubsidy objectForKey:@"SubsidyType"] isEqualToString:@"2"]) {
        if ([[dicSubsidy objectForKey:@"SubsidyArea"] length] == 0) {
            [self showAlertControllerWithMessage:@"請選擇補助地區"];
            return;
        }
        else if ([[dicSubsidy objectForKey:@"LocalSubsidyType"] length] == 0) {
            [self showAlertControllerWithMessage:@"請選擇補助類型"];
            return;
        }
        else if ([[dicSubsidy objectForKey:@"LicensingSite"] length] == 0) {
            [self showAlertControllerWithMessage:@"請選擇領牌中心"];
            return;
        }
    }
    
    [ObjectManager showLodingHUDinView:self.view];
    
    //NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callOrderAdd:dicSubsidy Source:self];
    
    isAddingOrder = YES;
}
- (void)callBackOrderAdd:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        if (_didFromSupplement) { //從補單來
            [ObjectManager hideHUDForView:self.view];
            
            if (didSelectNextWhenEditOrder) { //編輯訂單時，選擇下一步
                //更新欄位項目，進CheckList畫面
                [self GotoConfirmInfoVCWithOrderNo:_stringOrderNo];
            }
            else {
                //更新欄位資訊，回初始畫面(待處理訂單清單)
                //到補助選擇
                NSArray *array = self.navigationController.viewControllers;
                UIViewController *destVC = nil;
                for (id vc in array) {
                    NSLog(@"vc:%@",vc);
                    if ([vc isKindOfClass:[SearchResultVC class]]) {
                        destVC = (SearchResultVC *)vc;
                    }
                }
                NSLog(@"destVC:%@",destVC);
                [self.navigationController popToViewController:destVC animated:NO];
            }
        }
        else { //新增訂單
            //上傳補助資料成功，抓取訂單資料
            [self GetOrderDetail];
        }
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
    
    isAddingOrder = NO;
}
#pragma mark [301] 取得訂單
- (void) GetOrderDetail {
    //[ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringOrderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void)callBackGetOrderDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        [self GotoGetOrderDetailVCWithData:Data];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}

#pragma mark - 換頁
-(void) GotoGetOrderDetailVCWithData:(NSDictionary *)data { //
    GetOrderDetailVC *destVC = [[GetOrderDetailVC alloc] initWithNibName:@"GetOrderDetailVC" bundle:nil];
    destVC.dicData = data;
    destVC.didFromSubsidy = YES;
    destVC.dicSubsidy = dicSubsidy;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConfirmInfoVCWithOrderNo:(NSString *)orderNo { //3.6確認資訊
    ConfirmInfoVC *destVC = [[ConfirmInfoVC alloc] initWithNibName:@"ConfirmInfoVC" bundle:nil];
    destVC.OrderNo = orderNo;
    destVC.CustomerName = _CustomerName;
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateFollowingViewWithLocationView:(UIView*)locationView{
    if (ViewBasic) {
        ViewInfo.frame = CGRectMake(ViewInfo.frame.origin.x, ViewBasic.frame.origin.y + ViewBasic.frame.size.height + gapBetweenView, ViewInfo.frame.size.width, ViewInfo.frame.size.height);
        
        ViewLicense.frame = CGRectMake(ViewLicense.frame.origin.x, ViewInfo.frame.origin.y + ViewInfo.frame.size.height + gapBetweenView, ViewLicense.frame.size.width, ViewLicense.frame.size.height);
        
        ViewUser.frame = CGRectMake(ViewUser.frame.origin.x, ViewLicense.frame.origin.y + ViewLicense.frame.size.height + gapBetweenView, ViewUser.frame.size.width, ViewUser.frame.size.height);
        
        _ViewCommand.frame = CGRectMake(_ViewCommand.frame.origin.x, ViewUser.frame.origin.y + ViewUser.frame.size.height + gapBetweenView, _ViewCommand.frame.size.width, _ViewCommand.frame.size.height);
    }
    if (ViewInfo) {
        ViewLicense.frame = CGRectMake(ViewLicense.frame.origin.x, ViewInfo.frame.origin.y + ViewInfo.frame.size.height + gapBetweenView, ViewLicense.frame.size.width, ViewLicense.frame.size.height);
        
        ViewUser.frame = CGRectMake(ViewUser.frame.origin.x, ViewLicense.frame.origin.y + ViewLicense.frame.size.height + gapBetweenView, ViewUser.frame.size.width, ViewUser.frame.size.height);
        
        _ViewCommand.frame = CGRectMake(_ViewCommand.frame.origin.x, ViewUser.frame.origin.y + ViewUser.frame.size.height + gapBetweenView, _ViewCommand.frame.size.width, _ViewCommand.frame.size.height);
    }
    if (ViewLicense) {
        ViewUser.frame = CGRectMake(ViewUser.frame.origin.x, ViewLicense.frame.origin.y + ViewLicense.frame.size.height + gapBetweenView, ViewUser.frame.size.width, ViewUser.frame.size.height);
        
        _ViewCommand.frame = CGRectMake(_ViewCommand.frame.origin.x, ViewUser.frame.origin.y + ViewUser.frame.size.height + gapBetweenView, _ViewCommand.frame.size.width, _ViewCommand.frame.size.height);
    }
}

@end
