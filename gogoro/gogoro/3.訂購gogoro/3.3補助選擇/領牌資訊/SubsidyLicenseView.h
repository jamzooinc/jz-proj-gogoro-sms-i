
#import <UIKit/UIKit.h>
#import "SubsidyRootView.h"

@protocol SubsidyLicenseViewDelegate <NSObject>
@optional
-(void) SubsidyLicenseViewSite:(NSString *)site;
-(void) SubsidyLicenseViewStatus:(NSString *)status;
-(void) SubsidyLicenseViewYear:(NSString *)year;
-(void) SubsidyLicenseViewStamp:(NSString *)stamp;
-(void) SubsidyLicenseViewChoose:(NSString *)choose;
-(void) SubsidyLicenseViewInfo:(NSString *)info;
-(void) SubsidyLicenseViewInfoEditStatus:(BOOL)didBegin;
@end

@interface SubsidyLicenseView : SubsidyRootView <UITextViewDelegate> {
    __weak id <SubsidyLicenseViewDelegate> delegate;
    BOOL isLicenseStatusOther; //是否為代領牌
    NSMutableArray *arrayLicensingSite; //領牌中心
    NSInteger selectedIndex;
    BOOL didShowedApiLicenseSite; //是否顯示過後台回傳的領牌中心
}
@property (weak) id delegate;
@property (nonatomic,strong) NSString *OrderNo;
@property (nonatomic,weak) UIViewController *sourceVC;

@property (nonatomic,weak) NSString *subsidyArea; //補助區域
@property BOOL didGiveup; //是否為放棄補助
//@property BOOL canShowLicensingSiteSelect; //是否可以選擇領牌中心（只有在放棄補助時才可選）
@property (nonatomic,weak) IBOutlet UIView *ViewLicensePlace;
//@property (nonatomic,weak) IBOutlet UILabel *LabelLicensePlace;
@property (nonatomic,weak) IBOutlet UIButton *ButtonLicensePlace; //領牌中心
@property (nonatomic,weak) IBOutlet UIView *ViewLicenseStatus;
@property (nonatomic,weak) IBOutlet UIButton *ButtonLicenseStatusSelf; //自領牌
@property (nonatomic,weak) IBOutlet UIButton *ButtonLicenseStatusOther; //代領牌
@property (nonatomic,weak) IBOutlet UIView *ViewYear;
@property (nonatomic,weak) IBOutlet UIButton *ButtonYear1;
@property (nonatomic,weak) IBOutlet UIButton *ButtonYear2;
@property (nonatomic,weak) IBOutlet UIView *ViewStamp;
@property (nonatomic,weak) IBOutlet UIButton *ButtonStampSelf; //自帶印章
@property (nonatomic,weak) IBOutlet UIButton *ButtonStampOther; //代刻印章
@property (nonatomic,weak) IBOutlet UIView *ViewChoose;
@property (nonatomic,weak) IBOutlet UIButton *ButtonChooseYes; //選號
@property (nonatomic,weak) IBOutlet UIButton *ButtonChooseNo; //不選號
@property (nonatomic,weak) IBOutlet UIButton *ButtonChooseSame; //鐵支
@property (nonatomic,weak) IBOutlet UIView *ViewInfo;
@property (nonatomic,weak) IBOutlet UILabel *TextInfo;
//- (void) GetLicensingList; //取得領牌中心
- (void) GetSubsidyTypeList; //取得領牌中心
- (void) ResetLicensingListUI; //重設領牌中心資料跟UI
@end
