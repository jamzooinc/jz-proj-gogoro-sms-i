


#import "SubsidyLicenseView.h"

@implementation SubsidyLicenseView

@synthesize delegate;
#pragma mark - Delegate
-(void) SubsidyLicenseViewSite:(NSString *)site {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewSite:)]) {
        [[self delegate] SubsidyLicenseViewSite:site];
    }
}
-(void) SubsidyLicenseViewStatus:(NSString *)status {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewStatus:)]) {
        [[self delegate] SubsidyLicenseViewStatus:status];
    }
}
-(void) SubsidyLicenseViewYear:(NSString *)year {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewYear:)]) {
        [[self delegate] SubsidyLicenseViewYear:year];
    }
}
-(void) SubsidyLicenseViewStamp:(NSString *)stamp {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewStamp:)]) {
        [[self delegate] SubsidyLicenseViewStamp:stamp];
    }
}
-(void) SubsidyLicenseViewChoose:(NSString *)choose {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewChoose:)]) {
        [[self delegate] SubsidyLicenseViewChoose:choose];
    }
}
-(void) SubsidyLicenseViewInfo:(NSString *)info {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewInfo:)]) {
        [[self delegate] SubsidyLicenseViewInfo:info];
    }
}
-(void) SubsidyLicenseViewInfoEditStatus:(BOOL)didBegin {
    if ([[self delegate] respondsToSelector:@selector(SubsidyLicenseViewInfoEditStatus:)]) {
        [[self delegate] SubsidyLicenseViewInfoEditStatus:didBegin];
    }
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data {
    [super UpdateUIWithData:data];
    
    arrayLicensingSite = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ChooseLicenseSite:) name:@"kNotification_ChooseLicenseSite" object:nil];
     
    //預設
    [self pressLicenseStatusOtherBtn]; //代領牌
    [self pressYear2Btn]; //投保2年
    [self pressStampOtherBtn]; //印章代刻
    
    //依據資料調整
    if (data) {
        //LicensingSite 領牌中心
        NSString *LicensingSite = [data objectForKey:@"LicensingSite"]; //Memo
        if ([LicensingSite length] > 0) {
            NSString *site = LicensingSite;
            [_ButtonLicensePlace setTitle:site forState:UIControlStateNormal];
            [_ButtonLicensePlace setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
            
            //Delegate
            [self SubsidyLicenseViewSite:site];
        }
        else { //API回傳的補助資料沒有領牌中心
            didShowedApiLicenseSite = YES;
        }
        
        //LicensingAgent 領牌狀況 ● 代領牌0 ● 自領牌1
        NSInteger LicensingAgent = [[data objectForKey:@"LicensingAgent"] integerValue];
        if (LicensingAgent == 0) { //代領牌0
            [self pressLicenseStatusOtherBtn];
        }
        else if (LicensingAgent == 1) { //自領牌1
            [self pressLicenseStatusSelfBtn];
        }
        
        //InsuranceYear  投保年限 ● 一年1 ● 兩年2
        NSInteger InsuranceYear = [[data objectForKey:@"InsuranceYear"] integerValue];
        if (InsuranceYear == 1) { //一年1
            [self pressYear1Btn];
        }
        else if (InsuranceYear == 2) { //兩年2
            [self pressYear2Btn];
        }
        
        //SealType  印章 ● 自備0 ● 代刻1
        NSInteger SealType = [[data objectForKey:@"SealType"] integerValue];
        if (SealType == 0) { //自備0
            [self pressStampSelfBtn];
        }
        else if (SealType == 1) { //代刻1
            [self pressStampOtherBtn];
        }
        
        //LicensingType 選牌狀況 ● 不選號0 ● 選號1 ● 鐵支號2
        NSInteger LicensingType = [[data objectForKey:@"LicensingType"] integerValue];
        if (LicensingType == 0) { //不選號0
            [self pressChooseNoBtn];
        }
        else if (LicensingType == 1) { //選號1
            [self pressChooseYesBtn];
        }
        else if (LicensingType == 2) { //鐵支號2
            [self pressChooseSameBtn];
        }
        
        //LicensingInfo 領牌資訊備註
        NSString *LicensingInfo = [data objectForKey:@"Memo"];
        //LicensingInfo = @"第一行\n第一行\n第一行\n第一行\n第一行\n第一行\n第一行\n第一行\n第一行";
        //LicensingInfo = @"第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行第一行";
        if ([LicensingInfo length] > 0) {
            [_TextInfo setText:LicensingInfo];
            [self SubsidyLicenseViewInfo:_TextInfo.text];
        }
        
        //抓領牌地點
        //2016.06.09 領牌中心的下拉選單改用303，但不會有SubsidyArea，所以不要帶
        _subsidyArea = [data objectForKey:@"SubsidyArea"];
        [self GetSubsidyTypeList];
        
        /*//放棄補助的時候，要去抓領排地點
         //SubsidyType    補助狀況 ● 放棄補助0 ● 放棄地方補助1 ● T E S 補 助 +  地  方 補 助 2
         NSInteger SubsidyType = [[data objectForKey:@"SubsidyType"] integerValue];
         if (SubsidyType == 2) { //T E S 補 助 +  地  方 補 助 2
         
         }
         else if (SubsidyType == 1) { //放棄地方補助1
         
         }
         else if (SubsidyType == 0) { //放棄補助0
         [self GetLicensingList];
         }*/
    }
}
- (void) ResetLicensingListUI { //重設領牌中心資料跟UI
    [_ButtonLicensePlace setTitle:@"請選擇領牌地點" forState:UIControlStateNormal];
    [_ButtonLicensePlace setTitleColor:TEXT_COLOR_UNSELECTED forState:UIControlStateNormal];
    [arrayLicensingSite removeAllObjects];
}
#pragma mark - Button
-(IBAction) pressLicenseStatusSelfBtn { //領牌狀況:自領牌
    if (!isLicenseStatusOther) {
        return;
    }
    isLicenseStatusOther = NO;
    
    //UI調整
    _ViewChoose.hidden = YES; //選號隱藏
    _ViewYear.hidden = YES; //投保年限隱藏
    //印章往上
    _ViewStamp.frame = CGRectMake(_ViewStamp.frame.origin.x, 165, _ViewStamp.frame.size.width, _ViewStamp.frame.size.height);
    //備註往上
    _ViewInfo.frame = CGRectMake(_ViewInfo.frame.origin.x, 221, _ViewInfo.frame.size.width, _ViewInfo.frame.size.height);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 525 - 56 - 56);
    
    [self SetButtonOnWithSender:_ButtonLicenseStatusSelf];
    [self SetButtonOffWithSender:_ButtonLicenseStatusOther];
    
    [self SubsidyLicenseViewStatus:@"自領牌"];
    
    
}
-(IBAction) pressLicenseStatusOtherBtn { //領牌狀況:代領牌
    if (isLicenseStatusOther) {
        return;
    }
    isLicenseStatusOther = YES;
    
    //UI調整
    _ViewChoose.hidden = NO;
    _ViewYear.hidden = NO;
    _ViewStamp.frame = CGRectMake(_ViewStamp.frame.origin.x, 221, _ViewStamp.frame.size.width, _ViewStamp.frame.size.height);
    _ViewInfo.frame = CGRectMake(_ViewInfo.frame.origin.x, 333, _ViewInfo.frame.size.width, _ViewInfo.frame.size.height);
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 525);
    
    [self SetButtonOnWithSender:_ButtonLicenseStatusOther];
    [self SetButtonOffWithSender:_ButtonLicenseStatusSelf];
    
    [self SubsidyLicenseViewStatus:@"代領牌"];
    
    //代領牌預設：不選號
    [self pressChooseNoBtn];
}
-(IBAction) pressYear1Btn { //投保年限:1年
    [self SetButtonOnWithSender:_ButtonYear1];
    [self SetButtonOffWithSender:_ButtonYear2];
    
    [self SubsidyLicenseViewYear:@"1"];
}
-(IBAction) pressYear2Btn { //投保年限:2年
    [self SetButtonOnWithSender:_ButtonYear2];
    [self SetButtonOffWithSender:_ButtonYear1];
    
    [self SubsidyLicenseViewYear:@"2"];
}
-(IBAction) pressStampSelfBtn { //印章:自備
    [self SetButtonOnWithSender:_ButtonStampSelf];
    [self SetButtonOffWithSender:_ButtonStampOther];
    
    [self SubsidyLicenseViewStamp:@"自備"];
}
-(IBAction) pressStampOtherBtn { //印章:代刻
    [self SetButtonOnWithSender:_ButtonStampOther];
    [self SetButtonOffWithSender:_ButtonStampSelf];
    
    [self SubsidyLicenseViewStamp:@"代刻"];
}
-(IBAction) pressChooseYesBtn { //選牌狀況:選號
    [self SetButtonOnWithSender:_ButtonChooseYes];
    [self SetButtonOffWithSender:_ButtonChooseNo];
    [self SetButtonOffWithSender:_ButtonChooseSame];
    
    [self SubsidyLicenseViewChoose:@"選號"];
}
-(IBAction) pressChooseNoBtn { //選牌狀況:不選號
    [self SetButtonOnWithSender:_ButtonChooseNo];
    [self SetButtonOffWithSender:_ButtonChooseYes];
    [self SetButtonOffWithSender:_ButtonChooseSame];
    
    [self SubsidyLicenseViewChoose:@"不選號"];
}
-(IBAction) pressChooseSameBtn { //選牌狀況:鐵支
    [self SetButtonOnWithSender:_ButtonChooseSame];
    [self SetButtonOffWithSender:_ButtonChooseYes];
    [self SetButtonOffWithSender:_ButtonChooseNo];
    
    [self SubsidyLicenseViewChoose:@"鐵支"];
}
-(IBAction) pressLicensePlaceBtn { //領牌地點
    [self ShowLicenseSiteSelect];
}

#pragma mark - Call API
#pragma mark [303] 取得訂單地方補助類型選單
- (void) GetSubsidyTypeList {
    //[ObjectManager showLodingHUDinView:self];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //2016.10.30 領牌中心選項，在選擇"放棄補助"時(不管是兩者或僅地方)，和有選擇補助地區時一樣需要用API [303]，subsidyArea帶入"放棄補助"，撈出領牌中心選項，並將下拉選單預設為API回傳的第一筆領牌中心
    if (_didGiveup) {
        didShowedApiLicenseSite = YES;
        [parameter setObject:@"放棄補助" forKey:@"subsidyArea"];
    }
    else if (_subsidyArea) {
        [parameter setObject:_subsidyArea forKey:@"subsidyArea"];
    }
    
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetSubsidyTypeList:parameter Source:self];
}
- (void)callBackGetSubsidyTypeList:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    //參數復歸
    _didGiveup = NO;
    
    if (Syscode == kStatuscode_Sucess) {
        [arrayLicensingSite removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        if ([data count] > 0) {
            NSDictionary *_dic = [data objectAtIndex:0];
            NSArray *arraySite = [_dic objectForKey:@"LicenseSite"];
            [arrayLicensingSite addObjectsFromArray:arraySite];
            
            if (didShowedApiLicenseSite) { //已顯示過後台回傳的領牌中心
                //有資料的話預設帶第一個
                [self SetDefaultSite];
            }
            else { //預設顯示後台回傳的領牌中心
                didShowedApiLicenseSite = YES;
            }
        }
    }
    else {
        [ObjectManager showErrorHUDWithStatus:Sysmsg duration:3.0f inView:self];
    }
}
#pragma mark - 選擇領牌中心
-(void) ChooseLicenseSite:(NSNotification *)notif { //領牌地點與補助地區/類型對應
    NSArray *site = (NSArray *)[notif object];
    
    if ([site count] == 0) { //防呆
        [_ButtonLicensePlace setTitle:@"  " forState:UIControlStateNormal];
        
        
        //Delegate
        [self SubsidyLicenseViewSite:_ButtonLicensePlace.titleLabel.text];
    }
    
    [arrayLicensingSite removeAllObjects];
    NSArray *data = site;
    [arrayLicensingSite addObjectsFromArray:data];
    
    //有資料的話預設帶第一個
    [self SetDefaultSite];
}
-(void) ShowLicenseSiteSelect { //領牌中心
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇領牌中心" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayLicensingSite count]; i++) {
        NSString *title = nil;
        id object = [arrayLicensingSite objectAtIndex:i];
        if ([object isKindOfClass:[NSDictionary class]]) {
            NSDictionary *_dic = (NSDictionary *)object;
            title = [_dic objectForKey:@"Name"];
        }
        else {
            title = (NSString *)object;
        }
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonLicensePlace;
    popPresenter.sourceRect = _ButtonLicensePlace.bounds;
    
    [_sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) SetDefaultSite {
    //有資料的話預設帶第一個
    if ([arrayLicensingSite count] > 0) {
        selectedIndex = 0;
        
        NSString *site = nil;
        id object = [arrayLicensingSite objectAtIndex:selectedIndex];
        if ([object isKindOfClass:[NSDictionary class]]) {
            NSDictionary *_dic = (NSDictionary *)object;
            site = [_dic objectForKey:@"Name"];
        }
        else {
            site = (NSString *)object;
        }

        [_ButtonLicensePlace setTitle:site forState:UIControlStateNormal];
        [_ButtonLicensePlace setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
        
        //Delegate
        [self SubsidyLicenseViewSite:site];
    }
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    selectedIndex = buttonIndex;
    
    NSString *site = nil;
    id object = [arrayLicensingSite objectAtIndex:selectedIndex];
    if ([object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *_dic = (NSDictionary *)object;
        site = [_dic objectForKey:@"Name"];
    }
    else {
        site = (NSString *)object;
    }

    [_ButtonLicensePlace setTitle:site forState:UIControlStateNormal];
    [_ButtonLicensePlace setTitleColor:TEXT_COLOR_DIDSELECTED forState:UIControlStateNormal];
    
    //Delegate
    [self SubsidyLicenseViewSite:site];
}
#pragma mark - 雜項
-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_ChooseLicenseSite" object:nil];
}

- (void) GetOrderDetailWithOrderNo:(NSString *)orderNo {    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:orderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}

@end
