//

#import "ExternalWebVC.h"

@interface ExternalWebVC () {
}
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *act;
@property (nonatomic,weak) IBOutlet UIWebView *WebView;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@end

@implementation ExternalWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _LabelTitle.text = _attachTypeName;
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *urlString = nil;
    
    //新增"新購補助資格查詢"，文件ID=56，「查詢」連結網址：https://mobile.epa.gov.tw/LowPoll/qualification.aspx?Menu=MenuSearch
    if (_attachTypeId == 56) {
        urlString  = @"https://mobile.epa.gov.tw/LowPoll/qualification.aspx?Menu=MenuSearch";
    }
    else {
        urlString = @"http://mobile.epa.gov.tw/lowpoll/TwoStrokeNewReNewSearch.aspx?Menu=MenuSearch&Menu2=NewSearch";
    }
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    [_WebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [_act startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_act stopAnimating];
}
#pragma mark - Button Action
-(IBAction)pressFinishBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^(void) {
        //[_sourceVC GotoNextFunctionAfterQRCode];
    }];
}

#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
