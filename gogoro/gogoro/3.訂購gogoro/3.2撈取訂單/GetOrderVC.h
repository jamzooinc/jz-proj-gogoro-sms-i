//
//  GetOrderVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetOrderVC : TemplateVC

@property (weak, nonatomic) IBOutlet UITextField *orderNoTextField;
@property BOOL didFromSupplement; //YES:從補單上傳過來
@end
