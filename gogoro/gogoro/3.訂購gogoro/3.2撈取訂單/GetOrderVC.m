//
//  GetOrderVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "GetOrderVC.h"
#import "GetOrderDetailVC.h"
#import "SupplementOptionVC.h"

@interface GetOrderVC () {
}
@property (nonatomic,weak) IBOutlet UIView *ViewSearch;
@end

@implementation GetOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (_didFromSupplement) { //#4補件上傳
        [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
        [self LeftButtonBack]; //左側按鈕變成返回
    }
    else { //#3訂購
        [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
    }
    
#if TARGET_IPHONE_SIMULATOR
    _orderNoTextField.text = @"P1192"; //測試單號 S1511001317 P1039 P1079 P0200
#endif
    
    /*
     P1000
     P1030
     P1039
     P1050
     P1064
     P1071
     P1072
     P1073
     P1074
     P1075
     P1076
     P1079
     P1081
     P1082
     P1083
     P1085
     P1704
     P1804
     P1964
     P2000
     P2938
     P3016
     P3437
     P4660
     P4867
     P5207
     P6086
     S1511001317
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction
- (IBAction)submitButtonClick:(id)sender {
    if ([_orderNoTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請輸入訂單編號"];
    }
    else {
        [_orderNoTextField resignFirstResponder];
        
        if (_didFromSupplement) { //#4補件上傳
            [self GetOrderSingleAttach];
        }
        else { //#3訂購
            [self GetOrderDetail];
        }
    }
}
#pragma mark - 顯示UIAlertController
- (void)showAlertControllerWithMessage:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alertController addAction:submitAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - Call API
#pragma mark [301] 取得訂單
- (void) GetOrderDetail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_orderNoTextField.text forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void)callBackGetOrderDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        [self GotoGetOrderDetailVCWithData:Data];
    }
    //2016.06.20 取消，由後台判斷
    /*else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }*/
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [401] 補件上傳_單一訂單編號查詢
- (void) GetOrderSingleAttach {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_orderNoTextField.text forKey:@"OrderNo"];
    [GatewayManager callSingleOrder:parameter Source:self];
}
- (void)callBackSingleOrder:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        if ([Data count] == 0) { //判斷有沒有資料
            [self showAlertControllerWithMessage:@"查無訂單或不需要補件"];
        }
        else {
            [self GotoSupplementOptionVCWithData:Data];

        }
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoGetOrderDetailVCWithData:(NSDictionary *)data { //訂單詳細
    GetOrderDetailVC *destVC = [[GetOrderDetailVC alloc] initWithNibName:@"GetOrderDetailVC" bundle:nil];
    destVC.dicData = data;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoSupplementOptionVCWithData:(NSArray *)data { //補件選項
    SupplementOptionVC *destVC = [[SupplementOptionVC alloc] initWithNibName:@"SupplementOptionVC" bundle:nil];
    destVC.OrderNo = _orderNoTextField.text;
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
@end
