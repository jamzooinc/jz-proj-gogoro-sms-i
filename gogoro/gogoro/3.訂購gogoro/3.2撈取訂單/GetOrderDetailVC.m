//
//  GetOrderVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#define UI_GAP 3

#import "GetOrderDetailVC.h"
#import "SelectSubsidyVC.h"
#import "ConsentVC.h"

@interface GetOrderDetailVC ()
{
    NSDictionary *dicOrderDetail,*dicCustomerDetail,*dicDriverDetail;
    BOOL hasDriver; //判斷有沒有駕駛資料
    BOOL hasPrice; //判斷有沒有價格資料
    BOOL didSetupUI;
}

@property (nonatomic,weak) IBOutlet UIView *ViewInfo;
@property (nonatomic,weak) IBOutlet UIView *ViewInfoUser;   //包含車主&駕駛
@property (nonatomic,weak) IBOutlet UIView *ViewInfoOrder;  //包含訂單&價格
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@property (nonatomic,weak) IBOutlet UIView *ViewPurchaser;  //購買人
@property (nonatomic,weak) IBOutlet UIView *ViewOwner;      //車主
@property (nonatomic,weak) IBOutlet UIView *ViewDriver;     //駕駛
@property (nonatomic,weak) IBOutlet UIView *ViewOrder;      //訂單資訊
@property (nonatomic,weak) IBOutlet UIView *ViewPrice;      //價格相關
@property (nonatomic,weak) IBOutlet UIButton *ButtonOrderDetail;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOwnerDetail;
@property (nonatomic,weak) IBOutlet UILabel *LabelOwnerName;
@property (nonatomic,weak) IBOutlet UILabel *LabelOwnerBirthday;
@property (nonatomic,weak) IBOutlet UILabel *LabelOwnerAddress;
@property (nonatomic,weak) IBOutlet UILabel *LabelOwnerPhone;
@property (nonatomic,weak) IBOutlet UILabel *LabelOwnerEmail;
@property (nonatomic,weak) IBOutlet UILabel *LabelDriverName;
@property (nonatomic,weak) IBOutlet UILabel *LabelDriverBirthday;
@property (nonatomic,weak) IBOutlet UILabel *LabelDriverAddress;
@property (nonatomic,weak) IBOutlet UILabel *LabelDriverPhone;
@property (nonatomic,weak) IBOutlet UILabel *LabelDriverEmail;
@property (nonatomic,weak) IBOutlet UILabel *LabelOrderID;
@property (nonatomic,weak) IBOutlet UILabel *LabelOrderSales;
@property (nonatomic,weak) IBOutlet UILabel *LabelOrderLoginSales;
@property (nonatomic,weak) IBOutlet UILabel *LabelOrderSpec;
@property (nonatomic,weak) IBOutlet UILabel *LabelOrderColor;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceCar;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceOption;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceDiscount;
@property (nonatomic,weak) IBOutlet UILabel *LabelPricePayInDiscount;
@property (nonatomic,weak) IBOutlet UILabel *LabelPricePayment;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceFee;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceLoanCount;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceDownPayment;
@property (nonatomic,weak) IBOutlet UILabel *LabelPriceLoanAmount;
//Add in 2.0 vwesion by Justin in 2016/9/14 =====
@property (nonatomic,weak) IBOutlet UILabel *LabelPurchaserName;
@property (nonatomic,weak) IBOutlet UILabel *LabelPurchaserIDNumber;
//===================

@end

@implementation GetOrderDetailVC
-(void) viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    [self SetupData];
    [self UpdateUI_Data]; //以資料更新介面
}
-(void) SetupData {
    dicOrderDetail = nil;
    dicCustomerDetail = nil;
    dicDriverDetail = nil;
    
    @try {
        dicOrderDetail = [NSDictionary dictionaryWithDictionary:[_dicData objectForKey:@"OrderDetail"]];
        dicCustomerDetail = [NSDictionary dictionaryWithDictionary:[_dicData objectForKey:@"CustomerDetail"]];
        dicDriverDetail = [NSDictionary dictionaryWithDictionary:[_dicData objectForKey:@"DriverDetail"]];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

#pragma mark - 顯示UIAlertController
-(void)showAlertControllerWithMessage:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alertController addAction:submitAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UI調整
-(void) ResetUI {
    [self ResetUIWithView:_ViewPurchaser];
    [self ResetUIWithView:_ViewPurchaser];
    [self ResetUIWithView:_ViewDriver];
    [self ResetUIWithView:_ViewOwner];
    [self ResetUIWithView:_ViewOrder];
    [self ResetUIWithView:_ViewPrice];
    
    //[_ViewDriver removeFromSuperview];
    //[_ViewOwner removeFromSuperview];
    //[_ViewOrder removeFromSuperview];
    //[_ViewPrice removeFromSuperview];
}
-(void) ResetUIWithView:(UIView *)myView { //清空裡面的文字
    for (id _subView in myView.subviews) {
        if ([_subView isKindOfClass:[UILabel class]]) {
            UILabel *Label = (UILabel *)_subView;
            if (Label.tag > 0) { //表示為值的
                Label.text = @"";
            }
        }
    }
}
-(void) UpdateUI_Data { //以資料更新介面
    //以下為訂單資訊
    //訂單編號
    NSString *OrderNo = [dicOrderDetail objectForKey:@"OrderNo"];
    _LabelOrderID.text = OrderNo;
    
    //銷售員工名稱
    NSString *SalesName = [dicOrderDetail objectForKey:@"SalesName"];
    _LabelOrderSales.text = SalesName;
    
    //登錄業代員工名稱
    NSString *CreatorName = [dicOrderDetail objectForKey:@"CreatorName"];
    _LabelOrderLoginSales.text = CreatorName;
    
    //車型
    NSString *ModelName = [dicOrderDetail objectForKey:@"ModelName"];
    _LabelOrderSpec.text = ModelName;
    
    //車色
    NSString *ScooterColor = [dicOrderDetail objectForKey:@"ScooterColor"];
    _LabelOrderColor.text = ScooterColor;
    
    //判斷有沒有價格資料
    hasPrice = NO;
    
    //車價
    NSInteger Prices = [[dicOrderDetail objectForKey:@"Prices"] integerValue];
    if (Prices > 0) { //有車價
        hasPrice = YES;
        
        _LabelPriceCar.text = [NSString stringWithFormat:@"%ld",Prices];
        
        //百貨選配
        NSInteger Optional = [[dicOrderDetail objectForKey:@"Optional"] integerValue];
        _LabelPriceOption.text = [NSString stringWithFormat:@"%ld",Optional];
        
        //折扣金額
        NSInteger DiscountAmount = [[dicOrderDetail objectForKey:@"DiscountAmount"] integerValue];
        _LabelPriceDiscount.text = [NSString stringWithFormat:@"%ld",DiscountAmount];
        
        //補助內扣款金額
        NSInteger PreDeductMoney = [[dicOrderDetail objectForKey:@"PreDeductMoney"] integerValue];
        _LabelPricePayInDiscount.text = [NSString stringWithFormat:@"%ld",PreDeductMoney];
        
        //顧客應付金額
        NSInteger ReceivablesAmount = [[dicOrderDetail objectForKey:@"ReceivablesAmount"] integerValue];
        _LabelPricePayment.text = [NSString stringWithFormat:@"%ld",ReceivablesAmount];
        
        //代辦費
        NSInteger AgencyFees = [[dicOrderDetail objectForKey:@"AgencyFees"] integerValue];
        _LabelPriceFee.text = [NSString stringWithFormat:@"%ld",AgencyFees];
        
        //貸款期數
        NSInteger CreditPeriod = [[dicOrderDetail objectForKey:@"CreditPeriod"] integerValue];
        _LabelPriceLoanCount.text = [NSString stringWithFormat:@"%ld",CreditPeriod];
        
        //頭期款
        NSInteger Downpayment = [[dicOrderDetail objectForKey:@"Downpayment"] integerValue];
        _LabelPriceDownPayment.text = [NSString stringWithFormat:@"%ld",Downpayment];
        
        //貸款金額
        NSInteger CreditAmount = [[dicOrderDetail objectForKey:@"CreditAmount"] integerValue];
        _LabelPriceLoanAmount.text = [NSString stringWithFormat:@"%ld",CreditAmount];
    }
    
    //以下為車主資料
    //購買人
    //NSString *PurchaserName = [dicCustomerDetail objectForKey:@"BuyName"];
    NSString *PurchaserName = [dicOrderDetail objectForKey:@"BuyName"];
    _LabelPurchaserName.text = PurchaserName;
    
    //購買ID/統編
    //NSString *PurchaserIDNumber = [dicCustomerDetail objectForKey:@"OwnerProfileCode"];
    NSString *PurchaserIDNumber = [dicOrderDetail objectForKey:@"CustomerProfileCode"];
    _LabelPurchaserIDNumber.text = PurchaserIDNumber; //OwnerProfileCode

    //車主
    NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    _LabelOwnerName.text = CustomerName;
    
    //車主生日
    NSString *CustomerBirthday = [dicCustomerDetail objectForKey:@"CustomerBirthday"];
    _LabelOwnerBirthday.text = CustomerBirthday;
    
    //車主聯絡地址
    NSString *ContactAddress = [dicCustomerDetail objectForKey:@"ContactAddress"];
    _LabelOwnerAddress.text = ContactAddress;
    
    //車主行動電話
    NSString *OwnerPhone = [dicCustomerDetail objectForKey:@"OwnerPhone"];
    _LabelOwnerPhone.text = OwnerPhone;
    
    //車主Email
    NSString *OwnerEmail = [dicCustomerDetail objectForKey:@"OwnerEmail"];
    _LabelOwnerEmail.text = OwnerEmail;
    
    //判斷有沒有駕駛資料
    hasDriver = NO;
    //駕駛
    NSString *DriverName = [dicDriverDetail objectForKey:@"DriverName"];
    if ([DriverName length] > 0) {
        hasDriver = YES;
        
        //駕駛
        _LabelDriverName.text = DriverName;
        
        //駕駛生日
        NSString *DriverBirthday = [dicDriverDetail objectForKey:@"DriverBirthday"];
        _LabelDriverBirthday.text = DriverBirthday;
        
        //駕駛聯絡地址
        NSString *ContactAddress = [dicDriverDetail objectForKey:@"ContactAddress"];
        _LabelDriverAddress.text = ContactAddress;
        
        //駕駛行動電話
        NSString *DriverPhone = [dicDriverDetail objectForKey:@"DriverPhone"];
        _LabelDriverPhone.text = DriverPhone;
        
        //駕駛Email
        NSString *DriverEmail = [dicDriverDetail objectForKey:@"DriverEmail"];
        _LabelDriverEmail.text = DriverEmail;
    }
    [self UpdateUI_Info];
}
-(void) UpdateUI_Info { //更新車主資訊/訂單資訊
    //車主資訊
    //購買人
    _ViewPurchaser.frame = CGRectMake(0, 0, 582, 128);
    if (!didSetupUI) {
        [_ViewInfoUser addSubview:_ViewPurchaser];
    }
    
    //車主
    _ViewOwner.frame = CGRectMake(0, 0, 582, 272);
    if (!didSetupUI) {
        [_ViewInfoUser addSubview:_ViewOwner];
    }
    
    //駕駛
    _ViewDriver.frame = CGRectMake(0, 0, 582, 272);
    if (!didSetupUI) {
        [_ViewInfoUser addSubview:_ViewDriver];
    }
    
    //如果沒有駕駛資料，則隱藏並調整ＵＩ
    if (hasDriver) {
        //_ViewPurchaser.center = CGPointMake(ScreenWidth/2, 82+97+60+_ViewOwner.frame.size.height/2);
        _ViewPurchaser.center = CGPointMake(ScreenWidth/2, 82+97+60+ _ViewPurchaser.frame.size.height/2);
        _ViewOwner.center = CGPointMake(ScreenWidth/2, _ViewPurchaser.frame.origin.y + _ViewPurchaser.frame.size.height + UI_GAP + _ViewOwner.frame.size.height/2);
        _ViewDriver.center = CGPointMake(ScreenWidth/2, _ViewOwner.frame.origin.y + _ViewOwner.frame.size.height + UI_GAP + _ViewDriver.frame.size.height/2);
        _ViewDriver.hidden = NO;
    }
    else {
        //調整UI位置
        _ViewPurchaser.center = CGPointMake(ScreenWidth/2, 82+97+60+_ViewPurchaser.frame.size.height/2);
        _ViewOwner.center = CGPointMake(ScreenWidth/2, _ViewPurchaser.frame.origin.y + _ViewPurchaser.frame.size.height + UI_GAP + _ViewOwner.frame.size.height/2);
        _ViewDriver.hidden = YES;
    }
    
    //訂單資訊
    //訂單
    _ViewOrder.frame = CGRectMake(0, 0, 582, 229);
    if (!didSetupUI) {
        [_ViewInfoOrder addSubview:_ViewOrder];
    }
   
    //價格
    _ViewPrice.frame = CGRectMake(0, 0, 582, 423);
    
    if (!didSetupUI) {
        [_ViewInfoOrder addSubview:_ViewPrice];
    }
    
    //如果沒有價格資料，則隱藏並調整ＵＩ
    if (hasPrice) {
        _ViewOrder.center = CGPointMake(ScreenWidth/2, 82+97+60+_ViewOrder.frame.size.height/2);
        _ViewPrice.center = CGPointMake(ScreenWidth/2, _ViewOrder.frame.origin.y + _ViewOrder.frame.size.height + UI_GAP + _ViewPrice.frame.size.height/2);
        
        _ViewPrice.hidden = NO;
    }
    else {
        //調整UI位置
        _ViewOrder.center = CGPointMake(ScreenWidth/2, ScreenHeight/2);
        _ViewPrice.hidden = YES;
    }
    
    [self pressOrderDetailBtn];
    didSetupUI = YES;
}

-(IBAction) pressOrderDetailBtn { //顯示訂單內容
    [_ButtonOrderDetail setBackgroundImage:[UIImage imageNamed:@"btn_orderform_on.png"] forState:UIControlStateNormal];
    [_ButtonOwnerDetail setBackgroundImage:[UIImage imageNamed:@"btn_ownerinfo_off.png"] forState:UIControlStateNormal];
    _ViewInfoUser.hidden = YES;
    _ViewInfoOrder.hidden = NO;
    
    //調整UI位置
    if (hasPrice) {
        _ViewCommand.center = CGPointMake(ScreenWidth/2, _ViewPrice.frame.origin.y + _ViewPrice.frame.size.height + UI_GAP + _ViewCommand.frame.size.height/2);
    }
    else {
        _ViewCommand.center = CGPointMake(ScreenWidth/2, _ViewOrder.frame.origin.y + _ViewOrder.frame.size.height + UI_GAP + _ViewCommand.frame.size.height/2);
    }
}
-(IBAction) pressOwnerDetailBtn { //顯示車主內容
    [_ButtonOrderDetail setBackgroundImage:[UIImage imageNamed:@"btn_orderform_off.png"] forState:UIControlStateNormal];
    [_ButtonOwnerDetail setBackgroundImage:[UIImage imageNamed:@"btn_ownerinfo_on.png"] forState:UIControlStateNormal];
    _ViewInfoUser.hidden = NO;
    _ViewInfoOrder.hidden = YES;
    
    //調整UI位置
    if (hasDriver) {
        _ViewCommand.center = CGPointMake(ScreenWidth/2, _ViewDriver.frame.origin.y + _ViewDriver.frame.size.height + UI_GAP + _ViewCommand.frame.size.height/2);
    }
    else {
        _ViewCommand.center = CGPointMake(ScreenWidth/2, _ViewOwner.frame.origin.y + _ViewOwner.frame.size.height + UI_GAP + _ViewCommand.frame.size.height/2);
    }
}

#pragma mark - Button Action
-(IBAction) pressPreviousStepBtn { //上一步
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction) pressRefreshOrderBtn { //重取帳單資訊
    if (!dicOrderDetail) { //防呆
        return;
    }
    
    [self GetOrderDetail];
}
-(IBAction) pressNextStepBtn { //下一步
    if (!dicOrderDetail) { //防呆
        return;
    }
    
    //補助資料
    NSDictionary *dicSubsidyData = nil;
    
    @try {
        dicSubsidyData = [NSDictionary dictionaryWithDictionary:[_dicData objectForKey:@"OrderData"]];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    if (_didFromSupplement) { //從補單來
        //到補助選擇
        SelectSubsidyVC *destVC = [[SelectSubsidyVC alloc] initWithNibName:@"SelectSubsidyVC" bundle:nil];
        destVC.stringOrderNo = [dicOrderDetail objectForKey:@"OrderNo"];
        destVC.CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
        destVC.didFromSupplement = YES;
        destVC.dicSubsidyData = dicSubsidyData;
        destVC.dicOrderDetail = [_dicData objectForKey:@"OrderDetail"];
        [self.navigationController pushViewController:destVC animated:YES];
    }
    else if (!_didFromSubsidy) { //從訂單撈取來
        //到補助選擇
        SelectSubsidyVC *destVC = [[SelectSubsidyVC alloc] initWithNibName:@"SelectSubsidyVC" bundle:nil];
        destVC.stringOrderNo = [dicOrderDetail objectForKey:@"OrderNo"];
        destVC.CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
        destVC.dicSubsidyData = dicSubsidyData;
        destVC.dicOrderDetail = [_dicData objectForKey:@"OrderDetail"];
        [self.navigationController pushViewController:destVC animated:YES];
    }
    else { //從補助來，到過場文件
        ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
        destVC.enumCONSENT = CONSENT_TRANSITION_DOC;
        destVC.OrderNo = [dicOrderDetail objectForKey:@"OrderNo"];
        destVC.CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
        destVC.dicSubsidy = _dicSubsidy;
        [self.navigationController pushViewController:destVC animated:YES];
    }
}

#pragma mark - Call API
#pragma mark [301] 取得訂單
- (void) GetOrderDetail {
    [self ResetUI];
    
    [ObjectManager showLodingHUDinView:self.view];

    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:[dicOrderDetail objectForKey:@"OrderNo"] forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void) callBackGetOrderDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //Reload data and UI
        _dicData = nil;
        _dicData = [[NSDictionary alloc] initWithDictionary:Data];
        [self SetupData];
        
        //以資料更新介面
        [self UpdateUI_Data];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}

#pragma mark - 雜項
- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
