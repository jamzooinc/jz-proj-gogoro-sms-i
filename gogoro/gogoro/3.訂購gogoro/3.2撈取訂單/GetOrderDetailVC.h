//
//  GetOrderVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GetOrderDetailVC : TemplateVC

@property (nonatomic,strong) NSDictionary *dicData;
@property BOOL didFromSubsidy; //是不是選擇補助後過來的
@property BOOL didFromSupplement; //是不是選擇補單後過來的
@property (nonatomic,strong) NSMutableDictionary *dicSubsidy; //傳給後台的補助選擇資料
@end
