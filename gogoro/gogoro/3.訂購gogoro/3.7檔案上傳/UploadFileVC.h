//
//  UploadFileVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

static NSString *const kScopes = @"https://www.googleapis.com/auth/drive.file";
static NSString *const kKeychainItemName = @"Drive API";
static NSString *const kClientSecret = nil; //3khRO33vwYeWN2ASShpxgovN

#if Jamzoo
static NSString *const kClientID = @"790156036718-r86vvv8j6riat046tdlvrtkpckgmmmdk.apps.googleusercontent.com";
#else
static NSString *const kClientID = @"329424597000-40pur3qj7f4t4p9f3m9loi2cfn9us3e4.apps.googleusercontent.com";
#endif


#import <UIKit/UIKit.h>
#import "MultiPhotographVC.h"
#import "TOCropViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTLDrive.h"
#import "GoogleDriveSelectVC.h"
#import <Google/SignIn.h>
#import <GoogleSignIn/GoogleSignIn.h>

@interface UploadFileVC : TemplateVC <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate,TOCropViewControllerDelegate,GIDSignInDelegate, GIDSignInUIDelegate>
@property (nonatomic, strong) GTLServiceDrive *service;
@property (nonatomic,strong) NSString *OrderNo;
@property (nonatomic,strong) NSString *attachName; //附件名稱
@property (nonatomic,strong) NSString *memo; //備註
//@property (nonatomic,strong) NSDictionary *dicData;
@property NSInteger type; //附件類型
@property BOOL didFromSupplement; //YES:從補單上傳過來
@property BOOL didFromBatchUpload; //YES:從4.3單一文件批次上傳來
@property BOOL didFromLackReturn; //YES:從5.2 5.3 5.4 缺件退回來
@property BOOL didFromTransfer; //YES:從過戶過來
@property (nonatomic,strong) NSDictionary *dicTransfer;
@property (nonatomic,strong) NSMutableDictionary *documentInfo;
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
@property BOOL didFromCustomer; //YES:從1.0自訂文件或2.0預約試乘過來
@property (nonatomic,strong) NSString *customerID; //從2.2試乘拍駕照過來會帶
@property NSInteger attachTypeId; //附件類型（首次參觀:101, 預約試乘: 102）

@end
