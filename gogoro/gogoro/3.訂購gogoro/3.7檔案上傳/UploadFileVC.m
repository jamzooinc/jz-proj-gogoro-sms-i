//
//  UploadFileVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "UploadFileVC.h"
#import "UIImage+fixOrientation.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

@interface UploadFileVC () {
    UIImagePickerController *imagePicker;
    BOOL didTakePhoto; //是否已拍照或選相片
    
    //當上傳資料為
    //1.身分證
    //2.貸款人身份證
    //3.保人身份證
    //上傳功能須強制執行正反面兩頁的呈現
    BOOL shouldTakeMultiPhoto;
    BOOL isDriverLicense; //YES:駕照
    BOOL isVehicleLicense; //YES:行照
    BOOL shouldWatermark; //YES:要加上浮水印
    BOOL shouldMask; //YES:拍照時要使用遮罩
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UITextView *TextViewContent;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIImageView *ImageResult;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSend;
@property (nonatomic,weak) IBOutlet UIView *ViewOverlay;
@property (nonatomic,weak) IBOutlet UILabel *LabelOverlayTitle;
@property (nonatomic,weak) IBOutlet UIView *ViewInfo;
@property (nonatomic,weak) IBOutlet UIView *ViewTransferInfo; //過戶資料
@property (weak, nonatomic) IBOutlet UILabel *LabelOrderStoreName;
@property (weak, nonatomic) IBOutlet UILabel *LableVIN;
@property (weak, nonatomic) IBOutlet UILabel *LabelLicensePlate;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderNo;
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerName;

@property (nonatomic,weak) IBOutlet UILabel *LabelStoreTransfer;
@property (nonatomic,weak) IBOutlet UILabel *LabelPersonTransfer;
@property (nonatomic,weak) IBOutlet UILabel *LabelVinTransfer;
@property (nonatomic,weak) IBOutlet UILabel *LabelCarNumberTransfer;
@property (nonatomic,weak) IBOutlet UILabel *LabelEmailTransfer;

@end

@implementation UploadFileVC
@synthesize service = _service;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.service = [[GTLServiceDrive alloc] init];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
    [[GIDSignIn sharedInstance] signInSilently];
    
    [self AddNotification];
    
    if (_didFromSupplement) {
        [self setupNavigationItemTitle:@"補件上傳" Image:nil];
        [_ButtonSend setBackgroundImage:[UIImage imageNamed:@"btn_send.png"] forState:UIControlStateNormal];
    }
    else {
        [self setupNavigationItemTitle:@"檔案上傳" Image:nil];
    }
    [self LeftButtonBack]; //左側按鈕變成返回
    
    _LabelTitle.text = _attachName;
    
    //取得訂單詳細資料
    //[self GetOrderDetail];
    if (_didFromTransfer) {
        _ViewInfo.hidden = YES;
        _ViewTransferInfo.hidden = NO;
        
        //服務門市
        _LabelStoreTransfer.text = [_dicTransfer objectForKey:@"StoreName"];
        
        //服務人員
        _LabelPersonTransfer.text = [_dicTransfer objectForKey:@"SalesAccountName"];
        
        //車牌號碼
        _LabelCarNumberTransfer.text = [_dicTransfer objectForKey:@"Plate"];
        
        //VIN
        _LabelVinTransfer.text = [_dicTransfer objectForKey:@"VinNo"];
        
        //新車主Email
        NSDictionary *NewOwner = [_dicTransfer objectForKey:@"NewOwner"];
        if (NewOwner && ![NewOwner isKindOfClass:[NSNull class]]) {
            _LabelEmailTransfer.text = [NewOwner objectForKey:@"Email"];
        }
        else {
            _LabelEmailTransfer.text = [_dicTransfer objectForKey:@"NewOwnerEmail"];
        }
        
        //備註
        _TextViewContent.text = _memo;
    }
    else if (_didFromCustomer) {
        
        
    
        
        //姓名
        _LabelOrderNo.text = [NSString stringWithFormat:@"姓名：%@",[_documentInfo objectForKey:@"Name"]];
        _LabelOrderNo.textColor = [UIColor blackColor];
        
        //Email
        _LabelOwnerName.text = [NSString stringWithFormat:@"Email：%@",[_documentInfo objectForKey:@"Email"]];
        
        //電話
        _LabelOrderStoreName.text = [NSString stringWithFormat:@"電話：%@",[_documentInfo objectForKey:@"Phone"]];
        
        //服務門市
        _LableVIN.text = [NSString stringWithFormat:@"服務門市：%@",[_documentInfo objectForKey:@"Store"]];
        
        //服務人員
        _LabelLicensePlate.text = [NSString stringWithFormat:@"服務人員：%@",[_documentInfo objectForKey:@"Person"]];
        
        //備註
        _TextViewContent.text = _memo;
    }
    else {
        [self GetTrackingOrderList];
    }
    
    
    //測試
    //_type = 150; //31 34 150
    //_attachName = @"行照"; //行照 車身照片+掛牌照(非台北) 駕照
    
    //須強制執行正反面兩頁的呈現
    //3 車主(購買人)ID+影本1份
    //17 貸款人ID+影本1份
    //18 保人ID+影本1份
    //31 行照
    //34 車身照片+掛牌照(非台北)
    //51 不同人汰購車主ＩＤ+影本
    //150 駕照
    //1001 新車主身分證
    //1002 新車主行照
    if (_type == 3 ||
        _type == 17 ||
        _type == 18 ||
        _type == 31 ||
        //_type == 34 || //2016.12.21 取消
        _type == 51 ||
        _type == 1001 ||
        _type == 1002) {
        shouldTakeMultiPhoto = YES;
    }
    
    //判斷是不是駕照
    if (_type == 150) {
        isDriverLicense = YES;
    }
    
    //判斷是不是行照（因為遮罩的圖檔不一樣）
    if (_type == 31 ||
        _type == 1002) {
        isVehicleLicense = YES;
    }
    
    //判斷要不要加上浮水印，拍照時要加上遮罩
    if (_type == 3 ||
        _type == 17 ||
        _type == 18 ||
        _type == 31 ||
        _type == 150 ||
        _type == 51 ||
        _type == 1001 ||
        _type == 1002) {
        shouldMask = YES;
        shouldWatermark = YES;
        _LabelOverlayTitle.text = _attachName;
    }
    
    //模擬器不能拍照，所以要把照片放到doc目錄下進行測試
#if TARGET_IPHONE_SIMULATOR
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempImage"]];
    NSLog(@"圖檔放置路徑:%@",imagePath);
#else
    [self performSelector:@selector(initImagePicker) withObject:nil afterDelay:0.5f];
#endif
}
#pragma mark - 相機
-(void) initImagePicker {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { //沒相機
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無法開啟相機！" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
}




- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //測試
#if TARGET_IPHONE_SIMULATOR
    //載入暫存（如果有的話）
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempImage"]];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    NSLog(@"original image:%@",NSStringFromCGSize(image.size));
    
    _ImageResult.image = image;
    
    //存到暫存位置
    /*imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    //可上傳
    didTakePhoto = YES;*/
    
    //呈現影像編輯VC
    [self presentCropViewController];
#else
    //NSString *url = [info objectForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"url:%@",url);
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]){
        //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //NSString *documentsDirectory = [paths objectAtIndex:0];
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        /*UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        if (imagePicker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) { //相簿只能用原圖
            image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }*/
        
        NSLog(@"original image:%@",NSStringFromCGSize(image.size));
        
        /*NSLog(@"ori:%ld",image.imageOrientation);
        image = [image rotate:image Orientation:UIImageOrientationLeft];
        
        NSLog(@"original image:%@",NSStringFromCGSize(image.size));
        NSLog(@"ori:%ld",image.imageOrientation);*/
        
        _ImageResult.image = image;
        
        //存到暫存位置
        /*NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
        NSData* data = UIImageJPEGRepresentation(image, 1.0f);
        BOOL result = [data writeToFile:imagePath atomically:YES];
        NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
        
        //可上傳
        didTakePhoto = YES;*/
    }
    else if ([mediaType isEqualToString:@"public.movie"]){
        NSLog(@"found a video");
        //NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        //NSData *webData = [NSData dataWithContentsOfURL:videoURL];
    }
    
    [picker dismissViewControllerAnimated:NO completion:^{
        if (shouldMask && imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera) { //遮罩拍照後，處理image
            UIImageOrientation orientation = _ImageResult.image.imageOrientation;
            //CGFloat totalWidth = 768;
            switch (orientation) {
                case UIImageOrientationUp:
                    _ImageResult.image = [self imageRotatedByDegrees:90 Image:_ImageResult.image];
                    break;
                case UIImageOrientationDown:
                    _ImageResult.image = [self imageRotatedByDegrees:90 Image:_ImageResult.image];
                    break;
                case UIImageOrientationLeft:
                    break;
                case UIImageOrientationRight:
                    break;
                default:
                    break;
            }
            
            [self ProcessPhotoWithImage:_ImageResult.image];
        }
        else { //呈現影像編輯VC
            [self presentCropViewController];
        }
    }];
#endif
}
-(void) ProcessPhotoWithImage:(UIImage *)image {
    //計算要抓取的位置（有用遮罩的話，要裁圖片的框框）
    CGFloat ratio =  image.size.width / 768; //因為原本rect是用寬768去算的
    //ratio = 1.0;
    NSLog(@"ratio:%f",ratio);
    
    CGRect rect = CGRectMake(163.0/2 * ratio,440.0/2 * ratio+50,1208.0/2 * ratio,855.0/2 * ratio);
    NSLog(@"要抓取的位置:%@",NSStringFromCGRect(rect));
    
    //判斷要不要加入浮水印
    UIImage *imageToUse = image;
    if (shouldWatermark) {
        UIImageView *ImageTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        ImageTemp.image = image;
        
        if (isDriverLicense) { //駕照
            CGFloat watermarkRatio = image.size.width/378.0 * 0.75 * 0.5;
            CGFloat watermarkWidth = 378 * watermarkRatio;
            CGFloat watermarkHeight = 38 * watermarkRatio;
            //浮水印放在左下角
            UIImageView *ImageWatermark = [[UIImageView alloc] initWithFrame:CGRectMake(0, rect.origin.y+rect.size.height-watermarkHeight, watermarkWidth, watermarkHeight)];
            ImageWatermark.image = [UIImage imageNamed:@"text_gogoro_testdrive_watermark.png"];
            [ImageTemp addSubview:ImageWatermark];
        }
        else {
            CGFloat watermarkRatio = image.size.width/300.0 * 0.75 * 0.5;
            CGFloat watermarkWidth = 300 * watermarkRatio;
            CGFloat watermarkHeight = 38 * watermarkRatio;
            //浮水印放在左下角
            UIImageView *ImageWatermark = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-watermarkHeight, watermarkWidth, watermarkHeight)];
            ImageWatermark.image = [UIImage imageNamed:@"text_gogoro_watermark.png"];
            //ImageWatermark.contentMode = UIViewContentModeScaleAspectFit;
            [ImageTemp addSubview:ImageWatermark];
        }
        
        UIImage *sourceImage = [self imageWithView:ImageTemp];
        imageToUse = sourceImage;
    }
    
    _ImageResult.image = imageToUse;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = nil;
    
    //存到暫存位置（驗證浮水印用）
    /*imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense222"]];
     NSData* data1 = UIImageJPEGRepresentation(sourceImage, 1.0f);
     BOOL result1 = [data1 writeToFile:imagePath atomically:YES];
     NSLog(@"%@ %@", (result1? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);*/
    
    UIImage *captureImage = imageToUse;
    if (shouldMask && imagePicker.sourceType == UIImagePickerControllerSourceTypeCamera) { //有用遮罩的話，要裁切到圖片中框框的位置
        captureImage = [self captureView:_ViewOverlay Rect:rect];
        
        //2016.06.13 也要縮圖
        CGSize sizeToBe = CGSizeMake(captureImage.size.width/2 * 0.5, captureImage.size.height/2 * 0.5);
        captureImage = [self ImageWithImage:captureImage scaledToSize:sizeToBe];
        NSLog(@"縮小後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    }
    else { //非裁切類，怕圖片太大所以縮小
        CGSize sizeToBe = CGSizeMake(captureImage.size.width * 0.5, captureImage.size.height * 0.5); //原為 *0.5 2016.11.17改為 0.75
        captureImage = [self ImageWithImage:captureImage scaledToSize:sizeToBe];
        NSLog(@"縮小後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    }
    
    _ImageResult.image = captureImage;
    
    NSLog(@"抓取後照片尺寸:%@",NSStringFromCGSize(captureImage.size));
    
    //存到暫存位置
    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
    NSData* data = UIImageJPEGRepresentation(captureImage, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    //可上傳
    didTakePhoto = YES;
}
- (UIImage*)captureView:(UIView *)yourView Rect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // translated rectangle for drawing sub image
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, _ImageResult.image.size.width,_ImageResult.image.size.height);
    
    // clip to the bounds of the image context
    // not strictly necessary as it will get clipped anyway?
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    
    // draw image
    [_ImageResult.image drawInRect:drawRect];
    
    // grab image
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return croppedImage;
    
    /*//CGRect screen = [[UIScreen mainScreen] bounds];
     UIGraphicsBeginImageContext(rect.size);
     CGContextClipToRect (UIGraphicsGetCurrentContext(),rect);
     CGContextRef context = UIGraphicsGetCurrentContext();
     [yourView.layer renderInContext:context];
     UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return image;*/
}
- (UIImage *) imageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
#pragma mark - 影像裁切VC
- (void)presentCropViewController {
    UIImage *image = _ImageResult.image; //Load an image
    
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    //cropViewController.rotateButtonsHidden = YES;
    [self presentViewController:cropViewController animated:YES completion:nil];
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    //2016.11.17 裁切功能只要有去調整，3thparty工具本身就會縮一次，所以改成用原圖去裁
    //2017.03.26 修改open source library，改為不縮圖
    //UIImage *captureImage = [self captureView:_ImageResult Rect:cropRect];
    [self ProcessPhotoWithImage:image];
    
    [cropViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled {
    if (cancelled) {
        _ImageResult.image = nil;
        
        //可上傳
        didTakePhoto = NO;
    }
    
    [cropViewController dismissViewControllerAnimated:YES completion:nil];
}
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees Image:(UIImage *)sourceImage {
        // calculate the size of the rotated view's containing box for our drawing space
        UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,sourceImage.size.width, sourceImage.size.height)];
        CGAffineTransform t = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
        rotatedViewBox.transform = t;
        CGSize rotatedSize = rotatedViewBox.frame.size;
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize);
        CGContextRef bitmap = UIGraphicsGetCurrentContext();
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
        
        //   // Rotate the image context
        CGContextRotateCTM(bitmap, DEGREES_TO_RADIANS(degrees));
        
        // Now, draw the rotated/scaled image into the context
        CGContextScaleCTM(bitmap, 1.0, -1.0);
        CGContextDrawImage(bitmap, CGRectMake(-sourceImage.size.width / 2, -sourceImage.size.height / 2, sourceImage.size.width, sourceImage.size.height), [sourceImage CGImage]);
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
        
}
#pragma mark - TextView Delegate
-(void) textViewDidBeginEditing:(UITextView *)textView {
    [_ScrollView setContentOffset:CGPointMake(0, ScreenHeight*0.1) animated:YES];
}
-(void) textViewDidEndEditing:(UITextView *)textView {
    [_ScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
#pragma mark - Button Action
-(IBAction) pressSaveBtn {
    if (_didFromTransfer) { //過戶-->不用判斷有沒有照片
        
    }
    else { //其他功能-->判斷是否已有照片
        if (!didTakePhoto) { //是否已拍照或選相片
            return;
        }
    }
    
    
    if (_didFromTransfer) { //過戶移轉
        [self UploadAttachTransfer];
    }
    else if (_didFromCustomer) { //從1.0自訂文件或2.0預約試乘過來
        [self UploadContract];
    }
    else {
        [self UploadAttach];
    }
    
}
- (IBAction)pressDriverUpLoad:(id)sender {
    //2016.11.07
    /*"UAT#58_點擊Google Drive上傳時，先顯示目前登入帳號，並詢問User是否要切換。
    提示訊息：目前登入的帳號是「peter.you@jamzoo.com.tw」，請問是否切換帳號？
    否：繼續原本邏輯
    是：先進行清除local帳號紀錄"*/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userEmail = [defaults objectForKey:@"GoogleDriveUserEmail"];
    
    if (userEmail) {
        NSString *message = [NSString stringWithFormat:@"目前登入的帳號是「%@」，請問是否切換帳號？",userEmail];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"系統通知"
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"是"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //清除Google Drive憑證
                                        [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
                                        
                                        //登出
                                        [[GIDSignIn sharedInstance] signOut];
                                        
                                        //移除紀錄
                                        [defaults removeObjectForKey:@"GoogleDriveUserEmail"];
                                        [defaults synchronize];
                                        
                                        [self UploadByGoogleDrive];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"不，請繼續"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [self UploadByGoogleDrive];
                                   }];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else {
        [self UploadByGoogleDrive];
    }
}
-(IBAction) pressCameraUploadBtn {
    if (shouldTakeMultiPhoto) { //須強制執行正反面兩頁的呈現
        [self GotoMultiPhotographVCWithSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else { //單一頁即可
#if TARGET_IPHONE_SIMULATOR
        [self imagePickerController:nil didFinishPickingMediaWithInfo:nil];
#else
        //imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if (shouldMask) { //使用遮罩
            imagePicker.showsCameraControls = NO;
            imagePicker.cameraOverlayView = _ViewOverlay;
            _ViewOverlay.hidden = NO;
        }
        
        [self presentViewController:imagePicker animated:NO completion:nil];
#endif
    }
}
-(IBAction) pressAlbumUploadBtn {
    if (shouldTakeMultiPhoto) { //須強制執行正反面兩頁的呈現
        [self GotoMultiPhotographVCWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    else { //單一頁即可
#if TARGET_IPHONE_SIMULATOR
        [self imagePickerController:nil didFinishPickingMediaWithInfo:nil];
#else
        //imagePicker.allowsEditing = NO;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePicker animated:NO completion:nil];
#endif
    }
}
#pragma mark - 拍照Overlay 的 Button Action
-(IBAction) pressImageBackBtn:(id)sender {
    [imagePicker dismissViewControllerAnimated:NO completion:^{
    }];
}
- (IBAction) pressImageHomeBtn:(id)sender {
    [imagePicker dismissViewControllerAnimated:NO completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}
- (IBAction) pressTakePictureBtn:(id)sender {
    [imagePicker takePicture];
}
#pragma mark - 4.4特殊上傳（拍多張照片）
-(void) MultiPhotoFinishWithImage:(NSNotification *)notif {
    UIImage *image = (UIImage *)[notif object];
    _ImageResult.image = image;
    
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    if (result) {
        didTakePhoto = YES;
    }
}
#pragma mark - Call API
#pragma mark [501] 追蹤訂單_搜尋列表
- (void) GetTrackingOrderList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    
    [GatewayManager callTrackingOrderList:parameter Source:self];
}
- (void)callBackTrackingOrderList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSArray *Orders = [Data objectForKey:@"Orders"];

        if ([Orders count] == 0) { //表示無資料
        }
        else {
            NSDictionary *Data = [Orders objectAtIndex:0];
            
            //訂單編號
            _LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",_OrderNo];
            
            //車主姓名
            _LabelOwnerName.text = [NSString stringWithFormat:@"車主姓名：%@",[Data objectForKey:@"CustomerName"]];
            
            //訂單門市
            _LabelOrderStoreName.text = [NSString stringWithFormat:@"訂單門市：%@",[Data objectForKey:@"StoreName"]];
            
            //VIN
            _LableVIN.text = [NSString stringWithFormat:@"VIN：%@",[Data objectForKey:@"VinNo"]];
            
            //車牌
            _LabelLicensePlate.text = [NSString stringWithFormat:@"車牌：%@",[Data objectForKey:@"Plate"]];
        }
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        //[self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [301] 取得訂單
- (void) GetOrderDetail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void) callBackGetOrderDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSDictionary *CustomerDetail = [Data objectForKey:@"CustomerDetail"];
        
        //訂單編號
        _LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",_OrderNo];
        
        //車主姓名
        _LabelOwnerName.text = [NSString stringWithFormat:@"車主姓名：%@",[CustomerDetail objectForKey:@"CustomerName"]];
        
        //訂單門市
        _LabelOrderStoreName.text = [NSString stringWithFormat:@"訂單門市：%@",[Data objectForKey:@"StoreName"]];
        
        //VIN
        _LableVIN.text = [NSString stringWithFormat:@"VIN：%@",[Data objectForKey:@"VinNo"]];
        
        //車牌
        _LabelLicensePlate.text = [NSString stringWithFormat:@"車牌：%@",[Data objectForKey:@"Plate"]];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        //[self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        //[self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [311] 上傳訂單附件電子檔
- (void) UploadAttach {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)_type] forKey:@"type"];
    [parameter setObject:_TextViewContent.text forKey:@"remark"];
    [parameter setObject:@"tempUploadFile.jpg" forKey:@"filename"];
    [GatewayManager callUploadAttach:parameter Source:self];
}
- (void)callBackUploadAttach:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSArray *Data = [parameter objectForKey:@"Data"];
        
        //上傳成功，從不同頁面過來時處理的狀況不同
        if (_didFromSupplement) { //從補單上傳，查詢訂單後選擇文件的畫面
            //[self.navigationController popToRootViewControllerAnimated:YES];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if (_didFromBatchUpload) { //從4.3單一文件批次上傳來，回前頁
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if (_didFromLackReturn) { //從從5.2 5.3 5.4 缺件退回來，回前頁
            [self.navigationController popViewControllerAnimated:YES];
        }
        else { //訂購，回前頁
           [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [607] 上傳或更新過戶附件
- (void) UploadAttachTransfer {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)_type] forKey:@"attachType"];
    [parameter setObject:_TextViewContent.text forKey:@"Memo"];
    [parameter setObject:@"tempUploadFile.jpg" forKey:@"filename"];
    [GatewayManager callTransferUpload:parameter Source:self];
}
- (void)callBacTransferUpload:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSArray *Data = [parameter objectForKey:@"Data"];
        
        //上傳成功，回前頁
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [107] 上傳非訂單同意書
- (void) UploadContract {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_customerID forKey:@"customer"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",_type] forKey:@"attachType"];
    //[parameter setObject:_TextViewContent.text forKey:@"remark"];
    [parameter setObject:@"tempUploadFile.jpg" forKey:@"filename"];
    [GatewayManager callContractUploadContract:parameter Source:self];
}
- (void)callBackContractUploadContract:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSArray *Data = [parameter objectForKey:@"Data"];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - Google Drive 相關
-(void) UploadByGoogleDrive {
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    //if (self.fetchEmailToggle.isEnabled) {
        //signIn.shouldFetchBasicProfile = YES;
    //}
    signIn.clientID = kClientID;
    signIn.scopes = @[ @"profile", @"email",kGTLAuthScopeDrive];
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
    [[GIDSignIn sharedInstance] signIn];
    
    return;
    
    self.service = [[GTLServiceDrive alloc] init];
    
    //NSLog(@"kClientID:%@",kClientID);
    
    //判斷是不是已有授權
    GTMOAuth2Authentication *credentials;
    credentials = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:kClientID clientSecret:kClientSecret];
    //NSLog(@"credentials.accessToken:%@",credentials.accessToken);
    
    if (![credentials canAuthorize]) { //沒授權
        GTMOAuth2ViewControllerTouch *authController;
        authController = [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kScopes clientID:kClientID clientSecret:kClientSecret keychainItemName:kKeychainItemName delegate:self finishedSelector:@selector(viewController:finishedWithAuth:error:)];
        [self presentViewController:[self createAuthController] animated:YES completion:nil];
    }
    else { //已有授權，抓取Google Drive檔案列表
        self.service.authorizer = credentials;
        [self GotoGoogleDriveSelectVC];
    }
}
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    //self.statusField.text = @"Signed in user";
    if (error) {
        [self showAlertControllerWithMessage:error.localizedDescription];
    }
    else {
        //紀錄Email
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        
        //GTMOAuth2Authentication *credentials = user.authentication;
        
        NSLog(@"user.authentication.fetcherAuthorizer:%@",user.authentication.fetcherAuthorizer);
        
        self.service.authorizer =  user.authentication.fetcherAuthorizer;
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:email forKey:@"GoogleDriveUserEmail"];
        [defaults synchronize];
        
        //前往功能畫面
        [self GotoGoogleDriveSelectVC];
        //[self dismissViewControllerAnimated:YES completion:^(void) {
            //[self GotoGoogleDriveSelectVC];
        //}];
    }
}
- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    //self.statusField.text = @"Disconnected user";
    [self showAlertControllerWithMessage:error.localizedDescription];
}
// Creates the auth controller for authorizing access to Drive API.
- (GTMOAuth2ViewControllerTouch *)createAuthController {
    GTMOAuth2ViewControllerTouch *authController;
    // If modifying these scopes, delete your previously saved credentials by
    // resetting the iOS simulator or uninstall the app.
    NSArray *scopes = [NSArray arrayWithObjects:kGTLAuthScopeDrive, nil]; //kGTLAuthScopeDriveMetadataReadonly
    authController = [[GTMOAuth2ViewControllerTouch alloc]
                      initWithScope:[scopes componentsJoinedByString:@" "]
                      clientID:kClientID
                      clientSecret:kClientSecret
                      keychainItemName:kKeychainItemName
                      delegate:self
                      finishedSelector:@selector(viewController:finishedWithAuth:error:)];
    return authController;
}
// Handle completion of the authorization process, and update the Drive API
// with the new credentials.
- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)authResult
                 error:(NSError *)error {
    if (error != nil) {
        [self showAlertControllerWithMessage:error.localizedDescription];
        self.service.authorizer = nil;
    }
    else { //取得授權，抓取Google Drive檔案列表
        self.service.authorizer = authResult;
        
        //紀錄Email
        NSLog(@"authResult email:%@",authResult.userEmail);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:authResult.userEmail forKey:@"GoogleDriveUserEmail"];
        [defaults synchronize];
        
        //前往功能畫面
        [self dismissViewControllerAnimated:YES completion:^(void) {
            [self GotoGoogleDriveSelectVC];
            //[self fetchFiles];
        }];
    }
}
- (void)fetchFiles {
    //self.output.text = @"Getting files...";
    /*GTLQueryDrive *query =
     [GTLQueryDrive queryForFilesList];
     query.pageSize = 10;
     query.fields = @"nextPageToken, files(id, name)";
     [self.service executeQuery:query
     delegate:self
     didFinishSelector:@selector(displayResultWithTicket:finishedWithObject:error:)];*/
    
    NSString *parentId = @"root";
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    //query.q = [NSString stringWithFormat:@"'%@' in parents and trashed = false and (mimeType contains 'image/' or mimeType contains 'pdf' or mimeType contains 'folder')", parentId];
    query.q = [NSString stringWithFormat:@"'%@' in parents and trashed = false and (mimeType contains 'folder' or mimeType contains 'image/' or mimeType contains 'pdf')", parentId];
    query.orderBy = @"folder"; //?orderBy=folder,modifiedDate desc,title
    NSLog(@"query.q:%@",query.q);
    //mimeType = 'application/vnd.google-apps.folder'
    //application/vnd.ms-excel
    [self.service executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                         GTLDriveFileList *fileList,
                                                         NSError *error) {
        if (error == nil) {
            NSLog(@"Have results");
            // Iterate over fileList.files array
            NSLog(@"檔案總數:%ld",fileList.files.count);
            if (fileList.files.count > 0) {
                for (GTLDriveFile *file in fileList.files) {
                    NSLog(@"名稱:%@",file.name);
                    
                    NSString *mimeType = file.mimeType;
                    if ([mimeType rangeOfString:@"folder"].length > 0) {
                        NSLog(@"檔案類型:資料夾");
                    }
                    else if ([mimeType rangeOfString:@"image"].length > 0) {
                        NSLog(@"檔案類型:圖檔");
                    }
                    else if ([mimeType rangeOfString:@"pdf"].length > 0) {
                        NSLog(@"檔案類型:pdf");
                    }
                    else {
                        NSLog(@"檔案類型:%@",mimeType);
                    }
                    
                }
            }
            else {
                NSLog(@"no files found..");
            }
        } else {
            NSLog(@"An error occurred: %@", error);
            //[self showAlert:@"Error" message:error.localizedDescription];
        }
    }];
}
- (void)displayResultWithTicket:(GTLServiceTicket *)ticket
             finishedWithObject:(GTLDriveFileList *)response
                          error:(NSError *)error {
    if (error == nil) {
        NSMutableString *filesString = [[NSMutableString alloc] init];
        NSLog(@"檔案總數:%ld",response.files.count);
        if (response.files.count > 0) {
            [filesString appendString:@"Files:\n"];
            for (GTLDriveFile *file in response.files) {
                NSLog(@"檔名:%@",file.name);
                //[filesString appendFormat:@"%@ (%@)\n", file.name, file.identifier];
            }
        }
        else {
            [filesString appendString:@"No files found."];
        }
        //NSLog(@"filesString:%@",filesString);
        //self.output.text = filesString;
    } else {
        //[self showAlert:@"Error" message:error.localizedDescription];
    }
}
// Construct a query to get names and IDs of 10 files using the Google Drive API.
#pragma mark - 換頁
-(void) GotoMultiPhotographVCWithSourceType:(UIImagePickerControllerSourceType)sourceType { //4.4特殊資料上傳
    MultiPhotographVC *destVC = [[MultiPhotographVC alloc] initWithNibName:@"MultiPhotographVC" bundle:nil];
    destVC.attachName = _attachName;
    destVC.sourceType = sourceType;
    destVC.shouldMask = shouldMask;
    destVC.shouldWatermark = shouldWatermark;
    destVC.isVehicleLicense = isVehicleLicense;
    destVC.type = _type;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoGoogleDriveSelectVC {
    GoogleDriveSelectVC *destVC = [[GoogleDriveSelectVC alloc] initWithNibName:@"GoogleDriveSelectVC" bundle:nil];
    destVC.service = self.service;
    destVC.parentId = @"root"; //根目錄
    destVC.OrderNo = _OrderNo;
    destVC.attachName = _attachName;
    destVC.type = _type;
    destVC.remark = _TextViewContent.text;
    destVC.didFromTransfer = _didFromTransfer;
    destVC.dicTransfer = _dicTransfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    destVC.didFromCustomer = _didFromCustomer;
    destVC.customerID = _customerID;
    destVC.attachTypeId = _attachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GoogleDriveUploadFinish {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MultiPhotoFinishWithImage:) name:@"kNotification_MultiPhoto_Finish" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GoogleDriveUploadFinish) name:@"kNotification_Google_Drive_Upload_Finish" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_MultiPhoto_Finish" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Google_Drive_Upload_Finish" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) dealloc {
    [self RemoveNotification];
}
@end
