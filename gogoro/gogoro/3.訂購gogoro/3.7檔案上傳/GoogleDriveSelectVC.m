

#import "GoogleDriveSelectVC.h"
#import "GoogleDriveSelectVCCell.h"
#import "CustomerDocumentSignVC.h"

@interface GoogleDriveSelectVC () {
    NSMutableArray *arrayFolders,*arrayFiles;
    NSInteger selectedIndex;
    NSInteger selectedSection;
    BOOL didShowSearchBar;
}
@property (nonatomic,weak) IBOutlet UIView *ViewSearch;
@property (nonatomic,weak) IBOutlet UIButton *ButtonClear;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSearch;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (nonatomic,weak) IBOutlet UIView *ListView;
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@end

@implementation GoogleDriveSelectVC
@synthesize service = _service;
@synthesize parentId = _parentId;
@synthesize keywordToSearch = _keywordToSearch;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _ViewSearch.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_all"]];
    
    [self setupNavigationItemTitle:_attachName Image:[UIImage imageNamed:@"icn_res.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    //參數初始化
    arrayFolders = [[NSMutableArray alloc] init];
    arrayFiles = [[NSMutableArray alloc] init];
    
    /* 手勢 */
    UISwipeGestureRecognizer *swipeUp;
    UISwipeGestureRecognizer *swipeDown;
    
    //設定所要偵測的UIView與對應的函式
    swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];

    //設定偵測手勢的類型
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];    //向上滑
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];  //向下滑
    
    //將辨識的機制加入
    [self.view addGestureRecognizer:swipeUp];
    [self.view addGestureRecognizer:swipeDown];
    [self handleSwipeUp:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
    [self fetchFiles];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
#pragma mark - 上下滑動
- (void)handleSwipeUp:(UISwipeGestureRecognizer *)recognizer {
    didShowSearchBar = NO;
    
    //隱藏搜尋列
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _ViewSearch.frame = CGRectMake(0, 21, _ViewSearch.frame.size.width, _ViewSearch.frame.size.height);
        _ListView.frame = CGRectMake(0, 82, 768, 942);
    } completion:^(BOOL finished) {
    }];

}
- (void)handleSwipeDown:(UISwipeGestureRecognizer *)recognizer {
    didShowSearchBar = YES;
    
    //顯示搜尋列
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _ViewSearch.frame = CGRectMake(0, 82, _ViewSearch.frame.size.width, _ViewSearch.frame.size.height);
        _ListView.frame = CGRectMake(0, 82 + 61, 768, 942 - 61);
    } completion:^(BOOL finished) {
    }];
}
-(IBAction)pressDismissKeyboardBtn:(id)sender {
    [_searchTextField resignFirstResponder];
}
#pragma mark - Google Drive 相關
- (void)fetchFiles {
    [ObjectManager showLodingHUDinView:self.view];
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    
    if ([self.keywordToSearch length] > 0) { //關鍵字搜尋
        query.q = [NSString stringWithFormat:@"'%@' in parents and trashed = false and name contains '%@' and (mimeType contains 'image/' or mimeType contains 'pdf' or mimeType contains 'folder')", self.parentId,self.keywordToSearch];
    }
    else {
        query.q = [NSString stringWithFormat:@"'%@' in parents and trashed = false and (mimeType contains 'image/' or mimeType contains 'pdf' or mimeType contains 'folder')", self.parentId];
    }

    NSLog(@"query.q:%@",query.q);
    query.orderBy = @"folder,modifiedTime desc"; //?orderBy=folder,modifiedDate desc,title
    //mimeType = 'application/vnd.google-apps.folder'
    //application/vnd.ms-excel
    [self.service executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                         GTLDriveFileList *fileList,
                                                         NSError *error) {
        
        [ObjectManager hideHUDForView:self.view];

        if (error == nil) {
            [arrayFolders removeAllObjects];
            [arrayFiles removeAllObjects];
            
            NSLog(@"Have results");
            // Iterate over fileList.files array
            NSLog(@"檔案與資料夾總數:%ld",fileList.files.count);
            if (fileList.files.count > 0) {
                for (GTLDriveFile *file in fileList.files) {
                    NSLog(@"名稱:%@",file.name);
                    
                    NSString *mimeType = file.mimeType;
                    if ([mimeType rangeOfString:@"folder"].length > 0) {
                        //NSLog(@"檔案類型:資料夾");
                        [arrayFolders addObject:file];
                    }
                    else if ([mimeType rangeOfString:@"image"].length > 0) {
                        //NSLog(@"檔案類型:圖檔");
                        [arrayFiles addObject:file];
                    }
                    else if ([mimeType rangeOfString:@"pdf"].length > 0) {
                        //NSLog(@"檔案類型:pdf");
                        [arrayFiles addObject:file];
                    }
                    else {
                        NSLog(@"檔案類型:%@",mimeType);
                    }
                }
            }
            else {
                NSLog(@"no files found..");
            }
            NSLog(@"資料夾數量:%ld",[arrayFolders count]);
            NSLog(@"檔案數量:%ld",[arrayFiles count]);
            
            [_TableView reloadData];
            
        }
        else {
            NSLog(@"An error occurred: %@", error);
            [self showAlertControllerWithMessage:error.localizedDescription];
        }
    }];
}
// Process the response and display output.
/*- (void)displayResultWithTicket:(GTLServiceTicket *)ticket
 finishedWithObject:(GTLDriveFileList *)response
 error:(NSError *)error {
 if (error == nil) {
 NSMutableString *filesString = [[NSMutableString alloc] init];
 NSLog(@"檔案總數:%ld",response.files.count);
 if (response.files.count > 0) {
 [filesString appendString:@"Files:\n"];
 for (GTLDriveFile *file in response.files) {
 NSLog(@"檔名:%@",file.name);
 //[filesString appendFormat:@"%@ (%@)\n", file.name, file.identifier];
 }
 }
 else {
 [filesString appendString:@"No files found."];
 }
 //NSLog(@"filesString:%@",filesString);
 //self.output.text = filesString;
 } else {
 [self showAlertControllerWithMessage:error.localizedDescription];
 }
 }*/

#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    

    
    return YES;
}
-(IBAction) textFieldDidEditingChange:(id)sender {
    if ([_searchTextField.text length] > 0) {
        _ButtonClear.hidden = NO;
        _ButtonSearch.hidden = NO;
    }
    else {
        _ButtonClear.hidden = YES;
        _ButtonSearch.hidden = YES;
    }
}
#pragma mark - IBAction
-(IBAction) pressSearchBtn {
    [_searchTextField resignFirstResponder];
    if ([_searchTextField.text length] > 0) {
        self.keywordToSearch = _searchTextField.text;
        [self GotoGoogleDriveSelectVC_Search];
    }
}
-(IBAction) pressClearBtn {
    [_searchTextField resignFirstResponder];
    _searchTextField.text = @"";
    [self handleSwipeUp:nil];
}
#pragma mark - tableView 相關
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger count = 0;
    
    //判斷有無資料夾
    if ([arrayFolders count] > 0) {
        count++;
    }
    
    //判斷有無檔案
    if ([arrayFiles count] > 0) {
        count++;
    }
    
    return count;
}
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    
    //第一區，可能是資料夾或檔案
    if (section == 0) {
        if ([arrayFolders count] > 0) {
            count = [arrayFolders count];
        }
        else {
            count = [arrayFiles count];
        }
    }
    else { //第二區，一定是檔案
        count = [arrayFiles count];
    }
    
    return count;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 61)];
    view.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0f];
    
    UILabel *Label = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, 100, 61)];
    Label.font = [UIFont boldSystemFontOfSize:28];
    [view addSubview:Label];
    //第一區，可能是資料夾或檔案
    if (section == 0) {
        if ([arrayFolders count] > 0) {
            Label.text = @"資料夾";
        }
        else {
            Label.text = @"檔案";
        }
    }
    else { //第二區，一定是檔案
        Label.text = @"檔案";
    }
    
    return view;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GoogleDriveSelectVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"GoogleDriveSelectVCCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    GoogleDriveSelectVCCell *myCell = (GoogleDriveSelectVCCell *)cell;
    myCell.indexPath = indexPath;
    
    GTLDriveFile *file = nil;
    BOOL isFolder = NO;
    
    if (indexPath.section == 0) {
        if ([arrayFolders count] > 0) { //資料夾
            file = [arrayFolders objectAtIndex:indexPath.row];
            isFolder = YES;
        }
        else { //檔案
            file = [arrayFiles objectAtIndex:indexPath.row];
        }
    }
    else { //第二區，一定是檔案
        file = [arrayFiles objectAtIndex:indexPath.row];
    }
    
    if (isFolder) {
        myCell.ImageArrow.hidden = NO;
        myCell.ImageTitle.image = [UIImage imageNamed:@"folder.png"];
    }
    else {
        myCell.ImageArrow.hidden = YES;
        myCell.ImageTitle.image = [UIImage imageNamed:@"flie.png"];
    }
    
    //名稱
    //NSLog(@"file.name:%@",file.name);
    myCell.LabelTitle.text = file.name;
    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void) GoogleDriveSelectAtIndex:(NSNotification *)notif {
    NSIndexPath *indexPath = [notif object];

    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
    
    if (indexPath.section == 0) {
        if ([arrayFolders count] > 0) { //資料夾
            [self GotoGoogleDriveSelectVC];
        }
        else { //檔案
            [self ShowConfirmMessage];
        }
    }
    else { //第二區，一定是檔案
        [self ShowConfirmMessage];
    }
}
#pragma mark - ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height) {
        // we are at the end
        NSLog(@"we are at the end");
    }
}
-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    float scrollOffset = scrollView.contentOffset.y;
    NSLog(@"did scroll:%f",scrollOffset); //NSStringFromCGRect(ViewHeaderRefresh.frame)
    if (scrollOffset < 0) {
        /*[UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.01];
        _ViewSearch.transform = CGAffineTransformMakeTranslation(0, -scrollOffset);
        [UIView setAnimationDelegate:self];
        [UIView commitAnimations];*/
    }
    
    if (scrollOffset <= -61 && !didShowSearchBar) {
        [self handleSwipeDown:nil];
        
        /*[UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.01];
        _ViewSearch.transform = CGAffineTransformMakeTranslation(0, 0);
        [UIView setAnimationDelegate:self];
        [UIView commitAnimations];*/
    }
    else if (scrollOffset >= 61 && didShowSearchBar) {
        [self handleSwipeUp:nil];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    float scrollOffset = scrollView.contentOffset.y;
    NSLog(@"end drag:%f",scrollOffset);
    if (scrollOffset <= -100 ) { //Refresh
        //[self GetAnnounceList];
    }
}
#pragma mark - 檔案下載與上傳
-(void) ShowConfirmMessage {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否以所選檔案上傳？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"上傳" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self DownloadFileFromGoogleDrive];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:submitAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void) DownloadFileFromGoogleDrive { //先把檔案從Google Drive下載
    GTLDriveFile *file = [arrayFiles objectAtIndex:selectedIndex];
    NSLog(@"檔名:%@",file.name);

    NSString *url = [NSString stringWithFormat:@"https://www.googleapis.com/drive/v3/files/%@?alt=media",file.identifier];
    //url = @"https://www.googleapis.com/drive/v3/files/0ByoQRKKFvqV_ZHJMSUhmTFlpNlU?alt=media";
    NSLog(@"url:%@",url);
    
    GTMSessionFetcher *fetcher = [self.service.fetcherService fetcherWithURLString:url];
    
    [ObjectManager showLodingHUDinView:self.view];
    
    [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
        if (error == nil) {
            NSLog(@"Retrieved file content");
            if (data){
                NSString *name = file.name;
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", name]];
                NSLog(@"path:%@",path);
                if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
                    BOOL success = [data writeToFile:path atomically:NO];
                    if (success) {
                        NSLog(@"檔案儲存成功，準備上傳");
                        if (_didFromTransfer) { //過戶移轉
                            [self UploadAttachTransferWithFilename:name];
                        }
                        else if (_didFromCustomer) { //1.0自訂文件或2.0預約試乘
                            [self UploadContractWithFilename:name];
                        }
                        else {
                            [self UploadAttachWithFilename:name];
                        }
                    }
                    else {
                        [ObjectManager hideHUDForView:self.view];
                        [self showAlertControllerWithMessage:@"下載Google Drive檔案失敗(本地端暫存失敗)"];
                        NSLog(@"檔案儲存失敗");
                    }
                }
                else {
                    [ObjectManager hideHUDForView:self.view];
                    [self showAlertControllerWithMessage:@"下載Google Drive檔案失敗(本地端存在重複檔案)"];
                    NSLog(@"檔案已存在，儲存失敗");
                }
            }
        } else {
            [ObjectManager hideHUDForView:self.view];
            [self showAlertControllerWithMessage:[error localizedDescription]];
            NSLog(@"An error occurred: %@", [error localizedDescription]);
        }
    }];
}
- (void)myFetcher:(GTMSessionFetcher *)fetcher receivedData:(NSData *)dataReceivedSoFar {
    
}
-(NSString*)directoryPathForSavingFile:(NSString *)directoryName {
    NSString *applicationDirectory= [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    applicationDirectory = [applicationDirectory stringByAppendingPathComponent:directoryName];
    return applicationDirectory;
}
#pragma mark - Call API
#pragma mark [311] 上傳訂單附件電子檔
- (void) UploadAttachWithFilename:(NSString *)filename {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)_type] forKey:@"type"];
    [parameter setObject:_remark forKey:@"remark"];
    [parameter setObject:filename forKey:@"filename"];
    [GatewayManager callUploadAttach:parameter Source:self];
}
- (void)callBackUploadAttach:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSArray *Data = [parameter objectForKey:@"Data"];
        
        /*//上傳成功，從不同頁面過來時處理的狀況不同
        if (_didFromSupplement) { //從補單上傳，查詢訂單後選擇文件的畫面
            //[self.navigationController popToRootViewControllerAnimated:YES];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if (_didFromBatchUpload) { //從4.3單一文件批次上傳來，回前頁
            [self.navigationController popViewControllerAnimated:YES];
        }
        else if (_didFromLackReturn) { //從從5.2 5.3 5.4 缺件退回來，回前頁
            [self.navigationController popViewControllerAnimated:YES];
        }
        else { //訂購，回前頁
            [self.navigationController popViewControllerAnimated:YES];
        }*/
        //上傳成功後，關閉此頁面、4.2.6檔案上傳頁面，並刷新檔案清單頁面狀況
        [self GoBackToUploadFileVC];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [607] 上傳或更新過戶附件
- (void) UploadAttachTransferWithFilename:(NSString *)filename {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if ([_stringCarNumber length] > 0) {
        [parameter setObject:_stringCarNumber forKey:@"Plate"];
    }
    
    if ([_stringOriginalOwnerID length] > 0) {
        [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    }
    
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)_type] forKey:@"attachType"];
    [parameter setObject:_remark forKey:@"Memo"];
    [parameter setObject:filename forKey:@"filename"];
    [GatewayManager callTransferUpload:parameter Source:self];
}
- (void)callBacTransferUpload:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //上傳成功後，關閉此頁面、4.2.6檔案上傳頁面，並刷新檔案清單頁面狀況
        [self GoBackToUploadFileVC];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [107] 上傳非訂單同意書
- (void) UploadContractWithFilename:(NSString *)filename {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_customerID forKey:@"customer"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",_type] forKey:@"attachType"];
    //[parameter setObject:_remark forKey:@"Memo"];
    [parameter setObject:filename forKey:@"filename"];
    [GatewayManager callContractUploadContract:parameter Source:self];
}
- (void)callBackContractUploadContract:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //上傳成功後，關閉此頁面、回到1.3
        [self GoBackToCustomerDocumentSignVC];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoGoogleDriveSelectVC { //資料夾下層
    GTLDriveFile *file = [arrayFolders objectAtIndex:selectedIndex];
    NSLog(@"file.identifier:%@",file.identifier);
    
    GoogleDriveSelectVC *destVC = [[GoogleDriveSelectVC alloc] initWithNibName:@"GoogleDriveSelectVC" bundle:nil];
    destVC.service = self.service;
    destVC.parentId = file.identifier; //目錄id
    destVC.keywordToSearch = self.keywordToSearch; //搜尋關鍵字
    destVC.OrderNo = _OrderNo;
    destVC.attachName = _attachName;
    destVC.type = _type;
    destVC.remark = _remark;
    destVC.didFromTransfer = _didFromTransfer;
    destVC.dicTransfer = _dicTransfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    destVC.didFromCustomer = _didFromCustomer;
    destVC.customerID = _customerID;
    destVC.attachTypeId = _attachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoGoogleDriveSelectVC_Search { //搜尋
    GoogleDriveSelectVC *destVC = [[GoogleDriveSelectVC alloc] initWithNibName:@"GoogleDriveSelectVC" bundle:nil];
    destVC.service = self.service;
    destVC.parentId = self.parentId; //目錄id
    destVC.keywordToSearch = self.keywordToSearch; //搜尋關鍵字
    destVC.OrderNo = _OrderNo;
    destVC.attachName = _attachName;
    destVC.type = _type;
    destVC.remark = _remark;
    destVC.didFromTransfer = _didFromTransfer;
    destVC.dicTransfer = _dicTransfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    destVC.didFromCustomer = _didFromCustomer;
    destVC.customerID = _customerID;
    destVC.attachTypeId = _attachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
    
    self.keywordToSearch = @"";
}
-(void) GoBackToUploadFileVC {
    //回到檔案上傳
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[UploadFileVC class]]) {
            destVC = (UploadFileVC *)vc;
        }
    }
    NSLog(@"destVC:%@",destVC);
    [self.navigationController popToViewController:destVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Google_Drive_Upload_Finish" object:nil];
}
-(void) GoBackToCustomerDocumentSignVC {
    //回到1.3
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[CustomerDocumentSignVC class]]) {
            destVC = (CustomerDocumentSignVC *)vc;
        }
    }
    NSLog(@"destVC:%@",destVC);
    [self.navigationController popToViewController:destVC animated:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Google_Drive_Upload_Finish" object:nil];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GoogleDriveSelectAtIndex:) name:@"kNotification_Google_Drive_Select" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Google_Drive_Select" object:nil];
}
@end
