

#import <UIKit/UIKit.h>

@interface GoogleDriveSelectVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageTitle;
@property (nonatomic, weak) IBOutlet UIImageView *ImageArrow;
@property (nonatomic, weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic, weak) NSIndexPath *indexPath;
@end
