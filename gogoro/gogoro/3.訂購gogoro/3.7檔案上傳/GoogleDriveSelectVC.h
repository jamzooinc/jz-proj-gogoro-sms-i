

#import <UIKit/UIKit.h>
#import "TemplateVC.h"
#import "UploadFileVC.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTLDrive.h"

@interface GoogleDriveSelectVC : TemplateVC {
    
}
@property (nonatomic, strong) GTLServiceDrive *service;
@property (nonatomic,strong) NSString *OrderNo;
@property NSInteger type; //附件類型
@property (nonatomic,strong) NSString *remark; //備註
@property (nonatomic, strong) NSString *parentId;
@property (nonatomic, strong) NSString *keywordToSearch;
@property (nonatomic,strong) NSString *attachName; //附件名稱
@property BOOL didFromTransfer; //YES:從過戶過來
@property (nonatomic,strong) NSDictionary *dicTransfer;
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼

@property BOOL didFromCustomer; //YES:從1.0自訂文件或2.0預約試乘過來
@property (nonatomic,strong) NSString *customerID; //從2.2試乘拍駕照過來會帶
@property NSInteger attachTypeId; //附件類型（首次參觀:101, 預約試乘: 102）
@end
