
#import <UIKit/UIKit.h>

@protocol ConfirmInfoViewDelegate <NSObject>
@optional
-(void) ConfirmInfoViewCheckAtIndex:(NSInteger)index; //勾選
-(void) ConfirmInfoViewUnCheckAtIndex:(NSInteger)index; //取消勾選
-(void) ConfirmInfoViewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect; //列印
-(void) ConfirmInfoViewUploadAtIndex:(NSInteger)index; //上傳
-(void) ConfirmInfoViewFileUrldAtIndex:(NSInteger)index; //fileUrl
-(void) ConfirmInfoViewTemplateAtIndex:(NSInteger)index; //版型
-(void) ConfirmInfoViewReTemplateAtIndex:(NSInteger)index; //重新套版
-(void) ConfirmInfoViewQueryAtIndex:(NSInteger)index; //二行程補助查詢
-(void) ConfirmInfoViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl; //預覽列印
@end

@interface ConfirmInfoView : UIView {
    __weak id <ConfirmInfoViewDelegate> delegate;
    BOOL IsChecked; //文件是否已勾選
    NSMutableArray *arrayPreviewPrint; //預覽列印的文件
}
@property (weak) id delegate;
@property BOOL didFromECDocumentListVC; //從5.1.1電子文件清單（功能按鈕不需作用）
@property (nonatomic,weak) IBOutlet UIImageView *ImageCamera;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCamera;
@property (nonatomic,weak) IBOutlet UIButton *ButtonUpload;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPrint;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCheck;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPrintSign; //版型
@property (nonatomic,weak) IBOutlet UIButton *ButtonTemplate; //重新套版
@property (nonatomic,weak) IBOutlet UIButton *ButtonReSign; //重簽
@property (nonatomic,weak) IBOutlet UIButton *ButtonPreviewPrint; //預覽列印
@property (nonatomic,weak) IBOutlet UIButton *ButtonQuery; //二行程補助資格查詢
@property (nonatomic,weak) IBOutlet UIImageView *ImageCheckBox;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
-(void) UpdateUIWithData:(NSDictionary *)data;
@end
