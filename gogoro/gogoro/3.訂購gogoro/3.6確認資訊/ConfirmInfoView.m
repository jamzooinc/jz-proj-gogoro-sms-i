


#import "ConfirmInfoView.h"

@implementation ConfirmInfoView

@synthesize delegate;
#pragma mark - Delegate
-(void) ConfirmInfoViewCheckAtIndex:(NSInteger)index { //勾選
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewCheckAtIndex:)]) {
        [[self delegate] ConfirmInfoViewCheckAtIndex:index];
    }
}
-(void) ConfirmInfoViewUnCheckAtIndex:(NSInteger)index { //取消勾選
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewUnCheckAtIndex:)]) {
        [[self delegate] ConfirmInfoViewUnCheckAtIndex:index];
    }
}
-(void) ConfirmInfoViewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect { //列印
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewPrintAtIndex:Rect:)]) {
        [[self delegate] ConfirmInfoViewPrintAtIndex:index Rect:rect];
    }
}
-(void) ConfirmInfoViewUploadAtIndex:(NSInteger)index { //上傳
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewUploadAtIndex:)]) {
        [[self delegate] ConfirmInfoViewUploadAtIndex:index];
    }
}
-(void) ConfirmInfoViewFileUrldAtIndex:(NSInteger)index { //fileUrl
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewFileUrldAtIndex:)]) {
        [[self delegate] ConfirmInfoViewFileUrldAtIndex:index];
    }
}
-(void) ConfirmInfoViewTemplateAtIndex:(NSInteger)index { //版型
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewTemplateAtIndex:)]) {
        [[self delegate] ConfirmInfoViewTemplateAtIndex:index];
    }
}
-(void) ConfirmInfoViewReTemplateAtIndex:(NSInteger)index { //重新套版
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewReTemplateAtIndex:)]) {
        [[self delegate] ConfirmInfoViewReTemplateAtIndex:index];
    }
}
-(void) ConfirmInfoViewQueryAtIndex:(NSInteger)index { //二行程補助查詢
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewQueryAtIndex:)]) {
        [[self delegate] ConfirmInfoViewQueryAtIndex:index];
    }
}
-(void) ConfirmInfoViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl{ //預覽列印
    if ([[self delegate] respondsToSelector:@selector(ConfirmInfoViewPreviewPrintAtIndex:Rect:UrlString:FromTempUrl:)]) {
        [[self delegate] ConfirmInfoViewPreviewPrintAtIndex:index Rect:rect UrlString:urlString FromTempUrl:fromTempUrl];
    }
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data {
    NSMutableArray *arrayButtons = [[NSMutableArray alloc] init];
    arrayPreviewPrint = [[NSMutableArray alloc] init];
    
    _LabelTitle.text = [data objectForKey:@"AttachTypeName"];
    NSLog(@"文件名稱:%@",_LabelTitle.text);
    
    //IsOriginal 正本文件
    BOOL IsOriginal = [[data objectForKey:@"IsOriginal"] boolValue];
    //IsOriginal = YES;
    if (IsOriginal) {
        //NSLog(@"正本文件:%@",_LabelTitle.text);
    }
    
    //是否已勾
    IsChecked = [[data objectForKey:@"IsChecked"] boolValue];
    if (IsChecked) { //已勾的文件，不能取消已勾
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_y.png"];
        _ButtonCheck.hidden = YES; //隱藏勾選按鈕
    }
    else { //未勾的文件
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_n.png"];
        
        //正本文件：如果沒勾的話要可以勾，（上傳後）已勾的話不能取消
        if (IsOriginal) {
            _ButtonCheck.hidden = NO;
        }
        else { //其他文件不能手動打勾
            _ButtonCheck.hidden = YES;
        }
    }
    
    /*//判斷有沒有FileUrl（可以觀看檔案）
    NSString *FileUrl = [data objectForKey:@"FileUrl"];
    BOOL hasFileUle = [FileUrl length] > 0;
    hasFileUle = YES;
    if (hasFileUle) {
        _ImageCamera.hidden = NO;
        _ButtonCamera.hidden = NO;
    }*/
    
    //是否版型
    //2016.10.13 取消
    /*BOOL NeedPrintSign = [[data objectForKey:@"NeedPrintSign"] boolValue];
    //NeedPrintSign = YES;
    if (NeedPrintSign) {
        [arrayButtons addObject:_ButtonPrintSign];
    }
    else {
    }*/
    
    //二行程輔助資格查詢 AttachTypeId : 53
    NSInteger AttachTypeId = [[data objectForKey:@"AttachTypeId"] integerValue];
    if (AttachTypeId == 53 || AttachTypeId == 56) {
        [arrayButtons addObject:_ButtonQuery];
    }
    
    //是否重套版
    //2016.12.13 改為重簽
    BOOL UseTemplate = [[data objectForKey:@"UseTemplate"] boolValue];
    //UseTemplate = YES;
    if (UseTemplate) {
        //[arrayButtons addObject:_ButtonTemplate];
        [arrayButtons addObject:_ButtonReSign];
        NSLog(@"UseTemplate:YES");
    }
    else {
        NSLog(@"UseTemplate:NO");
    }
    
    //是否需要上傳檔案
    BOOL IsUploadFile = [[data objectForKey:@"IsUploadFile"] boolValue];
    //IsUploadFile = YES;
    if (IsUploadFile) {
        [arrayButtons addObject:_ButtonUpload];
        NSLog(@"IsUploadFile:YES");
    }
    else {
        NSLog(@"IsUploadFile:NO");
    }
    
    //是否可以列印
    //2016.10.13 改為預覽列印
    BOOL IsOutPaper = [[data objectForKey:@"IsOutPaper"] boolValue];
    //IsOutPaper = YES;
    if (IsOutPaper) {
        //[arrayButtons addObject:_ButtonPrint];
        [arrayButtons addObject:_ButtonPreviewPrint];
    }
    else {
    }
    
    //判斷要不要顯示相機按鈕
    if (IsUploadFile || UseTemplate) {
        _ImageCamera.hidden = NO;
        _ButtonCamera.hidden = NO;
    }
    
    NSString *TemplateFileUrl = [data objectForKey:@"TemplateFileUrl"];
    BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //hasTemplateFileUrl = YES;
    if (hasTemplateFileUrl) {
        NSDictionary *_dic = [NSDictionary dictionaryWithObjectsAndKeys:@"尚未簽名的空白文件",@"title",TemplateFileUrl,@"url", nil];
        [arrayPreviewPrint addObject:_dic];
    }
    
    NSString *FileUrl = [data objectForKey:@"FileUrl"];
    BOOL hasFileUle = [FileUrl length] > 0;
    //hasFileUle = YES;
    if (hasFileUle) {
        NSDictionary *_dic = [NSDictionary dictionaryWithObjectsAndKeys:@"電子檔文件",@"title",FileUrl,@"url", nil];
        [arrayPreviewPrint addObject:_dic];
    }
    
    //判斷有沒有按鈕，及按鈕位置
    //NSLog(@"arrayButtons:%ld",[arrayButtons count]);
    if ([arrayButtons count] > 0 && !_didFromECDocumentListVC) { //有按鈕 且 從5.1.1電子文件清單（功能按鈕不需作用）
        //調整自己的高度
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 98);
        
        //調整按鈕位置
        for (NSInteger i = 0; i < [arrayButtons count]; i++) {
            UIButton *Button = [arrayButtons objectAtIndex:i];
            CGFloat x = 0;
            switch (i) {
                case 0:
                    x = 164;
                    break;
                case 1:
                    x = 298;
                    break;
                case 2:
                    x = 432;
                    break;
                case 3:
                    x = 566;
                    break;
                default:
                    break;
            }
            Button.hidden = NO;
            Button.frame = CGRectMake(x, Button.frame.origin.y, Button.frame.size.width, Button.frame.size.height);
        }
    }
    else { //沒有按鈕
        
    }
}
#pragma mark - Button
-(IBAction) pressCheckBtn:(UIButton *)sender { //勾選按鈕
    if (sender.tag == 0) { //未勾-->已勾
        sender.tag = 1;
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_y.png"];
        [self ConfirmInfoViewCheckAtIndex:self.tag];
    }
    else if (sender.tag == 1) { //已勾-->未勾
        sender.tag = 0;
        _ImageCheckBox.image = [UIImage imageNamed:@"checkbox_n.png"];
        [self ConfirmInfoViewUnCheckAtIndex:self.tag];
    }
}
-(IBAction) pressUploadBtn:(id)sender { //上傳
    [self ConfirmInfoViewUploadAtIndex:self.tag];
}
-(IBAction) pressFileUrlBtn:(id)sender { //FileUrl
    [self ConfirmInfoViewFileUrldAtIndex:self.tag];
}
-(IBAction) pressPrintBtn { //列印
    [self ConfirmInfoViewPrintAtIndex:self.tag Rect:_ButtonPrint.frame];
}
-(IBAction) pressTemplateBtn:(id)sender { //版型
    [self ConfirmInfoViewTemplateAtIndex:self.tag];
}
-(IBAction) pressReTemplateBtn:(id)sender { //重新套版
    [self ConfirmInfoViewReTemplateAtIndex:self.tag];
}
-(IBAction) pressReSignBtn:(id)sender { //重簽
    [self ConfirmInfoViewReTemplateAtIndex:self.tag];
}
-(IBAction) pressQueryBtn:(id)sender { //二行程補助查詢
    [self ConfirmInfoViewQueryAtIndex:self.tag];
}
-(IBAction) pressPreviewPrintBtn:(id)sender { //預覽列印
    if ([arrayPreviewPrint count] == 0) return; //防呆
    [self ShowPreviewPrintSelect];
}
#pragma mark - UIAlertController Handle
-(void) ShowPreviewPrintSelect {
    NSString *message = [NSString stringWithFormat:@"請選擇"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayPreviewPrint count]; i++) {
        NSDictionary *_dic = [arrayPreviewPrint objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"title"];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonPreviewPrint;
    popPresenter.sourceRect = _ButtonPreviewPrint.bounds;
    
    UIViewController *sourceVC = [ObjectManager viewController:self];
    [sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);

    NSDictionary *_dic = [arrayPreviewPrint objectAtIndex:buttonIndex];
    
    NSString *urlString = [_dic objectForKey:@"url"];
    NSLog(@"urlString:%@",urlString);
    
    NSString *title = [_dic objectForKey:@"title"];
    
    BOOL FromTempUrl = NO;
    if ([title isEqualToString:@"尚未簽名的空白文件"]) {
        FromTempUrl = YES;
    }
    [self ConfirmInfoViewPreviewPrintAtIndex:self.tag Rect:_ButtonPreviewPrint.frame UrlString:urlString FromTempUrl:FromTempUrl];
}
@end
