//
//  ConfirmInfoVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfirmInfoView.h"
#import "ECDocumentListCustomerNameView.h"

@interface ConfirmInfoVC : TemplateVC <ConfirmInfoViewDelegate>
@property (nonatomic,strong) NSString *OrderNo;
@property (nonatomic,strong) NSString *CustomerName;
@property BOOL didFromSupplement; //YES:從補單上傳過來
@property NSInteger Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
@property NSInteger stepSupplementTesOrLocal; //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
@end
