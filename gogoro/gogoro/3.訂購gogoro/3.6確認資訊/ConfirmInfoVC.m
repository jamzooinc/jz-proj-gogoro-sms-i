//
//  ConfirmInfoVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "ConfirmInfoVC.h"
#import "UploadFileVC.h"
#import "PhotoViewerVC.h"
#import "ConsentVC.h"
#import "OrderSearchRootVC.h"
#import "ExternalWebVC.h"

@interface ConfirmInfoVC () {
    NSMutableArray *arrayData;
    NSInteger selectedIndex;
    NSMutableArray *arrayModifiedAttachedID; //有異動的附件ID
    CGFloat totalHeight,gapBetweenView;
    BOOL didApply; //YES:申請領牌 NO：草稿儲存
    BOOL didSaveDraftBeforeApply; //2016.10.30 申請領牌前要先儲存草稿
    BOOL didOrzFace; //囧臉 申請領牌驗證
    NSDictionary *dicOrder;
    NSString *ownerName,*buyerName;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@property (nonatomic,weak) IBOutlet UIButton *ButtonApply;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSmile;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOrz;
@property (nonatomic,weak) IBOutlet UIImageView *ImageOwnIdentity;
@property (nonatomic,weak) IBOutlet UIImageView *ImageAccount;
@property (nonatomic,weak) IBOutlet UIImageView *ImageInsuranceIdentity;
@property (nonatomic,weak) IBOutlet UIImageView *ImageLoanerIndetity;
@property (nonatomic,weak) IBOutlet UIImageView *ImageSeperateDocument;
@property (nonatomic,weak) IBOutlet UIImageView *ImageGogoroDocument;
@property (nonatomic,weak) IBOutlet UIImageView *ImageGetProduct;
@property (nonatomic,weak) IBOutlet UIImageView *ImageFee;
@property (nonatomic,weak) IBOutlet UIImageView *ImageInvoiceAll;
@property (nonatomic,weak) IBOutlet UIImageView *ImageInvoice10000;
@property (nonatomic,weak) IBOutlet UIImageView *ImageLawBack;
@property (nonatomic,weak) IBOutlet UIImageView *ImageNewTP2;
@property (nonatomic,weak) IBOutlet UIImageView *ImageNewTPOnline;
@property (nonatomic,weak) IBOutlet UIImageView *ImageLocalApply;
@end

@implementation ConfirmInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"訂單編號" Image:nil];
    
    arrayData = [[NSMutableArray alloc] init];
    arrayModifiedAttachedID = [[NSMutableArray alloc] init];
    
    if (_didFromSupplement) {
        //左上角按鈕變成返回
        for (id _subView in self.view.subviews) {
            if ([_subView isKindOfClass:[TemplateVCTitleView class]]) {
                TemplateVCTitleView *myView = (TemplateVCTitleView *)_subView;
                [myView LeftButtonBack];
            }
        }
        
        //Step;  //0:   待處理訂單, 1: 領牌階段, 2: 補助階段 3: TES
        if (_Step == 1) { //領牌階段按鈕應為"申請補助"
            [_ButtonApply setBackgroundImage:[UIImage imageNamed:@"btn_confirm_bjapp.png"] forState:UIControlStateNormal];
        }
        else if (_Step == 2) { //補助階段按鈕應為"政府申請"
            [_ButtonApply setBackgroundImage:[UIImage imageNamed:@"btn_confirm_goverapp.png"] forState:UIControlStateNormal];
        }
    }
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //取得訂單詳細資料（為了得知車主與購買人）
    //[self GetOrderDetail];

    //取得訂單附件清單
    [self GetOrderAttachList];
}
#pragma mark - Call API
#pragma mark [301] 取得訂單
- (void) GetOrderDetail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void) callBackGetOrderDetail:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    //NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        dicOrder = nil;
        dicOrder = [[NSDictionary alloc] initWithDictionary:Data];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        //[self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        //[self showAlertControllerWithMessage:Sysmsg];
    }
    
    //取得訂單附件清單
    [self GetOrderAttachList];
}
#pragma mark [309] 取得訂單附件清單
- (void) GetOrderAttachList {
    //參數復歸
    [arrayModifiedAttachedID removeAllObjects];
    selectedIndex = -1;
    
    //移除既有View
    _ViewCommand.hidden = YES;
    for (id _subView in _ScrollView.subviews) {
        if ([_subView isKindOfClass:[ConfirmInfoView class]] ||
            [_subView isKindOfClass:[ECDocumentListCustomerNameView class]]) {
            [_subView removeFromSuperview];
        }
    }
    
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    
    //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
    if (_stepSupplementTesOrLocal == 1) {
        [parameter setObject:@"1" forKey:@"Step"];
    }
    else if (_stepSupplementTesOrLocal == 2) {
        [parameter setObject:@"2" forKey:@"Step"];
    }
    else {
    }
    
    [GatewayManager callGetOrderAttachList:parameter Source:self];
}
- (void)callBackGetOrderAttachList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSArray *Data = [parameter objectForKey:@"Data"];
        
        NSDictionary *_dic = [parameter objectForKey:@"Data"];
        NSArray *Data = [_dic objectForKey:@"Attachs"];
        
        ownerName = [_dic objectForKey:@"OwnerName"];
        buyerName = [_dic objectForKey:@"BuyerName"];
        
        [arrayData removeAllObjects];
        [arrayData addObjectsFromArray:Data];
        
        for (NSDictionary *_dic in arrayData) {
            NSLog(@"Name:%@",[_dic objectForKey:@"AttachTypeName"]);
            NSLog(@"_dic:%@",_dic);
            
            //是否已勾
            BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
            if (IsChecked) { //已勾的文件，記起來等上傳API的時候使用
                NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
                [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
            }
        }
        
        //產生項目
        [self GenerateSubview];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [310] 更新訂單附件確認清單
- (void) UpdateOrderAttachList:(NSString *)nextStep {
    if (!didSaveDraftBeforeApply) { //領牌前的草稿儲存
        [ObjectManager showLodingHUDinView:self.view];
    }
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [parameter setObject:arrayModifiedAttachedID forKey:@"type"];
    [parameter setObject:nextStep forKey:@"nextStep"];
    [GatewayManager callUpdateOrderAttachList:parameter Source:self];
}
- (void)callBackUpdateOrderAttachList:(NSDictionary *)parameter {
    if (!didSaveDraftBeforeApply) { //領牌前的草稿儲存
        [ObjectManager hideHUDForView:self.view];
    }
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        if (didApply) { //申請領牌
            NSDictionary *Data = [parameter objectForKey:@"Data"];
            NSString *Msg = [Data objectForKey:@"Msg"];
            NSInteger State = [[Data objectForKey:@"State"] integerValue];
            
            //回傳申請狀態
            //● 0:無動作、失敗
            //● 1:成功
            //● 2:可回溯 (申請領牌才有)
            if (State == 0) { //● 0:無動作、失敗，確認後不做動
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Msg preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }];
                [alertController addAction:actionConfirm];
                
                [self presentViewController:alertController animated:YES completion:^(void){
                }];
            }
            else if (State == 1) { //● 1:成功，確認後回首頁
                if (didOrzFace) { //囧臉 申請領牌驗證成功，顯示訊息跟笑臉
                    didOrzFace = NO;
                    _ButtonSmile.hidden = NO;
                    _ButtonOrz.hidden = YES;
                    [self showAlertControllerWithMessage:@"自我檢查過關，可請MOD進行申請領牌！"];
                }
                else { //申請領牌成功
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Msg preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        //2016.08.17
                        if (_didFromSupplement) { //補單上傳
                            [self GotoSearchVC];
                        }
                        else { //訂購回到首頁
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        }
                    }];
                    [alertController addAction:actionConfirm];
                    
                    [self presentViewController:alertController animated:YES completion:^(void){
                    }];
                }
            }
            //2017.04.27 Peter說已不會有2的狀況
            /*else if (State == 2) { //● 2:可回溯 (申請領牌才有)，詢問是否回溯
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Msg preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //呼叫API進行回溯
                    [self Backtracking];
                }];
                [alertController addAction:actionConfirm];
                
                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //2016.08.17
                    if (_didFromSupplement) { //補單上傳
                        [self GotoSearchVC];
                    }
                    else { //訂購回到首頁
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                }];
                [alertController addAction:actionCancel];
                
                [self presentViewController:alertController animated:YES completion:^(void){
                }];
            }*/
            
            if (didSaveDraftBeforeApply) {
                didSaveDraftBeforeApply = NO;
                [ObjectManager hideHUDForView:self.view];
            }
        }
        else if (didSaveDraftBeforeApply) { //申請領牌前的草稿儲存
            [self DoApply];
        }
        else { //草稿
            [self showAlertControllerWithMessage:@"草稿儲存完成"];
            
            //重新跟API取得資料
            //[ObjectManager showLodingHUDinView:self.view];
            [self performSelector:@selector(GetOrderAttachList) withObject:nil afterDelay:0.5f];
        }
    }
    else {
        if (didSaveDraftBeforeApply) {
            didSaveDraftBeforeApply = NO;
            [ObjectManager showLodingHUDinView:self.view];
        }
        
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [103] 文件重新套版
- (void) ReAapplyDocumentWithAttachType:(NSInteger)attachType {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callReAapplyDocument:parameter Source:self];
}
- (void)callBackReAapplyDocument:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //2016.10.13 改為重新簽名
        //重新跟API取得資料
        //[self performSelector:@selector(GetOrderAttachList) withObject:nil afterDelay:0.5f];
        [self GotoConsentResign];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [312] 訂單回溯
- (void) Backtracking {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callBacktracking:parameter Source:self];
}
- (void)callBackBacktracking:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //回溯成功回首頁
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"回溯成功" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //2016.08.17
            if (_didFromSupplement) { //補單上傳
                [self GotoSearchVC];
            }
            else { //訂購回到首頁
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
        [alertController addAction:actionConfirm];
        
        [self presentViewController:alertController animated:YES completion:^(void){
        }];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [105] 訂單同意書發信
- (void)callSendOrderContractMail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if (_OrderNo) {
        [parameter setObject:_OrderNo forKey:@"orderNo"]; //訂單編號
    }
    
    //表單類型(n:指定類型,無(null):所有同意書)
    [parameter setObject:@"" forKey:@"attachType"];
    
    [GatewayManager callSendOrderContractMail:parameter Source:self];
}
- (void)callBackSendOrderContractMail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [self showAlertControllerWithMessage:@"文件已成功寄出！"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - UI產生
-(void) GenerateSubview {
    //訂單資料
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"ECDocumentListCustomerNameView" owner:self options:nil];
    ECDocumentListCustomerNameView *view = [toplevels objectAtIndex:0];
    view.frame = CGRectMake((ScreenWidth - view.frame.size.width)/2 , 82 + 5, view.frame.size.width, view.frame.size.height);
    [self.view addSubview:view];
    [view UpdateUIWithOrderNo:_OrderNo Name:_CustomerName];
    
    gapBetweenView = 15;
    totalHeight = 20;
    
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *dic = [arrayData objectAtIndex:i];
        
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"ConfirmInfoView" owner:self options:nil];
        ConfirmInfoView *view = [toplevels objectAtIndex:0];
        view.delegate = self;
        view.tag = i;
        view.frame = CGRectMake(0, totalHeight, ScreenWidth, 49);
        [_ScrollView addSubview:view];
        [view UpdateUIWithData:dic];
        
        totalHeight = view.frame.origin.y + view.frame.size.height + gapBetweenView;
    }
    
    //功能按鈕(目前在畫面最底端，如果超過一頁就往下調)
    if (totalHeight >= _ViewCommand.frame.origin.y) {
        _ViewCommand.frame = CGRectMake(0, totalHeight, ScreenWidth, 71);
    }
    else {
    }
    
    totalHeight = _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height;
    _ViewCommand.hidden = NO;
    
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}
#pragma mark - 勾選/取消勾選
-(void) ConfirmInfoViewCheckAtIndex:(NSInteger)index { //勾選
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}
-(void) ConfirmInfoViewUnCheckAtIndex:(NSInteger)index { //取消勾選
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID removeObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}
#pragma mark - 上傳
-(void) ConfirmInfoViewUploadAtIndex:(NSInteger)index { //上傳
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    [self GotoUploadFileVCWithType:AttachTypeId AttachName:AttachTypeName];
}
#pragma mark - 列印
-(void) ConfirmInfoViewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect { //列印
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    if ([FileUrl length] == 0) { //有沒檔案，提示訊息
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];

    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    FileUrl = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)FileUrl,NULL,NULL,kCFStringEncodingUTF8));
    
    //NSLog(@"rect:%@",NSStringFromCGRect(rect));
    
    [ObjectManager showLodingHUDinView:self.view];
    
    //下載
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:FileUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        
        [ObjectManager hideHUDForView:self.view];
        
        NSData *dataForPrint = [NSData dataWithContentsOfURL:filePath];
        [self ShowPrintVCWithData:dataForPrint Rect:rect];
        
    }];
    [downloadTask resume];
}
#pragma mark - FileUrl
-(void) ConfirmInfoViewFileUrldAtIndex:(NSInteger)index { //fileUrl
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    
    //是否重套版
    /*BOOL UseTemplate = [[_dic objectForKey:@"UseTemplate"] boolValue];
    //UseTemplate = YES;
    if (UseTemplate) {
        FileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    }
    else {
    }*/
    
    if ([FileUrl length] == 0) {
        [self showAlertControllerWithMessage:@"請先上傳電子檔！"];
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];
    
    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([FileUrl rangeOfString:@".jpg"].length > 0 || [FileUrl rangeOfString:@".png"].length > 0 || [FileUrl rangeOfString:@".JPG"].length > 0 || [FileUrl rangeOfString:@".PNG"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:FileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:FileUrl Name:AttachTypeName FromTempUrl:NO];
    }
}
#pragma mark - 版型
-(void) ConfirmInfoViewTemplateAtIndex:(NSInteger)index { //版型
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    if ([TemplateFileUrl length] == 0) {
        return;
    }
    
    TemplateFileUrl = [NSString stringWithFormat:@"%@",TemplateFileUrl];
    
    //測試
    //TemplateFileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"TemplateFileUrl:%@",TemplateFileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([TemplateFileUrl rangeOfString:@".jpg"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:TemplateFileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:TemplateFileUrl Name:AttachTypeName FromTempUrl:YES];
    }
}
#pragma mark - 重新套版
-(void) ConfirmInfoViewReTemplateAtIndex:(NSInteger)index { //重新套版
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [self ReAapplyDocumentWithAttachType:AttachTypeId];
}
#pragma mark - 二行程補助查詢
-(void) ConfirmInfoViewQueryAtIndex:(NSInteger)index { //二行程補助查詢
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [self GotoExternalWebVC];
}
#pragma mark - 預覽列印
-(void) ConfirmInfoViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl { //預覽列印
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    if ([urlString length] == 0) {
        return;
    }
 
    //測試
    //TemplateFileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    //NSLog(@"TemplateFileUrl:%@",TemplateFileUrl);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([urlString rangeOfString:@".jpg"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:urlString Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:urlString Name:AttachTypeName FromTempUrl:fromTempUrl];
    }
}
#pragma mark - Button Action
#pragma mark 草稿/申請
-(IBAction) pressDraftBtn:(id)sender { //草稿儲存
    if ([arrayModifiedAttachedID count] == 0) { //防呆
        return;
    }
    
    didApply = NO;
    
    //申請下一階段(0:無動作,僅更新附件、1: 申請領牌、2:申請補助、3:地方政府申 請、4:TES政府申請、5:地方及TES政府申 請)
    [self UpdateOrderAttachList:@"0"];
}
-(IBAction) pressApplyBtn:(id)sender { //申請
    //2016.10.19 全勾才能申請
    NSInteger totalCount = 0;
    NSInteger needCount = [arrayData count];
    NSInteger noCehckIndex = -1; //哪一筆沒有打勾
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *_dic = [arrayData objectAtIndex:i];
        BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
        if (IsChecked) { //已勾
            totalCount++;
        }
        else { //未勾，判斷有沒有在手動勾選清單裡面
            BOOL didExist = NO;
            NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
            for (NSString *_attachID in arrayModifiedAttachedID) {
                NSInteger attachID = [_attachID integerValue];
                if (AttachTypeId == attachID) {
                    didExist = YES;
                    totalCount++;
                }
            }
            if (!didExist) {
                noCehckIndex = i;
                break;
            }
        }
    }
    
    if (totalCount != needCount) {
        if (noCehckIndex >= 0) { //防呆
            NSDictionary *_dic = [arrayData objectAtIndex:noCehckIndex];
            NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
            NSString *message = [NSString stringWithFormat:@"請確認「%@」已勾選！",AttachTypeName];
            [self showAlertControllerWithMessage:message];
        }
        
        return;
    }
    
    NSString *message = @"請確認是否進行「申請領牌」？";
    //Step;  //0:   待處理訂單, 1: 領牌階段, 2: 補助階段 3: TES
    if (_Step == 1) { //領牌階段按鈕應為"申請補助"
        message = @"請確認是否進行「申請補助」？";
    }
    else if (_Step == 2) { //補助階段按鈕應為"政府申請"
        message = @"請確認是否進行「政府申請」？";
    }
    
    //點擊申請領牌時，應先將畫面上的勾選文件做一次"草稿儲存的動作
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        //2016.10.30 申請領牌前要先儲存草稿
        didSaveDraftBeforeApply = YES;
        [ObjectManager showLodingHUDinView:self.view];
        [self pressDraftBtn:nil];
        
        /*didApply = YES;
        
        //申請下一階段(0:無動作,僅更新附件、1: 申請領牌、2:申請補助、3:地方政府申請、4:TES政府申請、5:地方及TES政府申 請)
        if (_Step == 0) { //0: 待處理訂單
            [self UpdateOrderAttachList:@"1"];
        }
        else if (_Step == 1) { //1: 領牌階段
            [self UpdateOrderAttachList:@"2"];
        }
        else if (_Step == 2) { //2:補助管理
            //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
            if (_stepSupplementTesOrLocal == 1) {
                [self UpdateOrderAttachList:@"3"];
            }
            else if (_stepSupplementTesOrLocal == 2) {
                [self UpdateOrderAttachList:@"4"];
            }
            else {
                [self UpdateOrderAttachList:@"5"];
            }
        }*/
    }];
    [alertController addAction:actionConfirm];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    }];
    [alertController addAction:actionCancel];
    
    [self presentViewController:alertController animated:YES completion:^(void){
    }];
}
-(void) DoApply {
    didApply = YES;
    
    //申請下一階段(0:無動作,僅更新附件、1: 申請領牌、2:申請補助、3:地方政府申請、4:TES政府申請、5:地方及TES政府申 請)
    if (_Step == 0) { //0: 待處理訂單
        [self UpdateOrderAttachList:@"1"];
    }
    else if (_Step == 1) { //1: 領牌階段
        [self UpdateOrderAttachList:@"2"];
    }
    else if (_Step == 2) { //2:補助管理
        //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
        if (_stepSupplementTesOrLocal == 1) {
            [self UpdateOrderAttachList:@"3"];
        }
        else if (_stepSupplementTesOrLocal == 2) {
            [self UpdateOrderAttachList:@"4"];
        }
        else {
            [self UpdateOrderAttachList:@"5"];
        }
    }
}
#pragma mark Email
-(IBAction)pressEmailBtn:(id)sender {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知"
                                                                             message:@"請確認是否要寄出系統上所有已簽核的文件？"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self callSendOrderContractMail];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alertController addAction:submitAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark 笑臉
-(IBAction)pressSmileBtn:(id)sender {
}
#pragma mark 囧臉
-(IBAction)pressCryBtn:(id)sender {
    didOrzFace = YES;
    didApply = YES;
    [self UpdateOrderAttachList:@"-1"];
}
#pragma mark - 換頁
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.OrderNo = _OrderNo;
    destVC.type = attachType;
    destVC.attachName = attachName;
    //destVC.dicData = [arrayData objectAtIndex:selectedIndex];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //API 309 有 Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = ownerName;
    NSString *BuyName = buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    destVC.OrderNo = _OrderNo;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentResign { //重簽
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_CONTRACT_RESIGN;
    destVC.OrderNo = _OrderNo;
    destVC.isReSign = YES;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoSearchVC { //退回搜尋VC
    //到補助選擇
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[OrderSearchRootVC class]]) {
            OrderSearchRootVC *_dest = (OrderSearchRootVC *)vc;
            _dest.shouldSearchAgain = YES;
            destVC = _dest;
        }
    }
    NSLog(@"destVC:%@",destVC);
    [self.navigationController popToViewController:destVC animated:NO];
}
-(void) GotoExternalWebVC { //3.8外部連結頁面
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    ExternalWebVC *destVC = [[ExternalWebVC alloc] initWithNibName:@"ExternalWebVC" bundle:nil];
    //[self.navigationController pushViewController:destVC animated:YES];
    destVC.attachTypeId = AttachTypeId;
    destVC.attachTypeName = AttachTypeName;
    [self.navigationController presentViewController:destVC animated:YES completion:nil];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
