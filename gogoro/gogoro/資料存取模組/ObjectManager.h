//
//  ObjectManager.h
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AESCrypt.h"
#import <AESCrypt/AESCrypt.h>

@interface ObjectManager : NSObject

#pragma mark - 網路狀態判斷
+ (BOOL)isConnectedToInternet;

#pragma mark - 顯示LodingHUD
+ (void)showLodingHUDinView:(UIView *)view;

#pragma mark - 顯示Loading + 文字 HUD
+ (void)showTextLoadingHUDinView:(UIView *)view text:(NSString *)text;

#pragma mark - 隱藏所有HUD
+ (void)hideHUDForView:(UIView *)view;

#pragma mark - 顯示成功訊息HUD
+ (void)showSuccessHUDWithStatus:(NSString *)status duration:(int)duration inView:(UIView *)view;

#pragma mark - 顯示失敗訊息HUD
+ (void)showErrorHUDWithStatus:(NSString *)status duration:(int)duration inView:(UIView *)view;

#pragma mark - 顯示無網路連結HUD
+ (void)showIsConnectedToInternetFailWithDuration:(int)duration inView:(UIView *)view;

#pragma mark - 顯示API查無資料HUD
+ (void)showNoDataWithDuration:(int)duration inView:(UIView *)view;

#pragma mark - 設定千位符號FromString
+ (NSString *) setThousandsFromNumber:(NSNumber *)number;

#pragma mark - 檢查Email格式
+ (BOOL)isValidateEmail:(NSString *)email;

#pragma mark - 檢查Password格式
+ (BOOL)isValidatePassword:(NSString *)password;

#pragma mark - 加解密
/* 加密 */
+ (NSString *)base64StringFromText:(NSString *)text;

/* 解密 */
+ (NSString *)textFromBase64String:(NSString *)base64;

#pragma mark - 雜項
+ (UIViewController*)viewController:(UIView *)myView; //回傳現在UIView所在的UIViewController
+ (void) UpdateFrameWithStringLength:(UILabel *)sender;
+ (CGSize) CalculateSizeWithText:(NSString *)text Font:(UIFont *)font Size:(CGSize)size;
@end

