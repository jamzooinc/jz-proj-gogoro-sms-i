
#import <Foundation/Foundation.h>

typedef enum {
    CONSENT_PERSONAL_Data                               = 0,    //101 首次參觀個資同意書（無版本）
    CONSENT_TEST_RIDE                                   = 1,    //102 預約試乘個資同意書（無版本）
    CONSENT_ORDER_PERSONAL_Data                         = 2,    //36 訂購個資同意書（有版本）
    CONSENT_TRANSITION_DOC                              = 3,    //151 過場文件（有版本）
    CONSENT_ORDER_CUSTOMER                              = 4,    //36 訂購契約書（無版本）
    CONSENT_ORDER_CONTRACT_PERSONAL                     = 5,    //103 合約用_個資同意書（無版本）
    CONSENT_ORDER_DEDUCT_CONTRACT                       = 6,    //補助內扣款切結書
    CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL              = 7,    //已簽名的個資同意書
    CONSENT_SIGNED_ORDER_CUSTOMER                       = 8,    //顯示已簽名的訂購單
    CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT                = 9,    //顯示已簽名的(放棄)補助切結書
//    CONSENT_GOGORO2_CONTRACT_SUPPLEMENTARY_PROVISIONS   = 10,   //Gogoro 2 合約補充條文
//    CONSENT_GOGORO2_CHECKLIST                           = 11,   //Gogoro 2 交車檢查表
    CONSENT_URL_ONLY                                    = 99,   //只給Url，直接下載檔案打開，支援Air Print
    CONSENT_CONTRACT_NEED_SIGNATURE                     = 97,   //結束簽署合約
    CONSENT_CONTRACT_REVIEW_SIGNED                      = 96,   //檢視已簽文件
    CONSENT_CONTRACT_RESIGN                             = 95,   //重簽文件
    CONSENT_TRANSFER_DOC                                = 111,  //1101 過戶說明文件
    CONSENT_TRANSFER_CONTRACT                           = 112   //轉讓需簽名同意書及順序
} enumCONSENT;

@interface DocumentManager : NSObject

+ (instancetype)shareInstance;

@property (nonatomic, strong) NSString *apiGatewayUrl;  //後台網址
/* 需要儲存的資訊 */
@property (nonatomic, strong) NSString *attachPersonalVersion;  //個資同意書版本
@property (nonatomic, strong) NSString *attachPersonalUrl;      //下載網址
@property (nonatomic, strong) NSString *attachTestDriveVersion; //試乘同意書版本
@property (nonatomic, strong) NSString *attachTestDriveUrl;     //下載網址
@property (nonatomic, strong) NSString *attachOrderVersion;     //未套版訂購契約書版本
@property (nonatomic, strong) NSString *attachOrderUrl;         //下載網址
@property (nonatomic, strong) NSString *attachTransitVersion;   //過場文件版本
@property (nonatomic, strong) NSString *attachTransitUrl;       //下載網址

-(NSString *) GetDocumentVersionWithType:(enumCONSENT) enumCONSENT;
-(void) SetDocumentVersionWithType:(enumCONSENT) enumCONSENT Version:(NSString *)version;
-(void) SetDocumentUrlWithType:(enumCONSENT) enumCONSENT Url:(NSString *)url;
@end
