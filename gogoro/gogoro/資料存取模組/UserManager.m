//
//  UserManager.m
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager {
    NSUserDefaults *userDefault;
}

/* return Singleton Instance */
+ (instancetype)shareInstance {
    
    static dispatch_once_t onceToken;
    static UserManager *_shareInstance = nil;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[UserManager alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        userDefault = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

#pragma mark - [登入後需要儲存的資訊]
#pragma mark - 使用者是否登入

- (void)setIsLogin:(BOOL)isLogin {
    [userDefault setObject:[NSNumber numberWithBool:isLogin] forKey:@"isLogin"];
    [userDefault synchronize];
}

- (BOOL)isLogin {
    return [[userDefault objectForKey:@"isLogin"] boolValue];
}

#pragma mark - 使用者帳號

- (void)setAccount:(NSString *)account {
    [userDefault setObject:account forKey:@"account"];
    [userDefault synchronize];
}

- (NSString *)account {
    return [userDefault objectForKey:@"account"];
}

#pragma mark - 使用者姓名

- (void)setStaffName:(NSString *)staffName {
    [userDefault setObject:staffName forKey:@"staffName"];
    [userDefault synchronize];
}

- (NSString *)staffName {
    return [userDefault objectForKey:@"staffName"];
}

#pragma mark - 使用者密碼

- (void)setPassword:(NSString *)password {
    [userDefault setObject:password forKey:@"password"];
    [userDefault synchronize];
}

- (NSString *)password {
    return [userDefault objectForKey:@"password"];
}

#pragma mark - 門市名稱

- (void)setStoreName:(NSString *)storeName {
    [userDefault setObject:storeName forKey:@"storeName"];
    [userDefault synchronize];
}

- (NSString *)storeName {
    return [userDefault objectForKey:@"storeName"];
}

#pragma mark - 門市代碼

- (void)setStoreID:(NSString *)storeID {
    [userDefault setObject:storeID forKey:@"storeID"];
    [userDefault synchronize];
}

- (NSString *)storeID {
    return [userDefault objectForKey:@"storeID"];
}

#pragma mark - Access Token

- (void)setAccessToken:(NSString *)accessToken {
    [userDefault setObject:accessToken forKey:@"accessToken"];
    [userDefault synchronize];
}

- (NSString *)accessToken {
    return [userDefault objectForKey:@"accessToken"];
}

- (void)clearAllUserData {
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        //某些資料不要清除，以檢查部分已下載文件時不會在下載一次
        if ([key isEqualToString:@"attachPersonalVersion"] ||
            [key isEqualToString:@"attachPersonalUrl"] ||
            [key isEqualToString:@"attachTestDriveVersion"] ||
            [key isEqualToString:@"attachTestDriveUrl"] ||
            [key isEqualToString:@"attachOrderVersion"] ||
            [key isEqualToString:@"attachOrderUrl"] ||
            [key isEqualToString:@"attachTransitVersion"] ||
            [key isEqualToString:@"attachTransitUrl"]) {
            
        }
        else {
            [defs removeObjectForKey:key];
        }
    }
    
    [defs synchronize];
}

@end
