

#import "DocumentManager.h"

@implementation DocumentManager {
    NSUserDefaults *userDefault;
}

/* return Singleton Instance */
+ (instancetype)shareInstance {
    
    static dispatch_once_t onceToken;
    static DocumentManager *_shareInstance = nil;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[DocumentManager alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        userDefault = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

#pragma mark - [登入後需要儲存的資訊]
#pragma mark - 使用者是否登入

- (void)setIsLogin:(BOOL)isLogin {
    [userDefault setObject:[NSNumber numberWithBool:isLogin] forKey:@"isLogin"];
    [userDefault synchronize];
}

- (BOOL)isLogin {
    return [[userDefault objectForKey:@"isLogin"] boolValue];
}
#pragma mark - 後台網址
- (void)setApiGatewayUrl:(NSString *)apiGatewayUrl {
    [userDefault setObject:apiGatewayUrl forKey:@"apiGatewayUrl"];
    [userDefault synchronize];
}
- (NSString *)apiGatewayUrl {
    NSString *url = [userDefault objectForKey:@"apiGatewayUrl"];
    if (!url) {
#if Jamzoo
        url = kServerDomainTest; //預設值
#else
        url = kServerDomainProduction; //預設值
#endif
        
    }
    return url;
}
#pragma mark - 個資同意書
- (void)setAttachPersonalVersion:(NSString *)attachPersonalVersion {
    [userDefault setObject:attachPersonalVersion forKey:@"attachPersonalVersion"];
    [userDefault synchronize];
}
- (NSString *)attachPersonalVersion {
    return [userDefault objectForKey:@"attachPersonalVersion"];
}
- (void)setAttachPersonalUrl:(NSString *)attachPersonalUrl {
    [userDefault setObject:attachPersonalUrl forKey:@"attachPersonalUrl"];
    [userDefault synchronize];
}
- (NSString *)attachPersonalUrl {
    return [userDefault objectForKey:@"attachPersonalUrl"];
}
#pragma mark - 試乘同意書
- (void)setAttachTestDriveVersion:(NSString *)attachTestDriveVersion {
    [userDefault setObject:attachTestDriveVersion forKey:@"attachTestDriveVersion"];
    [userDefault synchronize];
}
- (NSString *)attachTestDriveVersion {
    return [userDefault objectForKey:@"attachTestDriveVersion"];
}
- (void)setAttachTestDriveUrl:(NSString *)attachTestDriveUrl {
    [userDefault setObject:attachTestDriveUrl forKey:@"attachTestDriveUrl"];
    [userDefault synchronize];
}
- (NSString *)attachTestDriveUrl {
    return [userDefault objectForKey:@"attachTestDriveUrl"];
}
#pragma mark - 未套版訂購契約書
- (void)setAttachOrderVersion:(NSString *)attachOrderVersion {
    [userDefault setObject:attachOrderVersion forKey:@"attachOrderVersion"];
    [userDefault synchronize];
}
- (NSString *)attachOrderVersion {
    return [userDefault objectForKey:@"attachOrderVersion"];
}
- (void)setAttachOrderUrl:(NSString *)attachOrderUrl {
    [userDefault setObject:attachOrderUrl forKey:@"attachOrderUrl"];
    [userDefault synchronize];
}
- (NSString *)attachOrderUrl {
    return [userDefault objectForKey:@"attachOrderUrl"];
}
#pragma mark - 過場文件
- (void)setAttachTransitVersion:(NSString *)attachTransitVersion {
    [userDefault setObject:attachTransitVersion forKey:@"attachTransitVersion"];
    [userDefault synchronize];
}
- (NSString *)attachTransitVersion {
    return [userDefault objectForKey:@"attachTransitVersion"];
}
- (void)setAttachTransitUrl:(NSString *)attachTransitUrl {
    [userDefault setObject:attachTransitUrl forKey:@"attachTransitUrl"];
    [userDefault synchronize];
}
- (NSString *)attachTransitUrl {
    return [userDefault objectForKey:@"attachTransitUrl"];
}
#pragma mark - Get/Set
-(NSString *) GetDocumentVersionWithType:(enumCONSENT) enumCONSENT {
    NSString *stringCurrentVersion = nil;
    switch (enumCONSENT) {
        case CONSENT_PERSONAL_Data: //個資同意書
            stringCurrentVersion = [[DocumentManager shareInstance] attachPersonalVersion];
            break;
        case CONSENT_TEST_RIDE: //試乘同意書
            stringCurrentVersion = [[DocumentManager shareInstance] attachTestDriveVersion];
            break;
        case CONSENT_ORDER_PERSONAL_Data: //103 訂購個資同意書
            stringCurrentVersion = [[DocumentManager shareInstance] attachOrderVersion];
            break;
        case CONSENT_TRANSITION_DOC: //151 過場文
            stringCurrentVersion = [[DocumentManager shareInstance] attachTransitVersion];
            break;
        default:
            break;
    }
    return stringCurrentVersion;
}
-(void) SetDocumentVersionWithType:(enumCONSENT) enumCONSENT Version:(NSString *)version {
    switch (enumCONSENT) {
        case CONSENT_PERSONAL_Data: //個資同意書
            [self setAttachPersonalVersion:version];
            break;
        case CONSENT_TEST_RIDE: //試乘同意書
            [self setAttachTestDriveVersion:version];
            break;
        case CONSENT_ORDER_PERSONAL_Data: //103 訂購個資同意書
            [self setAttachOrderVersion:version];
            break;
        case CONSENT_TRANSITION_DOC: //151 過場文件
            [self setAttachTransitVersion:version];
            break;
        default:
            break;
    }
}
-(void) SetDocumentUrlWithType:(enumCONSENT) enumCONSENT Url:(NSString *)url {
    switch (enumCONSENT) {
        case CONSENT_PERSONAL_Data: //個資同意書
            [self setAttachPersonalUrl:url];
            break;
        case CONSENT_TEST_RIDE: //試乘同意書
            [self setAttachTestDriveUrl:url];
            break;
        case CONSENT_ORDER_PERSONAL_Data: //103 訂購個資同意書
            [self setAttachOrderUrl:url];
            break;
        case CONSENT_TRANSITION_DOC: //151 過場文件
            [self setAttachTransitUrl:url];
        default:
            break;
    }
}
@end
