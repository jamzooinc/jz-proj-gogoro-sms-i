//
//  ObjectManager.m
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "ObjectManager.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import <CommonCrypto/CommonCrypto.h>


@implementation ObjectManager

static const char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

#pragma mark - 網路狀態判斷
+ (BOOL)isConnectedToInternet {
    
    Reachability *pInternet = [Reachability reachabilityForInternetConnection];
    NetworkStatus status = [pInternet currentReachabilityStatus];
    if ( status == NotReachable ) {
        //NSLog(@"No Internet");
        return NO;
    } else if( status == ReachableViaWiFi ) {
        //NSLog(@"ReachableViaWiFi");
        return YES;
    } else {
        //NSLog(@"ReachableViaWWAN");
        return YES;
    }
    return NO;
}

#pragma mark - 顯示LodingHUD
+ (void)showLodingHUDinView:(UIView *)view{
    
    //NSLog(@"%@",view);
    if ([ObjectManager allHUDsForView:view].count > 0) {
        // Already have a hud view, don't show again
        return;
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = @"Loading...";
}
#pragma mark - 顯示Loading + 文字 HUD
+ (void)showTextLoadingHUDinView:(UIView *)view text:(NSString *)text{
    //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    //hud.labelText = text;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if ([text length] >= 20) {
        hud.detailsLabelText = text; //支援多行
    }
    else {
        hud.labelText = text;
    }
}
#pragma mark - 隱藏所有HUD
+ (void)hideHUDForView:(UIView *)view {
    [MBProgressHUD hideHUDForView:view animated:YES];
}

+ (NSArray *)allHUDsForView:(UIView *)view {
    
    return [MBProgressHUD allHUDsForView:view];
}
#pragma mark - 顯示成功訊息HUD
+ (void)showSuccessHUDWithStatus:(NSString *)status duration:(int)duration inView:(UIView *)view {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = status;
    [hud show:YES];
    [hud hide:YES afterDelay:duration];
}

#pragma mark - 顯示失敗訊息HUD
+ (void)showErrorHUDWithStatus:(NSString *)status duration:(int)duration inView:(UIView *)view {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Xmark.png"]];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = status;
    [hud show:YES];
    [hud hide:YES afterDelay:duration];
}
#pragma mark - 顯示無網路連結HUD
+ (void)showIsConnectedToInternetFailWithDuration:(int)duration inView:(UIView *)view{
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = @"請連結網路使用";
    [hud show:YES];
    [hud hide:YES afterDelay:duration];
}

#pragma mark - 顯示API查無資料HUD
+ (void)showNoDataWithDuration:(int)duration inView:(UIView *)view {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:hud];
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = @"查無資料";
    [hud show:YES];
    [hud hide:YES afterDelay:duration];
}

#pragma mark - 設定千位符號FromString
+ (NSString *) setThousandsFromNumber:(NSNumber *)number {
    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    return [numberFormatter stringFromNumber:number];
}

#pragma mark - 檢查Email格式
+ (BOOL)isValidateEmail:(NSString *)email{
    NSRange r;
    NSString *regEx = @"\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b";
    r = [email rangeOfString:regEx options:NSRegularExpressionSearch];
    if (r.location != NSNotFound) {
        //NSLog(@"Email is %@", [email substringWithRange:r]);
        return YES;
    }else {
        //NSLog(@"Not found.");
        return NO;
    }
}

#pragma mark - 檢查Password格式
+ (BOOL)isValidatePassword:(NSString *)password{
    NSRange r;
    NSString *regEx = @"^[0-9a-zA-Z]+$";
    r = [password rangeOfString:regEx options:NSRegularExpressionSearch];
    
    if ([password length] <= 12 &&
        [password length] >= 8 &&
        r.location != NSNotFound) {
        NSLog(@"Password is %@", [password substringWithRange:r]);
        return YES;
    }else{
        NSLog(@"Not found.");
        return NO;
    }
}

#pragma mark - 密碼加解密

/*  base64格式字串轉換NSData */
+ (NSData *)dataWithBase64EncodedString:(NSString *)string{
    if (string == nil)
        [NSException raise:NSInvalidArgumentException format:@""];
    if ([string length] == 0)
        return [NSData data];
    
    static char *decodingTable = NULL;
    if (decodingTable == NULL)
    {
        decodingTable = malloc(256);
        if (decodingTable == NULL)
            return nil;
        memset(decodingTable, CHAR_MAX, 256);
        NSUInteger i;
        for (i = 0; i < 64; i++)
            decodingTable[(short)encodingTable[i]] = i;
    }
    
    const char *characters = [string cStringUsingEncoding:NSASCIIStringEncoding];
    if (characters == NULL)     //  Not an ASCII string!
        return nil;
    char *bytes = malloc((([string length] + 3) / 4) * 3);
    if (bytes == NULL)
        return nil;
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while (YES)
    {
        char buffer[4];
        short bufferLength;
        for (bufferLength = 0; bufferLength < 4; i++)
        {
            if (characters[i] == '\0')
                break;
            if (isspace(characters[i]) || characters[i] == '=')
                continue;
            buffer[bufferLength] = decodingTable[(short)characters[i]];
            if (buffer[bufferLength++] == CHAR_MAX)      //  Illegal character!
            {
                free(bytes);
                return nil;
            }
        }
        
        if (bufferLength == 0)
            break;
        if (bufferLength == 1)      //  At least two characters are needed to produce one byte!
        {
            free(bytes);
            return nil;
        }
        
        //  Decode the characters in the buffer to bytes.
        bytes[length++] = (buffer[0] << 2) | (buffer[1] >> 4);
        if (bufferLength > 2)
            bytes[length++] = (buffer[1] << 4) | (buffer[2] >> 2);
        if (bufferLength > 3)
            bytes[length++] = (buffer[2] << 6) | buffer[3];
    }
    
    bytes = realloc(bytes, length);
    return [NSData dataWithBytesNoCopy:bytes length:length];
}

/* NSData轉換為base64格式字串 */
+ (NSString *)base64EncodedStringFrom:(NSData *)data{
    if ([data length] == 0)
        return @"";
    
    char *characters = malloc((([data length] + 2) / 3) * 4);
    if (characters == NULL)
        return nil;
    NSUInteger length = 0;
    
    NSUInteger i = 0;
    while (i < [data length])
    {
        char buffer[3] = {0,0,0};
        short bufferLength = 0;
        while (bufferLength < 3 && i < [data length])
            buffer[bufferLength++] = ((char *)[data bytes])[i++];
        
        //  Encode the bytes in the buffer to four characters, including padding "=" characters if necessary.
        characters[length++] = encodingTable[(buffer[0] & 0xFC) >> 2];
        characters[length++] = encodingTable[((buffer[0] & 0x03) << 4) | ((buffer[1] & 0xF0) >> 4)];
        if (bufferLength > 1)
            characters[length++] = encodingTable[((buffer[1] & 0x0F) << 2) | ((buffer[2] & 0xC0) >> 6)];
        else characters[length++] = '=';
        if (bufferLength > 2)
            characters[length++] = encodingTable[buffer[2] & 0x3F];
        else characters[length++] = '=';
    }
    
    return [[NSString alloc] initWithBytesNoCopy:characters length:length encoding:NSASCIIStringEncoding freeWhenDone:YES];
}

/* NSData進行DES加密 */
+ (NSData *)DESEncrypt:(NSData *)data WithKey:(NSString *)key{
    char keyPtr[kCCKeySizeAES256+1];
    bzero(keyPtr, sizeof(keyPtr));
    
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [data length];
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesEncrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeDES,
                                          NULL,
                                          [data bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesEncrypted);
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
    }
    
    free(buffer);
    return nil;
}

/*  NSData進行DES解密 */
+ (NSData *)DESDecrypt:(NSData *)data WithKey:(NSString *)key{
    char keyPtr[kCCKeySizeAES256+1];
    bzero(keyPtr, sizeof(keyPtr));
    
    [key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
    
    NSUInteger dataLength = [data length];
    
    size_t bufferSize = dataLength + kCCBlockSizeAES128;
    void *buffer = malloc(bufferSize);
    
    size_t numBytesDecrypted = 0;
    CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt, kCCAlgorithmDES,
                                          kCCOptionPKCS7Padding | kCCOptionECBMode,
                                          keyPtr, kCCBlockSizeDES,
                                          NULL,
                                          [data bytes], dataLength,
                                          buffer, bufferSize,
                                          &numBytesDecrypted);
    
    if (cryptStatus == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
    }
    
    free(buffer);
    return nil;
}

/* 加密 */
+ (NSString *)base64StringFromText:(NSString *)text{
    if (text) {
        NSString *key = [[NSBundle mainBundle] bundleIdentifier];
        NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
        data = [self DESEncrypt:data WithKey:key];
        return [self base64EncodedStringFrom:data];
    }else{
        return @"";
    }
}

/* 解密 */
+ (NSString *)textFromBase64String:(NSString *)base64{
    if (base64) {
        //取项目的bundleIdentifier作为KEY
        NSString *key = [[NSBundle mainBundle] bundleIdentifier];
        NSData *data = [self dataWithBase64EncodedString:base64];
        //IOS 自带DES解密 Begin
        data = [self DESDecrypt:data WithKey:key];
        //IOS 自带DES加密 End
        return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }else{
        return @"";
    }
}
#pragma mark - 雜項
+ (UIViewController*)viewController:(UIView *)myView { //回傳現在UIView所在的UIViewController
    for (UIView* next = [myView superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}
#pragma mark - 動態計算/調整文字分行
+(CGSize) CalculateSizeWithText:(NSString *)text Font:(UIFont *)font Size:(CGSize)size {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    
    CGSize maximumLabelSize = CGSizeMake(size.width, CGFLOAT_MAX);
    
    CGSize labelSize = [text boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:attributes context:nil].size;
    NSLog(@"labelSize:%@",NSStringFromCGSize(labelSize));
    
    return labelSize;
}
+ (void) UpdateFrameWithStringLength:(UILabel *)sender {
    CGSize labelSize = [self CalculateSizeWithText:sender.text Font:sender.font Size:sender.frame.size];
    
    NSLog(@"labelSize:%@",NSStringFromCGSize(labelSize));
    
    sender.frame = CGRectMake(sender.frame.origin.x, sender.frame.origin.y, sender.frame.size.width, labelSize.height);
}
@end
