//
//  GatewayManager.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "DocumentManager.h"

#define kStatuscode_Sucess 200
#define kStatuscode_Sucess_But_No_Content 204
#define kStatuscode_Sucess_But_No_Data 401

/* 測試 */
#define kServerDomain [DocumentManager shareInstance].apiGatewayUrl

//正式環境
#define kServerDomainProduction @"http://10.0.1.168/"

//測試環境
#define kServerDomainTest @"http://10.0.1.169/"

//開發環境
#define kServerDomainDevelop @"http://10.0.1.178/"

/* 正式 */
//#define kServerDomain @""
#pragma mark - API Url
#define kLogin @"App/Account/Login"                                      /* [001] 登入 */
#define kGetStoreList @"App/Account/GetStoreList"                        /* [002] 取得門市列表 */
#define kCustomerFind @"App/Customer/Find"                               /* [003] 搜尋客戶 */
#define kCustomerData @"App/Customer/Data"                               /* [004] 新增或修改客戶 */
#define kUploadDriverLicense @"App/Customer/UploadDriverLicense"         /* [005] 上傳客戶駕照圖檔 */
#define kGetLicensingList @"App/Account/GetLicensingList"                /* [006] 取得領牌中心列表 */
#define kGetStepState @"App/Account/GetStepState"                        /* [007] 取得處理狀況列表 */
#define kGetAccountList @"App/Account/GetAccountList"                    /* [008] 取得門市人員列表 */

#define kGetPartnerStoreList @"App/Account/GetPartnerStoreList"          /* [009] 追蹤訂單_取得通路店別列表 */

#define kSurveyGetList @"App/Survey/GetList"                             /* [010] 取得服務項目問卷列表 */

#define kSurveyGetShopSalesList @"App/Survey/GetShopSalesList"           /* [011] 取得問卷門市及服務人員列表  */

#define kGetContractVersion @"App/Contract/GetContractVersion"           /* [101] 取得非附件類表單目前版號及位址 */
#define kUploadContractSign @"App/Contract/UploadContractSign"           /* [102] 上傳同意書簽名 */
#define kReAapplyDocument @"App/Contract/ReAapplyDocument"               /* [103] 文件重新套版 */

#define kGetOrderContractList @"App/Contract/GetOrderContractList"       /* [104] 訂購簽名文件及順序 */
#define kSendOrderContractMail @"App/Contract/SendOrderContractMail"     /* [105] 訂單同意書發信 */
#define kContractGetCustomerDocument @"App/Contract/GetCustomerDocument" /* [106] 取得客戶非訂單類文件 */
#define kContractUploadContract @"App/Contract/UploadContract" /* [107] 上傳非訂單同意書 */

#define kGetOrderDetail @"App/Order/GetDetail"                           /* [301] 取得訂單 */
#define kGetSubsidyAreaList @"App/Order/GetSubsidyAreaList"              /* [302] 取得訂單補助地區選單 */
#define kGetSubsidyTypeList @"App/Order/GetSubsidyTypeList"              /* [303] 取得訂單地方補助類型選單 */
#define kGetAgencyFees @"App/Order/GetAgencyFees"                        /* [304] 取得代辦費總額 */
#define kOrderAdd @"App/Order/Add"                                       /* [305] 新增訂單(補助選擇完後呼叫) */
#define kGetContract @"App/Contract/GetContract"                         /* [306] 取得已套版非附件類表單URL */
#define kGetPreDeductContract @"App/Contract/GetPreDeductContract"       /* [307] 取得補助內扣款切結書 */
#define kUploadOrderContractSign @"App/Contract/UploadOrderContractSign" /* [308] 上傳訂購契約or補助內扣款切結書簽名 */
#define kGetOrderAttachList @"App/Order/GetOrderAttachList"              /* [309] 取得訂單附件清單 */
#define kUpdateOrderAttachList @"App/Order/UpdateOrderAttachList"        /* [310] 更新訂單附件確認清單 */
#define kUploadAttach @"App/Order/Upload"                                /* [311] 上傳訂單附件電子檔 */
#define kBacktracking @"App/Order/Backtracking"                          /* [312] 訂單回溯 */
#define kOrderDelete @"App/Order/Delete"                                 /* [313] 刪除訂單 */
#define kSingleOrder @"App/MissAttach/SingleOrder"                       /* [401] 補件上傳_單一訂單編號查詢 */ ///Order/UploadAttach
#define kRunningList @"App/MissAttach/RunningList"                       /* [402] 補件上傳_執行中訂單查詢 */
#define kSingleAttrachType @"App/MissAttach/SingleAttrachType"           /* [403] 補件上傳_單一文件批次上傳 */
#define kGetAttachList @"App/MissAttach/GetAttachList"                   /* [404] 補件上傳_取得文件資料清單 */
#define kTrackingOrderList @"App/Order/TrackingOrderList"                /* [501] 追蹤訂單_搜尋列表 */
#define kMoneyTrackingList @"App/Order/MoneyTrackingList"                /* [502] 金錢追蹤_單一訂單編號查詢*/
#define kTrackingOrderReturn @"App/Order/TrackingOrderReturn"            /* [503] 追蹤訂單_缺件退回(儲存在AppLocal端) */
#define kTransferFind @"App/Transfer/Find"                              /* [601] 搜尋過戶移轉案件 */
#define kTransferGetDetail @"App/Transfer/GetDetail"                    /* [601] 取得過戶移轉案件 */
#define kTransferGet @"App/Transfer/Get"                            /* [602] 取得過戶移轉案件 */
#define kTransferCheckEmail @"App/Transfer/CheckEmail"            /* [603] 車主email 是否已註冊 */
#define kTransferAdd @"App/Transfer/Add"            /* [604] 新增租約轉讓資料(租約轉讓資料選擇完後呼叫) */
#define kTransferGetContractList @"App/Transfer/GetContractList"            /* [605] 取得租約轉讓需簽名同意書及順序 */
#define kTransferUploadContractSign @"App/Transfer/UploadContractSign"            /* [606] 上傳租約轉讓相關同意書簽名 */
#define kTransferUpload @"App/Transfer/Upload"            /* [607] 上傳或更新過戶附件 */
#define kTransferGetAttachList @"App/Transfer/GetAttachList"            /* [608] 取得執行文件清單 */
#define kTransferGetArticleList @"App/Transfer/GetArticleList"            /* [609] 取得電馳方案列表 */
#define kTransferCheckDMSTransfer @"App/Transfer/CheckDMSTransfer"            /* [610] 是否已於DMS完成過戶 */
#define kTransferReApplyDocument @"App/Transfer/ReApplyDocument"            /* [611] 過戶文件重新套版(重簽時呼叫) */
#define kTransferSendMail @"App/Transfer/SendMail"            /* [612] 過戶文件財務發信 */

@protocol GatewayManagerDelegate <NSObject>
@optional
#pragma mark - CallBack
- (void)callBackGetStoreList:(NSDictionary *)parameter;
- (void)callBackLogin:(NSDictionary *)parameter;
- (void)callBackCustomerFind:(NSDictionary *)parameter;
- (void)callBackCustomerData:(NSDictionary *)parameter;
- (void)callBackGetContractVersion:(NSDictionary *)parameter;
- (void)callBackUploadDriverLicense:(NSDictionary *)parameter;
- (void)callBackUploadContractSign:(NSDictionary *)parameter;
- (void)callBackGetOrderDetail:(NSDictionary *)parameter;
- (void)callBackGetContract:(NSDictionary *)parameter;
- (void)callBackGetLicensingList:(NSDictionary *)parameter;
- (void)callBackGetStepState:(NSDictionary *)parameter;
- (void)callBackGetSubsidyAreaList:(NSDictionary *)parameter;
- (void)callBackGetSubsidyTypeList:(NSDictionary *)parameter;
- (void)callBackGetAgencyFees:(NSDictionary *)parameter;
- (void)callBackOrderAdd:(NSDictionary *)parameter;
- (void)callBackGetPreDeductContract:(NSDictionary *)parameter;
- (void)callBackUploadOrderContractSign:(NSDictionary *)parameter;
- (void)callBackGetOrderAttachList:(NSDictionary *)parameter;
- (void)callBackUpdateOrderAttachList:(NSDictionary *)parameter;
- (void)callBackUploadAttach:(NSDictionary *)parameter;
- (void)callBackSingleOrder:(NSDictionary *)parameter;
- (void)callBackRunningList:(NSDictionary *)parameter;
- (void)callBackSingleAttrachType:(NSDictionary *)parameter;
- (void)callBackTrackingOrderList:(NSDictionary *)parameter;
- (void)callBackReAapplyDocument:(NSDictionary *)parameter;
- (void)callBackBacktracking:(NSDictionary *)parameter;
- (void)callBackOrderDelete:(NSDictionary *)parameter;
- (void)callBackGetAttachList:(NSDictionary *)parameter;
- (void)callBackTrackingOrderReturn:(NSDictionary *)parameter;
- (void)callBackGetAccountList:(NSDictionary *)parameter;
- (void)callBackGetPartnerStoreList:(NSDictionary*)parameter;
- (void)callBackGetOrderContractList:(NSDictionary*)parameter;
- (void)callBackSendOrderContractMail:(NSDictionary*)parameter;
- (void)callBackMoneyTrackingList:(NSDictionary*)parameter;
- (void)callBacTransferFind:(NSDictionary*)parameter;
- (void)callBacTransferGetDetail:(NSDictionary*)parameter;
- (void)callBacTransferGet:(NSDictionary*)parameter;
- (void)callBacTransferCheckEmail:(NSDictionary*)parameter;
- (void)callBacTransferAdd:(NSDictionary*)parameter;
- (void)callBacTransferGetContractList:(NSDictionary*)parameter;
- (void)callBacTransferUploadContractSign:(NSDictionary*)parameter;
- (void)callBacTransferUpload:(NSDictionary*)parameter;
- (void)callBacTransferGetAttachList:(NSDictionary*)parameter;
- (void)callBacTransferGetArticleList:(NSDictionary*)parameter;
- (void)callBacTransferCheckDMSTransfer:(NSDictionary*)parameter;
- (void)callBackTransferReApplyDocument:(NSDictionary*)parameter;
- (void)callBackTransferSendMail:(NSDictionary*)parameter;
- (void)callBackSurveyGetList:(NSDictionary*)parameter;
- (void)callBackSurveyGetShopSalesList:(NSDictionary*)parameter;
- (void)callBackContractGetCustomerDocument:(NSDictionary*)parameter;
- (void)callBackContractUploadContract:(NSDictionary*)parameter;

#pragma mark 檔案下載CallBack
- (void)callBackDownloadFile:(NSError *)error;
@end

@interface GatewayManager : NSObject

#pragma mark - 呼叫Method
+ (void)callGetStoreList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callLogin:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callCustomerFind:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callCustomerData:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetContractVersion:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callUploadDriverLicense:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callUploadContractSign:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetOrderDetail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetContract:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetLicensingList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetStepState:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetSubsidyAreaList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetSubsidyTypeList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetAgencyFees:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callOrderAdd:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetPreDeductContract:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callUploadOrderContractSign:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetOrderAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callUpdateOrderAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callUploadAttach:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callSingleOrder:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callRunningList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callSingleAttrachType:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTrackingOrderList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callReAapplyDocument:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callBacktracking:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callOrderDelete:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTrackingOrderReturn:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetAccountList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetPartnerStoreList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callGetOrderContractList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callSendOrderContractMail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callMoneyTrackingList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferFind:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferGetDetail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferGet:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferCheckEmail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferAdd:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferGetContractList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferUploadContractSign:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferUpload:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferGetAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferGetArticleList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferCheckDMSTransfer:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferReApplyDocument:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callTransferSendMail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callSurveyGetList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callSurveyGetShopSalesList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callContractGetCustomerDocument:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;
+ (void)callContractUploadContract:(NSMutableDictionary *)parameters Source:(id)sourceDelegate;

#pragma mark - 檔案下載呼叫Method
+ (void) DownloadFileWithURL:(NSURL *)url FileName:(NSString *)fileName Folder:(NSString *)folder Source:(id)sourceDelegate;

#pragma mark - Old
+ (void)requestServer:(NSString *)serverPath
                  URL:(NSString *)url
           parameters:(NSDictionary *)params
              success:(void (^)(AFHTTPSessionManager *sessionManager, id responseObject))success
              failure:(void (^)(AFHTTPSessionManager *sessionManager, NSError *error))failure;

+ (void)requestServer:(NSString *)serverPath
                  URL:(NSString *)url
          contentTpye:(NSString *)contentTpye
               method:(NSString *)method
           parameters:(NSDictionary *)params
    requestSerializer:(AFHTTPRequestSerializer *)requestSerializer
   responseSerializer:(AFHTTPResponseSerializer *)responseSerializer
           htmlHeader:(NSDictionary *)authorization
      uploadImageName:(NSString *)nameString
 uploadImageDataArray:(NSArray *)imageDataArray
              success:(void (^)(AFHTTPSessionManager *sessionManager, id responseObject))success
              failure:(void (^)(AFHTTPSessionManager *sessionManager, NSError *error))failure;
@end
