//
//  GatewayManager.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "GatewayManager.h"
#import "ObjectManager.h"
#import "AFSecurityPolicy.h"
#import "MyAFHTTPRequestSerializer.h"

@interface GatewayManager () {
    
}
@end

@implementation GatewayManager

#pragma mark - 共用
+ ( void )safeCallback:( id )sourceDelegate Selector:( SEL )selector Parameter:( NSDictionary * )pDic {
    if ( sourceDelegate && [sourceDelegate respondsToSelector:selector] ) {
        IMP imp = [sourceDelegate methodForSelector:selector];
        void (*func)(id, SEL, NSDictionary *) = (void *)imp;
        func(sourceDelegate, selector, pDic);
    }
}
+(NSDictionary *) GetErrorParameter {
    NSDictionary *parameter = [NSDictionary dictionaryWithObjectsAndKeys:@"500",@"SysCode",
                               @"請確認您的連線是否正常",@"SysMsg",
                               nil];
    return parameter;
}
+ (void) RunWithPath:(NSString *)apiPath Parameter:(NSDictionary *)parameter CallBack:(SEL)callbackSelector Source:(id)sourceDelegate {
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kServerDomain, apiPath];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameter];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];
    
    NSLog(@"呼叫網址:%@",urlString);
    NSLog(@"參數:%@",_parameter);
    
    //以下測試
    /*NSArray *allkeys = [parameter allKeys];
    NSArray *allValues = [parameter allValues];
    if ([allkeys count] > 0) {
        urlString = [urlString stringByAppendingString:@"?"];
    }
    for (NSInteger i = 0; i < [allValues count]; i++) {
        NSString *key = [allkeys objectAtIndex:i];
        NSString *value = [allkeys objectAtIndex:i];
        NSString *string = [NSString stringWithFormat:@"%@=%@",key,value];
        urlString = [urlString stringByAppendingString:string];
        if (i != [allkeys count] - 1) {
            urlString = [urlString stringByAppendingString:@"&"];
        }
    }
    NSLog(@"urlString:%@",urlString);
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    [request setTimeoutInterval:60*3];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
        }
    }];
    [dataTask resume];*/

    /*NSMutableURLRequest *request = [[MyAFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:_parameter error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
        }
    }];
    [dataTask resume];*/
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForRequest = 60 * 30; //180
    //NSLog(@"timeout1:%f",configuration.timeoutIntervalForRequest);
    configuration.timeoutIntervalForResource = 60 * 30; //180
    //NSLog(@"timeout2:%f",configuration.timeoutIntervalForResource);
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    //AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
   
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //manager.requestSerializer = [MyAFHTTPRequestSerializer serializer];
    //NSLog(@"timeout:%f",manager.requestSerializer.timeoutInterval);
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setTimeoutInterval:60.0 * 30]; //60.0 * 3
    
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager.requestSerializer clearAuthorizationHeader];
    
    [manager GET:urlString
      parameters:_parameter
        progress:^(NSProgress * _Nonnull uploadProgress) {
            //
        }
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             //
             //if (success) {
             //success(manager, responseObject);
             //}
              NSLog(@"[API] 回傳成功: %@", responseObject);
             [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             //if (failure) {
             //failure(manager, error);
             //}
             NSLog(@"[API] 回傳錯誤: %@", error);
             [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
         }];
}
+ (void) RunPostWithPath:(NSString *)apiPath Parameter:(NSDictionary *)parameter CallBack:(SEL)callbackSelector Source:(id)sourceDelegate {
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", kServerDomain, apiPath];
    
    /*NSArray *allKeys = [parameter allKeys];
    for (int i = 0; i < [allKeys count]; i++) {
        NSString *key = [allKeys objectAtIndex:i];
        NSString *value = [parameter objectForKey:key];
        
        if (i == 0) {
            urlString = [urlString stringByAppendingFormat:@"?%@=%@",key,value];
        }
        else {
            urlString = [urlString stringByAppendingFormat:@"&%@=%@",key,value];
        }
    }*/
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameter];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];
    
    NSLog(@"呼叫網址:%@",urlString);
    NSLog(@"參數:%@",_parameter);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.timeoutIntervalForRequest = 60.0 * 30;
    //NSLog(@"timeout1:%f",configuration.timeoutIntervalForRequest);
    configuration.timeoutIntervalForResource = 60.0 * 30;
    //NSLog(@"timeout2:%f",configuration.timeoutIntervalForResource);
    
    //AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager.requestSerializer setTimeoutInterval:60.0 * 30];
    
    //manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    
    [manager.requestSerializer clearAuthorizationHeader];
    
    [manager POST:urlString
      parameters:_parameter
        progress:^(NSProgress * _Nonnull uploadProgress) {
            //
        }
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             //
             //if (success) {
             //success(manager, responseObject);
             //}
             NSLog(@"[API] 回傳: %@", responseObject);
             [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
         }
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             //if (failure) {
             //failure(manager, error);
             //}
             NSLog(@"[API] 回傳錯誤: %@", error);
             [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
         }];
}

#pragma mark - 呼叫Method-GET
+ (void)callGetStoreList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetStoreList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetStoreList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetContractVersion:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetContractVersion:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetContractVersion];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetOrderDetail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetOrderDetail:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetOrderDetail];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetContract:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetContract:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetContract];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetLicensingList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetLicensingList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetLicensingList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetStepState:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetStepState:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetStepState];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetSubsidyAreaList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetSubsidyAreaList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetSubsidyAreaList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetSubsidyTypeList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetSubsidyTypeList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetSubsidyTypeList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetAgencyFees:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetAgencyFees:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetAgencyFees];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetPreDeductContract:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetPreDeductContract:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetPreDeductContract];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetOrderAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetOrderAttachList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetOrderAttachList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callSingleOrder:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackSingleOrder:);
    NSString *path = [NSString stringWithFormat:@"%@",kSingleOrder];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callRunningList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackRunningList:);
    NSString *path = [NSString stringWithFormat:@"%@",kRunningList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callSingleAttrachType:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackSingleAttrachType:);
    NSString *path = [NSString stringWithFormat:@"%@",kSingleAttrachType];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTrackingOrderList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackTrackingOrderList:);
    NSString *path = [NSString stringWithFormat:@"%@",kTrackingOrderList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetAttachList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetAttachList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetAccountList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetAccountList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetAccountList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callGetPartnerStoreList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate{
    SEL callbackSelector = @selector(callBackGetPartnerStoreList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetPartnerStoreList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callMoneyTrackingList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackMoneyTrackingList:);
    NSString *path = [NSString stringWithFormat:@"%@",kMoneyTrackingList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTrackingOrderReturn:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackTrackingOrderReturn:);
    NSString *path = [NSString stringWithFormat:@"%@",kTrackingOrderReturn];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferFind:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferFind:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferFind];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferGetDetail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferGetDetail:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferGetDetail];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferGet:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferGet:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferGet];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferCheckEmail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferCheckEmail:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferCheckEmail];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferGetContractList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferGetContractList:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferGetContractList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferGetAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferGetAttachList:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferGetAttachList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferGetArticleList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferGetArticleList:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferGetArticleList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferCheckDMSTransfer:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferCheckDMSTransfer:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferCheckDMSTransfer];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callSurveyGetList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackSurveyGetList:);
    NSString *path = [NSString stringWithFormat:@"%@",kSurveyGetList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callSurveyGetShopSalesList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackSurveyGetShopSalesList:);
    NSString *path = [NSString stringWithFormat:@"%@",kSurveyGetShopSalesList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callContractGetCustomerDocument:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackContractGetCustomerDocument:);
    NSString *path = [NSString stringWithFormat:@"%@",kContractGetCustomerDocument];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
#pragma mark - 呼叫Method-POST
+ (void)callLogin:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackLogin:);
    NSString *path = [NSString stringWithFormat:@"%@",kLogin];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callCustomerFind:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackCustomerFind:);
    NSString *path = [NSString stringWithFormat:@"%@",kCustomerFind];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callCustomerData:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackCustomerData:);
    NSString *path = [NSString stringWithFormat:@"%@",kCustomerData];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callOrderAdd:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackOrderAdd:);
    NSString *path = [NSString stringWithFormat:@"%@",kOrderAdd];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callUpdateOrderAttachList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackUpdateOrderAttachList:);
    NSString *path = [NSString stringWithFormat:@"%@",kUpdateOrderAttachList];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callReAapplyDocument:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackReAapplyDocument:);
    NSString *path = [NSString stringWithFormat:@"%@",kReAapplyDocument];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callBacktracking:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackBacktracking:);
    NSString *path = [NSString stringWithFormat:@"%@",kBacktracking];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callOrderDelete:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackOrderDelete:);
    NSString *path = [NSString stringWithFormat:@"%@",kOrderDelete];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferAdd:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferAdd:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferAdd];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferUploadContractSign:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferUploadContractSign:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kTransferUploadContractSign];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];
    
    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //簽名1圖檔(原車主)
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempOwnerSign"]];
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名1圖檔(原車主)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"OwnerSign" fileName:@"filename1.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名2圖檔(新車主)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempNewOwnerSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名2圖檔(新車主)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"NewOwnerSign" fileName:@"filename2.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名3圖檔(新車主代理人-->用法定代理人)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempLawSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名3圖檔(新車主代理人-->用法定代理人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"NewOwnerAgentSign" fileName:@"filename3.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名4圖檔(原車主委託人)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempOwnerDelegateSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名4圖檔(原車主委託人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"OwnerAgentSign" fileName:@"filename4.jpg" mimeType:@"image/jpeg" error:nil]; //OwnerDelegateSign
        }
        
        //簽名5圖檔(新車主委託人)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempNewOwnerDelegateSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名5圖檔(新車主委託人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"NewOwnerAgentSign" fileName:@"filename5.jpg" mimeType:@"image/jpeg" error:nil]; //NewOwnerDelegateSign
        }
        
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                  }];
    
    [uploadTask resume];
}
+ (void)callTransferUpload:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBacTransferUpload:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kTransferUpload];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //取出要上傳的檔案
    NSString *filename = [parameters objectForKey:@"filename"];
    [parameters removeObjectForKey:@"filename"];
    
    //mime type
    NSString *mimeType = @"";
    if ([filename rangeOfString:@"jpg"].length > 0) {
        mimeType = @"image/jpeg";
    }
    else if ([filename rangeOfString:@"png"].length > 0) {
        mimeType = @"image/png";
    }
    else if ([filename rangeOfString:@"pdf"].length > 0) {
        mimeType = @"application/pdf";
    }
    NSLog(@"mimeType:%@",mimeType);
    
    //本地端路徑
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];
    
    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //附件電子檔 todo
        //NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的附件檔案大小:%lld",fileSize);
            //[formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"files" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"files" fileName:filename mimeType:mimeType error:nil];
        }
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                      
                      //刪除暫存檔
                      [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
                  }];
    
    [uploadTask resume];
}
+ (void)callUploadDriverLicense:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackUploadDriverLicense:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kUploadDriverLicense];
    //[self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
    
    NSError *attributesError;
    NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
    NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
    long long fileSize = [fileSizeNumber longLongValue];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];
    
    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    NSLog(@"要上傳的圖檔檔案大小:%lld",fileSize);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"file" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                  }];
    
    [uploadTask resume];
}
+ (void)callUploadContractSign:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackUploadContractSign:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kUploadContractSign];
    //[self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];

    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //簽名1圖檔(立書同意人)
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempMySign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名1圖檔(立書同意人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"CustomerSign" fileName:@"filename1.jpg" mimeType:@"image/jpeg" error:nil]; //Sign1
        }
        
        //簽名2圖檔(法定代理人)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempLawSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名2圖檔(法定代理人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"AgentSign" fileName:@"filename2.jpg" mimeType:@"image/jpeg" error:nil]; //Sign2
        }
        
        //駕照圖檔(試乘同意書須回傳)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的駕照圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"DriverLicense" fileName:@"filename3.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                  }];
    
    [uploadTask resume];
}
+ (void)callUploadOrderContractSign:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackUploadOrderContractSign:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kUploadOrderContractSign];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];

    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //買方跟立書同意人不會同時存在
        //簽名1圖檔(買方)
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempBuyerSign"]];
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名1圖檔(買方)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"BuySign" fileName:@"filename1.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名1圖檔(立書同意人)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempMySign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名1圖檔(立書同意人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"CustomerSign" fileName:@"filename1.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名2圖檔(法定代理人)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempLawSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名2圖檔(法定代理人)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"AgentSign" fileName:@"filename2.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名3圖檔(店長)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempStoreOwnerSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名3圖檔(店長)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"MasterSign" fileName:@"filename3.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
        //簽名4圖檔(銷售)
        imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempSalesSign"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的簽名4圖檔(銷售)圖檔大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"SalesSign" fileName:@"filename4.jpg" mimeType:@"image/jpeg" error:nil];
        }
        
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                  }];
    
    [uploadTask resume];
}
+ (void)callUploadAttach:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackUploadAttach:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kUploadAttach];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //取出要上傳的檔案
    NSString *filename = [parameters objectForKey:@"filename"];
    [parameters removeObjectForKey:@"filename"];
    
    //mime type
    NSString *mimeType = @"";
    if ([filename rangeOfString:@"jpg"].length > 0) {
        mimeType = @"image/jpeg";
    }
    else if ([filename rangeOfString:@"png"].length > 0) {
        mimeType = @"image/png";
    }
    else if ([filename rangeOfString:@"pdf"].length > 0) {
        mimeType = @"application/pdf";
    }
    NSLog(@"mimeType:%@",mimeType);
    
    //本地端路徑
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];

    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //附件電子檔 todo
        //NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的附件檔案大小:%lld",fileSize);
            //[formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"files" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"files" fileName:filename mimeType:mimeType error:nil];
        }
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                      
                      //刪除暫存檔
                      [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
                  }];
    
    [uploadTask resume];
}
+ (void)callContractUploadContract:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackContractUploadContract:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSString *path = [NSString stringWithFormat:@"%@%@",kServerDomain,kContractUploadContract];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //取出要上傳的檔案
    NSString *filename = [parameters objectForKey:@"filename"];
    [parameters removeObjectForKey:@"filename"];
    
    //mime type
    NSString *mimeType = @"";
    if ([filename rangeOfString:@"jpg"].length > 0) {
        mimeType = @"image/jpeg";
    }
    else if ([filename rangeOfString:@"png"].length > 0) {
        mimeType = @"image/png";
    }
    else if ([filename rangeOfString:@"pdf"].length > 0) {
        mimeType = @"application/pdf";
    }
    NSLog(@"mimeType:%@",mimeType);
    
    //本地端路徑
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
    
    NSMutableDictionary *_parameter = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if ([UserManager shareInstance].accessToken) {
        [_parameter setObject:[UserManager shareInstance].accessToken forKey:@"accessToken"];
    }
    [_parameter setObject:@"ipad" forKey:@"os"];
    
    NSLog(@"呼叫網址:%@",path);
    NSLog(@"參數:%@",_parameter);
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:_parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //附件電子檔 todo
        //NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:imagePath error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            NSLog(@"要上傳的附件檔案大小:%lld",fileSize);
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"file" fileName:filename mimeType:mimeType error:nil];
        }
    } error:nil];
    
    [request setTimeoutInterval:60 * 30];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      NSLog(@"上傳百分比:%.2f",uploadProgress.fractionCompleted * 100);
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
                      } else {
                          //NSLog(@"%@ %@", response, responseObject);
                          [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:responseObject];
                      }
                      
                      //刪除暫存檔
                      [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
                  }];
    
    [uploadTask resume];
}
+ (void)callGetOrderContractList:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackGetOrderContractList:);
    NSString *path = [NSString stringWithFormat:@"%@",kGetOrderContractList];
    [self RunWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}

+ (void)callSendOrderContractMail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate{
    SEL callbackSelector = @selector(callBackSendOrderContractMail:);
    NSString *path = [NSString stringWithFormat:@"%@",kSendOrderContractMail];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferReApplyDocument:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackTransferReApplyDocument:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferReApplyDocument];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}
+ (void)callTransferSendMail:(NSMutableDictionary *)parameters Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackTransferSendMail:);
    NSString *path = [NSString stringWithFormat:@"%@",kTransferSendMail];
    [self RunPostWithPath:path Parameter:parameters CallBack:callbackSelector Source:sourceDelegate];
}

#pragma mark - 檔案下載呼叫Method
+ (void)DownloadFileWithURL:(NSURL *)url FileName:(NSString *)fileName Folder:(NSString *)folder Source:(id)sourceDelegate {
    SEL callbackSelector = @selector(callBackDownloadFile:);
    
    if (![ObjectManager isConnectedToInternet]) {
        [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:[self GetErrorParameter]];
        return;
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = url;
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress *downloadProgress) {
        // This is not called back on the main queue.
        // You are responsible for dispatching to the main queue for UI updates
        NSLog(@"下載百分比:%.2f",downloadProgress.fractionCompleted * 100);
        dispatch_async(dispatch_get_main_queue(), ^{
            //Update the progress view
            //[progressView setProgress:uploadProgress.fractionCompleted];
        });
    } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);

        //把下載的檔案存到資料夾中
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        NSString *originalPath = [documentsPath stringByAppendingPathComponent:response.suggestedFilename];
        //NSLog(@"originalPath:%@",originalPath);
        
        NSString *destFilePath = [documentsPath stringByAppendingPathComponent:folder];
        //NSLog(@"destFilePath:%@",destFilePath);
        
        NSString *destFilename = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.pdf",folder,fileName]];
        //NSLog(@"destFilename:%@",destFilename);
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:originalPath]){
            if ([[NSFileManager defaultManager] fileExistsAtPath:destFilePath]){
                NSError *errorMove;
                BOOL result = [[NSFileManager defaultManager] moveItemAtPath:originalPath toPath:destFilename error:&errorMove];
                if (!result) {
                    NSLog(@"儲存檔案錯誤:%@",[errorMove localizedDescription]);
                    [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:errorMove];
                }
                else {
                    [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:nil];
                }
            }
            else {
                
            }
        }
        else {
            [GatewayManager safeCallback:sourceDelegate Selector:callbackSelector Parameter:nil];
        }
    }];
    
    [downloadTask resume];
    /*NSURLRequest *request = [NSURLRequest requestWithURL:url];
     AFHTTPRequestOperation *operator = [[AFHTTPRequestOperation alloc] initWithRequest:request];
     [operator setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
     NSLog(@"url:%@",url);
     [ToogetImageManager saveImage:[UIImage imageWithData:responseObject] WithFileName:fileName Folder:folder];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
     NSLog(@"Error:%@ %@", error,fileName);
     }];
     [operator start];*/
}

#pragma mark - Old
+ (void)requestServer:(NSString *)serverPath
                  URL:(NSString *)url
           parameters:(NSDictionary *)params
              success:(void (^)(AFHTTPSessionManager *sessionManager, id responseObject))success
              failure:(void (^)(AFHTTPSessionManager *sessionManager, NSError *error))failure{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];

    manager.requestSerializer = [AFHTTPRequestSerializer serializer];

    manager.responseSerializer = [AFJSONResponseSerializer serializer];

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];

    [manager.requestSerializer clearAuthorizationHeader];
    [manager POST:[serverPath stringByAppendingString:url]
       parameters:params
         progress:^(NSProgress * _Nonnull uploadProgress) {
             //
         }
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              //
              if (success) {
                  success(manager, responseObject);
              }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              if (failure) {
                  failure(manager, error);
              }
          }];

}
/*
+ (void)requestServer:(NSString *)serverPath
                  URL:(NSString *)url
           parameters:(NSDictionary *)params
              success:(void (^)(AFHTTPSessionManager *sessionManager, id responseObject))success
              failure:(void (^)(AFHTTPSessionManager *sessionManager, NSError *error))failure
{
    [self requestServer:serverPath
                    URL:url
            contentTpye:@"text/html"
                 method:@"POST"
             parameters:params
      requestSerializer:[AFHTTPRequestSerializer serializer]
     responseSerializer:[AFJSONResponseSerializer serializer]
             htmlHeader:nil
        uploadImageName:nil
   uploadImageDataArray:nil
                success:^(AFHTTPSessionManager *sessionManager, id responseObject) {
                    //
                    if (success) {
                        success(sessionManager, responseObject);
                    }

                } failure:^(AFHTTPSessionManager *sessionManager, NSError *error) {
                    //
                    if (failure) {
                        failure(sessionManager,error);
                    }
                }];

}
*/
+ (void)requestServer:(NSString *)serverPath
                  URL:(NSString *)url
          contentTpye:(NSString *)contentTpye
               method:(NSString *)method
           parameters:(NSDictionary *)params
    requestSerializer:(AFHTTPRequestSerializer *)requestSerializer
   responseSerializer:(AFHTTPResponseSerializer *)responseSerializer
           htmlHeader:(NSDictionary *)authorization
      uploadImageName:(NSString *)nameString
 uploadImageDataArray:(NSArray *)imageDataArray
              success:(void (^)(AFHTTPSessionManager *sessionManager, id responseObject))success
              failure:(void (^)(AFHTTPSessionManager *sessionManager, NSError *error))failure
{
    //    NSLog(@"%s url - %@", __PRETTY_FUNCTION__, [serverPath stringByAppendingString:url]);

    NSMutableDictionary *mutableParams = params.mutableCopy;
    [mutableParams setObject:@"iOS" forKey:@"platform"];

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];

    manager.requestSerializer = requestSerializer;

    manager.responseSerializer = responseSerializer;

    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:contentTpye];

    [manager.requestSerializer clearAuthorizationHeader];
    if (authorization) {
        [manager.requestSerializer setValue:[authorization objectForKey:@"Authorization"] forHTTPHeaderField:@"Authorization"];
    }

    if ([method isEqualToString:@"POST"]) {
        if (imageDataArray) {
            [manager POST:[serverPath stringByAppendingString:url]
               parameters:params
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    for (int index=0; index<[imageDataArray count]; index++) {
        NSData *imageData = [imageDataArray objectAtIndex:index];
        //        NSString *name = [NSString stringWithFormat:@"image_%d", index];
        //        NSString *fileName = [NSString stringWithFormat:@"image_%d.png", index];
        [formData appendPartWithFileData:imageData name:nameString fileName:@"image.png" mimeType:@"image/png"];
                }
                }progress:^(NSProgress * _Nonnull uploadProgress) {
                    //
                } success:^(NSURLSessionDataTask *task, id responseObject) {
                    if (success) {
                        success(manager, responseObject);
                    }
                } failure:^(NSURLSessionDataTask *task, NSError *error) {
                    if (failure) {
                        failure(manager, error);
                    }
                }];
        } else {
            [manager POST:[serverPath stringByAppendingString:url]
               parameters:mutableParams
                 progress:^(NSProgress * _Nonnull uploadProgress) {
                     //
                 }success:^(NSURLSessionDataTask *task, id responseObject) {
                      if (success) {
                          success(manager, responseObject);
                      }
                  } failure:^(NSURLSessionDataTask *task, NSError *error) {
                      if (failure) {
                          failure(manager, error);
                      }
                  }];
        }
    } else if ([method isEqualToString:@"GET"]) {
        [manager GET:[serverPath stringByAppendingString:url]
          parameters:mutableParams
            progress:^(NSProgress * _Nonnull uploadProgress) {
                //
            }success:^(NSURLSessionDataTask *task, id responseObject) {
                 if (success) {
                     success(manager, responseObject);
                 }
             } failure:^(NSURLSessionDataTask *task, NSError *error) {
                 if (failure) {
                     failure(manager, error);
                 }
             }];
    }
}


@end
