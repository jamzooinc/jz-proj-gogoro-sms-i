//
//  UserManager.h
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserManager : NSObject

+ (instancetype)shareInstance;

/* 登入後需要儲存的資訊 */
@property (nonatomic) BOOL isLogin;                             //是否登入
@property (nonatomic, strong) NSString *account;                //登入帳號
@property (nonatomic, strong) NSString *staffName;              //員工姓名
@property (nonatomic, strong) NSString *password;               //登入密碼
@property (nonatomic, strong) NSString *storeName;              //登入門市
@property (nonatomic, strong) NSString *storeID;                //登入門市代碼
@property (nonatomic, strong) NSString *accessToken;            //Access Token

- (void)clearAllUserData;

@end
