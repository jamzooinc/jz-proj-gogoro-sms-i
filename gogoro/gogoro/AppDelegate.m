//
//  AppDelegate.m
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginVC.h"
#import "MainVC.h"
#import "GogoroNavigationController.h"
#import "SlideMenuVC.h"
#import <MMDrawerController/MMDrawerController.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    /* Init */
    //LoginVC *loginVC = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    
    MainVC *mainVC = [[MainVC alloc] initWithNibName:@"MainVC" bundle:nil];
    GogoroNavigationController *gogoroNavigationController = [[GogoroNavigationController alloc] initWithRootViewController:mainVC];
    SlideMenuVC *slideMenuVC = [[SlideMenuVC alloc] initWithNibName:@"SlideMenuVC" bundle:nil];
    
    MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:gogoroNavigationController leftDrawerViewController:slideMenuVC]; //leftNavigationController
    drawerController.maximumLeftDrawerWidth = ScreenWidth / 2;
    drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeCustom;
    drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
    drawerController.showsShadow = NO;

    /* Create a UIWindow */
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window setRootViewController:drawerController]; //loginVC drawerController
    [self.window makeKeyAndVisible];
    
    /* StatusBarStyle */
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    //呼叫登入已更新accessToken
    [self callLogin];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {

    //為了拍照時支援直向
    return UIInterfaceOrientationMaskAll;
}
#pragma mark - Google Sign-In
- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    /*NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;*/
    // ...
}
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}
#pragma mark - Call API
#pragma mark [001] 登入
- (void)callLogin {
    //判斷之前是否已登入
    if ([UserManager shareInstance].isLogin) {
        NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
        
        @try {
            [parameter setObject:[UserManager shareInstance].account forKey:@"account"];
            [parameter setObject:[UserManager shareInstance].password forKey:@"password"];
            //[parameter setObject:[UserManager shareInstance].storeID forKey:@"storeId"];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
        
        [GatewayManager callLogin:parameter Source:self];
    }
}
- (void)callBackLogin:(NSDictionary *)parameter {
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    //NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSString *accessToken = [parameter objectForKey:@"Data"];
        
        NSDictionary *dataDic = [parameter objectForKey:@"Data"];
        NSString *accessToken = dataDic[@"AccessToken"];
        NSString *storeName = [NSString stringWithFormat:@"%@",dataDic[@"StoreName"]];
        
        //相關資料存起來
        [UserManager shareInstance].accessToken = accessToken;
        [UserManager shareInstance].storeName = storeName;
        [UserManager shareInstance].staffName = dataDic[@"Name"];
        
        //accessToken存起來
        //[UserManager shareInstance].isLogin = YES;
        //[UserManager shareInstance].accessToken = accessToken;
    }
    else {
        [UserManager shareInstance].isLogin = NO;
        //[self showAlertControllerWithMessage:Sysmsg];
    }
}
@end
