

#import "WriteRideDataListVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "WriteFirstDataListVCCell.h"
#import "PhotographVC.h"
#import "PhotoViewerVC.h"
#import "UploadFileVC.h"

@interface WriteRideDataListVC () {
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
    BOOL didUpdateCustomer; //YES:作更新  NO:作新增
}
@property (nonatomic,weak) IBOutlet WriteRideDataListView *ListView;
@end

@implementation WriteRideDataListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationItemTitle:@"預約試乘" Image:[UIImage imageNamed:@"icn_res.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayTableView = [[NSMutableArray alloc] init];
    for (NSDictionary *_dic in _Customer) {
        NSMutableDictionary *dic1 = [NSMutableDictionary dictionaryWithDictionary:_dic];
        [arrayTableView addObject:dic1];
    }
    _ListView.didFromList = YES;
     [_ListView UpdateUIWithData:arrayTableView];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction
/* 送出 */
- (IBAction)submitButtonClick:(id)sender {
    //新增
    [self callCustomerData];
}
#pragma mark - 列表選擇
-(void) CustomerPhotoAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:index];
    NSString *urlString = [_dic objectForKey:@"DriverLicense"];
    [self GotoPhotoViewerWithUrl:urlString];
}
-(void) CustomerSelectAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    //NSDictionary *_dic = [arrayTableView objectAtIndex:index];
    
    //[_dic objectForKey:@"Name"]
    NSString *name = [NSString stringWithFormat:@"姓名：%@",_stringName];
    
    //[_dic objectForKey:@"Email"]
    NSString *email= [NSString stringWithFormat:@"Email：%@",_stringEmail];
    
    //[_dic objectForKey:@"Phone"]
    NSString *phone = [NSString stringWithFormat:@"電話：%@",_stringPhone];
    NSString *message = [NSString stringWithFormat:@"確認要更新以下資訊\n\n%@\n%@\n%@",name,email,phone];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self callCustomerDataUpdate];
    }];
    UIAlertAction *actionCacnel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
    }];
    
    [alertController addAction:actionConfirm];
    [alertController addAction:actionCacnel];
    [self presentViewController:alertController animated:YES completion:^(void){
    }];
}
#pragma mark - Call API
#pragma mark [004] 新增或修改客戶
- (void)callCustomerData {
    didUpdateCustomer = NO;
    
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:@"" forKey:@"id"];
    [parameter setObject:_stringName forKey:@"name"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callCustomerDataUpdate {
    didUpdateCustomer = YES;
    
    [ObjectManager showLodingHUDinView:self.view];
    
    //用選到的ID+前一頁輸入的資料跟後台update
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *customerId = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Id"]];
  
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:customerId forKey:@"id"];
    [parameter setObject:_stringName forKey:@"name"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *customerID = [parameter objectForKey:@"Data"];
        
        if (didUpdateCustomer) { //更新成功，判斷有沒有駕照圖檔
            //NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
            
            //判斷有沒有駕照圖檔 2016.05.30取消判斷，一律都先拍照
            /*BOOL hasLicenseImage = [[_dic objectForKey:@"DriverLicense"] length];
            if (hasLicenseImage) { //有駕照
                [self GotoConsentPageWithCustomerID:customerID];
            }
            else { //沒駕照
                [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
            }*/
            [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
        }
        else { //新增成功
            [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoTakePhotoWithCustomerID:(NSString *)customerID { //拍照上傳
    //2016.05.26 改成跟其他文件上傳方式一致
    PhotographVC *destVC = [[PhotographVC alloc] initWithNibName:@"PhotographVC" bundle:nil];
    destVC.customerID = customerID;
    [self.navigationController pushViewController:destVC animated:YES];
    
    /*UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.customerID = customerID;
    [self.navigationController pushViewController:destVC animated:YES];*/
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString { //瀏覽駕照
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentPageWithCustomerID:(NSString *)customerID { //個資同意書
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_TEST_RIDE;
    consentVC.customerID = customerID;
    [self.navigationController pushViewController:consentVC animated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CustomerPhotoAtIndex:) name:@"kNotification_TestDrive_Customer_Photo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CustomerSelectAtIndex:) name:@"kNotification_TestDrive_Customer_Select" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_TestDrive_Customer_Photo" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_TestDrive_Customer_Select" object:nil];
}
@end
