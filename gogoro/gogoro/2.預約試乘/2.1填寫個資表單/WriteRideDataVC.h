//
//  WriteRideDataVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WriteRideDataListView.h"

@interface WriteRideDataVC : TemplateVC <UITextFieldDelegate,WriteRideDataListViewDelegate>

- (IBAction)submitButtonClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewInScrollView;

@end
