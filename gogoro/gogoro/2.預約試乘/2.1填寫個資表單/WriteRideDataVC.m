//
//  WriteRideDataVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "WriteRideDataVC.h"
#import "WriteRideDataListVC.h"
#import "PhotographVC.h"
#import "PhotoViewerVC.h"
#import "ConsentVC.h"

@interface WriteRideDataVC () {
    BOOL isSearch; //是否執行搜尋
    NSMutableArray *arrayCustomers;
    NSMutableArray *arrayListItem; //搜尋的類別項目
    NSInteger selectedIndex_ListItem;
    
    NSInteger selectedIndex;
}
@property (nonatomic,weak) IBOutlet UIView *ViewSearch;
@property (nonatomic,weak) IBOutlet UILabel *LabelItem;
@property (nonatomic,weak) IBOutlet UIButton *ButtonClear;
@property (nonatomic,weak) IBOutlet UIButton *ButtonSearch;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (nonatomic,weak) IBOutlet WriteRideDataListView *ListView;
@end

@implementation WriteRideDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _ViewSearch.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_all"]];
    
    [self setupNavigationItemTitle:@"預約試乘" Image:[UIImage imageNamed:@"icn_res.png"]];
    
#if TARGET_IPHONE_SIMULATOR
    _nameTextField.text = @"Aaron";
    _emailTextField.text = @"Aaron@123.com";
    _phoneTextField.text = @"0982123123";
#endif
    
    arrayCustomers = [[NSMutableArray alloc] init];
    
    //搜尋的類別項目
    selectedIndex_ListItem = -1;
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"name",@"key",@"姓名",@"displayName", nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"phone",@"key",@"電話",@"displayName", nil];
    NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"email",@"key",@"Email",@"displayName", nil];
    arrayListItem = [[NSMutableArray alloc] initWithObjects:dic1,dic2,dic3,nil];
    
    /* 手勢 */
    UISwipeGestureRecognizer *swipeUp;
    UISwipeGestureRecognizer *swipeDown;
    
    //設定所要偵測的UIView與對應的函式
    swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];

    //設定偵測手勢的類型
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];    //向上滑
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];  //向下滑
    
    //將辨識的機制加入
    [self.view addGestureRecognizer:swipeUp];
    [self.view addGestureRecognizer:swipeDown];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
#pragma mark - 上下滑動
- (void)handleSwipeUp:(UISwipeGestureRecognizer *)recognizer {
    _ListView.hidden = YES;
    
    //隱藏搜尋列
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _ViewSearch.frame = CGRectMake(0, 21, _ViewSearch.frame.size.width, _ViewSearch.frame.size.height);
        _viewInScrollView.frame = CGRectMake(0, 0, 768, 1024);
    } completion:^(BOOL finished) {
    }];

}
- (void)handleSwipeDown:(UISwipeGestureRecognizer *)recognizer {
    //顯示搜尋列
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _ViewSearch.frame = CGRectMake(0, 82, _ViewSearch.frame.size.width, _ViewSearch.frame.size.height);
        _viewInScrollView.frame = CGRectMake(0, _ViewSearch.frame.size.height, 768, 1024);
    } completion:^(BOOL finished) {
        //_ListView.hidden = NO;
    }];
}
#pragma mark - UITextField Delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == _phoneTextField ) {
        //限制輸入數字
        NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789#"] invertedSet];
        
        if (range.length == 1){
            return YES;
        }
        else{
            return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
        }
    }
    
    return YES;
}
-(IBAction) textFieldDidEditingChange:(id)sender {
    if ([_searchTextField.text length] > 0) {
        _ButtonClear.hidden = NO;
        _ButtonSearch.hidden = NO;
    }
    else {
        _ButtonClear.hidden = YES;
        _ButtonSearch.hidden = YES;
    }
}
#pragma mark - IBAction
/* 送出 */
- (IBAction)submitButtonClick:(id)sender {
    if ([_nameTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請填寫姓名"];
    }
    else if ([_emailTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請填寫Email"];
    }
    else if ([ObjectManager isValidateEmail:_emailTextField.text] == NO) {
        [self showAlertControllerWithMessage:@"請確認Email格式是否正確"];
    }
    else if ([_phoneTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請填寫電話"];
    }
    else {
        //關閉鍵盤
        [_nameTextField resignFirstResponder];
        [_emailTextField resignFirstResponder];
        [_phoneTextField resignFirstResponder];
        
        //檢查AppServer是否有類似資料
        [self callCustomerFind];
        //[self GotoTakePhoto];
    }
}
-(IBAction) pressSearchBtn {
    if (selectedIndex_ListItem < 0) { //表示沒選
        [self showAlertControllerWithMessage:@"用戶必須勾選類別"];
        return;
    }
    [_searchTextField resignFirstResponder];
    [self callCustomerSearch];
}
-(IBAction) pressClearBtn {
    [_searchTextField resignFirstResponder];
    _searchTextField.text = @"";
    _LabelItem.text = @"請選擇";
    selectedIndex_ListItem = -1;
    [_ListView UpdateUIWithData:nil];
    [self handleSwipeUp:nil];
}
#pragma mark - 選擇搜尋類別
-(IBAction) ShowListItemSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayListItem count]; i++) {
        NSDictionary *_dic = [arrayListItem objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"displayName"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _LabelItem;
    popPresenter.sourceRect = _LabelItem.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    selectedIndex_ListItem = buttonIndex;
    
    NSDictionary *_dic = [arrayListItem objectAtIndex:buttonIndex];
    _LabelItem.text = [_dic objectForKey:@"displayName"];
}
#pragma mark - 列表選擇
-(void) CustomerPhotoAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    
    NSDictionary *_dic = [arrayCustomers objectAtIndex:index];
    NSString *urlString = [_dic objectForKey:@"DriverLicense"];
    [self GotoPhotoViewerWithUrl:urlString];
}
-(void) CustomerSelectAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    //NSDictionary *_dic = [arrayCustomers objectAtIndex:index];
    
    //客戶編號
    //NSString *customerID = [_dic objectForKey:@"Id"];
    
    //判斷有沒有駕照圖檔 2016.05.30取消判斷，一律都先拍照
    /*BOOL hasLicenseImage = [[_dic objectForKey:@"DriverLicense"] length];
    if (hasLicenseImage) { //有駕照
        [self GotoConsentPageWithCustomerID:customerID];
    }
    else { //沒駕照
        [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
    }*/
    
    //2016.06.20 改為先更新資料
    [self callCustomerDataUpdate];
}
#pragma mark - 顯示UIAlertController
- (void)showAlertControllerWithMessage:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:submitAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - Call API
#pragma mark [003] 搜尋客戶
- (void)callCustomerFind { //新增時搜尋有沒有類似客戶資料
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_nameTextField.text forKey:@"name"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    //justin
//    [parameter setObject:_orderNOTextField.text forKey:@"orderNo"];
    [GatewayManager callCustomerFind:parameter Source:self];
}
- (void)callCustomerSearch { //搜尋Bar進行搜尋
    isSearch = YES;
    
    [ObjectManager showLodingHUDinView:self.view];
    
    //要搜尋哪個類別
    NSDictionary *_dic = [arrayListItem objectAtIndex:selectedIndex_ListItem];
    NSString *key = [_dic objectForKey:@"key"];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_searchTextField.text forKey:key];
    [GatewayManager callCustomerFind:parameter Source:self];
}
- (void)callBackCustomerFind:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSArray *Customers = [Data objectForKey:@"Customers"];
        
        [arrayCustomers removeAllObjects];
        [arrayCustomers addObjectsFromArray:Customers];
        if (isSearch) { //本頁顯示
            if ([arrayCustomers count] == 0) {
                [self showAlertControllerWithMessage:@"查無資料"];
                _ListView.hidden = YES;
            }
            else {
                _ListView.hidden = NO;
            }
            [_ListView UpdateUIWithData:Customers];
        }
        else { //到下頁
            [self GotoListPageWithData:Customers];
        }
        /*//判斷有沒有記錄
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        BOOL HasRecord = [[Data objectForKey:@"HasRecord"] boolValue];
        HasRecord = YES;
        if (HasRecord) { //有記錄
            NSArray *Customers = [Data objectForKey:@"Customers"];
            if (isSearch) { //本頁顯示
                [arrayCustomers removeAllObjects];
                [arrayCustomers addObjectsFromArray:Customers];
                if ([Customers count] == 0) {
                    [self showAlertControllerWithMessage:@"查無資料"];
                }
                [_ListView UpdateUIWithData:Customers];
            }
            else { //到下頁
                [self GotoListPageWithData:Customers];
            }
        }
        else { //沒記錄
            //呼叫API新增資料
            [self callCustomerData];
        }*/
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
    
    isSearch = NO;
}
#pragma mark [004] 新增或修改客戶
- (void)callCustomerDataUpdate {
    [ObjectManager showLodingHUDinView:self.view];
    
    //用選到的ID+前一頁輸入的資料跟後台update
    NSDictionary *_dic = [arrayCustomers objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    NSString *customerId = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Id"]];
    NSString *name = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Name"]];
    NSString *email = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Email"]];
    NSString *phone = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Phone"]];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:customerId forKey:@"id"];
    [parameter setObject:name forKey:@"name"];
    [parameter setObject:email forKey:@"email"];
    [parameter setObject:phone forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *customerID = [parameter objectForKey:@"Data"];
        [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
/*#pragma mark [004] 新增或修改客戶
- (void)callCustomerData {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:@"" forKey:@"id"];
    [parameter setObject:_nameTextField.text forKey:@"name"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *customerID = [parameter objectForKey:@"Data"];
        [self GotoTakePhotoWithCustomerID:customerID]; //到2.2拍照上傳
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}*/
#pragma mark - 換頁
-(void) GotoTakePhotoWithCustomerID:(NSString *)customerID { //拍照上傳
    PhotographVC *destVC = [[PhotographVC alloc] initWithNibName:@"PhotographVC" bundle:nil];
    destVC.customerID = customerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoListPageWithData:(NSArray *)data { //相似資料
    WriteRideDataListVC *destVC = [[WriteRideDataListVC alloc] initWithNibName:@"WriteRideDataListVC" bundle:nil];
    destVC.Customer = data;
    destVC.stringName = _nameTextField.text;
    destVC.stringPhone = _phoneTextField.text;
    destVC.stringEmail = _emailTextField.text;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString { //瀏覽駕照
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentPageWithCustomerID:(NSString *)customerID { //個資同意書
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_TEST_RIDE;
    consentVC.customerID = customerID;
    [self.navigationController pushViewController:consentVC animated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CustomerPhotoAtIndex:) name:@"kNotification_TestDrive_Customer_Photo" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CustomerSelectAtIndex:) name:@"kNotification_TestDrive_Customer_Select" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_TestDrive_Customer_Photo" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_TestDrive_Customer_Select" object:nil];
}
@end
