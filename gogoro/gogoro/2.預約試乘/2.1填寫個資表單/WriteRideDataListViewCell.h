

#import <UIKit/UIKit.h>

@interface WriteRideDataListViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageBG;
@property (nonatomic, weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic, weak) IBOutlet UILabel *LabelEmail;
@property (nonatomic, weak) IBOutlet UILabel *LabelPhone;
@property (nonatomic, weak) IBOutlet UIButton *ButtonDriverLicense;
@property (nonatomic, weak) IBOutlet UIButton *ButtonSelect;
@end
