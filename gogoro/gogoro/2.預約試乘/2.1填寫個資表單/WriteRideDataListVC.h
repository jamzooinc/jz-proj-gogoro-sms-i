

#import <UIKit/UIKit.h>
#import "WriteRideDataListView.h"

@interface WriteRideDataListVC : TemplateVC <WriteRideDataListViewDelegate>
@property (strong, nonatomic) NSString *stringName;
@property (strong, nonatomic) NSString *stringEmail;
@property (strong, nonatomic) NSString *stringPhone;
@property (strong, nonatomic) NSArray *Customer;
- (IBAction)submitButtonClick:(id)sender;

@end
