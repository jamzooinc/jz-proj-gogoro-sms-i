
#import "WriteRideDataListView.h"
#import "WriteRideDataListViewCell.h"

@implementation WriteRideDataListView
@synthesize delegate;
#pragma mark - Delegate
-(void) WriteRideDataListViewFinish { //
    if ([[self delegate] respondsToSelector:@selector(WriteRideDataListViewFinish)]) {
        [[self delegate] WriteRideDataListViewFinish];
    }
}
#pragma mark - 初始化
-(void) UpdateUIWithData:(NSArray *)data {
    if (!arrayTableView) {
        arrayTableView = [[NSMutableArray alloc] init];
    }
    [arrayTableView removeAllObjects];
    [arrayTableView addObjectsFromArray:data];
    [_TableView reloadData];
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WriteRideDataListViewCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"WriteRideDataListViewCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    WriteRideDataListViewCell *myCell = (WriteRideDataListViewCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //Title
    myCell.LabelTitle.text = [NSString stringWithFormat:@"姓名：%@",[_dic objectForKey:@"Name"]];
    
    //Email
    myCell.LabelEmail.text = [NSString stringWithFormat:@"Email：%@",[_dic objectForKey:@"Email"]];
    
    //Phone
    myCell.LabelPhone.text = [NSString stringWithFormat:@"電話：%@",[_dic objectForKey:@"Phone"]];
    
    //判斷有沒有駕照圖檔
    NSLog(@"駕照網址:%@",[_dic objectForKey:@"DriverLicense"]);
    BOOL hasLicenseImage = [[_dic objectForKey:@"DriverLicense"] length];
    if (hasLicenseImage) {
        myCell.ButtonDriverLicense.hidden = NO;
    }
    else {
        myCell.ButtonDriverLicense.hidden = YES;
    }
    
    if (_didFromList) { //列表改為更新
        [myCell.ButtonSelect setBackgroundImage:[UIImage imageNamed:@"btn_renew.png"] forState:UIControlStateNormal];
    }
    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - 功能


#pragma mark - 雜項
-(void) dealloc {
}
@end
