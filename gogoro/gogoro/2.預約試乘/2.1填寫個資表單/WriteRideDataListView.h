@protocol WriteRideDataListViewDelegate <NSObject>
@optional
-(void) WriteRideDataListViewFinish; //
@end

#import <UIKit/UIKit.h>

@interface WriteRideDataListView : UIView  {
    __weak id <WriteRideDataListViewDelegate> delegate;
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
}
@property (weak) id delegate;
@property BOOL didFromList; //從列表來
@property (nonatomic,weak) IBOutlet UITableView *TableView;
-(void) UpdateUIWithData:(NSArray *)data;
@end
