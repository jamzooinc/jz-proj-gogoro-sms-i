//
//  PhotographVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOCropViewController.h"

@interface PhotographVC : TemplateVC <UINavigationControllerDelegate,UIImagePickerControllerDelegate,TOCropViewControllerDelegate>
@property (nonatomic,strong) NSString *customerID;
@property (nonatomic,strong) NSMutableDictionary *customerData;
@end
