
#import <UIKit/UIKit.h>

@interface PhotoViewerVC : TemplateVC

@property (nonatomic,strong) UIImage *imageToShow;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *stringTitle; //標題
@property (nonatomic,strong) IBOutlet UIImageView *ImageBG;
@property BOOL showPrintButton; //是否要顯示列印按鈕
@end
