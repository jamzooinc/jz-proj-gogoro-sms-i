//
//  PhotographVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "PhotographVC.h"
#import "ConsentVC.h"
#import <QuartzCore/QuartzCore.h>

@interface PhotographVC () {
    UIImagePickerController *imagePicker;
    TOCropViewController *cropVC;
    
    BOOL shouldTakeAgain;
}

@property (nonatomic,strong) IBOutlet UIView *ViewOverlay;
@property (nonatomic,weak) IBOutlet UIImageView *ImageSelectd;
@property (nonatomic,weak) IBOutlet UIButton *ButtonTakePhoto;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@end

@implementation PhotographVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //NSLog(@"customerID:%@",_customerID);
    
    [self setupNavigationItemTitle:@"駕照" Image:nil];
    
    shouldTakeAgain = YES;
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

    if (shouldTakeAgain) {
        shouldTakeAgain = NO;
        
        //模擬器不能拍照，所以要把照片放到doc目錄下進行測試
#if TARGET_IPHONE_SIMULATOR
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempImage"]];
        NSLog(@"圖檔放置路徑:%@",imagePath);
#else
        [self performSelector:@selector(initImagePicker) withObject:nil afterDelay:0.5f];
#endif
    }

}
#pragma mark - 相機
-(void) initImagePicker {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { //沒相機
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無法開啟相機！" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    NSLog(@"ViewOverlay:%@",_ViewOverlay);
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    //imagePicker.allowsEditing = YES;
    imagePicker.showsCameraControls = NO;
    imagePicker.cameraOverlayView = _ViewOverlay;
    [self presentViewController:imagePicker animated:NO completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //測試
#if TARGET_IPHONE_SIMULATOR
    //載入個人簽名的暫存（如果有的話）
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempImage"]];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    NSLog(@"original image:%@",NSStringFromCGSize(image.size));

    [self ProcessPhotoWithImage:image];
#else
    //NSString *url = [info objectForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"url:%@",url);
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        //UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        NSLog(@"original image:%@",NSStringFromCGSize(image.size));
        
        [self ProcessPhotoWithImage:image];
        
        //UIImage *imageModified = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        //NSLog(@"modified an image:%@",NSStringFromCGSize(imageModified.size));
        
        //UIImage *imageResize = [imageModified imageByScalingAndCroppingForSize:CGSizeMake(320, 320)];
        //NSLog(@"resize an image:%@",NSStringFromCGSize(imageResize.size));
        
        //計算要抓取的位置
        /*CGFloat ratio =  image.size.width / 768; //因為原本rect是用寬768去算的
        NSLog(@"ratio:%f",ratio);
        
        CGRect rect = CGRectMake(163.0/2 * ratio,440.0/2 * ratio,1208.0/2 * ratio,855.0/2 * ratio);
        NSLog(@"要抓取的位置:%@",NSStringFromCGRect(rect));
        
        //擷取拍到照片存起來
        _ImageSelectd.image = image;
        
        //遮罩
        UIImage *captureImage = [self captureView:_ViewOverlay Rect:rect];
        //裁切
        //UIImage *captureImage = image;
        NSLog(@"抓取後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
        
        //存到暫存位置
        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
        NSData* data = UIImageJPEGRepresentation(captureImage, 1.0f);
        BOOL result = [data writeToFile:imagePath atomically:YES];
        NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
        
        //遮罩：上傳
        [self callUploadDriveLicenseWithImage:captureImage];*/
    }
    else if ([mediaType isEqualToString:@"public.movie"]){
        NSLog(@"found a video");
        //NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        //NSData *webData = [NSData dataWithContentsOfURL:videoURL];
    }
    
    //裁切
    /*[picker dismissViewControllerAnimated:NO completion:^{
        //呈現影像編輯VC
        [self presentCropViewController];
    }];*/
#endif
}
-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self pressImageBackBtn:nil];
}
-(void) ProcessPhotoWithImage:(UIImage *)image {
    //計算要抓取的位置
    CGFloat ratio =  image.size.width / 768; //因為原本rect是用寬768去算的
    //ratio = 1.0;
    NSLog(@"ratio:%f",ratio);
    
    CGRect rect = CGRectMake(163.0/2 * ratio,440.0/2 * ratio+50,1208.0/2 * ratio,855.0/2 * ratio);
    NSLog(@"要抓取的位置:%@",NSStringFromCGRect(rect));
    
    //擷取拍到照片存起來
    //_ImageSelectd.image = image;
    
    //加入浮水印
    UIImageView *ImageTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    ImageTemp.image = image;
    
    CGFloat watermarkRatio = image.size.width/378.0 * 0.75 * 0.5;
    CGFloat watermarkWidth = 378 * watermarkRatio;
    CGFloat watermarkHeight = 38 * watermarkRatio;
    //UIImageView *ImageWatermark = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 378 * watermarkRatio, 38 * watermarkRatio)];
    //ImageWatermark.center = CGPointMake(ImageTemp.frame.size.width/2,ImageTemp.frame.size.height/2);
    //浮水印放在左下角
    UIImageView *ImageWatermark = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-watermarkHeight, watermarkWidth, watermarkHeight)];
    ImageWatermark.image = [UIImage imageNamed:@"text_gogoro_testdrive_watermark.png"];
    [ImageTemp addSubview:ImageWatermark];
    UIImage *sourceImage = [self imageWithView:ImageTemp];
    
    //NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //NSString *imagePath = nil;
    
    //存到暫存位置（驗證用）
    //imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense222"]];
    //NSData* data1 = UIImageJPEGRepresentation(sourceImage, 1.0f);
    //BOOL result1 = [data1 writeToFile:imagePath atomically:YES];
    //NSLog(@"%@ %@", (result1? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    _ImageSelectd.image = sourceImage;
    
    //2016.06.20 顯示功能按鈕
    [self ShowCommandView];
    
    /*//遮罩
    UIImage *captureImage = [self captureView:_ViewOverlay Rect:rect];
    //裁切
    //UIImage *captureImage = image;
    NSLog(@"抓取後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    
    //2016.06.13 也要縮圖
    CGSize sizeToBe = CGSizeMake(captureImage.size.width/2 * 0.5, captureImage.size.height/2 * 0.5);
    captureImage = [self ImageWithImage:captureImage scaledToSize:sizeToBe];
    NSLog(@"縮小後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    
    //存到暫存位置
    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
    NSData* data = UIImageJPEGRepresentation(captureImage, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    //遮罩：上傳
    [self callUploadDriveLicenseWithImage:captureImage];
    
    //裁切：呈現影像編輯VC
    //[self presentCropViewController];*/
}
#pragma mark - 影像裁切VC
- (void)presentCropViewController {
    UIImage *image = _ImageSelectd.image; //Load an image
    
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    //cropViewController.rotateButtonsHidden = YES;
    [self presentViewController:cropViewController animated:YES completion:nil];
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    _ImageSelectd.image = image;
    
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    //上傳
    cropVC = cropViewController;
    [self callUploadDriveLicenseWithImage:image];
    
    //[cropViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled {
    if (cancelled) {
        [cropViewController dismissViewControllerAnimated:YES completion:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
    //[cropViewController dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Button Action
-(IBAction) pressImageBackBtn:(id)sender {
    [imagePicker dismissViewControllerAnimated:NO completion:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (IBAction) pressImageHomeBtn:(id)sender {
    [imagePicker dismissViewControllerAnimated:NO completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}
- (IBAction) pressTakePictureBtn:(id)sender {
#if TARGET_IPHONE_SIMULATOR
    //使用裁切
    //[self performSelector:@selector(imagePickerController:didFinishPickingMediaWithInfo:) withObject:nil afterDelay:0.5f];
    
    //使用遮罩
    [self imagePickerController:nil didFinishPickingMediaWithInfo:nil];
#else
    [imagePicker takePicture];
#endif
}
-(IBAction) pressReTakeBtn:(id)sender { //重新拍攝按鈕
    _ImageSelectd.image = nil;
    
    //隱藏功能按鈕
    [self HideCommandView];
}
-(IBAction) pressUsePhotoBtn:(id)sender { //使用照片按鈕
    //隱藏功能按鈕
    //[self HideCommandView];
    
    UIImage *image = _ImageSelectd.image;
    
    //計算要抓取的位置
    CGFloat ratio =  image.size.width / 768; //因為原本rect是用寬768去算的
    //ratio = 1.0;
    NSLog(@"ratio:%f",ratio);
    
    CGRect rect = CGRectMake(163.0/2 * ratio,440.0/2 * ratio+50,1208.0/2 * ratio,855.0/2 * ratio);
    NSLog(@"要抓取的位置:%@",NSStringFromCGRect(rect));
    
    //遮罩
    UIImage *captureImage = [self captureView:_ViewOverlay Rect:rect];
    //裁切
    //UIImage *captureImage = image;
    NSLog(@"抓取後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    
    //2016.06.13 也要縮圖
    CGSize sizeToBe = CGSizeMake(captureImage.size.width/2 * 0.5, captureImage.size.height/2 * 0.5);
    captureImage = [self ImageWithImage:captureImage scaledToSize:sizeToBe];
    NSLog(@"縮小後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = nil;
    
    imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
    NSData* data = UIImageJPEGRepresentation(captureImage, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存圖片成功:" : @"儲存圖片失敗:"),imagePath);
    
    //遮罩：上傳
    [self callUploadDriveLicenseWithImage:captureImage];
    
    //裁切：呈現影像編輯VC
    //[self presentCropViewController];
}
- (UIImage*)captureView:(UIView *)yourView Rect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // translated rectangle for drawing sub image
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, _ImageSelectd.image.size.width,_ImageSelectd.image.size.height);
    
    // clip to the bounds of the image context
    // not strictly necessary as it will get clipped anyway?
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    
    // draw image
    [_ImageSelectd.image drawInRect:drawRect];
    
    // grab image
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return croppedImage;
    
    /*//CGRect screen = [[UIScreen mainScreen] bounds];
    UIGraphicsBeginImageContext(rect.size);
    CGContextClipToRect (UIGraphicsGetCurrentContext(),rect);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [yourView.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;*/
}
- (UIImage *) imageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
#pragma mark - Call API
#pragma mark [005] 上傳客戶駕照圖檔
- (void)callUploadDriveLicenseWithImage:(UIImage *)image {
    [ObjectManager showLodingHUDinView:_ViewOverlay]; //遮罩用imagePicker.view 裁切用cropVC.view //_ViewOverlay
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_customerID forKey:@"id"];
    //[parameter setObject:image forKey:@"file"];
    [GatewayManager callUploadDriverLicense:parameter Source:self];
}
- (void)callBackUploadDriverLicense:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:_ViewOverlay]; //遮罩用imagePicker.view 裁切用cropVC.view //_ViewOverlay
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
#if TARGET_IPHONE_SIMULATOR
        //遮罩
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"駕照歸檔完成" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //至2.3觀看試乘同意書
            [self GotoConsentPageWithCustomerID:_customerID];
        }];
        
        [alertController addAction:actionConfirm];
        [self presentViewController:alertController animated:NO completion:nil];
        
        //裁切
        /*UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"駕照歸檔完成" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [cropVC dismissViewControllerAnimated:NO completion:^{
                //至2.3觀看試乘同意書
                [self GotoConsentPageWithCustomerID:_customerID];
            }];
        }];
        
        [alertController addAction:actionConfirm];
        [cropVC presentViewController:alertController animated:NO completion:nil];*/
#else
        //遮罩
        [imagePicker dismissViewControllerAnimated:NO completion:^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"駕照歸檔完成" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //至2.3觀看試乘同意書
                [self GotoConsentPageWithCustomerID:_customerID];
            }];
            
            [alertController addAction:actionConfirm];
            [self presentViewController:alertController animated:NO completion:nil];
        }];
        
        
        //裁切
        /*UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"駕照歸檔完成" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [cropVC dismissViewControllerAnimated:NO completion:^{
                //至2.3觀看試乘同意書
                [self GotoConsentPageWithCustomerID:_customerID];
            }];
        }];
        
        [alertController addAction:actionConfirm];
        [cropVC presentViewController:alertController animated:NO completion:nil];*/
#endif
    }
    else {
        [ObjectManager showErrorHUDWithStatus:Sysmsg duration:3.0f inView:_ViewOverlay]; //遮罩用imagePicker.view 裁切用cropVC.view //_ViewOverlay
    }
    
    //刪除暫存檔
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
    if (error) {
        NSLog(@"刪除暫存駕照檔失敗:%@",error);
    }
    else {
        NSLog(@"刪除暫存駕照檔成功");
    }
}
#pragma mark - UI
-(void) ShowCommandView { //顯示重新拍攝/使用照片
    _ViewCommand.hidden = NO;
    _ButtonTakePhoto.hidden = YES;
}
-(void) HideCommandView { //隱藏重新拍攝/使用照片
    _ViewCommand.hidden = YES;
    _ButtonTakePhoto.hidden = NO;
    _ImageSelectd.image = nil;
}
#pragma mark - 換頁
-(void) GotoConsentPageWithCustomerID:(NSString *)customerID { //個資同意書
    shouldTakeAgain = YES;
    
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_TEST_RIDE;
    consentVC.customerID = customerID;
    consentVC.customerData = _customerData;
    [self.navigationController pushViewController:consentVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) dealloc {
}
@end
