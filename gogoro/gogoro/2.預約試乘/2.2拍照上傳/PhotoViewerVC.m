

#import "PhotoViewerVC.h"
#import "TemplateVCTitleView.h"

@interface PhotoViewerVC () {
    NSData *dataForPrint;
}
@property (nonatomic,weak) IBOutlet UIImageView *ImageTitle;
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet TemplateVCTitleView *ViewTitle;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPrint;
@end

@implementation PhotoViewerVC

#pragma mark - 初始化相關
- (void)viewDidLayoutSubviews {
}
-(void) viewDidLoad {
    //_ImageTitle.image = _imageToShow;
    if ([_stringTitle length] > 0) {
        _ViewTitle.LabelTitle.text = _stringTitle;
    }
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //檔名
    NSArray *array = [_urlString componentsSeparatedByString:@"/"];
    NSString *filename = [array lastObject];
    
    //刪除暫存檔
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", filename]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存圖檔失敗:%@",error);
        }
        else {
            NSLog(@"刪除暫存圖檔成功");
        }
    }
    
    //_urlString = [_urlString stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
    NSLog(@"下載圖檔網址:%@",_urlString);
    
    //_urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)_urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    //下載圖檔
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:_urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", filePath);
        
        dataForPrint = [NSData dataWithContentsOfURL:filePath];
        UIImage *image = [UIImage imageWithData:dataForPrint];
        _ImageTitle.image = image;
        
        //視需要顯示列印按鈕
        if (_showPrintButton) {
            _ButtonPrint.hidden = NO;
        }
        
    }];
    [downloadTask resume];
}
#pragma mark - ScrollView Delegate
-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    //NSLog(@"size:%@",NSStringFromCGSize(scrollView.contentSize));
    //NSLog(@"frame:%@",NSStringFromCGRect(scrollView.frame));
    //return _ImageTitle;
    return nil;
}
#pragma mark - Button Action
-(IBAction) pressPrintBtn {
    [self ShowPrintVCWithData:dataForPrint Rect:_ButtonPrint.frame];
}
#pragma mark - 換頁
-(IBAction) Dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction) Home {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - 雜項
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) dealloc {
}
@end
