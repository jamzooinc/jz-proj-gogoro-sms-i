//
//  WriteFirstDataVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "WriteFirstDataVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "WriteFirstDataListVC.h"

@interface WriteFirstDataVC () {
    
}

@end

@implementation WriteFirstDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationItemTitle:@"首次參觀" Image:[UIImage imageNamed:@"icn_first.png"]];
    
#if TARGET_IPHONE_SIMULATOR
    _nameTextField.text = @"吳先生";
    _emailTextField.text = @"Aaron@123.com";
    _phoneTextField.text = @"0982123123";
#endif
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction

/* 送出 */
- (IBAction)submitButtonClick:(id)sender {
    if ([_nameTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請填寫姓名"];
    }
    else if ([_emailTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請填寫Email"];
    }
    else if ([ObjectManager isValidateEmail:_emailTextField.text] == NO) {
            [self showAlertControllerWithMessage:@"請確認Email格式是否正確"];
    }
    else if ([_phoneTextField.text isEqualToString:@""]) {
        [self showAlertControllerWithMessage:@"請填寫電話"];
    }
    else {
        //關閉鍵盤
        [_nameTextField resignFirstResponder];
        [_emailTextField resignFirstResponder];
        [_phoneTextField resignFirstResponder];
        
        //檢查AppServer是否有類似資料
        [self callCustomerFind];
    }
}

#pragma mark - 顯示UIAlertController
- (void)showAlertControllerWithMessage:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:submitAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Call API
#pragma mark [003] 搜尋客戶
- (void)callCustomerFind {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_nameTextField.text forKey:@"name"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    //justin
//    [parameter setObject:_orderNOTextField.text forKey:@"orderNo"];
    [GatewayManager callCustomerFind:parameter Source:self];
}
- (void)callBackCustomerFind:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //到列表頁
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSArray *Customers = [Data objectForKey:@"Customers"];
        [self GotoListPageWithData:Customers];

        /*//判斷有沒有記錄
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        BOOL HasRecord = [[Data objectForKey:@"HasRecord"] boolValue];
        if (HasRecord) { //有記錄
            NSArray *Customers = [Data objectForKey:@"Customers"];
            [self GotoListPageWithData:Customers];
        }
        else { //沒記錄
            //呼叫API新增資料
            [self callCustomerData];
        }*/
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
/*#pragma mark [004] 新增或修改客戶
- (void)callCustomerData {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:@"" forKey:@"id"];
    [parameter setObject:_nameTextField.text forKey:@"name"];
    [parameter setObject:_emailTextField.text forKey:@"email"];
    [parameter setObject:_phoneTextField.text forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *customerID = [parameter objectForKey:@"Data"];
        [self GotoConsentPageWithCustomerID:customerID];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}*/
#pragma mark - TextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //限制輸入數字
    NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789#"] invertedSet];
    
    if (range.length == 1){
        return YES;
    }
    else{
        return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
    }
    
    return YES;
}
#pragma mark - 換頁
-(void) GotoConsentPageWithCustomerID:(NSString *)customerID { //個資同意書
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_PERSONAL_Data;
    consentVC.customerID = customerID;
    [self.navigationController pushViewController:consentVC animated:YES];
}
-(void) GotoListPageWithData:(NSArray *)data { //相似資料
    WriteFirstDataListVC *destVC = [[WriteFirstDataListVC alloc] initWithNibName:@"WriteFirstDataListVC" bundle:nil];
    destVC.Customer = data;
    destVC.stringName = _nameTextField.text;
    destVC.stringPhone = _phoneTextField.text;
    destVC.stringEmail = _emailTextField.text;
    [self.navigationController pushViewController:destVC animated:YES];
}
@end
