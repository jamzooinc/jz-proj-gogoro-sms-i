

#import "WriteFirstDataListVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "WriteFirstDataListVCCell.h"

@interface WriteFirstDataListVC () {
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@end

@implementation WriteFirstDataListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationItemTitle:@"首次參觀" Image:[UIImage imageNamed:@"icn_first.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayTableView = [[NSMutableArray alloc] init];
    for (NSDictionary *_dic in _Customer) {
        NSMutableDictionary *dic1 = [NSMutableDictionary dictionaryWithDictionary:_dic];
        [arrayTableView addObject:dic1];
    }
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction

/* 送出 */
- (IBAction)submitButtonClick:(id)sender {
    //新增
    [self callCustomerData];
}

#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WriteFirstDataListVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"WriteFirstDataListVCCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    WriteFirstDataListVCCell *myCell = (WriteFirstDataListVCCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //Title
    myCell.LabelTitle.text = [NSString stringWithFormat:@"姓名：%@",[_dic objectForKey:@"Name"]];
    
    //Email
    myCell.LabelEmail.text = [NSString stringWithFormat:@"Email：%@",[_dic objectForKey:@"Email"]];
    
    //Phone
    myCell.LabelPhone.text = [NSString stringWithFormat:@"電話：%@",[_dic objectForKey:@"Phone"]];
    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - 變更
-(void) ModifyCustomAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    //NSDictionary *_dic = [arrayTableView objectAtIndex:index];

    //[_dic objectForKey:@"Name"]
    NSString *name = [NSString stringWithFormat:@"姓名：%@",_stringName];
    
    //[_dic objectForKey:@"Email"]
    NSString *email= [NSString stringWithFormat:@"Email：%@",_stringEmail];
    
    //[_dic objectForKey:@"Phone"]
    NSString *phone = [NSString stringWithFormat:@"電話：%@",_stringPhone];
    NSString *message = [NSString stringWithFormat:@"確認要更新以下資訊\n\n%@\n%@\n%@",name,email,phone];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self callCustomerDataUpdate];
    }];
    UIAlertAction *actionCacnel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
    }];
    
    [alertController addAction:actionConfirm];
    [alertController addAction:actionCacnel];
    [self presentViewController:alertController animated:YES completion:^(void){
    }];
}
#pragma mark - Call API
#pragma mark [004] 新增或修改客戶
- (void)callCustomerData {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:@"" forKey:@"id"];
    [parameter setObject:_stringName forKey:@"name"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callCustomerDataUpdate {
    [ObjectManager showLodingHUDinView:self.view];
    
    //用選到的ID+前一頁輸入的資料跟後台update
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *customerId = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Id"]];
  
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:customerId forKey:@"id"];
    [parameter setObject:_stringName forKey:@"name"];
    [parameter setObject:_stringEmail forKey:@"email"];
    [parameter setObject:_stringPhone forKey:@"phone"];
    [GatewayManager callCustomerData:parameter Source:self];
}
- (void)callBackCustomerData:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *customerID = [parameter objectForKey:@"Data"];
        [self GotoConsentPageWithCustomerID:customerID];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoConsentPageWithCustomerID:(NSString *)customerID { //個資同意書
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_PERSONAL_Data;
    consentVC.customerID = customerID;
    [self.navigationController pushViewController:consentVC animated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ModifyCustomAtIndex:) name:@"kNotification_Modify_Customer" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Modify_Customer" object:nil];
}
@end
