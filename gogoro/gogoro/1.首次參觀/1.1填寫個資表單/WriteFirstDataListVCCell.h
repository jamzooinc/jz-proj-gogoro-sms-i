

#import <UIKit/UIKit.h>

@interface WriteFirstDataListVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageBG;
@property (nonatomic, weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic, weak) IBOutlet UILabel *LabelEmail;
@property (nonatomic, weak) IBOutlet UILabel *LabelPhone;
@end
