

#import <UIKit/UIKit.h>

@interface WriteFirstDataListVC : TemplateVC
@property (strong, nonatomic) NSString *stringName;
@property (strong, nonatomic) NSString *stringEmail;
@property (strong, nonatomic) NSString *stringPhone;
@property (strong, nonatomic) NSArray *Customer;
- (IBAction)submitButtonClick:(id)sender;

@end
