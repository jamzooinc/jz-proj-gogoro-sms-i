//
//  MainVC.m
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "MainVC.h"
#import "WriteFirstDataVC.h"
#import "WriteRideDataVC.h"
#import "ConsentVC.h"
#import "SingleSupplementVC.h"
#import "TrackOrderVC.h"
#import "LoginVC.h"
#import "TransferVC.h"
#import "CustomDocumentVC.h"
#import "SingleMainVC.h"
#import "UndoneOrderVC.h"

//temp
#import "UploadFileVC.h"
#import "SelectSubsidyVC.h"
#import "ConfirmInfoVC.h"
#import "OrderExecutionVC.h"

@interface MainVC ()
@property (nonatomic,weak) IBOutlet UILabel *LabelVersion;
@property (nonatomic,weak) IBOutlet UIButton *ButtonLogin;
@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([UserManager shareInstance].isLogin) {
        [_ButtonLogin setTitle:@"登出" forState:UIControlStateNormal];
    }
    else {
        [_ButtonLogin setTitle:@"登入" forState:UIControlStateNormal];
    }
    
    /* 版號 */
    NSString *desc = @"測試環境";
    if ([[DocumentManager shareInstance].apiGatewayUrl isEqualToString:kServerDomainDevelop]) { //判斷是不是開發環境
        desc = @"開發環境";
    }
    else if ([[DocumentManager shareInstance].apiGatewayUrl isEqualToString:kServerDomainProduction]) { //判斷是不是正式環境
        desc = @"正式環境";
    }
    _LabelVersion.text = [NSString stringWithFormat:@"%@：%@(%@)",@"版本", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"],desc];
    [self showAcountInfo];
}

//Add UI for 2.0 version by Justin in 2016/9/12
-(void)showAcountInfo
{
    if ([UserManager shareInstance].isLogin)
    {
        _acountInfoView.hidden = NO;
        _storeNameLabel.text = [UserManager shareInstance].storeName;
        _staffAcountLabel.text = [UserManager shareInstance].account;
        _staffNameLabel.text = [UserManager shareInstance].staffName;
    }
    else
    {
        _acountInfoView.hidden = YES;
    }
}

-(BOOL) CheckLogin {
    if ([UserManager shareInstance].isLogin) {
        [_ButtonLogin setTitle:@"登出" forState:UIControlStateNormal];
        return YES;
    }
    else {
        [_ButtonLogin setTitle:@"登入" forState:UIControlStateNormal];
        
        //提示要先登入
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"請先登入Gogoro" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self GotoLoginVC];
        }];
        
        [alertController addAction:actionConfirm];
        [self presentViewController:alertController animated:YES completion:^(void){
        }];
        
        
        
        return NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction
/* 原首度參觀 2-2-2改為自訂文件 */
- (IBAction)firstVisitButtonClick:(id)sender {
    if (![self CheckLogin]) {
        return;
    }
    
    CustomDocumentVC *destVC = [[CustomDocumentVC alloc] initWithNibName:@"CustomDocumentVC" bundle:nil];
    destVC.attachTypeId = 101; //（首次參觀:101, 預約試乘: 102）
    [self.navigationController pushViewController:destVC animated:YES];
    
    //WriteFirstDataVC *writeFirstDataVC = [[WriteFirstDataVC alloc] initWithNibName:@"WriteFirstDataVC" bundle:nil];
    //[self.navigationController pushViewController:writeFirstDataVC animated:YES];
}

/* 預約試乘 */
- (IBAction)bookRideButtonClick:(id)sender {
    if (![self CheckLogin]) {
        return;
    }
    
    CustomDocumentVC *destVC = [[CustomDocumentVC alloc] initWithNibName:@"CustomDocumentVC" bundle:nil];
    destVC.attachTypeId = 102; //（首次參觀:101, 預約試乘: 102）
    [self.navigationController pushViewController:destVC animated:YES];
    
    //WriteRideDataVC *writeRideDataVC = [[WriteRideDataVC alloc] initWithNibName:@"WriteRideDataVC" bundle:nil];
    //[self.navigationController pushViewController:writeRideDataVC animated:YES];
}

/* 訂購Gogoro */
- (IBAction)buyGogoroButtonClick:(id)sender {
    if (![self CheckLogin]) {
        return;
    }
    
    ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    consentVC.enumCONSENT = CONSENT_ORDER_PERSONAL_Data;
    [self.navigationController pushViewController:consentVC animated:YES];

}

/* 補單上傳 */
- (IBAction)uploadSupplementButtonClick:(id)sender {
    if (![self CheckLogin]) {
        return;
    }
    
    SingleMainVC *destVC = [[SingleMainVC alloc] initWithNibName:@"SingleMainVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
    
    //SingleSupplementVC *singleSupplementVC = [[SingleSupplementVC alloc] initWithNibName:@"SingleSupplementVC" bundle:nil];
    //[self.navigationController pushViewController:singleSupplementVC animated:YES];
}

/* 追蹤訂單 */
- (IBAction)trackOrderButtonClick:(id)sender {
    if (![self CheckLogin]) {
        return;
    }
    
    UndoneOrderVC *destVC = [[UndoneOrderVC alloc] initWithNibName:@"UndoneOrderVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
    
    //TrackOrderVC *trackOrderVC = [[TrackOrderVC alloc] initWithNibName:@"TrackOrderVC" bundle:nil];
    //[self.navigationController pushViewController:trackOrderVC animated:YES];
}

/* 過戶移轉 */
- (IBAction)transferButtonClick:(id)sender {
    if (![self CheckLogin]) {
        return;
    }
    
    TransferVC *transferVC = [[TransferVC alloc] initWithNibName:@"TransferVC" bundle:nil];
    [self.navigationController pushViewController:transferVC animated:YES];
}

/* 登出 */
- (IBAction)logoutButtonClick:(id)sender {
#if TARGET_IPHONE_SIMULATOR
    //ConfirmInfoVC *destVC = [[ConfirmInfoVC alloc] initWithNibName:@"ConfirmInfoVC" bundle:nil];
    //destVC.OrderNo = @"S1511001317";
    //[self.navigationController pushViewController:destVC animated:YES];
    //return;
#else
#endif
    
    if ([UserManager shareInstance].isLogin) { //登入-->登出
        [_ButtonLogin setTitle:@"登入" forState:UIControlStateNormal];
        
        //清除資料
        [[UserManager shareInstance] clearAllUserData];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else { //登出-->登入
        [_ButtonLogin setTitle:@"登出" forState:UIControlStateNormal];
        
        //到登入畫面
        [self GotoLoginVC];
    }
    [self showAcountInfo];
}
#pragma mark - 換頁
-(void) GotoLoginVC {
    LoginVC *destVC = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
@end
