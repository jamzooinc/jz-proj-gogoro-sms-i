//
//  MainVC.h
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainVC : TemplateVC
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffAcountLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffNameLabel;
@property (weak, nonatomic) IBOutlet UIView *acountInfoView;



- (IBAction)firstVisitButtonClick:(id)sender;
- (IBAction)bookRideButtonClick:(id)sender;
- (IBAction)buyGogoroButtonClick:(id)sender;
- (IBAction)uploadSupplementButtonClick:(id)sender;
- (IBAction)trackOrderButtonClick:(id)sender;

@end
