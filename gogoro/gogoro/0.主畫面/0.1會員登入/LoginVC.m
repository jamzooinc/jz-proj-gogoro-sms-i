//
//  LoginVC.m
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "LoginVC.h"
#import "MainVC.h"
#import "GogoroNavigationController.h"
#import "SlideMenuVC.h"
#import <MMDrawerController/MMDrawerController.h>

#define retailViewHeight 400

@interface LoginVC () <UITextViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    NSMutableArray *arrayStore,*arrayApi;
    NSInteger selectedIndex_Store,selectedIndex_Api;
    UIView* shadowView;
    UITextField *selectedTextField;
}

@property(strong,nonatomic)NSArray* retails;
@end

@implementation LoginVC

@synthesize retails;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrayStore = [[NSMutableArray alloc] init];
    arrayApi = [[NSMutableArray alloc] init];
    
    self.retailTextField.delegate = self;

    retails = @[@"台北",@"台中",@"台南"];
    self.retailPickerView.delegate = self;
    self.retailPickerView.dataSource = self;
    
    [arrayApi addObject:[NSDictionary dictionaryWithObjectsAndKeys:kServerDomainTest,@"host",@"測試環境",@"Name", nil]];
    [arrayApi addObject:[NSDictionary dictionaryWithObjectsAndKeys:kServerDomainDevelop,@"host",@"開發環境",@"Name", nil]];
    [arrayApi addObject:[NSDictionary dictionaryWithObjectsAndKeys:kServerDomainProduction,@"host",@"正式環境",@"Name", nil]];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _TextApi.hidden = NO;
    
    //clear UI
    _accountTextField.text = @"";
    _passwordTextField.text = @"";
    _retailTextField.text = @"";
    selectedIndex_Store = -1;
    
    /*#warning Add by Justin (test omly) *******
    _accountTextField.text = @"H00677T";
    _passwordTextField.text = @"H00677T";
    //*********************************/
    
#if TARGET_IPHONE_SIMULATOR
    _accountTextField.text = @"H00926T"; //H00419T H00925T H00630T H00677T H00930T
    _passwordTextField.text = @"H00926T"; //H00419T H00925T H00630T H00677T H00930T
    _retailTextField.text = @"板橋館前店"; //光華八德店
    
#endif
    
#if Jamzoo
    _TextApi.hidden = NO;
#else
    _TextApi.hidden = NO;
#endif
    
    //抓取商店清單 2016.05.26取消-->又改回來--> 2016.06.07又取消
    //[self callGetStoreList];
    
    /*if ([UserManager shareInstance].isLogin) { //已登入就直接登入
        [ObjectManager showLodingHUDinView:self.view];
        
        NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
        [parameter setObject:[UserManager shareInstance].account forKey:@"account"];
        [parameter setObject:[UserManager shareInstance].password forKey:@"password"];
        
        @try {
            [parameter setObject:[UserManager shareInstance].storeID forKey:@"storeId"];
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
        
        [GatewayManager callLogin:parameter Source:self];
    }
    else { //未登入，抓取商店清單
        [self callGetStoreList];
    }*/
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction

/* 登入 */
- (IBAction)loginButtonClick:(id)sender {
    
    if ([_accountTextField.text isEqualToString:@""])
    {
        [self showAlertControllerWithMessage:@"請填寫員工帳號"];
    }
    else if ([_passwordTextField.text isEqualToString:@""]){
        [self showAlertControllerWithMessage:@"請填寫員工密碼"];
    }
    else{
        [self callLogin];
    }
}
- (IBAction)retailConfirmButonOnClick:(id)sender {
    NSInteger row =[self.retailPickerView selectedRowInComponent:0];
    self.retailTextField.text = retails[row];
    [self removeRetailView];
}

#pragma mark - 設置首頁 & SlideMenu
- (void)setupBasicView{
    [self.navigationController popViewControllerAnimated:YES];
    
    /*MainVC *mainVC = [[MainVC alloc] initWithNibName:@"MainVC" bundle:nil];
    GogoroNavigationController *gogoroNavigationController = [[GogoroNavigationController alloc] initWithRootViewController:mainVC];
    SlideMenuVC *slideMenuVC = [[SlideMenuVC alloc] initWithNibName:@"SlideMenuVC" bundle:nil];
    
    //UINavigationController *leftNavigationController = [[UINavigationController alloc] initWithRootViewController:slideMenuVC];
    
    MMDrawerController *drawerController = [[MMDrawerController alloc] initWithCenterViewController:gogoroNavigationController leftDrawerViewController:slideMenuVC]; //leftNavigationController
    drawerController.maximumLeftDrawerWidth = ScreenWidth / 2;
    drawerController.openDrawerGestureModeMask = MMOpenDrawerGestureModeCustom;
    drawerController.closeDrawerGestureModeMask = MMCloseDrawerGestureModeAll;
    drawerController.showsShadow = NO;
    [self presentViewController:drawerController animated:YES completion:nil];*/
}
- (void)showRetailView
{
    shadowView = [[UIView alloc] initWithFrame:self.view.bounds];
    shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    [self.view addSubview:shadowView];

    self.selectRetailView.frame = CGRectMake(0, ScreenHeight, ScreenWidth, retailViewHeight);
    [self.view addSubview:self.selectRetailView];


    [UIView animateWithDuration:1 animations:^{
        //
        shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];

        self.selectRetailView.frame = CGRectMake(0, ScreenHeight-retailViewHeight, ScreenWidth, retailViewHeight);
    }];

}

- (void)removeRetailView{
    [UIView animateWithDuration:1 animations:^{
        shadowView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        self.selectRetailView.frame = CGRectMake(0, ScreenHeight, ScreenWidth, retailViewHeight);

    } completion:^(BOOL finished) {

        [shadowView removeFromSuperview];
        [self.selectRetailView removeFromSuperview];
    }];
}
#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == self.retailTextField) {
        selectedTextField = self.retailTextField;
        [self.view endEditing:YES];
        //[self showRetailView];
        [self ShowStoreSelect];
        return NO;
    }
    else if (textField == _TextApi) {
        selectedTextField = _TextApi;
        [self.view endEditing:YES];
        [self ShowApiSelect];
        return NO;
    }
    
    selectedTextField = nil;
    
    return  YES;
}

#pragma mark - UIPickerViewDatasource & UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return retails.count;
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return retails[row];
}
#pragma mark - Call API
#pragma mark [001] 登入
- (void)callLogin {
    [ObjectManager showLodingHUDinView:self.view];
    
    //門市代號
    NSString *storeId = @"";
    @try {
        NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
        storeId = [_dic objectForKey:@"No"];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_accountTextField.text forKey:@"account"];
    [parameter setObject:_passwordTextField.text forKey:@"password"];
    //[parameter setObject:storeId forKey:@"storeId"];
    [GatewayManager callLogin:parameter Source:self];
}
- (void)callBackLogin:(NSDictionary *)parameter {

    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        //NSString *accessToken = [parameter objectForKey:@"Data"];
        NSDictionary *dataDic = [parameter objectForKey:@"Data"];
        
        //防呆，避免後台回傳null造成閃退
        if (![dataDic isKindOfClass:[NSNull class]]) {
            NSString *accessToken = dataDic[@"AccessToken"];
            NSString *storeName = [NSString stringWithFormat:@"%@",dataDic[@"StoreName"]];
            
            //相關資料存起來
            [UserManager shareInstance].isLogin = YES;
            [UserManager shareInstance].account = _accountTextField.text;
            [UserManager shareInstance].password = _passwordTextField.text;
            [UserManager shareInstance].accessToken = accessToken;
            [UserManager shareInstance].storeName = storeName;
            [UserManager shareInstance].staffName = dataDic[@"Name"];
            
            @try {
                NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
                [UserManager shareInstance].storeName = [_dic objectForKey:@"Name"];
                [UserManager shareInstance].storeID = [_dic objectForKey:@"No"];
            }
            @catch (NSException *exception) {
                
            }
            @finally {
                
            }
            
            [self setupBasicView];
        }
        else {
            [self showAlertControllerWithMessage:@"系統維護中"];
        }
        
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [002] 取得門市列表
- (void) callGetStoreList {
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetStoreList:parameter Source:self];
}
- (void)callBackGetStoreList:(NSDictionary *)parameter
{
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        _retailTextField.text = @"";
        selectedIndex_Store = -1;
        [arrayStore removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayStore addObjectsFromArray:data];
        
        //判斷之前是否已登入
        if ([UserManager shareInstance].isLogin) {
            _accountTextField.text = [UserManager shareInstance].account;
            _passwordTextField.text = [UserManager shareInstance].password;
            _retailTextField.text = [UserManager shareInstance].storeName;
            
            //判斷記憶的store的index
            for (int i = 0; i < [arrayStore count]; i++) {
                NSDictionary *_dic = [arrayStore objectAtIndex:i];
                if ([[_dic objectForKey:@"Name"] isEqualToString:[UserManager shareInstance].storeName]) {
                    selectedIndex_Store = i;
                    break;
                }
            }
            
            //已登入就直接登入
            [ObjectManager showLodingHUDinView:self.view];
            
            NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
            [parameter setObject:[UserManager shareInstance].account forKey:@"account"];
            [parameter setObject:[UserManager shareInstance].password forKey:@"password"];
            
            @try {
                [parameter setObject:[UserManager shareInstance].storeID forKey:@"storeId"];
            }
            @catch (NSException *exception) {
            }
            @finally {
            }
            
            [GatewayManager callLogin:parameter Source:self];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 選擇門市
-(void) ShowStoreSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayStore count]; i++) {
        NSDictionary *_dic = [arrayStore objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
                              style:UIAlertActionStyleDestructive
                            handler:^(UIAlertAction *action) {
                            }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _retailTextField;
    popPresenter.sourceRect = _retailTextField.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - 選擇後台
-(void) ShowApiSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayApi count]; i++) {
        NSDictionary *_dic = [arrayApi objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _TextApi;
    popPresenter.sourceRect = _TextApi.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    if (selectedTextField == self.retailTextField) {
        selectedIndex_Store = buttonIndex;
        
        NSDictionary *_dic = [arrayStore objectAtIndex:buttonIndex];
        _retailTextField.text = [_dic objectForKey:@"Name"];
    }
    else if (selectedTextField == _TextApi) {
        selectedIndex_Api = buttonIndex;
        
        NSDictionary *_dic = [arrayApi objectAtIndex:buttonIndex];
        _TextApi.text = [_dic objectForKey:@"Name"];
        
        //把選擇的後台存起來
        NSString *urlString = [_dic objectForKey:@"host"];
        [DocumentManager shareInstance].apiGatewayUrl = urlString;
        
        //重抓門市
        [self callGetStoreList];
    }
}
@end
