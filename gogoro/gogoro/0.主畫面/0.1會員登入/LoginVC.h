//
//  LoginVC.h
//  gogoro
//
//  Created by William on 2016/3/1.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateTextField.h"
@interface LoginVC : TemplateVC
@property (weak, nonatomic) IBOutlet TemplateTextField *TextApi;
@property (weak, nonatomic) IBOutlet TemplateTextField *accountTextField;
@property (weak, nonatomic) IBOutlet TemplateTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet TemplateTextField *retailTextField;

@property (strong, nonatomic) IBOutlet UIView *selectRetailView;
@property (weak, nonatomic) IBOutlet UIPickerView *retailPickerView;
@property (weak, nonatomic) IBOutlet UIButton *retailConfirmButton;

- (IBAction)loginButtonClick:(id)sender;
- (IBAction)retailConfirmButonOnClick:(id)sender;

@end
