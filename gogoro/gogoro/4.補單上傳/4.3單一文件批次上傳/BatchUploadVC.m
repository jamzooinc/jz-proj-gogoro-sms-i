//
//  BatchUploadVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "BatchUploadVC.h"
#import "OrderSearchToDoVC.h"
#import "OrderSearchLicenseStepVC.h"
#import "OrderSearchSupplementStepVC.h"

@interface BatchUploadVC () {
    NSMutableArray *arrayToDo,*arrayLicensingStep,*arraySubsidyStep;
    NSInteger selectedIndex_ToDo,selectedIndex_LicensingStep,selectedIndex_SubsidyStep;
    UITextField *selectedTextField;
}
@property (nonatomic,weak) IBOutlet UITextField *TextTodo; //待處理訂單
@property (nonatomic,weak) IBOutlet UITextField *TextLicensingStep; //領牌階段訂單
@property (nonatomic,weak) IBOutlet UITextField *TextSubsidyStep; //補助階段訂單
@end

@implementation BatchUploadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayToDo = [[NSMutableArray alloc] init];
    arrayLicensingStep = [[NSMutableArray alloc] init];
    arraySubsidyStep = [[NSMutableArray alloc] init];

    selectedIndex_ToDo = selectedIndex_LicensingStep = selectedIndex_SubsidyStep = -1;
}
#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    //記錄做動的物件
    selectedTextField = textField;
    
    //Step 0: 待處理訂單,1: 領牌管理,2: 地方補助管理, 3:TES 補助管理,4:地方&TES補助管理
    
    //沒資料就先抓資料，有資料就顯示
    if (textField == _TextTodo) { //待處理訂單
        if ([arrayToDo count] == 0) {
            [self GetAttachListWithStep:@"0"];
        }
        else {
            [self ShowSelectWithTextField:_TextTodo];
        }
    }
    else if (textField == _TextLicensingStep) { //領牌階段訂單
        if ([arrayLicensingStep count] == 0) {
            [self GetAttachListWithStep:@"1"];
        }
        else {
            [self ShowSelectWithTextField:_TextLicensingStep];
        }
    }
    else if (textField == _TextSubsidyStep) { //補助階段訂單
        if ([arraySubsidyStep count] == 0) {
            [self GetAttachListWithStep:@"4"];
        }
        else {
            [self ShowSelectWithTextField:_TextSubsidyStep];
        }
    }
    
    return NO;
}
#pragma mark - Call API
#pragma mark [404] 補件上傳_取得文件資料清單
- (void) GetAttachListWithStep:(NSString *)step {
    //Step 0: 待處理訂單,1: 領牌管理,2: 地方補助管理, 3:TES 補助管理,4:地方&TES補助管理
    
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:step forKey:@"Step"];
    [GatewayManager callGetAttachList:parameter Source:self];
}
- (void)callBackGetAttachList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *data = [parameter objectForKey:@"Data"];
        if (selectedTextField == _TextTodo) { //待處理訂單
            [arrayToDo removeAllObjects];
            [arrayToDo addObjectsFromArray:data];
        }
        else if (selectedTextField == _TextLicensingStep) { //領牌階段訂單
            [arrayLicensingStep removeAllObjects];
            [arrayLicensingStep addObjectsFromArray:data];
        }
        else if (selectedTextField == _TextSubsidyStep) { //補助階段訂單
            [arraySubsidyStep removeAllObjects];
            [arraySubsidyStep addObjectsFromArray:data];
        }
        
        [self ShowSelectWithTextField:selectedTextField];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 項目選擇
-(void) ShowSelectWithTextField:(UITextField *)textField {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    NSArray *arrayItems = nil;
    if (selectedTextField == _TextTodo) { //待處理訂單
        arrayItems = arrayToDo;
    }
    else if (selectedTextField == _TextLicensingStep) { //領牌階段訂單
        arrayItems = arrayLicensingStep;
    }
    else if (selectedTextField == _TextSubsidyStep) { //補助階段訂單
        arrayItems = arraySubsidyStep;
    }
    
    for (int i = 0; i < [arrayItems count]; i++) {
        NSDictionary *_dic = [arrayItems objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"AttachTypeName"];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = textField;
    popPresenter.sourceRect = textField.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    NSArray *arrayItems = nil;
    if (selectedTextField == _TextTodo) { //待處理訂單
        arrayItems = arrayToDo;
        selectedIndex_ToDo = buttonIndex;
    }
    else if (selectedTextField == _TextLicensingStep) { //領牌階段訂單
        arrayItems = arrayLicensingStep;
        selectedIndex_LicensingStep = buttonIndex;
    }
    else if (selectedTextField == _TextSubsidyStep) { //補助階段訂單
        arrayItems = arraySubsidyStep;
        selectedIndex_SubsidyStep = buttonIndex;
    }
    
    NSDictionary *_dic = [arrayItems objectAtIndex:buttonIndex];
    
    NSString *title = [_dic objectForKey:@"AttachTypeName"];
    
    selectedTextField.text = title;
}
#pragma mark - Button Action
-(IBAction) PressOrderToDo { //待處理訂單
    if (selectedIndex_ToDo < 0) return;
    
    NSDictionary *_dic = [arrayToDo objectAtIndex:selectedIndex_ToDo];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    OrderSearchToDoVC *destVC = [[OrderSearchToDoVC alloc] initWithNibName:@"OrderSearchToDoVC" bundle:nil];
    destVC.Step = 0; //0:   待處理訂單, 1: 領牌, 2:地方補助管理 3:TES 補助管理
    destVC.didFromBatchUpload = YES;
    destVC.AttachTypeId = AttachTypeId;
    destVC.AttachTypeName = AttachTypeName;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressLicenseStep { //領牌階段
    if (selectedIndex_LicensingStep < 0) return;
    
    NSDictionary *_dic = [arrayLicensingStep objectAtIndex:selectedIndex_LicensingStep];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    OrderSearchLicenseStepVC *destVC = [[OrderSearchLicenseStepVC alloc] initWithNibName:@"OrderSearchLicenseStepVC" bundle:nil];
    destVC.Step = 1;
    destVC.didFromBatchUpload = YES;
    destVC.AttachTypeId = AttachTypeId;
    destVC.AttachTypeName = AttachTypeName;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressSupplementStep { //補助階段
    if (selectedIndex_SubsidyStep  < 0) return;
    
    NSDictionary *_dic = [arraySubsidyStep objectAtIndex:selectedIndex_SubsidyStep];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    OrderSearchSupplementStepVC *destVC = [[OrderSearchSupplementStepVC alloc] initWithNibName:@"OrderSearchSupplementStepVC" bundle:nil];
    destVC.Step = 2;
    destVC.didFromBatchUpload = YES;
    destVC.AttachTypeId = AttachTypeId;
    destVC.AttachTypeName = AttachTypeName;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
