//
//  OrderSearchVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "OrderSearchRootVC.h"
#import "SearchResultVC.h"
#import "OrderSearchToDoVC.h"
#import "RMDateSelectionViewController.h"
#import "RMActionController.h"

@interface OrderSearchRootVC () <UITextFieldDelegate>
{
    NSMutableArray *arrayLicensingSite;     //領牌中心
    NSMutableArray *arrayStore;             //門市
    NSMutableArray *arrayPerson;            //登錄業代
    NSMutableArray *arrayLocation;          //交車地點
    NSMutableArray *arrayTES;               //TES地方
    NSMutableArray *arraySubsidyArea;       //補助地區
    NSMutableArray *arraySubsidyType;       //補助類型
    NSMutableArray *arrayStatus;            //處理狀態
    NSMutableArray *arrayOrder;             //訂單號碼／VIN／車牌
    
    NSInteger stepSupplementTesOrLocal;     //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
    NSInteger selectedIndex_LicensingSite;
    NSInteger selectedIndex_Store;
    NSInteger selectedIndex_Person;
    NSInteger selectedIndex_TES;
    NSInteger selectedIndex_Location;
    NSInteger selectedIndex_SubsidyArea;
    NSInteger selectedIndex_SubsidyType;
    NSInteger selectedIndex_Status;
    NSInteger selectedIndex_Order;          //訂單號碼:1／VIN:2／車牌:3
    
    UITextField *selectedTextField;
    BOOL isSyncing; //是否在進行同步步驟
}

@property (nonatomic,weak) IBOutlet UIButton *ButtonSend;
@property (nonatomic,weak) IBOutlet UITextField *TextLicensingCenter;         //領牌中心
@property (nonatomic,weak) IBOutlet UITextField *TextStore;                   //訂單門市
@property (nonatomic,weak) IBOutlet UITextField *TextPerson;                   //登錄業代
@property (nonatomic,weak) IBOutlet UITextField *TextTES;                     //TES地方
@property (nonatomic,weak) IBOutlet UITextField *TextLocation;                //交車地點
@property (nonatomic,weak) IBOutlet UITextField *TextSubsidyArea;             //補助地區
@property (nonatomic,weak) IBOutlet UITextField *TextSubsidyType;             //補助類型
@property (nonatomic,weak) IBOutlet UITextField *TextStatus;                  //處理狀況
@property (nonatomic,weak) IBOutlet UITextField *TextOrder;                   //訂單號碼
@property (nonatomic,weak) IBOutlet UITextField *TextOrderContent;            //查詢內容
@property (nonatomic,weak) IBOutlet UITextField *TextApplyLicensingStartDate; //申請領牌開始日期
@property (nonatomic,weak) IBOutlet UITextField *TextApplyLicensingEndDate;   //申請領牌結束日期
@property (nonatomic,weak) IBOutlet UITextField *TextCarStartDate;            //交車開始日期
@property (nonatomic,weak) IBOutlet UITextField *TextCarEndDate;              //交車結束日期
@property (nonatomic,weak) IBOutlet UITextField *TextOrderApproveStartDate;            //訂單審核開始日期
@property (nonatomic,weak) IBOutlet UITextField *TextOrderApproveEndDate;              //訂單審核結束日期
@property (nonatomic,weak) IBOutlet UITextField *TextLicensingStartDate;      //領牌開始日期
@property (nonatomic,weak) IBOutlet UITextField *TextLicensingEndDate;        //領牌結束日期
@property (nonatomic,weak) IBOutlet UITextField *TextApplySubsidyStartDate;   //申請補助開始日期
@property (nonatomic,weak) IBOutlet UITextField *TextApplySubsidyEndDate;     //申請補助結束日期
@end

@implementation OrderSearchRootVC

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayLicensingSite = [[NSMutableArray alloc] init];
    arrayStore = [[NSMutableArray alloc] init];
    arrayPerson = [[NSMutableArray alloc] init];
    arrayLocation = [[NSMutableArray alloc] init];
    arraySubsidyArea = [[NSMutableArray alloc] init];
    arraySubsidyType = [[NSMutableArray alloc] init];
    arrayStatus = [[NSMutableArray alloc] init];
    
    selectedIndex_LicensingSite = selectedIndex_Store = selectedIndex_Person = selectedIndex_TES = selectedIndex_Location = selectedIndex_SubsidyArea = selectedIndex_SubsidyType = selectedIndex_Status = -1;
    selectedIndex_Order = 0;
    
    //直接寫死資料的
    arrayTES = [[NSMutableArray alloc] initWithObjects:@"TES",@"地方", nil];
    arrayOrder = [[NSMutableArray alloc] initWithObjects:@"訂單號碼",@"VIN",@"車牌" ,nil];
    
    //取得門市列表
    [self callGetStoreList];
    [self registerForKeyboardNotifications];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_shouldSearchAgain) {
        [self PressSearchBtn];
    }
}
#pragma mark - Call API
#pragma mark [002] 取得門市列表
-(void) callGetStoreList {
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetStoreList:parameter Source:self];
}
-(void) callBackGetStoreList:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arrayStore removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayStore addObjectsFromArray:data];
        
        //交車地點用的是門市代號
        [arrayLocation removeAllObjects];
        [arrayLocation addObjectsFromArray:data];
        
        //[008] 取得門市人員列表
        [self GetAccountList];
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [008] 取得門市人員列表
- (void) GetAccountList {
    //[ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //門市代號
    /*if (selectedIndex_Store >= 0) {
        NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
        NSString *StoreId = [_dic objectForKey:@"No"];
        [parameter setObject:StoreId forKey:@"StoreNo"];
    }*/
    
    [GatewayManager callGetAccountList:parameter Source:self];
}
- (void)callBackGetAccountList:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        [arrayPerson removeAllObjects];
        [arrayPerson addObjectsFromArray:Data];
        
        /*if (selectedIndex_Person < 0) { //UI初始化時，沒選過人員
            //取得領牌中心列表
            [self GetLicensingList];
        }
        else { //選完門市，選人員
            //選擇門市人員
            [self pressStorePersonSelectBtn:nil];
        }*/
        
        //取得領牌中心列表
        [self GetLicensingList];
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [006] 取得領牌中心列表
-(void) GetLicensingList {
    //[ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetLicensingList:parameter Source:self];
}
-(void) callBackGetLicensingList:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arrayLicensingSite removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayLicensingSite addObjectsFromArray:data];
        
        //取得訂單補助地區選單
        [self GetSubsidyAreaList];
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [302] 取得訂單補助地區選單
-(void) GetSubsidyAreaList {
    //[ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetSubsidyAreaList:parameter Source:self];
}
-(void) callBackGetSubsidyAreaList:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arraySubsidyArea removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arraySubsidyArea addObjectsFromArray:data];
        
        //取得處理狀況列表
        [self GetStepState];
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [303] 取得訂單地方補助類型選單
-(void) GetSubsidyTypeListWithArea:(NSString *)area {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:area forKey:@"subsidyArea"];
    [GatewayManager callGetSubsidyTypeList:parameter Source:self];
}
-(void) callBackGetSubsidyTypeList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arraySubsidyType removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arraySubsidyType addObjectsFromArray:data];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [007] 取得處理狀況列表
-(void) GetStepState {
    //[ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:[NSString stringWithFormat:@"%ld",_Step] forKey:@"Step"];
    [GatewayManager callGetStepState:parameter Source:self];
}
-(void) callBackGetStepState:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        [arrayStatus removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayStatus addObjectsFromArray:data];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [402] 補件上傳_執行中訂單查詢
-(void) RunningList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [self GenerateApiParameter];
    
    //2016/10/12
    //4.2.1待處理訂單_篩選條件	送出查詢，不需要問是否要同步
    //4.2.3補助階段訂單_篩選條件	送出查詢，不需要問是否要同步
    //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
    if (_Step != 1) {
        [parameter setObject:@0 forKey:@"Sync"];
        [GatewayManager callRunningList:parameter Source:self];
    }
    else if (_shouldSearchAgain) {
        //UAT#61_領牌階段送申請補助完成後，回到搜尋條件頁不應跳出是否同步的系統訊息
        [parameter setObject:@0 forKey:@"Sync"];
        [GatewayManager callRunningList:parameter Source:self];
    }
    else {
        //Add by Justin 2016/9/20
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"系統通知"
                                     message:@"對搜尋結果進行DMS資料同步？\n( 注意：筆數越多所需同步時間越長 )"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"同步"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        isSyncing = YES;
                                        
                                        [parameter setObject:@1 forKey:@"Sync"];
                                        [GatewayManager callRunningList:parameter Source:self];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"不了，謝謝"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [parameter setObject:@0 forKey:@"Sync"];
                                       [GatewayManager callRunningList:parameter Source:self];
                                   }];
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(void) callBackRunningList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        if ([Data count] == 0) {
            isSyncing = NO;
            _shouldSearchAgain = NO;
            [self showAlertControllerWithMessage:@"沒有符合的資料"];
        }
        else {
            if (isSyncing) {
                //同步完，然後一樣的搜尋條件，再call一樣的API做搜尋
                //2016.11.08改為同步完成後，停留在原本畫面(不要進入下一頁)
                isSyncing = NO;
                //_shouldSearchAgain = YES;
                //[self RunningList];
                [self showAlertControllerWithMessage:@"同步完成！"];
            }
            else { //到搜尋結果
                _shouldSearchAgain = NO;
                [self GotoSearchResultWithData:Data];
            }
        }
    }
    else {
        isSyncing = NO;
        _shouldSearchAgain = NO;
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [403] 補件上傳_單一文件批次上傳
-(void) SingleAttrachType {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [self GenerateApiParameter];
    [parameter setObject:[NSString stringWithFormat:@
                          "%ld",_AttachTypeId] forKey:@"AttachType"];
    [GatewayManager callSingleAttrachType:parameter Source:self];
}
-(void) callBackSingleAttrachType:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        if ([Data count] == 0) {
            [self showAlertControllerWithMessage:@"查無訂單資料！"];
        }
        else {
            [self GotoSearchResultWithData:Data];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}


#pragma mark - TextField Delegate
-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    //記錄做動的物件
    selectedTextField = textField;
    
    if (textField == _TextOrderContent) {
        return YES;
    }
    
    if (textField == _TextLicensingCenter || //領牌中心
        textField == _TextStore ||           //訂單門市
        textField == _TextPerson ||          //登錄業代
        textField == _TextLocation ||        //交車地點
        textField == _TextTES ||             //TES地方
        textField == _TextSubsidyArea ||     //補助地區
        textField == _TextSubsidyType ||     //補助類型
        textField == _TextStatus ||          //處理狀況
        textField == _TextOrder              //訂單號碼
//        textField == TextOrderContent       //查詢內容
        ) {
        [self ShowSelectWithTextField:textField];
    }
    else if (textField == _TextApplyLicensingStartDate  || //申請領牌開始日期
             textField == _TextApplyLicensingEndDate ||    //申請領牌結束日期
             textField == _TextOrderApproveStartDate ||    //訂單審核開始日期
             textField == _TextOrderApproveEndDate ||      //訂單審核結束日期
             textField == _TextCarStartDate ||             //交車開始日期
             textField == _TextCarEndDate ||               //交車結束日期
             textField == _TextLicensingStartDate ||       //領牌開始日期
             textField == _TextLicensingEndDate ||         //領牌結束日期
             textField == _TextApplySubsidyStartDate ||    //申請補助開始日期
             textField == _TextApplySubsidyEndDate) {      //申請補助結束日期
        [self ShowDateSelectionWithTextField:textField];
    }
    
    
    return NO;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}
#pragma mark - 項目選擇
-(NSMutableArray *) GetSelectedArrayWithTextField:(UITextField *)textField {
    //用TextField取得對應陣列資料
    if (textField == _TextLicensingCenter) { //領牌中心
         return arrayLicensingSite;
    }
    else if (textField == _TextStore) { //訂單門市
        return arrayStore;
    }
    else if (textField == _TextPerson) { //登錄業代
        return arrayPerson;
    }
    else if (textField == _TextLocation) { //交車地點
        return arrayLocation;
    }
    else if (textField == _TextTES) { //TES地方
        return arrayTES;
    }
    else if (textField == _TextSubsidyArea) { //補助地區
        return arraySubsidyArea;
    }
    else if (textField == _TextSubsidyType) { //補助類型
        return arraySubsidyType;
    }
    else if (textField == _TextStatus) { //處理狀況
        return arrayStatus;
    }
    else if (textField == _TextOrder) {
        return arrayOrder;
    }
    
    return nil;
}
-(NSString *) GetSelectedKeyWithTextField:(UITextField *)textField {
    //用TextField取得Dic的Key
    if (textField == _TextLicensingCenter) { //領牌中心
        return @"Name";
    }
    else if (textField == _TextStore) { //訂單門市
        return @"Name";
    }
    else if (textField == _TextPerson) { //登錄業代
        return @"AccountName";
    }
    else if (textField == _TextLocation) { //交車地點
        return @"Name";
    }
    else if (textField == _TextTES) { //TES地方
        return nil;
    }
    else if (textField == _TextSubsidyArea) { //補助地區
        return nil;
    }
    else if (textField == _TextSubsidyType) { //補助類型
        return @"SubsidyType";
    }
    else if (textField == _TextStatus) { //處理狀況
        return nil;
    }
    
    return nil;
}
-(void) ShowSelectWithTextField:(UITextField *)textField {
    NSArray *arrayToUse = [self GetSelectedArrayWithTextField:textField];
    NSString *message = nil;
    NSString *titleKey = [self GetSelectedKeyWithTextField:textField];
    
    if (textField == _TextLicensingCenter) { //領牌中心
        message = @"領牌中心";
    }
    else if (textField == _TextStore) { //訂單門市
        message = @"訂單門市";
    }
    else if (textField == _TextPerson) { //登錄業代
        message = @"登錄業代";
    }
    else if (textField == _TextLocation) { //交車地點
        message = @"交車地點";
    }
    else if (textField == _TextTES) { //TES地方
        message = @"TES/地方";
    }
    else if (textField == _TextSubsidyArea) { //補助地區
        message = @"補助地區";
    }
    else if (textField == _TextSubsidyType) { //補助類型
        message = @"領牌類型";
    }
    else if (textField == _TextStatus) { //處理狀況
        message = @"處理狀況";
    }
    else if (textField == _TextOrder) {
        message = @"查詢項目";
    }
    
    message = [NSString stringWithFormat:@"請選擇%@",message];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayToUse count]; i++) {
        NSDictionary *_dic = [arrayToUse objectAtIndex:i];
        NSString *title = nil;
        if (titleKey) { //陣列內容是Dic
            title = [_dic objectForKey:titleKey];
        }
        else { //陣列內容是String
            title = [arrayToUse objectAtIndex:i];
        }
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = textField;
    popPresenter.sourceRect = textField.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    NSArray *arrayToUse = [self GetSelectedArrayWithTextField:selectedTextField];
    NSString *titleKey = [self GetSelectedKeyWithTextField:selectedTextField];
    
    NSDictionary *_dic = [arrayToUse objectAtIndex:buttonIndex];
    
    NSString *title = nil;
    if (titleKey) { //陣列內容是Dic
        title = [_dic objectForKey:titleKey];
    }
    else { //陣列內容是String
        title = [arrayToUse objectAtIndex:buttonIndex];
    }
    selectedTextField.text = title;
    
    //記錄Index
    if (selectedTextField == _TextLicensingCenter) { //領牌中心
        selectedIndex_LicensingSite = buttonIndex;
    }
    else if (selectedTextField == _TextStore) { //訂單門市
        selectedIndex_Store = buttonIndex;
    }
    else if (selectedTextField == _TextPerson) { //登錄業代
        selectedIndex_Person = buttonIndex;
    }
    else if (selectedTextField == _TextLocation) { //交車地點
        selectedIndex_Location = buttonIndex;
    }
    else if (selectedTextField == _TextTES) { //TES地方
        selectedIndex_TES = buttonIndex;
    }
    else if (selectedTextField == _TextSubsidyArea) { //補助地區
        selectedIndex_SubsidyArea = buttonIndex;
    }
    else if (selectedTextField == _TextSubsidyType) { //補助類型
        selectedIndex_SubsidyType = buttonIndex;
    }
    else if (selectedTextField == _TextStatus) { //處理狀況
        selectedIndex_Status = buttonIndex;
    }
    else if (selectedTextField == _TextOrder) { //訂單號碼
        selectedIndex_Order = buttonIndex;
    }
    
    //必須做後續動作的項目
    if (selectedTextField == _TextSubsidyArea) { //補助地區
        if (_Step == 2 || _Step == 3) { //0: 待處理訂單,1: 領牌階段, 2:地方補助管理 3:TES 補助管理
            //抓取補助類型
            [self GetSubsidyTypeListWithArea:title];
        }
    }
}
#pragma mark - 選擇日期
-(void) ShowDateSelectionWithTextField:(UITextField *)textField {
    NSString *message = nil;
    if (textField == _TextApplyLicensingStartDate) { //申請領牌開始日期
        message = @"申請領牌開始日期";
    }
    else if (textField == _TextApplyLicensingEndDate) { //申請領牌結束日期
        message = @"申請領牌結束日期";
    }
    else if (textField == _TextCarStartDate) { //交車開始日期
        message = @"交車開始日期";
    }
    else if (textField == _TextCarEndDate) { //交車結束日期
        message = @"交車結束日期";
    }
    else if (textField == _TextLicensingStartDate) { //領牌開始日期
        message = @"領牌開始日期";
    }
    else if (textField == _TextLicensingEndDate) { ///領牌結束日期
        message = @"領牌結束日期";
    }
    else if (textField == _TextApplySubsidyStartDate) { //申請補助開始日期
        message = @"申請補助開始日期";
    }
    else if (textField == _TextApplySubsidyEndDate) { //申請補助結束日期
        message = @"申請補助結束日期";
    }
    
    //Create select action
    RMAction *selectAction = [RMAction actionWithTitle:@"選擇" style:RMActionStyleDone andHandler:^(RMActionController *controller) {
        NSLog(@"Successfully selected date: %@", ((UIDatePicker *)controller.contentView).date);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy/MM/dd";
        NSString *stringToDelegate = [formatter stringFromDate:((UIDatePicker *)controller.contentView).date];
        //NSLog(@"stringToDelegate:%@",stringToDelegate);
        
        selectedTextField.text = stringToDelegate;
    }];
    
    //Create cancel action
    RMAction *cancelAction = [RMAction actionWithTitle:@"取消" style:RMActionStyleCancel andHandler:^(RMActionController *controller) {
        NSLog(@"Date selection was canceled");
    }];
    
    message = [NSString stringWithFormat:@"請選擇%@",message];
    
    //Create date selection view controller
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleDefault selectAction:selectAction andCancelAction:cancelAction];
    dateSelectionController.title = message;
    dateSelectionController.message = @"";
    
    //Create now action and add it to date selection view controller
    RMAction *nowAction = [RMAction actionWithTitle:@"今天" style:RMActionStyleAdditional andHandler:^(RMActionController *controller) {
        ((UIDatePicker *)controller.contentView).date = [NSDate date];
        NSLog(@"Now button tapped");
    }];
    nowAction.dismissesActionController = NO;
    
    [dateSelectionController addAction:nowAction];
    
    //設定時區
    NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    dateSelectionController.datePicker.locale = datelocale;
    dateSelectionController.datePicker.timeZone = [NSTimeZone timeZoneWithName:@"GMT+8"];
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    
    //不能大於今天
    //dateSelectionController.datePicker.minimumDate = [NSDate date];
    
    //On the iPad we want to show the date selection view controller within a popover.
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        //First we set the modal presentation style to the popover style
        dateSelectionController.modalPresentationStyle = UIModalPresentationPopover;
        
        //Then we tell the popover presentation controller, where the popover should appear
        dateSelectionController.popoverPresentationController.sourceView = self.view;
        dateSelectionController.popoverPresentationController.sourceRect = selectedTextField.frame;
    }
    
    //Now just present the date selection controller using the standard iOS presentation method
    [self presentViewController:dateSelectionController animated:YES completion:nil];
}
#pragma mark - Button Action
-(IBAction) PressSearchBtn {
    [_TextOrderContent resignFirstResponder];
    
    if (_didFromBatchUpload) { //單一文件批次上傳
        [self SingleAttrachType];
    }
    else { //補件上傳_執行中訂單查詢
        [self RunningList];
    }
}
/*-(IBAction)pressStorePersonSelectBtn:(id)sender { //門市人員選擇
    //要先選門市，再選人員
    if (selectedIndex_Store < 0) {
        [self ShowSelectWithTextField:_TextStore];
    }
    else {
        selectedTextField = _TextPerson;
        [self ShowSelectWithTextField:_TextPerson];
    }
}*/
#pragma mark - 換頁
-(void) GotoSearchResultWithData:(NSArray *)data { //搜尋結果
    SearchResultVC *destVC = [[SearchResultVC alloc] initWithNibName:@"SearchResultVC" bundle:nil];
    destVC.Step = _Step;
    destVC.Orders = data;
    destVC.didFromSupplement = _didFromSupplement;
    destVC.didFromBatchUpload = _didFromBatchUpload;
    destVC.AttachTypeId = _AttachTypeId;
    destVC.AttachTypeName = _AttachTypeName;
    destVC.stepSupplementTesOrLocal = stepSupplementTesOrLocal;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
-(NSMutableDictionary *) GenerateApiParameter {
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
    [parameter setObject:[NSString stringWithFormat:@"%ld",_Step] forKey:@"Step"];
    
    NSDictionary *_dic = nil;
    
    //領牌中心
    if (selectedIndex_LicensingSite >= 0) {
        _dic = [arrayLicensingSite objectAtIndex:selectedIndex_LicensingSite];
        NSString *No = [_dic objectForKey:@"Name"]; //No
        [parameter setObject:No forKey:@"LicensingSite"];
    }
    
    //訂單門市
    if (selectedIndex_Store >= 0) {
        _dic = [arrayStore objectAtIndex:selectedIndex_Store];
        NSString *No = [_dic objectForKey:@"No"];
        [parameter setObject:No forKey:@"StoreId"];
    }
    
    //登錄業代
    if (selectedIndex_Person >= 0) {
        _dic = [arrayPerson objectAtIndex:selectedIndex_Person];
        NSString *No = [_dic objectForKey:@"AccountNo"];
        [parameter setObject:No forKey:@"CreatorStaff"];
    }
    
    //交車地點
    if (selectedIndex_Location >= 0) {
        _dic = [arrayLocation objectAtIndex:selectedIndex_Location];
        NSString *No = [_dic objectForKey:@"No"];
        [parameter setObject:No forKey:@"DeliverySiteId"];
    }
    
    //補助地區
    if (selectedIndex_SubsidyArea >= 0) {
        NSString *SubsidyArea = [arraySubsidyArea objectAtIndex:selectedIndex_SubsidyArea];
        [parameter setObject:SubsidyArea forKey:@"SubsidyArea"];
    }
    
    //補助類型
    if (selectedIndex_SubsidyType >= 0) {
        _dic = [arraySubsidyType objectAtIndex:selectedIndex_SubsidyType];
        NSString *SubsidyType = [_dic objectForKey:@"SubsidyType"];
        [parameter setObject:SubsidyType forKey:@"SubsidyType"];
    }
    
    //處理狀況
    if (selectedIndex_Status >= 0) {
        NSString *status = [arrayStatus objectAtIndex:selectedIndex_Status];
        [parameter setObject:status forKey:@"State"];
    }
    
    //TES地方
    if (selectedIndex_TES >= 0) {
        NSString *TES = [arrayTES objectAtIndex:selectedIndex_TES];
        
        //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
        if ([TES isEqualToString:@"地方"]) {
            stepSupplementTesOrLocal = 1;
        }
        else if ([TES isEqualToString:@"TES"]) {
            stepSupplementTesOrLocal = 2;
        }
    }
    //Add by Justin 2016/9/20*******
    //訂單號碼
    if (_TextOrderContent.text.length > 0) {
        NSString *order = [arrayOrder objectAtIndex:selectedIndex_Order];
        
        if ([order isEqualToString:@"訂單號碼"]) {
            [parameter setObject:_TextOrderContent.text forKey:@"OrderNo"];
        }
        else if ([order isEqualToString:@"VIN"]) {
            [parameter setObject:_TextOrderContent.text forKey:@"VinNo"];
        }
        else if ([order isEqualToString:@"車牌"]) {
            [parameter setObject:_TextOrderContent.text forKey:@"Plate"];
        }
    }
    //**********************

    //訂單審核開始日期
    if ([_TextOrderApproveStartDate.text length] > 0) {
        [parameter setObject:_TextOrderApproveStartDate.text forKey:@"OrderApprovalStartDate"];
    }
    
    //訂單審核結束日期
    if ([_TextOrderApproveEndDate.text length] > 0) {
        [parameter setObject:_TextOrderApproveEndDate.text forKey:@"OrderApprovalEndDate"];
    }
    
    //申請領牌日期開始
    if ([_TextApplyLicensingStartDate.text length] > 0) {
        [parameter setObject:_TextApplyLicensingStartDate.text forKey:@"LicensingRequestStartDate"];
    }
    
    //申請領牌日期結束
    if ([_TextApplyLicensingEndDate.text length] > 0) {
        [parameter setObject:_TextApplyLicensingEndDate.text forKey:@"LicensingRequestEndDate"];
    }
    
    //承諾交車日期開始
    if ([_TextCarStartDate.text length] > 0) {
        [parameter setObject:_TextCarStartDate.text forKey:@"DeliverStartDate"];
    }
    
    //承諾交車日期結束
    if ([_TextCarEndDate.text length] > 0) {
        [parameter setObject:_TextCarEndDate.text forKey:@"DeliverEndDate"];
    }
    
    //領牌日期開始
    if ([_TextLicensingStartDate.text length] > 0) {
        [parameter setObject:_TextLicensingStartDate.text forKey:@"LicensingStartDate"];
    }
    
    //領牌日期結束
    if ([_TextLicensingEndDate.text length] > 0) {
        [parameter setObject:_TextLicensingEndDate.text forKey:@"LicensingEndDate"];
    }
    

    //補助日期開始
    /*if ([_TextLicensingStartDate.text length] > 0) {
     [parameter setObject:_TextLicensingStartDate.text forKey:@"LicensingStartDate"];
     }
     
     //補助日期結束
     if ([_TextLicensingEndDate.text length] > 0) {
     [parameter setObject:_TextLicensingEndDate.text forKey:@"LicensingEndDate"];
     }*/
    
    return parameter;
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHidden:) name:UIKeyboardDidHideNotification object:nil];
}
//selectedTextField
// Called when the UIKeyboardDidShowNotification is sent.
-(void)keyboardWasShown:(NSNotification*)aNotification
{
    /*NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, selectedTextField.frame.origin) ) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - kbSize.height, self.view.frame.size.width, self.view.frame.size.height);
    }*/
    
    CGRect keyboardBounds;
    [[aNotification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSLog(@"keyboardBounds:%@",NSStringFromCGRect(keyboardBounds));
    
    CGRect kRectTextField = _TextOrderContent.frame;
    
    //NSLog(@"kRect:%@",NSStringFromCGRect(kRect));
    //NSLog(@"kRectTextField:%@",NSStringFromCGRect(kRectTextField));
    
    //NSLog(@"self.view.frame:%@",NSStringFromCGRect(self.view.frame));
    
    CGFloat yToBe = keyboardBounds.origin.y - kRectTextField.size.height;
    //NSLog(@"yToBe:%lf",yToBe);
    
    CGFloat currentY = kRectTextField.origin.y + kRectTextField.size.height/2;
    
    CGFloat offset = currentY - yToBe;
    
    if (offset > 0) {
        CGRect rectToBe = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        rectToBe.origin.y = rectToBe.origin.y - offset;
        
        [self.view setFrame:rectToBe];
        //NSLog(@"_TextOrderContent:%@",NSStringFromCGRect(_TextOrderContent.frame));
    }
    else {
        
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    //
    
//    scrollView.contentInset = contentInsets;
//    scrollView.scrollIndicatorInsets = contentInsets;
}
-(void) keyboardDidHidden:(NSNotification*)aNotification {
    //[_TextOrderContent resignFirstResponder];
    
}
@end
