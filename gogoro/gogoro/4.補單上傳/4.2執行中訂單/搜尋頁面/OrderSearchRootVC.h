//
//  OrderSearchVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderSearchRootVC : TemplateVC
@property NSInteger Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
@property BOOL didFromSupplement; //YES:從補單上傳過來
@property BOOL didFromBatchUpload; //YES:從4.3單一文件批次上傳來
@property NSInteger AttachTypeId; //文件類型代碼（從4.3單一文件批次上傳來）
@property (nonatomic,strong) NSString *AttachTypeName; //文件名稱（從4.3單一文件批次上傳來）
@property BOOL shouldSearchAgain; //是否要重新搜尋一次
@end
