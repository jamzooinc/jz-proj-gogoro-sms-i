

#import <UIKit/UIKit.h>

@interface SearchResultVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageBG;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderNo;
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerName;
@property (nonatomic, weak) IBOutlet UILabel *LabelLoginSales;
@property (nonatomic, weak) IBOutlet UILabel *LabelSales;
@property (nonatomic, weak) IBOutlet UILabel *LabelDays;
@property (nonatomic, weak) IBOutlet UIImageView *ImageStuck;
@property (nonatomic,weak) IBOutlet UIButton *ButtonEdit;

//Add at version 2.0 by Justin in 2016/09/19
@property (nonatomic, weak) IBOutlet UILabel *LabelStoreName;
@property (nonatomic, weak) IBOutlet UILabel *LabelVIN;
@property (nonatomic, weak) IBOutlet UILabel *LabelLicencePlate;
@property (weak, nonatomic) IBOutlet UILabel *BadgeNumber;

@end
