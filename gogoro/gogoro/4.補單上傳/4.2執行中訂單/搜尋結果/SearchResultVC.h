

#import <UIKit/UIKit.h>

@interface SearchResultVC : TemplateVC
@property (strong, nonatomic) NSArray *Orders;
@property NSInteger Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
@property BOOL didFromSupplement; //YES:從補單上傳過來
@property BOOL didFromBatchUpload; //YES:從4.3單一文件批次上傳來
@property NSInteger AttachTypeId; //文件類型代碼（從4.3單一文件批次上傳來）
@property (nonatomic,strong) NSString *AttachTypeName; //文件名稱（從4.3單一文件批次上傳來）
@property NSInteger stepSupplementTesOrLocal; //補助階段篩選 1: 地方補助管理,2:TES 補助管理 (訂單為補助階段且須區分時才填)
@end
