

#import "SearchResultVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "SearchResultVCCell.h"
#import "ConfirmInfoVC.h"
#import "GetOrderDetailVC.h"
#import "UploadFileVC.h"

#define ACTION_HEIGHT 61 //底下箭頭Bar的高度

typedef enum : NSUInteger {
    ResignType_NotSelect = 0,
    ResignType_OrderContract, //重簽訂購單
    ResignType_PersonalData, //重簽個資
    ResignType_DeductContract, //重簽補助/放棄補助切結書
    ResignType_call104, //從api 104取的
} ResignType;

@interface SearchResultVC () {
    NSInteger selectedIndex,selectedIndex_ResignDocument;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
    NSMutableArray *arrayCommand;
    NSMutableArray *arrayContracts;
    ResignType selectedResignType;
    BOOL didGetDeductType; //先取的該訂單是補助還是放棄補助
    NSInteger deductAttachType; //補助還是放棄補助
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UIView *ViewOrderAction;
@property (nonatomic,weak) IBOutlet UIImageView *ImageArrow;
@end

@implementation SearchResultVC

-(void) viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayTableView = [[NSMutableArray alloc] init];
    arrayContracts = [[NSMutableArray alloc] init];
    arrayCommand =  [[NSMutableArray alloc] init];
    
    for (NSDictionary *_dic in _Orders) {
        NSMutableDictionary *dic1 = [NSMutableDictionary dictionaryWithDictionary:_dic];
        [arrayTableView addObject:dic1];
    }
    
    selectedResignType = ResignType_NotSelect;
    
    //arrayCommand = [[NSMutableArray alloc] initWithObjects:@"編輯此訂單",@"重簽訂購契約書",@"重簽個資",@"重簽切結書",@"刪除此訂單", nil];
    
    //待處理訂單要調整UI，顯示功能按鈕 //2016.05.07 取消判斷
    /*if (_Step == 0 && !_didFromBatchUpload && [arrayTableView count] > 0) { //Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
        _ViewOrderAction.hidden = NO;
        _TableView.frame = CGRectMake(_TableView.frame.origin.x, _TableView.frame.origin.y, _TableView.frame.size.width, _TableView.frame.size.height - ACTION_HEIGHT);
    }*/
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
    
    //待處理訂單要調整UI，離開的時候隱藏功能按鈕
    if (_Step == 0 && !_didFromBatchUpload) { //Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
        [self HideBar];
    }
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchResultVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SearchResultVCCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    SearchResultVCCell *myCell = (SearchResultVCCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    //Add by jstin
    myCell.BadgeNumber.hidden = YES;
    
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //訂單編號
    myCell.LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",[_dic objectForKey:@"OrderNo"]];
    
    //車主姓名
    myCell.LabelOwnerName.text = [NSString stringWithFormat:@"車主姓名：%@",[_dic objectForKey:@"CustomerName"]];
    
    //登入業代
    myCell.LabelLoginSales.text = [NSString stringWithFormat:@"登錄業代：%@",[_dic objectForKey:@"LoginSalesName"]];
    
    //銷售業代
    myCell.LabelSales.text = [NSString stringWithFormat:@"銷售人員：%@",[_dic objectForKey:@"CreateSalesName"]];
    
    ////Add at version 2.0 by Justin in 2016/09/19
    //訂單門市
    myCell.LabelStoreName.text = [NSString stringWithFormat:@"訂單門市：%@",[_dic objectForKey:@"StoreName"]];
    
    //VIN
    myCell.LabelVIN.text = [NSString stringWithFormat:@"VIN：%@",[_dic objectForKey:@"VinNo"]];
    
    //車牌
    myCell.LabelLicencePlate.text = [NSString stringWithFormat:@"車牌：%@",[_dic objectForKey:@"Plate"]];
    
    //Padge
    //myCell.BadgeNumber.text = [NSString stringWithFormat:<#(nonnull NSString *), ...#>]
    
    //領牌階段、補助階段的訂單清單，不應出現"編輯(鉛筆)"圖示
    if (_Step == 0) { //Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
        myCell.ButtonEdit.hidden = NO;
        myCell.BadgeNumber.hidden = NO;
    }
    else {
        myCell.ButtonEdit.hidden = YES;
        myCell.BadgeNumber.hidden = YES;
    }
    
    //卡關天數
    myCell.LabelDays.text = [NSString stringWithFormat:@"%@天",[_dic objectForKey:@"StepMissDays"]];
    
    if (_didFromBatchUpload) { //從4.3單一文件批次上傳來
        //調整UI
        myCell.ButtonEdit.hidden = YES;
        myCell.LabelDays.frame = CGRectOffset(myCell.LabelDays.frame, 89, 0);
        myCell.ImageStuck.frame = CGRectOffset(myCell.ImageStuck.frame, 89, 0);
    }
    else {
        
        
    }
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - 變更
-(void) SelectOrderAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    //2016.05.07 取消判斷
    /*if (_Step == 0 && !_didFromBatchUpload) { //Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
        [self ShowBar]; //顯示功能選單
    }
    else*/
    if (_didFromBatchUpload) { //從4.3單一文件批次上傳來，到上傳
        NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
        [self GotoUploadFileVCWithType:_AttachTypeId AttachName:_AttachTypeName OrderNo:OrderNo];
        
        //先抓取訂單明細
        //[self GetOrderDetailWithOrderNo:orderNo];
    }
    else { //到補單功能
        //訂單編號
        NSString *orderNo = [_dic objectForKey:@"OrderNo"];
        [self GotoConfirmInfoVCWithOrderNo:orderNo];
    }
}
#pragma mark - 編輯
-(void) EditOrderAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    //NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    if (_didFromBatchUpload) { //從4.3單一文件批次上傳來，到上傳
        
    }
    else {
        //2016.11.04 取得要簽名的文件
        [self callGetOrderContractList];
    }
}
#pragma mark - 顯示/隱藏待處理訂單功能Bar
-(IBAction) ShowOrHideBar:(UIButton *)sender{
    if (sender.tag == 0) { //隱藏 --> 顯示
        sender.tag = 1;
        _ImageArrow.image = [UIImage imageNamed:@"icn_downarrow.png"];
        [self ShowBar];
    }
    else { //顯示 --> 隱藏
        sender.tag = 0;
        _ImageArrow.image = [UIImage imageNamed:@"icn_uparrow.png"];
        [self HideBar];
    }
}
- (void) ShowBar {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGFloat y = ScreenHeight - _ViewOrderAction.frame.size.height;
        _ViewOrderAction.frame = CGRectMake(_ViewOrderAction.frame.origin.x, y, _ViewOrderAction.frame.size.width, _ViewOrderAction.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}
- (void) HideBar {
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGFloat y = ScreenHeight - ACTION_HEIGHT;
        _ViewOrderAction.frame = CGRectMake(_ViewOrderAction.frame.origin.x, y, _ViewOrderAction.frame.size.width, _ViewOrderAction.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - 編輯功能選單
-(void) ShowCommandSelect {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
    SearchResultVCCell *myCell = [_TableView cellForRowAtIndexPath:indexPath];
    
    CGRect rect = [myCell.ButtonEdit convertRect:myCell.ButtonEdit.bounds toView:self.view];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    [arrayCommand removeAllObjects];
    [arrayCommand addObject:@"編輯此訂單"];
    
    //動態文件的功能列表
    for (int i = 0; i < [arrayContracts count]; i++) {
        NSDictionary *_dic = [arrayContracts objectAtIndex:i];
        
        NSString *AttachName = [_dic objectForKey:@"AttachName"];
        NSString *title = [NSString stringWithFormat:@"重簽%@",AttachName];
        
        [arrayCommand addObject:title];
    }

    
    [arrayCommand addObject:@"刪除此訂單"];
    
    for (int i = 0; i < [arrayCommand count]; i++) {
        NSString *title = [arrayCommand objectAtIndex:i];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }

    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = self.view;
    popPresenter.sourceRect = rect;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    NSString *title = [arrayCommand objectAtIndex:buttonIndex];
   
    if ([title isEqualToString:@"編輯此訂單"]) {
        [self pressEditBtn];
    }
    /*else if ([title isEqualToString:@"重簽訂購契約書"]) {
        [self pressResignOrderContractBtn];
    }
    else if ([title isEqualToString:@"重簽個資"]) {
        [self pressResignPersonalBtn];
    }
    else if ([title isEqualToString:@"重簽切結書"]) {
        [self pressDeductContractBtn];
    }*/
    else if ([title isEqualToString:@"刪除此訂單"]) {
        [self pressDeleteBtn];
    }
    else { //重簽文件
        selectedIndex_ResignDocument = buttonIndex - 1; //因為第一個是編輯
        [self pressResignContractBtn];
    }
}
#pragma mark - 選單功能
-(IBAction) pressEditBtn { //編輯此訂單
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    //先抓取訂單明細
    [self GetOrderDetailWithOrderNo:orderNo];
}
-(IBAction) pressDeleteBtn { //刪除此訂單
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    [self DeleteOrderWithOrderNo:orderNo];
}
-(IBAction) pressResignContractBtn { //重簽文件
    selectedResignType = ResignType_call104;
    
    NSDictionary *_dic = [arrayContracts objectAtIndex:selectedIndex_ResignDocument];
    NSInteger AttachType = [[_dic objectForKey:@"AttachType"] integerValue];
    
    //重套版
    [self ReAapplyDocumentWithAttachType:AttachType];
}
-(IBAction) pressResignOrderContractBtn { //重簽訂購契約書
    selectedResignType = ResignType_OrderContract;
    
    [self ReAapplyDocumentWithAttachType:36];
}
-(IBAction) pressResignPersonalBtn { //重簽個資
    selectedResignType = ResignType_PersonalData;
    
    [self ReAapplyDocumentWithAttachType:103];
}
-(IBAction) pressDeductContractBtn { //重簽切結書
    selectedResignType = ResignType_DeductContract;
    didGetDeductType = YES;
    [self callGetPreDeductContract]; //先取得是補助還是放棄補助
}
#pragma mark - Call API
#pragma mark [301] 取得訂單
- (void) GetOrderDetailWithOrderNo:(NSString *)orderNo {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:orderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void)callBackGetOrderDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //編輯文件，到詳細頁
        [self GotoGetOrderDetailVCWithData:Data];
        
        /*if (_didFromBatchUpload) { //從4.3單一文件批次上傳來，到上傳
           NSInteger AttachTypeId = [[Data objectForKey:@"AttachType"] integerValue];
            NSString *AttachTypeName = [Data objectForKey:@"AttachTypeName"];
            NSString *OrderNo = [Data objectForKey:@"OrderNo"];
            [self GotoUploadFileVCWithType:AttachTypeId AttachName:AttachTypeName OrderNo:OrderNo];
        }
        else { //編輯文件，到詳細頁
            [self GotoGetOrderDetailVCWithData:Data];
        }*/
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [313] 刪除訂單
- (void) DeleteOrderWithOrderNo:(NSString *)orderNo {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:orderNo forKey:@"orderNo"];
    [GatewayManager callOrderDelete:parameter Source:self];
}
- (void)callBackOrderDelete:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //更新UI
        [arrayTableView removeObjectAtIndex:selectedIndex];
        [_TableView reloadData];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [307] 取得補助內扣款切結書
- (void)callGetPreDeductContract {
    if (didGetDeductType) { //取得Type的時候在loading
        [ObjectManager showLodingHUDinView:self.view];
    }
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:orderNo forKey:@"orderNo"];
    [GatewayManager callGetPreDeductContract:parameter Source:self];
}
- (void)callBackGetPreDeductContract:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSDictionary *dicDeductContact = [[NSDictionary alloc] initWithDictionary:Data];
        
        //依據不同的回覆資料，顯示不同的選項
        NSDictionary *Attach = [dicDeductContact objectForKey:@"Attach"];
        
        if (didGetDeductType) { //取得是補助或放棄補助後，進行重新套版
            didGetDeductType = NO;
            
            //atachtype 28: 補助說明切結書 29: 補助放棄切結書
            deductAttachType = [[Attach objectForKey:@"Type"] integerValue];
            [self ReAapplyDocumentWithAttachType:deductAttachType];
        }
        else {
            [ObjectManager hideHUDForView:self.view];
            
            NSString *deductDocumentUrl = [Attach objectForKey:@"FileUrl"];
            NSLog(@"網址:%@",deductDocumentUrl);
            
            //檢視補助/放棄補助切結書
            [self GotoConsentVCDeductContractWithUrl:deductDocumentUrl];
        }
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [103] 文件重新套版
- (void) ReAapplyDocumentWithAttachType:(NSInteger)attachType {
    //補助切結書時，不用重複顯示loading
    if (selectedResignType != ResignType_DeductContract) {
        [ObjectManager showLodingHUDinView:self.view];
    }
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:orderNo forKey:@"OrderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callReAapplyDocument:parameter Source:self];
}
- (void)callBackReAapplyDocument:(NSDictionary *)parameter {
    //補助切結書時，不用關閉loading
    if (selectedResignType != ResignType_DeductContract) {
        [ObjectManager hideHUDForView:self.view];
    }
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        if (selectedResignType == ResignType_OrderContract) { //檢視重簽訂購單
            [self GotoConsentVCOrderContract];
        }
        else if (selectedResignType == ResignType_PersonalData) { //檢視個資
            [self GotoConsentVCPersonData];
        }
        else if (selectedResignType == ResignType_DeductContract) { //呼叫API 307以取得FileUrl
            [self callGetPreDeductContract];
        }
        else if (selectedResignType == ResignType_call104) {
            [self GotoConsentVCResign];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [104] 訂購簽名文件及順序
-(void)callGetOrderContractList{
    [ObjectManager showLodingHUDinView:self.view];
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderContractList:parameter Source:self];
}
-(void)callBackGetOrderContractList:(NSDictionary *)parameter {
    
    [ObjectManager hideHUDForView:self.view];
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *data = [parameter objectForKey:@"Data"];
        
        [arrayContracts removeAllObjects];
        [arrayContracts addObjectsFromArray:data[@"Contracts"]];
        
        [self ShowCommandSelect]; //顯示功能選單
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoConfirmInfoVCWithOrderNo:(NSString *)orderNo { //3.6確認資訊
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];

    ConfirmInfoVC *destVC = [[ConfirmInfoVC alloc] initWithNibName:@"ConfirmInfoVC" bundle:nil];
    destVC.OrderNo = orderNo;
    destVC.CustomerName = [_dic objectForKey:@"CustomerName"];
    destVC.didFromSupplement = YES;
    destVC.Step = _Step;
    destVC.stepSupplementTesOrLocal = _stepSupplementTesOrLocal;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoGetOrderDetailVCWithData:(NSDictionary *)data { //訂單詳細
    GetOrderDetailVC *destVC = [[GetOrderDetailVC alloc] initWithNibName:@"GetOrderDetailVC" bundle:nil];
    destVC.dicData = data;
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName OrderNo:(NSString *)orderNo { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.OrderNo = orderNo;
    destVC.type = attachType;
    destVC.attachName = attachName;
    destVC.didFromBatchUpload = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentVCOrderContract { //檢視訂購契約書
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_ORDER_CUSTOMER;
    destVC.OrderNo = orderNo;
    destVC.isReSign = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentVCPersonData { //檢視個資
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_ORDER_CONTRACT_PERSONAL;
    destVC.OrderNo = orderNo;
    destVC.isReSign = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentVCDeductContractWithUrl:(NSString *)deductDocumentUrl { //檢視補助/放棄補助切結書
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    //設定給下一頁判斷是補助或是放棄補助
    NSMutableDictionary *dicSubsidy = [[NSMutableDictionary alloc] init];
    if (deductAttachType == 28) { //補助
        [dicSubsidy setObject:@"2" forKey:@"SubsidyType"];
    }
    else if (deductAttachType == 29) { //放棄補助
        [dicSubsidy setObject:@"1" forKey:@"SubsidyType"];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_ORDER_DEDUCT_CONTRACT;
    destVC.OrderNo = orderNo;
    destVC.isReSign = YES;
    destVC.dicSubsidy = dicSubsidy;
    destVC.deductDocumentUrl = deductDocumentUrl;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentVCResign { //重簽
    NSDictionary *_dic1 = [arrayContracts objectAtIndex:selectedIndex_ResignDocument];
    NSInteger AttachType = [[_dic1 objectForKey:@"AttachType"] integerValue];
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSString *orderNo = [_dic objectForKey:@"OrderNo"];
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_CONTRACT_RESIGN;
    destVC.OrderNo = orderNo;
    destVC.isReSign = YES;
    destVC.newAttachType = AttachType;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectOrderAtIndex:) name:@"kNotification_Select_Order" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EditOrderAtIndex:) name:@"kNotification_Edit_Order" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Select_Order" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Edit_Order" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 縮鍵盤
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


@end
