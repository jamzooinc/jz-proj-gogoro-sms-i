

#import "SearchResultVCCell.h"

@implementation SearchResultVCCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setEditing:YES animated:YES];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected) {
        //_ImageBG.image = [UIImage imageNamed:@"list03_press.png"];
    }
    else {
        //_ImageBG.image = [UIImage imageNamed:@"list03.png"];
    }
}
-(IBAction) pressSelectBtn:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Select_Order" object:[NSNumber numberWithInteger:self.tag]];
}
-(IBAction) pressEditBtn:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Edit_Order" object:[NSNumber numberWithInteger:self.tag]];
}
- (void)dealloc {
}
@end
