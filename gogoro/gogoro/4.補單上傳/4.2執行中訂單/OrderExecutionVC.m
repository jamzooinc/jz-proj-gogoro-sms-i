//
//  OrderExecutionVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "OrderExecutionVC.h"
#import "OrderSearchToDoVC.h"
#import "OrderSearchLicenseStepVC.h"
#import "OrderSearchSupplementStepVC.h"

@interface OrderExecutionVC ()

@end

@implementation OrderExecutionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
}
#pragma mark - Button Action
-(IBAction) PressOrderToDo { //待處理訂單
    OrderSearchToDoVC *destVC = [[OrderSearchToDoVC alloc] initWithNibName:@"OrderSearchToDoVC" bundle:nil];
    destVC.Step = 0; //0:   待處理訂單, 1: 領牌, 2:地方補助管理 3:TES 補助管理
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
    
}
-(IBAction) PressLicenseStep { //領牌階段
    OrderSearchLicenseStepVC *destVC = [[OrderSearchLicenseStepVC alloc] initWithNibName:@"OrderSearchLicenseStepVC" bundle:nil];
    destVC.Step = 1;
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressSupplementStep { //補助階段
    OrderSearchSupplementStepVC *destVC = [[OrderSearchSupplementStepVC alloc] initWithNibName:@"OrderSearchSupplementStepVC" bundle:nil];
    destVC.Step = 2;
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
