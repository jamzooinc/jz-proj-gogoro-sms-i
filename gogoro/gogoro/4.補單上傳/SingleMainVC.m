

#import "SingleMainVC.h"
#import "TrackOrderVC.h"
#import "BatchUploadVC.h"
#import "SingleSupplementVC.h"

@interface SingleMainVC ()

@end

@implementation SingleMainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
}
#pragma mark - Button Action
-(IBAction) PressSingleOrder { //4.1 執行文件收集
    SingleSupplementVC *destVC = [[SingleSupplementVC alloc] initWithNibName:@"SingleSupplementVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressExecutingOrder { //4.2 APPLE管理
    TrackOrderVC *destVC = [[TrackOrderVC alloc] initWithNibName:@"TrackOrderVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressBatchUpload { //4.3 金錢追蹤
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知"
                                                                             message:@"此功能即將開放！"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"謝謝"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alertController addAction:submitAction];
    [self presentViewController:alertController animated:YES completion:nil];
    [self showAlertControllerWithMessage:@""];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
