//
//  SingleSupplementVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "SingleSupplementVC.h"
#import "GetOrderVC.h"
#import "OrderExecutionVC.h"
#import "BatchUploadVC.h"

@interface SingleSupplementVC ()

@end

@implementation SingleSupplementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
    [self LeftButtonBack];
}
#pragma mark - Button Action
-(IBAction) PressSingleOrder { //單一訂單編號查詢（使用3.2撈取訂單元件）
    GetOrderVC *destVC = [[GetOrderVC alloc] initWithNibName:@"GetOrderVC" bundle:nil];
    destVC.didFromSupplement = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressExecutingOrder { //執行中訂單
    OrderExecutionVC *destVC = [[OrderExecutionVC alloc] initWithNibName:@"OrderExecutionVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressBatchUpload { //批次上傳
    BatchUploadVC *destVC = [[BatchUploadVC alloc] initWithNibName:@"BatchUploadVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
