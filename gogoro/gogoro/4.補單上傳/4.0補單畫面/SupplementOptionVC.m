//
//  GetOrderVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "SupplementOptionVC.h"
#import "GetOrderDetailVC.h"
#import "UploadFileVC.h"

@interface SupplementOptionVC () {
    NSInteger selectedIndex;
    NSMutableArray *arrayData;
}
@property (nonatomic,weak) IBOutlet UIView *ViewSearch;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOption;
@end

@implementation SupplementOptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"補件上傳" Image:[UIImage imageNamed:@"icn_bj.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayData = [[NSMutableArray alloc] init];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self GetOrderSingleAttach];
}
#pragma mark [401] 補件上傳_單一訂單編號查詢
- (void) GetOrderSingleAttach {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callSingleOrder:parameter Source:self];
}
- (void)callBackSingleOrder:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        if ([Data count] == 0) { //判斷有沒有資料
            [self showAlertControllerWithMessage:@"查無訂單或不需要補件"];
        }
        else {
            selectedIndex = -1;
            [arrayData removeAllObjects];
            [arrayData addObjectsFromArray:Data];
        }
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - IBAction
- (IBAction)submitButtonClick:(id)sender {
    if (selectedIndex >= 0) { //防呆
        NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
        NSInteger AttachTypeId = [[_dic objectForKey:@"AttachType"] integerValue];
        NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
        NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
        [self GotoUploadFileVCWithType:AttachTypeId AttachName:AttachTypeName OrderNo:OrderNo];
        
        //清除UI
        [_ButtonOption setTitle:@"" forState:UIControlStateNormal];
    }
}
- (IBAction) pressOptionBtn {
    [self ShowOptionSelect]; //顯示選項
}
#pragma mark - 選擇選項
-(void) ShowOptionSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇補件選項" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayData count]; i++) {
        NSDictionary *_dic = [arrayData objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"AttachTypeName"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonOption;
    popPresenter.sourceRect = _ButtonOption.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    selectedIndex = buttonIndex;
    
    NSDictionary *_dic = [arrayData objectAtIndex:buttonIndex];
    [_ButtonOption setTitle:[_dic objectForKey:@"AttachTypeName"] forState:UIControlStateNormal];
}
#pragma mark - 換頁
-(void) GotoGetOrderDetailVCWithData:(NSDictionary *)data {
    GetOrderDetailVC *destVC = [[GetOrderDetailVC alloc] initWithNibName:@"GetOrderDetailVC" bundle:nil];
    destVC.dicData = data;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName OrderNo:(NSString *)orderNo { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.OrderNo = orderNo;
    destVC.type = attachType;
    destVC.attachName = attachName;
    destVC.didFromSupplement = YES;
    //destVC.dicData = [arrayData objectAtIndex:selectedIndex];
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
