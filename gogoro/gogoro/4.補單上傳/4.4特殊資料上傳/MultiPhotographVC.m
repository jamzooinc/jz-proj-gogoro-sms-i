//
//  PhotographVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "MultiPhotographVC.h"
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)

@interface MultiPhotographVC () {
    UIImagePickerController *imagePicker;
    NSMutableArray *arrayTempImages; //照片的暫存
    NSInteger totalImagesNeed; //總共要拍多少照片
    NSInteger currentIndex_Image; //現在拍到第幾個
    UIImage *selectedImage;
}
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIImageView *ImageSelectd;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@property (nonatomic,weak) IBOutlet UIButton *ButtonTakePhoto;
@property (nonatomic,weak) IBOutlet UIView *ViewOverlay;
@property (nonatomic,weak) IBOutlet UIImageView *ImageMask;
@property (nonatomic,weak) IBOutlet UIView *ViewTips;
@property (nonatomic,weak) IBOutlet UILabel *LabelTips;
@end

@implementation MultiPhotographVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _LabelTitle.text = _attachName;
    [self setupNavigationItemTitle:_attachName Image:nil];
    
    if (_type == 31) { //行照
        _LabelTips.text = @"請拍攝封面(機關用印面)";
    }
    else {
        _LabelTips.text = @"請拍攝正面";
    }
    
    arrayTempImages = [[NSMutableArray alloc] init];
    
    totalImagesNeed = 2; //總共要拍多少照片
    currentIndex_Image = 0; //現在拍到第幾個
    
    //模擬器不能拍照，所以要把照片放到doc目錄下進行測試
#if TARGET_IPHONE_SIMULATOR
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempImageMulti1"]];
    NSLog(@"圖檔放置路徑:%@",imagePath);
#else
    [self performSelector:@selector(initImagePicker) withObject:nil afterDelay:0.5f];
#endif
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
#pragma mark - 相機
-(void) initImagePicker {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { //沒相機
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"無法開啟相機！" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return;
    }
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = _sourceType;
    
    //某些文件的拍照要用Overlay，相簿不能
    if (_sourceType == UIImagePickerControllerSourceTypeCamera && _shouldMask) {
        imagePicker.showsCameraControls = NO;
        imagePicker.cameraOverlayView = _ViewOverlay;
        _ViewOverlay.hidden = NO;
        _ImageMask.hidden = NO;
        //imagePicker.allowsEditing = YES;
        
        //行照更換遮罩圖檔
        if (_isVehicleLicense) {
            _ImageMask.image = [UIImage imageNamed:@"bg_v_liscense_shot.png"];
            _ImageSelectd.frame = CGRectMake(167, 152, 458, 621);
        }
        else {
            _ImageSelectd.frame = CGRectMake(163.0/2 ,440.0/2 ,1208.0/2 ,855.0/2);
        }
        
        //navigation bar 跟 拍攝框 中間的地方
        //_LabelTips.frame = _ImageSelectd.frame;
        _LabelTips.frame = CGRectMake(0, 0 , _ImageSelectd.frame.size.width, 50);
        _LabelTips.center = CGPointMake(_ImageSelectd.center.x, 82 + (_ImageSelectd.frame.origin.y - 82)/2);
    }
    
    [self presentViewController:imagePicker animated:NO completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //測試
#if TARGET_IPHONE_SIMULATOR
    //載入個人簽名的暫存（如果有的話）
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%ld.jpg", @"tempImageMulti",currentIndex_Image+1]];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    NSLog(@"original image:%@",NSStringFromCGSize(image.size));
    
    selectedImage = image;
    
    if (_shouldMask) { //遮罩就自己裁切
        [self ProcessPhotoWithImage:selectedImage];
    }
    else { //呈現影像編輯VC
        [self presentCropViewController];
    }
#else
    //NSString *url = [info objectForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@"url:%@",url);
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        /*UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        //相簿的話
        if (_sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
            image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }*/
        NSLog(@"original image:%@",NSStringFromCGSize(image.size));
        
        selectedImage = image;
    }
    else if ([mediaType isEqualToString:@"public.movie"]){
        NSLog(@"found a video");
        //NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        //NSData *webData = [NSData dataWithContentsOfURL:videoURL];
    }
    
    [imagePicker dismissViewControllerAnimated:NO completion:^{
        if (_sourceType == UIImagePickerControllerSourceTypeCamera && _shouldMask) { //遮罩就自己裁切
            
            UIImageOrientation orientation = selectedImage.imageOrientation;
            //CGFloat totalWidth = 768;
            switch (orientation) {
                case UIImageOrientationUp:
                    selectedImage = [self imageRotatedByDegrees:90 Image:selectedImage];
                    break;
                case UIImageOrientationDown:
                    selectedImage = [self imageRotatedByDegrees:90 Image:selectedImage];
                    break;
                case UIImageOrientationLeft:
                    break;
                case UIImageOrientationRight:
                    break;
                default:
                    break;
            }
            //NSLog(@"totalWidth:%lf",totalWidth);
            
            [self ProcessPhotoWithImage:selectedImage];
        }
        else { //呈現影像編輯VC
            [self presentCropViewController];
        }
    }];
#endif
}
-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self pressImageBackBtn:nil];
}
-(void) ProcessPhotoWithImage:(UIImage *)image {
    //計算要抓取的位置（有用遮罩的話，要裁圖片的框框）
    CGFloat ratio =  image.size.width / 768; //因為原本rect是用寬768去算的
    //ratio = 1.0;
    NSLog(@"ratio:%f",ratio);
    
    CGRect rect = CGRectMake(163.0/2 * ratio,440.0/2 * ratio+50,1208.0/2 * ratio,855.0/2 * ratio);
    if (_isVehicleLicense) { //行照
        //x:167 y:152 長621px 寬458px
        rect = CGRectMake(167.0 * ratio, 152.0 * ratio,458.0 * ratio, 621.0 * ratio);
    }
    NSLog(@"要抓取的位置:%@",NSStringFromCGRect(rect));
    
    //判斷要不要加入浮水印
    UIImage *imageToUse = image;
    if (_shouldWatermark) {
        UIImageView *ImageTemp = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        ImageTemp.image = image;
        
        CGFloat watermarkRatio = image.size.width/300.0 * 0.75 * 0.5;
        if (_isVehicleLicense) {
            watermarkRatio = image.size.width/430.0 * 0.5 * 0.5;
        }
        CGFloat watermarkWidth = 300 * watermarkRatio;
        CGFloat watermarkHeight = 38 * watermarkRatio;
        
        //浮水印放在左下角
        UIImageView *ImageWatermark = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y+rect.size.height-watermarkHeight, watermarkWidth, watermarkHeight)];
        ImageWatermark.image = [UIImage imageNamed:@"text_gogoro_watermark.png"];
        //ImageWatermark.contentMode = UIViewContentModeScaleAspectFit;
        [ImageTemp addSubview:ImageWatermark];
        
        UIImage *sourceImage = [self imageWithView:ImageTemp];
        imageToUse = sourceImage;
    }
    
    //擷取拍到照片存起來
    _ImageSelectd.image = imageToUse;
    
    UIImage *captureImage = imageToUse;
    if (_sourceType == UIImagePickerControllerSourceTypeCamera && _shouldMask) { //遮罩
        captureImage = [self captureView:_ViewOverlay Rect:rect];
    }
    NSLog(@"抓取後的圖檔尺寸:%@",NSStringFromCGSize(captureImage.size));
    
    _ImageSelectd.image = captureImage;
    
    //拍的照片放到暫存檔
    [arrayTempImages addObject:captureImage];
    
    //顯示功能按鈕
    [self ShowCommandView];
}
- (UIImage*)imageByCombiningImage:(UIImage*)firstImage withImage:(UIImage*)secondImage {
    UIImage *image = nil;
    
    //CGSize newImageSize = CGSizeMake(MAX(firstImage.size.width, secondImage.size.width), MAX(firstImage.size.height, secondImage.size.height));
    CGSize newImageSize = CGSizeMake(firstImage.size.width + secondImage.size.width,MAX(firstImage.size.height, secondImage.size.height));
    
    UIGraphicsBeginImageContext(newImageSize);
    
    /*[firstImage drawAtPoint:CGPointMake(roundf((newImageSize.width-firstImage.size.width)/2),
                                        roundf((newImageSize.height-firstImage.size.height)/2))];
    [secondImage drawAtPoint:CGPointMake(roundf((newImageSize.width-secondImage.size.width)/2),
                                         roundf((newImageSize.height-secondImage.size.height)/2))];
    */
    
    [firstImage drawInRect:CGRectMake(0,0,firstImage.size.width,firstImage.size.height)];
    [secondImage drawInRect:CGRectMake(firstImage.size.width,0,secondImage.size.width,secondImage.size.height)];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
  
    UIGraphicsEndImageContext();
    
    return image;
}
- (UIImage*)captureView:(UIView *)yourView Rect:(CGRect)rect {
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // translated rectangle for drawing sub image
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, _ImageSelectd.image.size.width,_ImageSelectd.image.size.height);
    
    // clip to the bounds of the image context
    // not strictly necessary as it will get clipped anyway?
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    
    // draw image
    [_ImageSelectd.image drawInRect:drawRect];
    
    // grab image
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return croppedImage;
}
- (UIImage *) imageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees Image:(UIImage *)sourceImage {
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,sourceImage.size.width, sourceImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DEGREES_TO_RADIANS(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-sourceImage.size.width / 2, -sourceImage.size.height / 2, sourceImage.size.width, sourceImage.size.height), [sourceImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma mark - 影像裁切VC
- (void)presentCropViewController {
    UIImage *image = selectedImage; //Load an image
    
    TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:image];
    cropViewController.delegate = self;
    //cropViewController.rotateButtonsHidden = YES;
    [self presentViewController:cropViewController animated:YES completion:nil];
}
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
    selectedImage = image;
    
    [self ProcessPhotoWithImage:selectedImage];
    
    [cropViewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)cropViewController:(TOCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled {
    if (cancelled) {
        selectedImage = nil;
        
        //裁切取消後，重拍
        [cropViewController dismissViewControllerAnimated:NO completion:^(void) {
            [self presentViewController:imagePicker animated:NO completion:nil];
        }];
    }
    else {
        [cropViewController dismissViewControllerAnimated:YES completion:^(void) {
        }];
    }
}
#pragma mark - Button Action
-(IBAction) pressImageBackBtn:(id)sender {
    [imagePicker dismissViewControllerAnimated:NO completion:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}
- (IBAction) pressImageHomeBtn:(id)sender {
    [imagePicker dismissViewControllerAnimated:NO completion:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}
- (IBAction) pressTakePictureBtn:(id)sender {
    _ButtonTakePhoto.hidden = YES;
    
    //模擬器不能拍照，所以要把照片放到doc目錄下進行測試
#if TARGET_IPHONE_SIMULATOR
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempImageMulti1"]];
    NSLog(@"圖檔放置路徑:%@",imagePath);
    
    [self imagePickerController:nil didFinishPickingMediaWithInfo:nil];
#else
    [imagePicker takePicture]
#endif
    ;
}
-(IBAction) pressReTakeBtn:(id)sender { //重新拍攝按鈕
    //移除最後一個（因為拍的當下先暫存了）
    [arrayTempImages removeLastObject];
    
    //隱藏功能按鈕
    [self HideCommandView];

#if TARGET_IPHONE_SIMULATOR
#else
    /*//相簿的話要開啟相簿選取畫面
    if (_sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        if (imagePicker) {
            [self presentViewController:imagePicker animated:NO completion:nil];
        }
        else {
            [self initImagePicker];
        }
    }*/
    if (imagePicker) {
        [self presentViewController:imagePicker animated:NO completion:nil];
    }
    else {
        [self initImagePicker];
    }
#endif
}
-(IBAction) pressUsePhotoBtn:(id)sender { //使用照片按鈕
    //隱藏功能按鈕
    //[self HideCommandView];

    if (currentIndex_Image == totalImagesNeed - 1) { //最後一張選使用照片
        //把所有拍的照片合併成一張圖檔
        UIImage *firstImage = [arrayTempImages objectAtIndex:0];
        UIImage *secondImage = [arrayTempImages objectAtIndex:1];
        UIImage *finalImage = [self imageByCombiningImage:firstImage withImage:secondImage];
        NSLog(@"合成後的圖檔尺寸:%@",NSStringFromCGSize(finalImage.size));
        
        if (!_shouldWatermark) { //非裁切類需要拍二次的，怕圖片太大所以縮小
            CGSize sizeToBe = CGSizeMake(finalImage.size.width/2, finalImage.size.height/2);
            finalImage = [self ImageWithImage:finalImage scaledToSize:sizeToBe];
            NSLog(@"縮小後的圖檔尺寸:%@",NSStringFromCGSize(finalImage.size));
        }
        else {
            //2016.06.13 也要縮圖
            CGSize sizeToBe = CGSizeMake(finalImage.size.width/2 * 0.5, finalImage.size.height/2 * 0.5);
            finalImage = [self ImageWithImage:finalImage scaledToSize:sizeToBe];
            NSLog(@"縮小後的圖檔尺寸:%@",NSStringFromCGSize(finalImage.size));
        }
        
        //傳到前頁（上傳頁）
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_MultiPhoto_Finish" object:finalImage];
        
#if TARGET_IPHONE_SIMULATOR
        //回到前頁（上傳頁）
        [self.navigationController popViewControllerAnimated:YES];
#else
        /*//相簿的話直接回前頁（因為選照片當下已經關閉imagepicker）
        if (_sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
            //回到前頁（上傳頁）
            [self.navigationController popViewControllerAnimated:YES];
        }
        else { //相機
            //關閉相機
            [imagePicker dismissViewControllerAnimated:NO completion:^{
                //回到前頁（上傳頁）
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }*/
        //回到前頁（上傳頁）
        [self.navigationController popViewControllerAnimated:YES];
#endif
    }
    else { //不是最後一張照片，相簿的話要開啟相簿選取畫面
#if TARGET_IPHONE_SIMULATOR
#else
        /*//相簿的話要開啟相簿選取畫面
        if (_sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
            if (imagePicker) {
                [self presentViewController:imagePicker animated:NO completion:nil];
            }
            else {
                [self initImagePicker];
            }
        }*/
        if (imagePicker) {
            [self presentViewController:imagePicker animated:NO completion:nil];
        }
        else {
            [self initImagePicker];
        }
#endif
    }
    
    currentIndex_Image++;
    
    //隱藏功能按鈕
    [self HideCommandView];
}
- (UIImage*)captureImage:(UIImage *)image Rect:(CGRect)rect{
    /*CGImageRef imageRef = image.CGImage;
    CGImageRef imagePartRef = CGImageCreateWithImageInRect(imageRef, rect);
    UIImage *cropImage = [UIImage imageWithCGImage:imagePartRef];
    CGImageRelease(imagePartRef);
    return cropImage;*/
    
    //Create rectangle from middle of current image
    //CGRect croprect = CGRectMake(image.size.width / 4, image.size.height / 4 ,(image.size.width / 2), (image.size.height / 2));
    CGRect croprect = rect;
    
    // Draw new image in current graphics context
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], croprect);
    
    // Create new cropped UIImage
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    
    return croppedImage;
}
#pragma mark - UI
-(void) ShowCommandView { //顯示重新拍攝/使用照片
    _ViewCommand.hidden = NO;
    _ButtonTakePhoto.hidden = YES;
    _ViewTips.hidden = YES;
}
-(void) HideCommandView { //隱藏重新拍攝/使用照片
    _ViewCommand.hidden = YES;
    _ButtonTakePhoto.hidden = NO;
    _ImageSelectd.image = nil;
    
    _ViewTips.hidden = NO;
    if (currentIndex_Image == totalImagesNeed - 1) { //最後一張選使用照片
        if (_type == 31) { //行照
            _LabelTips.text = @"請拍攝內頁(個人資料頁)";
        }
        else {
            _LabelTips.text = @"請拍攝反面";
        }
    }
    else {
        if (_type == 31) { //行照
            _LabelTips.text = @"請拍攝封面(機關用印面)";
        }
        else {
            _LabelTips.text = @"請拍攝正面";
        }
    }
}
#pragma mark - 換頁

#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) dealloc {
}
@end
