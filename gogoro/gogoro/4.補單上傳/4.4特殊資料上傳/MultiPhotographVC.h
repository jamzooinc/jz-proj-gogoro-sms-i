//
//  PhotographVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TOCropViewController.h"

@interface MultiPhotographVC : TemplateVC <UINavigationControllerDelegate,UIImagePickerControllerDelegate,TOCropViewControllerDelegate> {
}
@property UIImagePickerControllerSourceType sourceType;
@property NSInteger type; //附件類型
@property (nonatomic,strong) NSString *attachName; //附件名稱
@property BOOL shouldWatermark; //YES:要加上浮水印
@property BOOL shouldMask; //YES:拍照時要使用遮罩
@property BOOL isVehicleLicense; //YES:行照（遮罩圖檔不一樣）
@end
