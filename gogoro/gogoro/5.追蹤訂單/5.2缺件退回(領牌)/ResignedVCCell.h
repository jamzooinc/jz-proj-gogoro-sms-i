

#import <UIKit/UIKit.h>

@interface ResignedVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageBG;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderNo;
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerName;
@property (nonatomic, weak) IBOutlet UILabel *LabelLoginSales;
@property (nonatomic, weak) IBOutlet UILabel *LabelSales;
@property (nonatomic, weak) IBOutlet UIImageView *ImageStuck;
@property (nonatomic, weak) IBOutlet UILabel *LabelDays;
@property (nonatomic, weak) IBOutlet UIImageView *ImageLack;
@property (nonatomic, weak) IBOutlet UILabel *LabelLackDays;
@property (nonatomic, weak) IBOutlet UILabel *LabelMissState;
@end
