//
//  ResignedVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "ResignedVC.h"
#import "ResignedVCCell.h"
#import "UploadFileVC.h"

@interface ResignedVC () {
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;

@end

@implementation ResignedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationItemTitle:@"追蹤訂單" Image:[UIImage imageNamed:@"icn_tracing.png"]];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayTableView = [[NSMutableArray alloc] init];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
    
    //取得資料
    [self TrackingOrderReturn];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResignedVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ResignedVCCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    ResignedVCCell *myCell = (ResignedVCCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //訂單編號
    myCell.LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",[_dic objectForKey:@"OrderNo"]];
    
    //車主姓名
    myCell.LabelOwnerName.text = [NSString stringWithFormat:@"車主姓名：%@",[_dic objectForKey:@"CustomerName"]];
    
    //登入業代
    myCell.LabelLoginSales.text = [NSString stringWithFormat:@"登錄業代：%@",[_dic objectForKey:@"LoginSalesName"]];
    
    //銷售業代
    myCell.LabelSales.text = [NSString stringWithFormat:@"銷售人員：%@",[_dic objectForKey:@"CreateSalesName"]];
    
    //缺失原因
    myCell.LabelMissState.text = [NSString stringWithFormat:@"缺失原因：%@",[_dic objectForKey:@"MissState"]];
    
    //該階段卡關天數：
    //進入該階段時間與現在時間的差距天數
    //(APP(待處理訂單)階段：銷售發票日與現在時間差距天數
    if (_Step == 0) { //Step;  //0:   待處理訂單, 1: 領牌階段, 2:地方補助管理 3:TES 補助管理
        
    }
    else {
        
    }
    
    //卡關天數
    myCell.LabelDays.text = [NSString stringWithFormat:@"%@天",[_dic objectForKey:@"MissDays"]]; //StepMissDays
    
    //狀態 AttachTypeName
    //myCell.LabelLackDays.text = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"MissState"]];
    
    //缺件
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    //AttachTypeName = @"AttachTypeNameAttachTypeNameAttachTypeNameAttachTypeName";
    myCell.LabelLackDays.text = [NSString stringWithFormat:@"%@",AttachTypeName];
    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - 變更
-(void) SelectOrderAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    NSInteger AttachType = [[_dic objectForKey:@"AttachType"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //附件需上傳電子檔(0:no,1:yes)
    BOOL IsUploadFile = [[_dic objectForKey:@"IsUploadFile"] boolValue];
    
    //附件為正本類型(0:no,1:yes)
    BOOL IsOriginal = [[_dic objectForKey:@"IsOriginal"] boolValue];
    
    //附件有使用套版(0:no,1:yes)
    BOOL UseTemplate = [[_dic objectForKey:@"UseTemplate"] boolValue];
    
    //邏輯判斷：參考WF5.2
    if (IsUploadFile) {
        //到上傳
        [self GotoUploadFileVCWithType:AttachType AttachName:AttachTypeName OrderNo:OrderNo];
    }
    else {
        if (IsOriginal) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"缺失是否已補正？" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //呼叫API上傳
                [self UploadAttach];
            }];
            [alertController addAction:actionConfirm];
            
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }];
            [alertController addAction:actionCancel];
            
            [self presentViewController:alertController animated:YES completion:^(void){
            }];
        }
        else {
            if (UseTemplate) {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"是否重新套版？" preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //呼叫API重新套版
                    [self ReAapplyDocumentWithAttachType:-1];
                }];
                [alertController addAction:actionConfirm];
                
                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                }];
                [alertController addAction:actionCancel];
                
                [self presentViewController:alertController animated:YES completion:^(void){
                }];
            }
            else {
                [self showAlertControllerWithMessage:@"文件類別有誤！"];
            }
        }
    }
    
}
#pragma mark - Call API
#pragma mark [503] 追蹤訂單_缺件退回(儲存在AppLocal端)
- (void) TrackingOrderReturn {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:[NSString stringWithFormat:@"%ld",_Step] forKey:@"Step"];
    //[parameter setObject:@"" forKey:@"StartDate"];
    //[parameter setObject:@"" forKey:@"EndDate"];
    [GatewayManager callTrackingOrderReturn:parameter Source:self];
}
- (void)callBackTrackingOrderReturn:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        [arrayTableView removeAllObjects];
        [arrayTableView addObjectsFromArray:Data];
        [_TableView reloadData];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [311] 上傳訂單附件電子檔
- (void) UploadAttach {
    [ObjectManager showLodingHUDinView:self.view];
    
    //先刪除暫存圖檔，以避免誤傳
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"tempUploadFile"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存圖檔失敗:%@",error);
        }
        else {
            NSLog(@"刪除暫存圖檔成功");
        }
    }

    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    //NSLog(@"_dic:%@",_dic);

    NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    NSInteger type = [[_dic objectForKey:@"AttachType"] integerValue];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:OrderNo forKey:@"OrderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",(long)type] forKey:@"type"];
    [parameter setObject:@"tempUploadFile.jpg" forKey:@"filename"];
    [GatewayManager callUploadAttach:parameter Source:self];
}
- (void)callBackUploadAttach:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //重抓資料，刷新頁面
        [self TrackingOrderReturn];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [103] 文件重新套版
- (void) ReAapplyDocumentWithAttachType:(NSInteger)attachType {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    NSInteger type = [[_dic objectForKey:@"AttachType"] integerValue];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:OrderNo forKey:@"OrderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",type] forKey:@"attachType"];
    [GatewayManager callReAapplyDocument:parameter Source:self];
}
- (void)callBackReAapplyDocument:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //重抓資料，刷新頁面
        [self TrackingOrderReturn];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName OrderNo:(NSString *)orderNo { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.OrderNo = orderNo;
    destVC.type = attachType;
    destVC.attachName = attachName;
    destVC.didFromLackReturn = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectOrderAtIndex:) name:@"kNotification_Select_Order" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Select_Order" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
