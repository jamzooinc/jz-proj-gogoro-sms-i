//
//  UndoneOrderVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#define kMessageTableViewLoading @"載入中..."
#define kMessageTableViewPullUp @"往上拉以載入更多"
#define kMessageTableViewRelease @"放開以載入更多"

#import "UndoneOrderVC.h"
#import "UndoneOrderVCCell.h"
#import "RMDateSelectionViewController.h"
#import "RMActionController.h"
#import "GetOrderDetailVC.h"
#import "ECDocumentListVC.h"

@interface UndoneOrderVC () <UITableViewDelegate, UITableViewDataSource>
{
    UITextField *selectedTextField;
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
    NSMutableArray *arrayPerson;
    NSInteger selectedIndex_Person;
    NSInteger currentPage; //資料載入到第幾頁
    BOOL didShowSearchView;
}

@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UIView *ViewDate;
@property (nonatomic,weak) IBOutlet UITextField *TextStartDate;
@property (nonatomic,weak) IBOutlet UITextField *TextEndDate;
@property (nonatomic,weak) IBOutlet UIView *ViewSearch;
@property (nonatomic,weak) IBOutlet UILabel *LabelPerson;
@property (nonatomic,weak) IBOutlet UITextField *TextOrder;
@end

@implementation UndoneOrderVC

#pragma mark -Life Circle
-(void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"我的訂單" Image:[UIImage imageNamed:@"icn_res.png"]];
    //[self LeftButtonBack]; //左側按鈕變成返回
    
    arrayTableView = [[NSMutableArray alloc] init];
    arrayPerson = [[NSMutableArray alloc] init];
    
    selectedIndex_Person = -1;
    
    [_TableView setDragDelegate:self refreshDatePermanentKey:@"key"];
    _TableView.showRefreshView = NO; //上面的refresh不需要
    [_TableView setFooterLoadingText:kMessageTableViewLoading];
    [_TableView setFooterPullUpText:kMessageTableViewPullUp]; //往上拉以載入更多
    [_TableView setFooterReleaseText:kMessageTableViewRelease]; //放開以載入更多
    
    /* 手勢 */
    UISwipeGestureRecognizer *swipeUp;
    UISwipeGestureRecognizer *swipeDown;
    
    //設定所要偵測的UIView與對應的函式
    swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    
    //設定偵測手勢的類型
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];      //向上滑
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];  //向下滑
    
    //將辨識的機制加入
    [self.view addGestureRecognizer:swipeUp];
    [self.view addGestureRecognizer:swipeDown];
    
    //取得門市人員列表
    [self GetAccountList];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self AddNotification];
    
    //取得資料
    //[self TrackingOrderReturn];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}

#pragma mark - 上下滑動
-(void) handleSwipeUp:(UISwipeGestureRecognizer *)recognizer
{
    didShowSearchView = NO;
    
    //隱藏搜尋列
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _ViewSearch.frame = CGRectMake(0, 21, _ViewSearch.frame.size.width, _ViewSearch.frame.size.height);
        _TableView.frame = CGRectMake(0, 82 + 2 + 2 + _ViewDate.frame.size.height, ScreenWidth, ScreenHeight - 82 - 2 - _ViewDate.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}

-(void) handleSwipeDown:(UISwipeGestureRecognizer *)recognizer
{
    didShowSearchView = YES;
    
    //顯示搜尋列
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        _ViewSearch.frame = CGRectMake(0, 82 + _ViewDate.frame.size.height + 2, _ViewSearch.frame.size.width, _ViewSearch.frame.size.height);
        _TableView.frame = CGRectMake(0, 82 + 2 + 2 + _ViewSearch.frame.size.height + _ViewDate.frame.size.height, ScreenWidth, ScreenHeight - 82 - 2 - _ViewSearch.frame.size.height - _ViewDate.frame.size.height);
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - 變更
-(void) SelectOrderAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    [self GotoECDocumentListVC];
    
    return;
    
    /*NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    //進行查詢 appServer是否有無訂單資料
    NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    if ([OrderNo length] > 0) { //有輸入訂單編號，先查一下有沒有該筆資料
        [self GetOrderDetailWithOrderNo:OrderNo];
    }*/
}

#pragma mark - TextField Delegate
-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    //記錄做動的物件
    selectedTextField = textField;
    
    if (textField == _TextStartDate || //開始時間
        textField == _TextEndDate       //結束時間
        ) {
        [self ShowDateSelectionWithTextField:textField];
        
        return NO;
    }
    
    return YES;
}

#pragma mark - tableView 相關
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 285;
}

-(NSInteger) tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayTableView count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UndoneOrderVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"UndoneOrderVCCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    UndoneOrderVCCell *myCell = (UndoneOrderVCCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //訂單編號
    myCell.LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",[_dic objectForKey:@"OrderNo"]];
    
    //訂單日期
    myCell.LabelOrderDate.text = [NSString stringWithFormat:@"訂單審核日：%@",[_dic objectForKey:@"OrderApprovalDate"]];
    
    //發票日期
    myCell.LabelInvoiceDate.text = [NSString stringWithFormat:@"發票銷售日：%@",[_dic objectForKey:@"InvoiceDate"]];
    
    //車主姓名
    myCell.LabelOwnerName.text = [NSString stringWithFormat:@"車主姓名：%@",[_dic objectForKey:@"CustomerName"]];
    
    
    //Add at 2.0 version by Justin in 2016/9/14
    //====================================================
    //訂單門市
    myCell.LabelOrderStoreName.text = [NSString stringWithFormat:@"訂單門市：%@",[_dic objectForKey:@"StoreName"]];

    //VIN
    myCell.LableVIN.text = [NSString stringWithFormat:@"VIN：%@",[_dic objectForKey:@"VinNo"]];
    
    //車牌
    myCell.LabelLicensePlate.text = [NSString stringWithFormat:@"車牌：%@",[_dic objectForKey:@"Plate"]];
    //=====================================================
    
    myCell.ImageStatus.hidden = YES;
    myCell.ImageTes.hidden = YES;
    myCell.LabelStatus.hidden = YES;
    myCell.LabelTes.hidden = YES;
    myCell.ViewTes.hidden = YES;
    myCell.ViewStatus.hidden = YES;
    
    //計算TES跟地方是不是都已結案
    NSInteger closeCount = 0;
    
    NSArray *OrderState = [_dic objectForKey:@"OrderState"];
    for (NSDictionary *_dicState in OrderState) {
        NSString *Status = [_dicState objectForKey:@"Status"];
        NSString *Step = [_dicState objectForKey:@"Step"];
        NSLog(@"狀態-->%@:%@",Step,Status);
        
        BOOL hasStatus = YES;
        //Status = @""; //測試
        if ([Status length] == 0 || [Status isKindOfClass:[NSNull class]]) {
            hasStatus = NO;
        }
        
        if ([Step rangeOfString:@"結案-"].length > 0) {
            closeCount++;
        }
        
        //先用右邊的二個物件，再用左邊
        UIImageView *ImageToUse = nil;
        UILabel *LabelToUse = nil;
        UIView *ViewToUse = nil;
        if (myCell.ImageStatus.hidden) { //右邊的還是隱藏，用右邊
            ImageToUse = myCell.ImageStatus;
            LabelToUse = myCell.LabelStatus;
            ViewToUse = myCell.ViewStatus;
        }
        else { //右邊沒隱藏，用左邊
            ImageToUse = myCell.ImageTes;
            LabelToUse = myCell.LabelTes;
            ViewToUse = myCell.ViewTes;
        }
        
        ImageToUse.hidden = NO;
        LabelToUse.hidden = NO;
        ViewToUse.hidden = NO;
        if ([Step isEqualToString:@"領牌管理"]) {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_lp.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:97.0/255.0 green:211.0/255.0 blue:251.0/255.0 alpha:1.0f];
        }
        else if ([Step isEqualToString:@"補助-TES"]) {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_bjtes.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:44.0/255.0 green:104.0/255.0 blue:203.0/255.0 alpha:1.0f];
        }
        else if ([Step isEqualToString:@"政府審核-地方"]) {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_checkdf.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:211.0/255.0 green:230.0/255.0 blue:104.0/255.0 alpha:1.0f];
        }
        else if ([Step isEqualToString:@"補助-地方"]) {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_bjdf.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:44.0/255.0 green:104.0/255.0 blue:203.0/255.0 alpha:1.0f];
        }
        else if ([Step isEqualToString:@"政府審核-TES"]) {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_checktes.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:211.0/255.0 green:230.0/255.0 blue:104.0/255.0 alpha:1.0f];
        }
        else if ([Step isEqualToString:@"結案-TES"]) {
            ImageToUse.image = [UIImage imageNamed:@"Icn_status_endtes.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:240.0/255.0 green:109.0/255.0 blue:128.0/255.0 alpha:1.0f];
        }
        else if ([Step isEqualToString:@"結案-地方"]) {
            ImageToUse.image = [UIImage imageNamed:@"Icn_status_enddf.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:240.0/255.0 green:109.0/255.0 blue:128.0/255.0 alpha:1.0f];
        }
        
        else if ([Step isEqualToString:@"訂單完成"]) {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_bjfinish.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:86.0/255.0 green:239.0/255.0 blue:103.0/255.0 alpha:1.0f];
        }
        else  {
            ImageToUse.image = [UIImage imageNamed:@"icn_status_pending.png"];
            LabelToUse.text = Status;
            LabelToUse.textColor = [UIColor colorWithRed:108.0/255.0 green:222.0/255.0 blue:169.0/255.0 alpha:1.0f];
        }
        
        //訂單沒有工作狀況(空值)時，階段UI如領牌、待處理要上下置中
        if (!hasStatus) {
            ImageToUse.center = CGPointMake(ImageToUse.center.x, myCell.frame.size.height/2);
        }
    }
    
    NSMutableArray *arrayViews = [NSMutableArray array];
    if ([OrderState count] == 0) {
        
    }
    else if ([OrderState count] == 1) {
        [arrayViews addObject:myCell.ViewStatus];
    }
    else if ([OrderState count] == 2) {
        [arrayViews addObject:myCell.ViewStatus];
        [arrayViews addObject:myCell.ViewTes];
    }
    //依照顯示View數量調整UI
    NSInteger viewCount = [arrayViews count];
    for (NSInteger i = 0; i < viewCount; i++) {
        UIView *_View = [arrayViews objectAtIndex:i];
        _View.center = CGPointMake(_View.center.x, myCell.frame.size.height /viewCount * (i + 0.5));
    }
    
    //Add at 2.0 version by Justin in 2016/9/14
    //=====================================================
    myCell.ViewPay.hidden = YES; //支付
    myCell.ViewTrace.hidden = YES; //追回
    myCell.ViewFinish.hidden = YES; //補助完成
    [self RadiusView:myCell.ViewFinish];
    //=====================================================
    
    //2016.10.27 by Aaron
    //TrackState 金錢追蹤狀況(0:無金錢追蹤, 1: 有須匯出款項, 2:有須追回款項, 3:有須匯出款項及追回款項)
    NSMutableArray *arrayImages = [NSMutableArray array];
    NSInteger TrackState = [[_dic objectForKey:@"TrackState"] integerValue];
    if (TrackState == 0) { //0:無金錢追蹤
    }
    else if (TrackState == 1) { //1: 有須匯出款項
        myCell.ViewPay.hidden = NO;
        [arrayImages addObject:myCell.ViewPay];
    }
    else if (TrackState == 2) { //2:有須追回款項
        myCell.ViewTrace.hidden = NO;
        [arrayImages addObject:myCell.ViewTrace];
    }
    else if (TrackState == 3) { // 3:有須匯出款項及追回款項
        myCell.ViewPay.hidden = NO;
        myCell.ViewTrace.hidden = NO;
        [arrayImages addObject:myCell.ViewPay];
        [arrayImages addObject:myCell.ViewTrace];
    }
    
    //是否補助完成 補助完成就是判斷兩個 TES跟地方都結案的時候顯示
    if (closeCount >= 2) {
        myCell.ViewFinish.hidden = NO;
        [arrayImages addObject:myCell.ViewFinish];
    }
    
    //依照顯示Image數量調整UI
    NSInteger imageCount = [arrayImages count];
    /*for (NSInteger i = 0; i < imageCount; i++) {
        UIImageView *Image = [arrayImages objectAtIndex:i];
        Image.center = CGPointMake(Image.center.x, myCell.frame.size.height /imageCount * (i + 0.5));
    }*/
    if (imageCount == 1) { //垂直置中
        UIView *View = [arrayImages objectAtIndex:0];
        View.center = CGPointMake(View.center.x, myCell.frame.size.height/2);
    }
    else if (imageCount == 2) { //對齊左邊二個圖檔
        UIView *View1 = [arrayImages objectAtIndex:0];
        CGFloat offset1 = (View1.frame.size.height - myCell.ViewTes.frame.size.height) / 2;
        View1.center = CGPointMake(View1.center.x, myCell.ViewTes.center.y + offset1);
        
        UIView *View2 = [arrayImages objectAtIndex:1];
        CGFloat offset2 = (View2.frame.size.height - myCell.ViewStatus.frame.size.height) / 2;
        View2.center = CGPointMake(View2.center.x, myCell.ViewStatus.center.y + offset2);
    }
    else if (imageCount == 3) { //垂直分散
        for (NSInteger i = 0; i < imageCount; i++) {
            UIView *View = [arrayImages objectAtIndex:i];
            View.center = CGPointMake(View.center.x, myCell.frame.size.height /imageCount * (i + 0.5));
        }
    }
    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    [self GetTrackingOrderList]; //抓取下一頁的資料
}

-(void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    
}

#pragma mark - 選擇日期
-(void) ShowDateSelectionWithTextField:(UITextField *)textField
{
    NSString *message = nil;
    if (textField == _TextStartDate) { //開始時間
        message = @"開始時間";
    }
    else if (textField == _TextEndDate) { //結束時間
        message = @"結束時間";
    }
  
    //Create select action
    RMAction *selectAction = [RMAction actionWithTitle:@"選擇" style:RMActionStyleDone andHandler:^(RMActionController *controller) {
        NSLog(@"Successfully selected date: %@", ((UIDatePicker *)controller.contentView).date);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy/MM/dd";
        NSString *stringToDelegate = [formatter stringFromDate:((UIDatePicker *)controller.contentView).date];
        //NSLog(@"stringToDelegate:%@",stringToDelegate);
        
        selectedTextField.text = stringToDelegate;
        
        if (selectedTextField == _TextEndDate && !didShowSearchView) { //搜尋列沒有出來，且有輸入結束時間，就搜尋
            //重新搜尋
            [self ResetSearchUIAndParameter];
            
            [self GetTrackingOrderList];
        }
    }];
    
    //Create cancel action
    RMAction *cancelAction = [RMAction actionWithTitle:@"取消" style:RMActionStyleCancel andHandler:^(RMActionController *controller) {
        NSLog(@"Date selection was canceled");
    }];
    
    message = [NSString stringWithFormat:@"請選擇%@",message];
    
    //Create date selection view controller
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:RMActionControllerStyleWhite selectAction:selectAction andCancelAction:cancelAction];
    dateSelectionController.title = message;
    dateSelectionController.message = @"";
    
    //Create now action and add it to date selection view controller
    RMAction *nowAction = [RMAction actionWithTitle:@"今天" style:RMActionStyleAdditional andHandler:^(RMActionController *controller) {
        ((UIDatePicker *)controller.contentView).date = [NSDate date];
        NSLog(@"Now button tapped");
    }];
    nowAction.dismissesActionController = NO;
    
    [dateSelectionController addAction:nowAction];
    
    //設定時區
    NSLocale *datelocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"];
    dateSelectionController.datePicker.locale = datelocale;
    dateSelectionController.datePicker.timeZone = [NSTimeZone timeZoneWithName:@"GMT+8"];
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    
    //不能大於今天
    //dateSelectionController.datePicker.minimumDate = [NSDate date];
    
    //On the iPad we want to show the date selection view controller within a popover.
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        //First we set the modal presentation style to the popover style
        dateSelectionController.modalPresentationStyle = UIModalPresentationPopover;
        
        //Then we tell the popover presentation controller, where the popover should appear
        dateSelectionController.popoverPresentationController.sourceView = _ViewDate;
        dateSelectionController.popoverPresentationController.sourceRect = selectedTextField.frame;
    }
    
    //Now just present the date selection controller using the standard iOS presentation method
    [self presentViewController:dateSelectionController animated:YES completion:nil];
}

#pragma mark - Call API
#pragma mark [008] 取得門市人員列表
- (void) GetAccountList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetAccountList:parameter Source:self];
}
- (void)callBackGetAccountList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        [arrayPerson removeAllObjects];
        [arrayPerson addObjectsFromArray:Data];
        
        //取得訂單
        [self GetTrackingOrderList];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [301] 取得訂單
- (void) GetOrderDetailWithOrderNo:(NSString *)orderNo {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:orderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void)callBackGetOrderDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        [self GotoGetOrderDetailVCWithData:Data];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [501] 追蹤訂單_搜尋列表
- (void) GetTrackingOrderList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    if ([_TextStartDate.text length] > 0) {
        [parameter setObject:_TextStartDate.text forKey:@"StartDate"];
    }

    if ([_TextEndDate.text length] > 0) {
        [parameter setObject:_TextEndDate.text forKey:@"EndDate"];
    }
    
    if ([_TextOrder.text length] > 0) {
        [parameter setObject:_TextOrder.text forKey:@"OrderNo"];
    }
    
    if (selectedIndex_Person >= 0) { //有選人
        NSDictionary *_dic = [arrayPerson objectAtIndex:selectedIndex_Person];
        [parameter setObject:[_dic objectForKey:@"AccountNo"] forKey:@"Account"];
    }
    else { //我的訂單 - 預設call api 501時，不需要帶門市人員ID，篩選時才要
        //[parameter setObject:[UserManager shareInstance].account forKey:@"Account"];
    }
    
    currentPage = currentPage + 1;
    [parameter setObject:[NSString stringWithFormat:@"%ld",currentPage] forKey:@"Page"];
     
    [GatewayManager callTrackingOrderList:parameter Source:self];
}
- (void)callBackTrackingOrderList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSArray *Orders = [Data objectForKey:@"Orders"];
        //[arrayTableView removeAllObjects];x
        
        if ([Orders count] == 0) { //表示已無下一頁資料
            currentPage = currentPage - 1;
        }
        else {
            [arrayTableView addObjectsFromArray:Orders];
            [_TableView reloadData];
        }
        
        if ([arrayTableView count] == 0) {
            _TableView.hidden = YES;
        }
        else {
            _TableView.hidden = NO;
        }
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        [self showAlertControllerWithMessage:@"查無此訂單編號"];
        
        currentPage = currentPage - 1;
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
        
        currentPage = currentPage - 1;
    }
    
    [_TableView finishLoadMore];
}
#pragma mark - Button Action
-(IBAction) PressPersonBtn:(id)sender{
    [self ShowPersonSelect];
}
-(IBAction) pressCancelBtn:(id)sender {
    [self handleSwipeUp:nil];
    
    _TextStartDate.text = @"";
    _TextEndDate.text = @"";
    [self ResetSearchUIAndParameter];
}
-(IBAction) pressSearchBtn:(id)sender {
    //重新搜尋
    [self ResetSearchUIAndParameter];
    
    [self GetTrackingOrderList];
}

#pragma mark - 選擇門市人員
-(void) ShowPersonSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市人員" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayPerson count]; i++) {
        NSDictionary *_dic = [arrayPerson objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"AccountName"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _LabelPerson;
    popPresenter.sourceRect = _LabelPerson.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    selectedIndex_Person = buttonIndex;
    
    NSDictionary *_dic = [arrayPerson objectAtIndex:buttonIndex];
    _LabelPerson.text = [_dic objectForKey:@"AccountName"];
}
#pragma mark - 換頁
-(void) GotoGetOrderDetailVCWithData:(NSDictionary *)data { //訂單詳細
    GetOrderDetailVC *destVC = [[GetOrderDetailVC alloc] initWithNibName:@"GetOrderDetailVC" bundle:nil];
    destVC.dicData = data;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoECDocumentListVC { //5.1.1電子文件清單
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    NSLog(@"_dic:%@",_dic);
    
    NSString *OrderNo = [_dic objectForKey:@"OrderNo"];
    NSString *CustomerName = [_dic objectForKey:@"CustomerName"];
    
    ECDocumentListVC *destVC = [[ECDocumentListVC alloc] initWithNibName:@"ECDocumentListVC" bundle:nil];
    destVC.OrderNo = OrderNo;
    destVC.CustomerName = CustomerName;
    destVC.dicData = _dic;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
-(void) ResetSearchUIAndParameter {
    currentPage = 0;
    [arrayTableView removeAllObjects];
    [_TableView reloadData];
    _TableView.hidden = YES;
}
-(void) RadiusView:(UIView *) myView { //把View變成圓角
    CALayer *imageLayer = myView.layer;
    [imageLayer setCornerRadius:5];
    //[imageLayer setBorderWidth:1];
    //[imageLayer setBorderColor:[[UIColor colorWithRed:33.0/255.0 green:132.0/255.0 blue:222.0/255.0 alpha:1.0] CGColor]];
    [imageLayer setMasksToBounds:YES];
}
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectOrderAtIndex:) name:@"kNotification_Select_Order" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Select_Order" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
