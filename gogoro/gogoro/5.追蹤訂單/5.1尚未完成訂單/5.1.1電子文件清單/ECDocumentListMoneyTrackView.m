


#import "ECDocumentListMoneyTrackView.h"

@implementation ECDocumentListMoneyTrackView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithData:(NSDictionary *)data TrackData:(NSDictionary *)trackData {
    //訂單編號
    _LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@",[data objectForKey:@"OrderNo"]];
    
    //訂單日期
    _LabelOrderDate.text = [NSString stringWithFormat:@"訂單審核日：%@",[data objectForKey:@"OrderApprovalDate"]];
    
    //發票日期
    _LabelInvoiceDate.text = [NSString stringWithFormat:@"發票銷售日：%@",[data objectForKey:@"InvoiceDate"]];
    
    //車主姓名
    _LabelOwnerName.text = [NSString stringWithFormat:@"車主姓名：%@",[data objectForKey:@"CustomerName"]];
    
    //車主電話
    _LabelOwnerPhone.text = [NSString stringWithFormat:@"車主電話：%@",[data objectForKey:@"OwnerPhone"]];
    
    //追蹤類型
    _LabelStatus.text = [NSString stringWithFormat:@"追蹤類型：%@",[trackData objectForKey:@"TrackType"]];
    
    //追蹤處理狀況(須追回, 須支付)
    NSString *TrackActionType = [trackData objectForKey:@"TrackActionType"];
    if ([TrackActionType isEqualToString:@"須追回"]) {
        _ImageStatus.image = [UIImage imageNamed:@"img_in.png"];
        _LabelMoney.textColor = [UIColor colorWithRed:44.0/255.0 green:104.0/255.0 blue:203.0/255.0 alpha:1.0f];
    }
    else if ([TrackActionType isEqualToString:@"須支付"]) {
        _ImageStatus.image = [UIImage imageNamed:@"img_out.png"];
        _LabelMoney.textColor = [UIColor colorWithRed:108.0/255.0 green:222.0/255.0 blue:169.0/255.0 alpha:1.0f];
    }
    
    NSNumberFormatter *moneyFormatter = [[NSNumberFormatter alloc] init];
    [moneyFormatter setPositiveFormat:@"NT$###,###,###"];
    
    //追蹤金額
    NSInteger Money = [[trackData objectForKey:@"Money"] integerValue];
    _LabelMoney.text = [moneyFormatter stringFromNumber:[NSNumber numberWithInteger:Money]];
}
@end
