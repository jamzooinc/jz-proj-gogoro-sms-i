
#import <UIKit/UIKit.h>

@interface ECDocumentListMoneyTrackView : UIView {
}
@property (weak) id delegate;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderNo;
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerName;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderDate;
@property (nonatomic, weak) IBOutlet UILabel *LabelInvoiceDate;
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerPhone;
@property (nonatomic, weak) IBOutlet UILabel *LabelStatus;
@property (nonatomic, weak) IBOutlet UILabel *LabelMoney;
@property (nonatomic, weak) IBOutlet UIImageView *ImageStatus;
-(void) UpdateUIWithData:(NSDictionary *)data TrackData:(NSDictionary *)trackData;
@end
