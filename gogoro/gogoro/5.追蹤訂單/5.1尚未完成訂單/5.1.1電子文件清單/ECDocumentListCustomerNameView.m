


#import "ECDocumentListCustomerNameView.h"

@implementation ECDocumentListCustomerNameView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUIWithOrderNo:(NSString *)orderNo Name:(NSString *)name {
    _LabelOrderNo.text = [NSString stringWithFormat:@"訂單編號：%@", orderNo];
    _LabelName.text = [NSString stringWithFormat:@"車主姓名：%@",name];
}
@end
