//
//  ConfirmInfoVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfirmInfoView.h"
#import "ECDocumentListCustomerNameView.h"
#import "ECDocumentListMoneyTrackView.h"

@interface ECDocumentListVC : TemplateVC <ConfirmInfoViewDelegate>
@property (nonatomic,strong) NSString *OrderNo;
@property (nonatomic,strong) NSString *CustomerName;
@property (nonatomic,strong) NSDictionary *dicData;
@end
