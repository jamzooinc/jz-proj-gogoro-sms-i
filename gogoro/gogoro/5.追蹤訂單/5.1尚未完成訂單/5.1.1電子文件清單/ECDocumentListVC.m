//
//  ConfirmInfoVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "ECDocumentListVC.h"
#import "UploadFileVC.h"
#import "PhotoViewerVC.h"
#import "ConsentVC.h"
#import "OrderSearchRootVC.h"

@interface ECDocumentListVC () {
    NSMutableArray *arrayData,*arrayTrackingList;
    NSInteger selectedIndex,selectedIndex_TrackingList;
    CGFloat totalHeight,gapBetweenView;
    CGFloat totalHeight_TrackingList,gapBetweenView_TrackingList;
    NSDictionary *dicOrder;
    NSString *ownerName,*buyerName;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollViewTrackingList;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@property (nonatomic,weak) IBOutlet UIView *ViewDocumentList;
@property (nonatomic,weak) IBOutlet UIView *ViewTrace;
@property (nonatomic,weak) IBOutlet UIButton *ButtonDocumentList;
@property (nonatomic,weak) IBOutlet UIButton *ButtonTrace;
@end

@implementation ECDocumentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"文件及金錢追蹤" Image:[UIImage imageNamed:@"icn_tracing.png"]];
    
    arrayData = [[NSMutableArray alloc] init];
    arrayTrackingList = [[NSMutableArray alloc] init];
    
    //左上角按鈕變成返回
    [self LeftButtonBack];
    
    //取得訂單詳細資料（為了得知車主與購買人）
    //[self GetOrderDetail];
    
    //取得訂單附件清單
    [self GetOrderAttachList];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
#pragma mark - Call API
#pragma mark [301] 取得訂單
- (void) GetOrderDetail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderDetail:parameter Source:self];
}
- (void) callBackGetOrderDetail:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    //NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        dicOrder = nil;
        dicOrder = [[NSDictionary alloc] initWithDictionary:Data];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        //查無此訂單編號
        //[self showAlertControllerWithMessage:@"查無此訂單編號"];
    }
    else {
        //[self showAlertControllerWithMessage:Sysmsg];
    }
    
    //取得訂單附件清單
    [self GetOrderAttachList];
}
#pragma mark [309] 取得訂單附件清單
- (void) GetOrderAttachList {
    //參數復歸
    selectedIndex = -1;
    
    //移除既有View
    for (id _subView in _ScrollView.subviews) {
        if ([_subView isKindOfClass:[ConfirmInfoView class]] ||
            [_subView isKindOfClass:[ECDocumentListCustomerNameView class]]) {
            [_subView removeFromSuperview];
        }
    }
    
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    
    [parameter setObject:@"-1" forKey:@"Step"];
    
    [GatewayManager callGetOrderAttachList:parameter Source:self];
}
- (void)callBackGetOrderAttachList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //NSArray *Data = [parameter objectForKey:@"Data"];
        
        NSDictionary *_dic = [parameter objectForKey:@"Data"];
        NSArray *Data = [_dic objectForKey:@"Attachs"];
        
        ownerName = [_dic objectForKey:@"OwnerName"];
        buyerName = [_dic objectForKey:@"BuyerName"];
        
        [arrayData removeAllObjects];
        //[arrayData addObjectsFromArray:Data];
    
        //附件需上傳電子檔
        for (NSDictionary *_dic in Data) {
            BOOL IsUploadFile = [[_dic objectForKey:@"IsUploadFile"] boolValue];
            if (IsUploadFile) {
                [arrayData addObject:_dic];
            }
        }
        
        //產生項目
        [self GenerateSubview];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
    
    //取得金錢追蹤清單
    [self GetMoneyTrackingList];
}
#pragma mark [502] 金錢追蹤_單一訂單編號查詢
- (void) GetMoneyTrackingList {
    //參數復歸
    selectedIndex = -1;
    
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    
    [GatewayManager callMoneyTrackingList:parameter Source:self];
}
- (void)callBackMoneyTrackingList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        
        [arrayTrackingList removeAllObjects];
        [arrayTrackingList addObjectsFromArray:Data];
        
        //產生項目
        [self GenerateSubview_TrackingList];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - UI產生
-(void) GenerateSubview {
    //訂單資料
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"ECDocumentListCustomerNameView" owner:self options:nil];
    ECDocumentListCustomerNameView *view = [toplevels objectAtIndex:0];
    view.frame = CGRectMake((ScreenWidth - view.frame.size.width)/2 , 0, view.frame.size.width, view.frame.size.height);
    [_ViewDocumentList addSubview:view];
    [view UpdateUIWithOrderNo:_OrderNo Name:_CustomerName];
    
    //產生文件清單
    gapBetweenView = 15;
    totalHeight = 15.0;
    
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *dic = [arrayData objectAtIndex:i];
        
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"ConfirmInfoView" owner:self options:nil];
        ConfirmInfoView *view = [toplevels objectAtIndex:0];
        view.delegate = self;
        view.tag = i;
        view.didFromECDocumentListVC = YES;
        view.frame = CGRectMake(0, totalHeight, ScreenWidth, 49);
        [_ScrollView addSubview:view];
        [view UpdateUIWithData:dic];
        
        totalHeight = view.frame.origin.y + view.frame.size.height + gapBetweenView;
    }
    
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
    
    _ViewDocumentList.hidden = NO;
}
-(void) GenerateSubview_TrackingList {
    gapBetweenView_TrackingList = 5;
    totalHeight_TrackingList = 0;
    
    for (NSInteger i = 0; i < [arrayTrackingList count]; i++) {
        NSDictionary *dic = [arrayTrackingList objectAtIndex:i];
        
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"ECDocumentListMoneyTrackView" owner:self options:nil];
        ECDocumentListMoneyTrackView *view = [toplevels objectAtIndex:0];
        view.tag = i;
        view.frame = CGRectMake((ScreenWidth - view.frame.size.width)/2 , totalHeight_TrackingList, view.frame.size.width, view.frame.size.height);
        [_ScrollViewTrackingList addSubview:view];
        [view UpdateUIWithData:_dicData TrackData:dic];
        
        totalHeight_TrackingList = view.frame.origin.y + view.frame.size.height + gapBetweenView_TrackingList;
    }
    
    [_ScrollViewTrackingList setContentSize:CGSizeMake(_ScrollViewTrackingList.frame.size.width, totalHeight_TrackingList)];
}
#pragma mark - FileUrl
-(void) ConfirmInfoViewFileUrldAtIndex:(NSInteger)index { //fileUrl
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    
    //是否重套版
    /*BOOL UseTemplate = [[_dic objectForKey:@"UseTemplate"] boolValue];
    //UseTemplate = YES;
    if (UseTemplate) {
        FileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    }
    else {
    }*/
    
    if ([FileUrl length] == 0) {
        [self showAlertControllerWithMessage:@"請先上傳電子檔！"];
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];
    
    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([FileUrl rangeOfString:@".jpg"].length > 0 || [FileUrl rangeOfString:@".png"].length > 0 || [FileUrl rangeOfString:@".JPG"].length > 0 || [FileUrl rangeOfString:@".PNG"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:FileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:FileUrl Name:AttachTypeName FromTempUrl:NO];
    }
}
#pragma mark - Button Action
-(IBAction) pressDocumentListBtn:(id)sender { //文件列表
    [_ButtonDocumentList setBackgroundImage:[UIImage imageNamed:@"btn_flieinfo_on.png"] forState:UIControlStateNormal];
    [_ButtonTrace setBackgroundImage:[UIImage imageNamed:@"btn_money_off.png"] forState:UIControlStateNormal];
    _ViewDocumentList.hidden = NO;
    _ViewTrace.hidden = YES;
}
-(IBAction) pressTraceBtn:(id)sender { //追蹤
    [_ButtonDocumentList setBackgroundImage:[UIImage imageNamed:@"btn_flieinfo_off.png"] forState:UIControlStateNormal];
    [_ButtonTrace setBackgroundImage:[UIImage imageNamed:@"btn_money_on.png"] forState:UIControlStateNormal];
    _ViewDocumentList.hidden = YES;
    _ViewTrace.hidden = NO;
    
    if ([arrayTrackingList count] == 0) {
        [self showAlertControllerWithMessage:@"目前沒有金錢追蹤項目！"];
    }
}
#pragma mark - 換頁
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl  { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //API 309有Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = ownerName;
    NSString *BuyName = buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    destVC.OrderNo = _OrderNo;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
