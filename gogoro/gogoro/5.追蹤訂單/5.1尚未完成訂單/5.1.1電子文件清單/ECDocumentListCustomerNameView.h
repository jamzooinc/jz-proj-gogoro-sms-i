
#import <UIKit/UIKit.h>

@interface ECDocumentListCustomerNameView : UIView {
}
@property (weak) id delegate;
@property (nonatomic,weak) IBOutlet UILabel *LabelOrderNo;
@property (nonatomic,weak) IBOutlet UILabel *LabelName;
-(void) UpdateUIWithOrderNo:(NSString *)orderNo Name:(NSString *)name;
@end
