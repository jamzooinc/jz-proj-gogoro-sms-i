

#import <UIKit/UIKit.h>

@interface UndoneOrderVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageBG;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderNo;
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerName;
@property (nonatomic, weak) IBOutlet UILabel *LabelOrderDate;
@property (nonatomic, weak) IBOutlet UILabel *LabelInvoiceDate;
@property (nonatomic, weak) IBOutlet UIImageView *ImageStatus;
@property (nonatomic, weak) IBOutlet UILabel *LabelStatus;
@property (nonatomic, weak) IBOutlet UIImageView *ImageTes;
@property (nonatomic, weak) IBOutlet UILabel *LabelTes;
@property (weak, nonatomic) IBOutlet UILabel *LabelOrderStoreName;
@property (weak, nonatomic) IBOutlet UILabel *LableVIN;
@property (weak, nonatomic) IBOutlet UILabel *LabelLicensePlate;
@property (nonatomic, weak) IBOutlet UIView *ViewTes;
@property (nonatomic, weak) IBOutlet UIView *ViewStatus;
@property (nonatomic, weak) IBOutlet UIView *ViewFinish; //補助完成
@property (nonatomic, weak) IBOutlet UIView *ViewTrace; //追回
@property (nonatomic, weak) IBOutlet UIView *ViewPay; //支付

@end
