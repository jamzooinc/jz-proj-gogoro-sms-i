//
//  TrackOrderVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "TrackOrderVC.h"
#import "UndoneOrderVC.h"
#import "ResignedVC.h"

@interface TrackOrderVC ()

@end

@implementation TrackOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"Apple管理" Image:[UIImage imageNamed:@"icn_apple.png"]]; //追蹤訂單 icn_tracing
    [self LeftButtonBack];
}
#pragma mark - Button Action
-(IBAction) PressMyOrder { //我的訂單
    UndoneOrderVC *destVC = [[UndoneOrderVC alloc] initWithNibName:@"UndoneOrderVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressRejectLicensing { //缺件退回(領牌)
    ResignedVC *destVC = [[ResignedVC alloc] initWithNibName:@"ResignedVC" bundle:nil];
    destVC.Step = 1; //1: 5_2缺件退回_領牌_, 2: 5_3缺件退回_補助_, 3: 5_4缺件退回_政府_
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressRejectSubsidy { //缺件退回(補助)
    ResignedVC *destVC = [[ResignedVC alloc] initWithNibName:@"ResignedVC" bundle:nil];
    destVC.Step = 2; //1: 5_2缺件退回_領牌_, 2: 5_3缺件退回_補助_, 3: 5_4缺件退回_政府_
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) PressRejectGovenment { //缺件退回(政府)
    ResignedVC *destVC = [[ResignedVC alloc] initWithNibName:@"ResignedVC" bundle:nil];
    destVC.Step = 3; //1: 5_2缺件退回_領牌_, 2: 5_3缺件退回_補助_, 3: 5_4缺件退回_政府_
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
