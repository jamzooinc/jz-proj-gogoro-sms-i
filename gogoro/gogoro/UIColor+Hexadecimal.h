//
//  UIColor+Hexadecimal.h
//  gogoro
//
//  Created by 張啟裕 on 2016/9/22.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor(Hexadecimal)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
