//

#import "TransferVC.h"
#import "TransferListVC.h"

@interface TransferVC () {
    NSMutableArray *arrayStore,*arrayPerson;
    NSInteger selectedIndex_Store,selectedIndex_Person;
    UIButton *selectedButton;
    BOOL isNewCase; //YES:新案 NO:舊案查詢
    BOOL didSkipCheck; //略過DMS檢查(0: 需檢查, 1: 略過檢查)
}
@property (nonatomic,weak) IBOutlet UIView *ViewNewTranser;
@property (nonatomic,weak) IBOutlet UIView *ViewOldQuery;
@property (nonatomic,weak) IBOutlet UIButton *ButtonNewTranser;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOldQuery;
@property (nonatomic,weak) IBOutlet UITextField *TextCarNumber;
@property (nonatomic,weak) IBOutlet UITextField *TextCarNumberA;
@property (nonatomic,weak) IBOutlet UITextField *TextCarNumberB;
@property (nonatomic,weak) IBOutlet UITextField *TextOriginalOwnerID;
@property (nonatomic,weak) IBOutlet UIButton *ButtonStore;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPerson;
@property (nonatomic,weak) IBOutlet UITextField *TextCarNumberOld;
@property (nonatomic,weak) IBOutlet UITextField *TextCarNumberOldA;
@property (nonatomic,weak) IBOutlet UITextField *TextCarNumberOldB;
@end

@implementation TransferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"過戶移轉(步驟1/4：確認原車主)" Image:nil];
    
    arrayPerson = [[NSMutableArray alloc] init];
    arrayStore = [[NSMutableArray alloc] init];
    selectedIndex_Person = -1;
    selectedIndex_Store = -1;
    isNewCase = YES;
    
    //更改textfield的placeholder顏色
    UIColor *color = [UIColor darkGrayColor];
    _TextCarNumberOld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"車牌號碼" attributes:@{NSForegroundColorAttributeName: color}];
    
    //取得門市列表
    [self callGetStoreList];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self SetupFakeData];
}
#pragma mark - Call API
#pragma mark [002] 取得門市列表
- (void) callGetStoreList {
    [ObjectManager showLodingHUDinView:self.view];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callGetStoreList:parameter Source:self];
}
- (void)callBackGetStoreList:(NSDictionary *)parameter
{
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        [arrayStore removeAllObjects];
        NSArray *data = [parameter objectForKey:@"Data"];
        [arrayStore addObjectsFromArray:data];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [008] 取得門市人員列表
- (void) GetAccountList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //門市代號
    if (selectedIndex_Store >= 0) {
        NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
        NSString *StoreId = [_dic objectForKey:@"No"];
        [parameter setObject:StoreId forKey:@"StoreNo"];
    }
    
    [GatewayManager callGetAccountList:parameter Source:self];
}
- (void)callBackGetAccountList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        [arrayPerson removeAllObjects];
        [arrayPerson addObjectsFromArray:Data];
        
        //選擇門市人員
        [self pressStorePersonSelectBtn:nil];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [601] 搜尋過戶移轉案件
- (void) CallTransferFind {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    if (isNewCase) { //新案件
        //原車主身份證字號
        NSString *OwnerProfileCode = _TextOriginalOwnerID.text;
        if ([OwnerProfileCode length] > 0) {
            [parameter setObject:OwnerProfileCode forKey:@"OwnerProfileCode"];
        }
        
        //車牌號碼
        //NSString *Plate = _TextCarNumber.text;
        NSString *carNo_A = _TextCarNumberA.text;
        NSString *carNo_B = _TextCarNumberB.text;
        NSString *Plate = [NSString stringWithFormat:@"%@-%@",carNo_A,carNo_B];
        if ([Plate length] > 0) {
            [parameter setObject:Plate forKey:@"Plate"];
        }
    }
    else { //舊案查詢
        //門市代號
        if (selectedIndex_Store >= 0) {
            NSDictionary *_dic = [arrayStore objectAtIndex:selectedIndex_Store];
            NSString *StoreId = [_dic objectForKey:@"No"];
            [parameter setObject:StoreId forKey:@"StoreId"];
        }
        
        //門市人員
        if (selectedIndex_Person >= 0) {
            NSDictionary *_dic = [arrayPerson objectAtIndex:selectedIndex_Person];
            NSString *SalesAccountNo = [_dic objectForKey:@"AccountNo"];
            [parameter setObject:SalesAccountNo forKey:@"SalesAccountNo"];
        }
        
        //原車牌號碼
        NSString *OldPlate = _TextCarNumberOld.text;
        //NSString *carNo_A = _TextCarNumberOldA.text;
        //NSString *carNo_B = _TextCarNumberOldB.text;
        //NSString *OldPlate = [NSString stringWithFormat:@"%@-%@",carNo_A,carNo_B];
        if ([OldPlate length] > 0) {
            [parameter setObject:OldPlate forKey:@"Plate"];
        }
    }
    
    //略過DMS檢查(0: 需檢查, 1: 略過檢查)
    if (didSkipCheck) {
        didSkipCheck = NO;
        [parameter setObject:@"1" forKey:@"SkipCheck"];
    }
    else {
        [parameter setObject:@"0" forKey:@"SkipCheck"];
    }
    
    [GatewayManager callTransferFind:parameter Source:self];
}
- (void) callBacTransferFind:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    //Syscode = 400;
    //Sysmsg = @"原車主身分證字號與DMS車主不符！";
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //到6.1案件資料查詢
        [self GotoTransferListWithData:Data];
    }
    else {
        //"【6.0 過戶移轉(new! for 2-2)】第一面輸入如果車牌和原車主身分證字號不match的話，現在會跳出「原車主身分證字號與DMS車主不符....」請改成「原車主身分證字號與DMS車主不符！」然後有兩個button
        //1. 已於DMS操作過戶→不檢查直接到下一步
        //2. 重新輸入→ 停在這一面"
        if ([Sysmsg rangeOfString:@"原車主身分證字號與DMS車主不符"].length > 0) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Sysmsg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"已於DMS操作過戶" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //再呼叫一次，帶入略過檢查參數
                didSkipCheck = YES;
                [self CallTransferFind];
            }];
            [alertController addAction:submitAction];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"重新輸入" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            }];
            [alertController addAction:cancelAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else {
            [self showAlertControllerWithMessage:Sysmsg];
        }
    }
}
#pragma mark - Button Action
-(IBAction) pressNewTranserBtn:(id)sender { //新過戶移轉
    //參數復歸
    isNewCase = YES;
    //selectedIndex_Store = -1;
    //selectedIndex_Person = -1;
    //[_ButtonPerson setTitle:@"" forState:UIControlStateNormal];
    //[_ButtonStore setTitle:@"" forState:UIControlStateNormal];
    //_TextCarNumberOld.text = @"";
    
    [self SetupFakeData];
    
    [_ButtonNewTranser setBackgroundImage:[UIImage imageNamed:@"btn_newdata_on.png"] forState:UIControlStateNormal];
    [_ButtonOldQuery setBackgroundImage:[UIImage imageNamed:@"btn_old_off.png"] forState:UIControlStateNormal];
    _ViewNewTranser.hidden = NO;
    _ViewOldQuery.hidden = YES;
}
-(IBAction) pressOldQueryBtn:(id)sender { //舊案件查詢
    //參數復歸
    isNewCase = NO;
    //_TextCarNumber.text = @"";
    //_TextOriginalOwnerID.text = @"";
    
    [_ButtonNewTranser setBackgroundImage:[UIImage imageNamed:@"btn_newdata_off.png"] forState:UIControlStateNormal];
    [_ButtonOldQuery setBackgroundImage:[UIImage imageNamed:@"btn_old_on.png"] forState:UIControlStateNormal];
    _ViewNewTranser.hidden = YES;
    _ViewOldQuery.hidden = NO;
}
-(IBAction)pressStoreSelectBtn:(id)sender { //門市選擇
    selectedButton = _ButtonStore;
    [self ShowStoreSelect];
}
-(IBAction)pressStorePersonSelectBtn:(id)sender { //門市人員選擇
    //要先選門市，再選人員
    if (selectedIndex_Store < 0) {
        [self pressStoreSelectBtn:nil];
    }
    else {
        selectedButton = _ButtonPerson;
        [self ShowPersonSelect];
    }
}
-(IBAction)pressSendBtn_NewTranser:(id)sender { //新過戶移轉:送出
    NSString *carNo_A = _TextCarNumberA.text;
    NSString *carNo_B = _TextCarNumberB.text;
    //NSString *carNo = [NSString stringWithFormat:@"%@-%@",carNo_A,carNo_B];
    NSString *ID = _TextOriginalOwnerID.text;
    
    //確認車牌與身分證皆有填寫
    if ([carNo_A length] == 0 || [carNo_B length] == 0 || [ID length] == 0) {
        [self showAlertControllerWithMessage:@"請確認車牌號碼與原車主身分證皆有填寫！"];
    }
    else {
        [self CallTransferFind];
    }
}
-(IBAction)pressSendBtn_OldQuery:(id)sender { //舊案件查詢:送出
    [self CallTransferFind];
}
#pragma mark - 選擇門市與人員
-(void) ShowStoreSelect { //選擇門市
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayStore count]; i++) {
        NSDictionary *_dic = [arrayStore objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonStore;
    popPresenter.sourceRect = _ButtonStore.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
-(void) ShowPersonSelect {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇門市人員" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayPerson count]; i++) {
        NSDictionary *_dic = [arrayPerson objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"AccountName"];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    //[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     //style:UIAlertActionStyleDestructive
     //handler:^(UIAlertAction *action) {
     //}]];
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonPerson;
    popPresenter.sourceRect = _ButtonPerson.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    if (selectedButton == _ButtonStore) {
        selectedIndex_Store = buttonIndex;
        
        NSDictionary *_dic = [arrayStore objectAtIndex:buttonIndex];
        [_ButtonStore setTitle:[_dic objectForKey:@"Name"] forState:UIControlStateNormal];
        
        //抓取對應的門市人員
        [self GetAccountList];
    }
    else if (selectedButton == _ButtonPerson) {
        selectedIndex_Person = buttonIndex;
        
        NSDictionary *_dic = [arrayPerson objectAtIndex:buttonIndex];
        [_ButtonPerson setTitle:[_dic objectForKey:@"AccountName"] forState:UIControlStateNormal];
    }
}
#pragma mark - 換頁
-(void) GotoTransferListWithData:(NSDictionary *)Data { //6.1案件資料查詢
    TransferListVC *destVC = [[TransferListVC alloc] initWithNibName:@"TransferListVC" bundle:nil];
    
    if (![Data isKindOfClass:[NSNull class]]) {
        destVC.isExist = [[Data objectForKey:@"IsExist"] boolValue];
        destVC.arrayTransfer = [Data objectForKey:@"Transfers"];
    }
    
    NSString *carNo_A = _TextCarNumberA.text;
    NSString *carNo_B = _TextCarNumberB.text;
    NSString *carNo = [NSString stringWithFormat:@"%@-%@",carNo_A,carNo_B];
    
    destVC.isNewCase = isNewCase;
    destVC.stringCarNumber = carNo;
    destVC.stringOriginalOwnerID = _TextOriginalOwnerID.text;
    destVC.OwnerProfileId = [Data objectForKey:@"OwnerProfileId"];;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - textField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}
#pragma mark - 雜項
-(void) SetupFakeData {
#if TARGET_IPHONE_SIMULATOR
    _TextOriginalOwnerID.text = @"A125725769"; //A125725769 A116455978
    
    //_TextCarNumber.text = @"QAZ-2468";
    _TextCarNumberA.text = @"ABB"; //QAZ
    _TextCarNumberB.text = @"555"; //2468
    
    _TextCarNumberOld.text = @""; //ABB-555 QAZ-2468
    //_TextCarNumberOldA.text = @"QAZ";
    //_TextCarNumberOldB.text = @"2468";
    
    //目前只有兩筆是可以測試
    //車牌:ABB-555    身分證:A116455978
    //車牌:GGR-1234   身分證:A123456789
    //新的Email 只有 howard.gdcrm@gmail.com  可以使用
#endif
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
