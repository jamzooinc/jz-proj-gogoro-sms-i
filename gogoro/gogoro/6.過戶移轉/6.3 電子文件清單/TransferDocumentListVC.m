//

#import "TransferDocumentListVC.h"

@interface TransferDocumentListVC () {
    NSMutableArray *arrayData;
    NSInteger selectedIndex;
    NSMutableArray *arrayModifiedAttachedID; //有異動的附件ID
    CGFloat totalHeight,gapBetweenView;
    NSString *ownerName,*buyerName;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UILabel *LabelStore;
@property (nonatomic,weak) IBOutlet UILabel *LabelPerson;
@property (nonatomic,weak) IBOutlet UILabel *LabelVin;
@property (nonatomic,weak) IBOutlet UILabel *LabelCarNumber;
@property (nonatomic,weak) IBOutlet UILabel *LabelNewOwnerName;
@property (nonatomic,weak) IBOutlet UILabel *LabelNewOwnerPhone;
@end

@implementation TransferDocumentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"電子文件清單" Image:nil];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayData = [[NSMutableArray alloc] init];
    arrayModifiedAttachedID = [[NSMutableArray alloc] init];
    
    //服務門市
    _LabelStore.text = [NSString stringWithFormat:@"服務門市：%@",[_dicTranfer objectForKey:@"StoreName"]];
    
    //服務人員
    _LabelPerson.text = [NSString stringWithFormat:@"服務人員：%@",[_dicTranfer objectForKey:@"SalesAccountName"]];
    
    //車牌號碼
    _LabelCarNumber.text = [NSString stringWithFormat:@"車牌號碼：%@",[_dicTranfer objectForKey:@"Plate"]];
    
    //VIN
    _LabelVin.text = [NSString stringWithFormat:@"VIN：%@",[_dicTranfer objectForKey:@"VinNo"]];
    
    //新車主姓名與電話
    NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
    if (![NewOwner isKindOfClass:[NSNull class]]) {
        _LabelNewOwnerName.text = [NSString stringWithFormat:@"新車主姓名：%@",[NewOwner objectForKey:@"Email"]];
        _LabelNewOwnerPhone.text = [NSString stringWithFormat:@"新車主電話：%@",[NewOwner objectForKey:@"Email"]];
    }
    
    
    //[self GetAttachList];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
#pragma mark - Call API
/*#pragma mark [601] 取得過戶移轉案件
- (void) TransferGetDetail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_TextCarNumber.text forKey:@"VinNo"];
    [parameter setObject:_TextOriginalOwnerID.text forKey:@"OwnerProfileCode"];
    [GatewayManager callTransferGetDetail:parameter Source:self];
}
- (void) callBacTransferGetDetail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //dicOrder = nil;
        //dicOrder = [[NSDictionary alloc] initWithDictionary:Data];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}*/
#pragma mark - Button Action
-(IBAction)pressSendBtn:(id)sender { //送出
    //NSString *newOwnerEmail = _TextNewOwnerEmail.text;
    
    //確認車牌與身分證皆有填寫
    /*if ([carNo length] == 0 || [ID length] == 0) {
        [self showAlertControllerWithMessage:@"請確認車牌號碼與原車主身分證皆有填寫！"];
    }
    else {
        [self TransferGetDetail];
    }*/
}
#pragma mark - 換頁
/*-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    BOOL hasFileUrl = [FileUrl length] > 0;
    //API 309有Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = YES;
    destVC.hasFileUrl = hasFileUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    destVC.OrderNo = _OrderNo;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}*/
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
