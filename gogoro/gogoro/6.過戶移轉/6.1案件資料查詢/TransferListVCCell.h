

#import <UIKit/UIKit.h>

@interface TransferListVCCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *ImageStuck;
@property (nonatomic,weak) IBOutlet UIButton *ButtonEdit;

@property (nonatomic, weak) IBOutlet UILabel *LabelCarNumber; //車牌號碼
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerNameOld; //原車主姓名
@property (nonatomic, weak) IBOutlet UILabel *LabelOwnerNameNew; //新車主姓名
@property (nonatomic, weak) IBOutlet UILabel *LabelDate; //租約到期日
@property (nonatomic, weak) IBOutlet UILabel *LabelPlan; //新車主電馳方案
@property (nonatomic, weak) IBOutlet UILabel *LabelPlanDesc; //方案的說明
@property (nonatomic, weak) IBOutlet UILabel *LabelPerson; //處理人員
@property (nonatomic, weak) IBOutlet UILabel *LabelStatus;
@end
