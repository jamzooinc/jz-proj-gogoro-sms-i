

#import <UIKit/UIKit.h>

@interface TransferListVC : TemplateVC
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
@property (strong, nonatomic) NSString *OwnerProfileId; //略過DMS檢查時，帶回該車牌號碼目前車主id
@property (strong, nonatomic) NSArray *arrayTransfer;

@property BOOL isExist; //YES:舊案查詢 NO:新增
@property BOOL isNewCase; //YES:新案 NO:舊案查詢
@end
