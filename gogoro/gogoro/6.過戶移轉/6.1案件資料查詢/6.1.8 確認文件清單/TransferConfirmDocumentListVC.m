//

#import "TransferConfirmDocumentListVC.h"
#import "TransferRentVC.h"
#import "PhotoViewerVC.h"
#import "ConsentVC.h"
#import "TransferVC.h"
#import "UploadFileVC.h"

@interface TransferConfirmDocumentListVC () {
    NSMutableArray *arrayData;
    NSInteger selectedIndex;
    NSMutableArray *arrayModifiedAttachedID; //有異動的附件ID
    CGFloat totalHeight,gapBetweenView;
    NSString *ownerName,*buyerName;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UILabel *LabelStore;
@property (nonatomic,weak) IBOutlet UILabel *LabelPerson;
@property (nonatomic,weak) IBOutlet UILabel *LabelVin;
@property (nonatomic,weak) IBOutlet UILabel *LabelCarNumber;
@property (nonatomic,weak) IBOutlet UILabel *LabelNewOwnerName;
@property (nonatomic,weak) IBOutlet UILabel *LabelNewOwnerPhone;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@end

@implementation TransferConfirmDocumentListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrayData = [[NSMutableArray alloc] init];
    arrayModifiedAttachedID = [[NSMutableArray alloc] init];
    
    if (_didDocumentList) { //YES:6.3電子文件清單
        [self setupNavigationItemTitle:@"電子文件清單" Image:nil];
        
         //6.3 電子文件清單 下方的"轉讓資料編輯"及"過戶完成"需要拿掉或是隱藏
        _ViewCommand.hidden = YES;
    }
    else { //NO:6.1.8確認文件清單
        [self setupNavigationItemTitle:@"確認文件清單(步驟4/4：執行文件收集)" Image:nil];
    }
    
    [self LeftButtonBack]; //左側按鈕變成返回
    
    //服務門市
    _LabelStore.text = [NSString stringWithFormat:@"%@",[_dicTranfer objectForKey:@"StoreName"]];
    
    //服務人員
    _LabelPerson.text = [NSString stringWithFormat:@"%@",[_dicTranfer objectForKey:@"SalesAccountName"]];
    
    //車牌號碼
    _LabelCarNumber.text = [NSString stringWithFormat:@"%@",[_dicTranfer objectForKey:@"Plate"]];
    
    //VIN
    _LabelVin.text = [NSString stringWithFormat:@"%@",[_dicTranfer objectForKey:@"VinNo"]];
    
    //新車主姓名與電話
    NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
    if (![NewOwner isKindOfClass:[NSNull class]]) {
        _LabelNewOwnerName.text = [NSString stringWithFormat:@"%@",[NewOwner objectForKey:@"Email"]];
        _LabelNewOwnerPhone.text = [NSString stringWithFormat:@"%@",[NewOwner objectForKey:@"Mobile1"]];
    }
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self GetAttachList];
}
#pragma mark - Call API
#pragma mark [608] 取得執行文件清單
- (void) GetAttachList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //車號
    NSString *Plate = [_dicTranfer objectForKey:@"Plate"];
    [parameter setObject:Plate forKey:@"Plate"];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        //原車主身份證字號
        NSString *OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        [parameter setObject:OwnerProfileCode forKey:@"OwnerProfileCode"];
    }
    else {
        [parameter setObject:@"" forKey:@"OwnerProfileCode"];
    }
    
    //只取得新車主需上傳證件（0:no,1:yes）
    [parameter setObject:@"0" forKey:@"NewOwnerCredential"];
    [GatewayManager callTransferGetAttachList:parameter Source:self];
}
- (void) callBacTransferGetAttachList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        NSArray *Attachs = [Data objectForKey:@"Attachs"];
        
        [arrayData removeAllObjects];
        [arrayData addObjectsFromArray:Attachs];
        
        for (NSDictionary *_dic in arrayData) {
            NSLog(@"Name:%@",[_dic objectForKey:@"AttachTypeName"]);
            NSLog(@"_dic:%@",_dic);
            
            //是否已勾
            BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
            if (IsChecked) { //已勾的文件，記起來等上傳API的時候使用
                NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
                [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
            }
        }
        
        //產生項目
        [self GenerateSubview];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [611] 過戶文件重新套版(重簽時呼叫)
- (void) ReAapplyDocumentWithAttachType:(NSInteger)attachType {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //車號
    NSString *Plate = [_dicTranfer objectForKey:@"Plate"];
    [parameter setObject:Plate forKey:@"Plate"];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        //原車主身份證字號
        NSString *OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        [parameter setObject:OwnerProfileCode forKey:@"OwnerProfileCode"];
    }
    else {
        [parameter setObject:@"" forKey:@"OwnerProfileCode"];
    }
    
    //附件類型
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callTransferReApplyDocument:parameter Source:self];
}
- (void)callBackTransferReApplyDocument:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //[605] 取得租約轉讓需簽名同意書及順序
        //[self callGetTransferContractList];
        //[self GotoConsentResignWithUrl:[parameter objectForKey:@"Data"]];
        
        NSDictionary *data = [parameter objectForKey:@"Data"];
        NSMutableArray *_contractArray = [[NSMutableArray alloc] init];
        [_contractArray addObject:data];
        
        //改成依照ContractArray來判斷目前該簽的文件
        if ([_contractArray count] > 0) {
            int _nowSeq = 0;
            NSInteger nowAttachType = [[[_contractArray objectAtIndex:_nowSeq] objectForKey:@"AttachType"] integerValue];
            
            //6.1.6 電子文件簽核
            ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
            destVC.enumCONSENT = CONSENT_CONTRACT_RESIGN;
            //destVC.OrderNo = _OrderNo;
            //destVC.CustomerName = _CustomerName;
            //destVC.dicSubsidy = _dicSubsidy;
            destVC.nowSeq = _nowSeq; //1
            destVC.contractArray = _contractArray;
            destVC.newAttachType = nowAttachType;
            destVC.dicTranfer = _dicTranfer;
            destVC.stringCarNumber = _stringCarNumber;
            destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
            destVC.isReSign = YES;
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [604] 新增租約轉讓資料(租約轉讓資料選擇完後呼叫)
- (void) callAdd {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //車號
    NSString *Plate = [_dicTranfer objectForKey:@"Plate"];
    [parameter setObject:Plate forKey:@"Plate"];
    
    //原車主
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        //原車主身份證字號
        NSString *OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        [parameter setObject:OwnerProfileCode forKey:@"OwnerProfileCode"];
        
        //原車主id(略過DMS檢查時，帶回該車牌號碼目前車主id)
        NSString *OwnerProfileId = [OriginalOwner objectForKey:@"OwnerProfileId"];
        if ([OwnerProfileId length] > 0) {
            [parameter setObject:OwnerProfileId forKey:@"OwnerProfileId"];
        }
        else {
            [parameter setObject:@"" forKey:@"OwnerProfileId"];
        }
        
        //原車主親辦(0: 親辦, 1: 授權委託人)
        [parameter setObject:[OriginalOwner objectForKey:@"IsAgent"] forKey:@"OriginalOwnerIsAgent"];
    }
    else {
        [parameter setObject:@"" forKey:@"OwnerProfileCode"];
        [parameter setObject:@"" forKey:@"OriginalOwnerIsAgent"];
    }
    
    //新車主
    NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
    if (![NewOwner isKindOfClass:[NSNull class]]) {
        //新車主親辦(0: 親辦, 1: 授權委託人)
        [parameter setObject:[NewOwner objectForKey:@"IsAgent"] forKey:@"NewOwnerIsAgent"];
        
        //電馳方案(租賃項目) 代碼
        [parameter setObject:[NewOwner objectForKey:@"ArticleNo"] forKey:@"ArticleNo"];
    }
    else {
        [parameter setObject:@"" forKey:@"NewOwnerIsAgent"];
    }
    
    //案件備註
    NSString *Memo = [_dicTranfer objectForKey:@"Memo"];
    [parameter setObject:Memo forKey:@"Memo"];
    
    //草稿:0, 處理中:1, 已結案:2
    [parameter setObject:@"2" forKey:@"State"];
    
    [GatewayManager callTransferAdd:parameter Source:self];
}
- (void) callBacTransferAdd:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"過戶完成！欲瀏覽電子檔請到過戶移轉首頁查詢。" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        }];
        [alertController addAction:actionConfirm];
        
        [self presentViewController:alertController animated:YES completion:^(void){
            [self performSelector:@selector(GotoTransferVC) withObject:nil afterDelay:0.5f]; //退回2.0過戶移轉
        }];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [605] 取得租約轉讓需簽名同意書及順序
-(void)callGetTransferContractList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
    }
    
    [parameter setObject:[_dicTranfer objectForKey:@"Plate"] forKey:@"Plate"];
    [GatewayManager callTransferGetContractList:parameter Source:self];
}
-(void)callBacTransferGetContractList:(NSDictionary *)parameter {
    
    [ObjectManager hideHUDForView:self.view];
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSArray *data = [parameter objectForKey:@"Data"];
        NSMutableArray *_contractArray = [NSMutableArray arrayWithArray:data];
        
        //改成依照ContractArray來判斷目前該簽的文件
        if ([_contractArray count] > 0) {
            int _nowSeq = 0;
            NSInteger nowAttachType = [[[_contractArray objectAtIndex:_nowSeq] objectForKey:@"AttachType"] integerValue];
            
            //6.1.6 電子文件簽核
            ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
            destVC.enumCONSENT = CONSENT_CONTRACT_NEED_SIGNATURE;
            //destVC.OrderNo = _OrderNo;
            //destVC.CustomerName = _CustomerName;
            //destVC.dicSubsidy = _dicSubsidy;
            destVC.nowSeq = _nowSeq; //1
            destVC.contractArray = _contractArray;
            destVC.newAttachType = nowAttachType;
            destVC.dicTranfer = _dicTranfer;
            destVC.stringCarNumber = _stringCarNumber;
            destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
    else{
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - UI產生
-(void) GenerateSubview {
    //移除既有View
    for (id _subView in _ScrollView.subviews) {
        if ([_subView isKindOfClass:[TransferUploadIDVCView class]]) {
            [_subView removeFromSuperview];
        }
    }
    
    gapBetweenView = 15;
    totalHeight = 20;
    
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *dic = [arrayData objectAtIndex:i];
        
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"TransferUploadIDVCView" owner:self options:nil];
        TransferUploadIDVCView *view = [toplevels objectAtIndex:0];
        view.delegate = self;
        view.tag = i;
        view.didFromECDocumentListVC = _didDocumentList;
        view.frame = CGRectMake(0, totalHeight, _ScrollView.frame.size.width, 49);
        [_ScrollView addSubview:view];
        [view UpdateUIWithData:dic];
        
        totalHeight = view.frame.origin.y + view.frame.size.height + gapBetweenView;
    }
    
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}
#pragma mark - 勾選/取消勾選
-(void) ConfirmInfoViewCheckAtIndex:(NSInteger)index { //勾選
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}
-(void) ConfirmInfoViewUnCheckAtIndex:(NSInteger)index { //取消勾選
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID removeObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}
#pragma mark - 上傳
-(void) TransferUploadIDVCViewUploadAtIndex:(NSInteger)index { //上傳
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    NSString *memo = [_dic objectForKey:@"Memo"];
    [self GotoUploadFileVCWithType:AttachTypeId AttachName:AttachTypeName Memo:memo];
}
#pragma mark - FileUrl
-(void) TransferUploadIDVCViewFileUrldAtIndex:(NSInteger)index { //fileUrl
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
        
    if ([FileUrl length] == 0) {
        [self showAlertControllerWithMessage:@"請先上傳電子檔！"];
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];
    
    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([FileUrl rangeOfString:@".jpg"].length > 0 || [FileUrl rangeOfString:@".png"].length > 0 || [FileUrl rangeOfString:@".JPG"].length > 0 || [FileUrl rangeOfString:@".PNG"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:FileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:FileUrl Name:AttachTypeName FromTempUrl:NO];
    }
}
#pragma mark - 重簽(重新套版)
-(void) TransferUploadIDVCViewReTemplateAtIndex:(NSInteger)index { //上傳
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [self ReAapplyDocumentWithAttachType:AttachTypeId];
}
#pragma mark - 預覽列印
-(void) TransferUploadIDVCViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl { //預覽列印
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    if ([urlString length] == 0) {
        return;
    }
    
    //測試
    //TemplateFileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    //NSLog(@"TemplateFileUrl:%@",TemplateFileUrl);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([urlString rangeOfString:@".jpg"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:urlString Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:urlString Name:AttachTypeName FromTempUrl:fromTempUrl];
    }
}

#pragma mark - Button Action
-(IBAction)pressSendBtn:(id)sender { //送出
    //NSString *newOwnerEmail = _TextNewOwnerEmail.text;
    
    //確認車牌與身分證皆有填寫
    /*if ([carNo length] == 0 || [ID length] == 0) {
        [self showAlertControllerWithMessage:@"請確認車牌號碼與原車主身分證皆有填寫！"];
    }
    else {
        [self TransferGetDetail];
    }*/
}
-(IBAction)pressEditBtn:(id)sender { //上一步
    //退回6.1.5 輸入租約轉讓資料
    [self GotoTransferRentVC];
}
-(IBAction)pressCompleteBtn:(id)sender { //下一步
    //2016.10.19 全勾才能申請
    NSInteger totalCount = 0;
    NSInteger needCount = [arrayData count];
    NSInteger noCehckIndex = -1; //哪一筆沒有打勾
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *_dic = [arrayData objectAtIndex:i];
        BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
        if (IsChecked) { //已勾
            totalCount++;
        }
        else { //未勾，判斷有沒有在手動勾選清單裡面
            BOOL didExist = NO;
            NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
            for (NSString *_attachID in arrayModifiedAttachedID) {
                NSInteger attachID = [_attachID integerValue];
                if (AttachTypeId == attachID) {
                    didExist = YES;
                    totalCount++;
                }
            }
            if (!didExist) {
                noCehckIndex = i;
                break;
            }
        }
    }
    
    if (totalCount != needCount) {
        if (noCehckIndex >= 0) { //防呆
            NSDictionary *_dic = [arrayData objectAtIndex:noCehckIndex];
            NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
            NSString *message = [NSString stringWithFormat:@"請確認「%@」已收集！",AttachTypeName];
            [self showAlertControllerWithMessage:message];
        }
        
        return;
    }
    
    [self TransferFinish];
}
-(void) TransferFinish {
    //"6.1.8 確認文件清單 點擊""過戶完成""請呼叫API [604] 新增租約轉讓資料(租約轉讓資料選擇完後呼叫)  將State = '2' 改變狀態成""已結案""狀態"
    [self callAdd];
}
#pragma mark - 換頁
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName Memo:(NSString *)memo { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.dicTransfer = _dicTranfer;
    destVC.type = attachType;
    destVC.attachName = attachName;
    destVC.didFromTransfer = YES;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.memo = memo;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl  { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //API 309有Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = ownerName;
    NSString *BuyName = buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = YES;
    destVC.hideEmailButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    //destVC.OrderNo = _OrderNo;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTransferRentVC { //退回6.1.5 輸入租約轉讓資料
    //到補助選擇
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[TransferRentVC class]]) {
            destVC = (TransferRentVC *)vc;
        }
    }
    NSLog(@"destVC:%@",destVC);
    if (destVC) { //退回6.1.5 輸入租約轉讓資料
        [self.navigationController popToViewController:destVC animated:NO];
    }
    else { //6.2 輸入租約轉讓資料
        TransferRentVC *destVC = [[TransferRentVC alloc] initWithNibName:@"TransferRentVC" bundle:nil];
        destVC.dicTranfer = _dicTranfer;
        destVC.stringCarNumber = _stringCarNumber;
        //destVC.stringCarNumberOld = _stringCarNumberOld;
        destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
        destVC.didFromTransferList = YES;
        [self.navigationController pushViewController:destVC animated:YES];
    }
}
-(void) GotoTransferVC { //退回6.0過戶移轉
    //到補助選擇
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[TransferVC class]]) {
            destVC = (TransferVC *)vc;
        }
    }
    NSLog(@"destVC:%@",destVC);
    [self.navigationController popToViewController:destVC animated:NO];
}
-(void) GotoConsentResignWithUrl:(NSString *)urlString { //重簽
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_CONTRACT_RESIGN;
    //destVC.OrderNo = _OrderNo;
    destVC.isReSign = YES;
    destVC.consentUrlStr = urlString;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
