
#import <UIKit/UIKit.h>
#import "TransferUploadIDVCView.h"

@interface TransferConfirmDocumentListVC : TemplateVC
@property (nonatomic, strong) NSDictionary *dicTranfer; //過戶移轉的資料
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
//@property (strong, nonatomic) NSString *stringCarNumberOld; //原車牌號碼
@property BOOL didDocumentList; //YES:6.3電子文件清單 NO:6.1.8確認文件清單
@end
