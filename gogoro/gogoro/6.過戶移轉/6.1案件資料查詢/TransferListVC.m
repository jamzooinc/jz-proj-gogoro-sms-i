

#import "TransferListVC.h"
#import "ConsentVC.h"
#import "ObjectManager.h"
#import "TransferListVCCell.h"
#import "TransferNewEmailVC.h"
#import "TransferUploadIDVC.h"
#import "TransferRentVC.h"
#import "TransferConfirmDocumentListVC.h"

@interface TransferListVC () {
    NSInteger selectedIndex;
    NSInteger selectedSection;
    NSMutableArray *arrayTableView;
    BOOL didEdit; //YES:編輯 NO：選擇該筆
    BOOL didSelectedCaseDraft; //選擇的案件是否為草稿
    NSDictionary *dicDetailSelectedCase; //選擇的案件呼叫602之後的詳細資料
}
@property (nonatomic,weak) IBOutlet UITableView *TableView;
@property (nonatomic,weak) IBOutlet UIButton *ButtonAdd;
@end

@implementation TransferListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNavigationItemTitle:@"案件資料查詢(步驟1/4：確認原車主)" Image:nil];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayTableView = [[NSMutableArray alloc] init];
    for (NSDictionary *_dic in _arrayTransfer) {
        NSMutableDictionary *dic1 = [NSMutableDictionary dictionaryWithDictionary:_dic];
        [arrayTableView addObject:dic1];
    }
    
    if (!_isExist) { //沒有比對出資料
        if (_isNewCase) { //新增
            [self CallTransferAdd:NO Data:nil];
        }
        else { //舊案查詢
            if ([arrayTableView count] == 0) {
                [self showAlertControllerWithMessage:@"查無條件相符案件資料！"];
            }

        }
    }
    
    //新增按鈕，舊案查詢時隱藏
    if (!_isNewCase) {
        _ButtonAdd.hidden = YES;
        _TableView.frame = CGRectMake(_TableView.frame.origin.x, _TableView.frame.origin.y, _TableView.frame.size.width, _TableView.frame.size.height + _ButtonAdd.frame.size.height);
    }
    else {
        _ButtonAdd.hidden = NO;
    }
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self AddNotification];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotification];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction
/* 送出 */
- (IBAction)submitButtonClick:(id)sender {
    //新增
    [self CallTransferAdd:NO Data:nil];
}
#pragma mark - tableView 相關
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
    return [arrayTableView count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransferListVCCell"];
    
    if (!cell) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TransferListVCCell" owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    TransferListVCCell *myCell = (TransferListVCCell *)cell;
    myCell.backgroundColor = [UIColor clearColor];
    myCell.tag = indexPath.row;
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:indexPath.row];
    NSLog(@"_dic:%@",_dic);
    
    //車牌號碼
    myCell.LabelCarNumber.text = [NSString stringWithFormat:@"車牌號碼：%@",[_dic objectForKey:@"Plate"]];
    
    //原車主姓名
    myCell.LabelOwnerNameOld.text = [NSString stringWithFormat:@"原車主姓名：%@",[_dic objectForKey:@"OwnerName"]];

    //新車主姓名
    myCell.LabelOwnerNameNew.text = [NSString stringWithFormat:@"新車主姓名：%@",[_dic objectForKey:@"NewOwnerName"]];
    
    //過戶生效日
    myCell.LabelDate.text = [NSString stringWithFormat:@"過戶生效日：%@",[_dic objectForKey:@"EffectiveDate"]];
    
    //服務門市
    myCell.LabelPlan.text = [NSString stringWithFormat:@"服務門市：%@",[_dic objectForKey:@"StoreName"]];
    
    //服務人員
    myCell.LabelPlanDesc.text = [NSString stringWithFormat:@"服務人員：%@",[_dic objectForKey:@"SalesAccountName"]];
    
    //建立日期
    myCell.LabelPerson.text = [NSString stringWithFormat:@"建立日期：%@",[_dic objectForKey:@"CreateDate"]];
    
    //案件狀態
    myCell.LabelStatus.text = [NSString stringWithFormat:@"%@",[_dic objectForKey:@"Step"]];
    
    //案件階段草稿, 處理中, 已結案
    NSString *State = [_dic objectForKey:@"State"];
    if ([State isEqualToString:@"草稿"]) {
        myCell.ImageStuck.image = [UIImage imageNamed:@"img_draft.png"];
        myCell.LabelStatus.textColor = [UIColor colorWithRed:207.0/255.0 green:231.0/255.0 blue:78.0/255.0 alpha:1.0];
        
        //"草稿"與"已結案"狀態不應顯示鉛筆圖型
        myCell.ButtonEdit.hidden = YES;
    }
    else if ([State isEqualToString:@"處理中"]) {
        myCell.ImageStuck.image = [UIImage imageNamed:@"img_doing.png"];
        myCell.LabelStatus.textColor = [UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:210.0/255.0 alpha:1.0];
        
        //"處理中"狀態該顯示鉛筆圖型
        myCell.ButtonEdit.hidden = NO;
    }
    else if ([State isEqualToString:@"已結案"]) {
        myCell.ImageStuck.image = [UIImage imageNamed:@"img_end.png"];
        myCell.LabelStatus.textColor = [UIColor colorWithRed:240.0/255.0 green:109.0/255.0 blue:128.0/255.0 alpha:1.0];
        
        //"草稿"與"已結案"狀態不應顯示鉛筆圖型
        myCell.ButtonEdit.hidden = YES;
    }
    
    myCell.contentView.userInteractionEnabled = NO; //為了讓Button可以trigger
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedSection = indexPath.section;
    selectedIndex = indexPath.row;
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{ //讓tableview的seperator可以跟螢幕一樣寬 http://stackoverflow.com/questions/26519248/how-to-set-the-full-width-of-separator-in-uitableview
    if ([_TableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_TableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_TableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_TableView setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - 選擇與編輯
-(void) SelectAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;

    NSDictionary *_dic = [arrayTableView objectAtIndex:index];

    didEdit = NO;
    
    //案件階段
    NSString *State = [_dic objectForKey:@"State"];
    if ([State isEqualToString:@"草稿"]) {
        didSelectedCaseDraft = YES;
        //先抓詳細資料
        [self TransferGet];
    }
    else {
        //先抓詳細資料
        [self TransferGet];
    }
}
-(void) EditAtIndex:(NSNotification *)notif {
    NSNumber *number = [notif object];
    NSInteger index = [number intValue];
    selectedIndex = index;
    //NSDictionary *_dic = [arrayTableView objectAtIndex:index];
    
    didEdit = YES;
    
    //先取得詳細資料
    [self TransferGet];
}
#pragma mark - Call API
#pragma mark [602] 取得過戶移轉案件
- (void) TransferGet {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:[_dic objectForKey:@"OwnerProfileCode"] forKey:@"OwnerProfileCode"];
    [parameter setObject:[_dic objectForKey:@"Plate"] forKey:@"Plate"];
    [GatewayManager callTransferGet:parameter Source:self];
}
- (void) callBacTransferGet:(NSDictionary *)parameter {
    if (!didSelectedCaseDraft) {
        [ObjectManager hideHUDForView:self.view];
    }
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        //NSDictionary *dicTransfer = nil;
        dicDetailSelectedCase = nil;
        if (![Data isKindOfClass:[NSNull class]]) {
            //dicTransfer = [[NSDictionary alloc] initWithDictionary:Data];
            dicDetailSelectedCase = [[NSDictionary alloc] initWithDictionary:Data];
        }
        
        if (dicDetailSelectedCase) { //防呆
        }
        else {
            if (didSelectedCaseDraft) {
                [ObjectManager hideHUDForView:self.view];
            }
            return;
        }
        
        NSDictionary *_dic = [arrayTableView objectAtIndex:selectedIndex];
        
        if (didEdit) { //編輯，到
            //6.2 輸入租約轉讓資料
            [self GotoTransferRentVCWithData:dicDetailSelectedCase];
        }
        else if (didSelectedCaseDraft) {
            didSelectedCaseDraft = NO;
            //[604] 新增租約轉讓資料(租約轉讓資料選擇完後呼叫)
            [self CallTransferAdd:YES Data:dicDetailSelectedCase];
        }
        else { //選擇該案件，判斷案件階段
            //案件階段
            NSString *State = [_dic objectForKey:@"State"];
            if ([State isEqualToString:@"草稿"]) {
                
            }
            else {
                if ([State isEqualToString:@"處理中"]) {
                    //6.1.8 確認文件清單
                    [self GotoConfirmDocumentListWithData:dicDetailSelectedCase didDocumentList:NO];
                }
                else if ([State isEqualToString:@"已結案"]) {
                    //6.3 電子文件清單
                    [self GotoConfirmDocumentListWithData:dicDetailSelectedCase didDocumentList:YES];
                }
            }
        }
    }
    else {
        if (didSelectedCaseDraft) {
            [ObjectManager hideHUDForView:self.view];
        }
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [604] 新增租約轉讓資料(租約轉讓資料選擇完後呼叫)
- (void) CallTransferAdd:(BOOL)isDraft Data:(NSDictionary *)dicTransfer {
    if (!didSelectedCaseDraft) {
        [ObjectManager showLodingHUDinView:self.view];
    }
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    if (isDraft) { //草稿案件，先新增
        //Plate 車牌號碼
        [parameter setObject:[dicTransfer objectForKey:@"Plate"] forKey:@"Plate"];
        
        //原車主
        NSDictionary *OriginalOwner = [dicTransfer objectForKey:@"OriginalOwner"];
        if (OriginalOwner && ![OriginalOwner isKindOfClass:[NSNull class]]) {
            //OwnerProfileCode 原車主身份證字號
            [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
            
            //原車主id(略過DMS檢查時，帶回該車牌號碼目前車主id)
            NSString *OwnerProfileId = [OriginalOwner objectForKey:@"OwnerProfileId"];
            if ([OwnerProfileId length] > 0) {
                [parameter setObject:OwnerProfileId forKey:@"OwnerProfileId"];
            }
            else {
                [parameter setObject:@"" forKey:@"OwnerProfileId"];
            }
            
            //OriginalOwnerIsAgent 原車主親辦(0: 親辦, 1: 授權委託人)
            [parameter setObject:[OriginalOwner objectForKey:@"IsAgent"] forKey:@"OriginalOwnerIsAgent"];
        }
        
        //新車主
        NSDictionary *NewOwner = [dicTransfer objectForKey:@"NewOwner"];
        if (NewOwner && ![NewOwner isKindOfClass:[NSNull class]]) {
            //NewOwnerProfileCode 新車主身份證字號 2017.02.23取消
            //[parameter setObject:[NewOwner objectForKey:@"ProfileCode"] forKey:@"NewOwnerProfileCode"];
            
            //NewOwnerIsAgent 新車主親辦(0: 親辦, 1: 授權委託人)
            [parameter setObject:[NewOwner objectForKey:@"IsAgent"] forKey:@"NewOwnerIsAgent"];
            
            //ArticleNo 電馳方案(租賃項目) 代碼
            [parameter setObject:[NewOwner objectForKey:@"ArticleNo"] forKey:@"ArticleNo"];
        }
        
        //Memo 案件備註
        [parameter setObject:[dicTransfer objectForKey:@"Memo"] forKey:@"Memo"];
        
        //State 草稿:0, 處理中:1, 已結案:2
        NSString *State = [dicTransfer objectForKey:@"State"];
        if ([State isEqualToString:@"草稿"]) {
            State = @"0";
        }
        else if ([State isEqualToString:@"處理中"]) {
            State = @"1";
        }
        else if ([State isEqualToString:@"已結案"]) {
            State = @"2";
        }
        [parameter setObject:State forKey:@"State"];
    }
    else {
        //原車主身份證字號
        NSString *OwnerProfileCode = _stringOriginalOwnerID;
        if ([OwnerProfileCode length] > 0) {
            [parameter setObject:OwnerProfileCode forKey:@"OwnerProfileCode"];
        }
        
        //車牌號碼
        NSString *Plate = _stringCarNumber;
        if ([Plate length] > 0) {
            [parameter setObject:Plate forKey:@"Plate"];
        }
    }
    
    //略過DMS檢查時，帶回該車牌號碼目前車主id
    if ([_OwnerProfileId length] > 0) {
        [parameter setObject:_OwnerProfileId forKey:@"OwnerProfileId"];
    }
    [GatewayManager callTransferAdd:parameter Source:self];
}
- (void) callBacTransferAdd:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        //@"DMS已無積欠帳款，當月帳款仍須繳清！"
        /*UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Sysmsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //6.1.3-1 上傳新車主證件
            [self GotoTransferUploadIDVC];
        }];
        [alertController addAction:submitAction];
        [self presentViewController:alertController animated:YES completion:nil];*/
        
        //6.1.3-1 上傳新車主證件
        [self GotoTransferUploadIDVC];
    }
    else if (Syscode == 210) {
        //210:Go Support沒有GGR記號 @"DMS已無積欠帳款，當月帳款仍須繳清！"
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Sysmsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //6.1.1 輸入新車主email
            [self GotoNewEmailVC];
        }];
        [alertController addAction:submitAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (Syscode == 211) {
        //211:配對系統查核沒有GGR記號 @"DMS已無積欠帳款，當月帳款仍須繳清！"
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Sysmsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //6.1.3-1 上傳新車主證件
            [self GotoTransferUploadIDVC];
        }];
        [alertController addAction:submitAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 換頁
-(void) GotoNewEmailVC { //6.1.1 輸入新車主email
    //預設資料從前頁帶過來
    //Plate 車牌號碼
    NSString *Plate = _stringCarNumber;
    //原車主
    NSString *OwnerProfileCode = _stringOriginalOwnerID;
    
    if (dicDetailSelectedCase) { //選到某一筆，就用該筆的資料
        Plate = [dicDetailSelectedCase objectForKey:@"Plate"];
        NSDictionary *OriginalOwner = [dicDetailSelectedCase objectForKey:@"OriginalOwner"];
        if (![OriginalOwner isKindOfClass:[NSNull class]]) {
            //OwnerProfileCode 原車主身份證字號
            OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        }
    }
    
    TransferNewEmailVC *destVC = [[TransferNewEmailVC alloc] initWithNibName:@"TransferNewEmailVC" bundle:nil];
    destVC.stringCarNumber = Plate;
    destVC.stringOriginalOwnerID = OwnerProfileCode;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTransferUploadIDVC { //6.1.3-1 上傳新車主證件
    //預設資料從前頁帶過來
    //Plate 車牌號碼
    NSString *Plate = _stringCarNumber;
    //原車主
    NSString *OwnerProfileCode = _stringOriginalOwnerID;
    
    if (dicDetailSelectedCase) { //選到某一筆，就用該筆的資料
        Plate = [dicDetailSelectedCase objectForKey:@"Plate"];
        NSDictionary *OriginalOwner = [dicDetailSelectedCase objectForKey:@"OriginalOwner"];
        if (![OriginalOwner isKindOfClass:[NSNull class]]) {
            //OwnerProfileCode 原車主身份證字號
            OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        }
    }
    
    TransferUploadIDVC *destVC = [[TransferUploadIDVC alloc] initWithNibName:@"TransferUploadIDVC" bundle:nil];
    destVC.stringCarNumber = Plate;
    destVC.stringOriginalOwnerID = OwnerProfileCode;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTransferRentVCWithData:(NSDictionary *)data  { //6.2 輸入租約轉讓資料（與6.1.5 輸入租約轉讓資料共用）
    //預設資料從前頁帶過來
    //Plate 車牌號碼
    NSString *Plate = _stringCarNumber;
    //原車主
    NSString *OwnerProfileCode = _stringOriginalOwnerID;
    
    if (dicDetailSelectedCase) { //選到某一筆，就用該筆的資料
        Plate = [dicDetailSelectedCase objectForKey:@"Plate"];
        NSDictionary *OriginalOwner = [dicDetailSelectedCase objectForKey:@"OriginalOwner"];
        if (![OriginalOwner isKindOfClass:[NSNull class]]) {
            //OwnerProfileCode 原車主身份證字號
            OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        }
    }
    
    TransferRentVC *destVC = [[TransferRentVC alloc] initWithNibName:@"TransferRentVC" bundle:nil];
    destVC.dicTranfer = data;
    destVC.stringCarNumber = Plate;
    destVC.stringOriginalOwnerID = OwnerProfileCode;
    //destVC.stringCarNumberOld = _stringCarNumberOld;
    destVC.didFromTransferList = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConfirmDocumentListWithData:(NSDictionary *)data didDocumentList:(BOOL)didDocumentList { //6.1.8 確認文件清單與6.3電子文件清單共用
    //預設資料從前頁帶過來
    //Plate 車牌號碼
    NSString *Plate = _stringCarNumber;
    //原車主
    NSString *OwnerProfileCode = _stringOriginalOwnerID;
    
    if (dicDetailSelectedCase) { //選到某一筆，就用該筆的資料
        Plate = [dicDetailSelectedCase objectForKey:@"Plate"];
        NSDictionary *OriginalOwner = [dicDetailSelectedCase objectForKey:@"OriginalOwner"];
        if (![OriginalOwner isKindOfClass:[NSNull class]]) {
            //OwnerProfileCode 原車主身份證字號
            OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        }
    }
    
    TransferConfirmDocumentListVC *destVC = [[TransferConfirmDocumentListVC alloc] initWithNibName:@"TransferConfirmDocumentListVC" bundle:nil];
    destVC.dicTranfer = data;
    destVC.stringCarNumber = Plate;
    //destVC.stringCarNumberOld = _stringCarNumberOld;
    destVC.stringOriginalOwnerID = OwnerProfileCode;
    destVC.didDocumentList = didDocumentList;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
-(void) AddNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SelectAtIndex:) name:@"kNotification_Select_Transfer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EditAtIndex:) name:@"kNotification_Edit_Transfer" object:nil];
}
-(void) RemoveNotification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Select_Transfer" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Edit_Transfer" object:nil];
}
@end
