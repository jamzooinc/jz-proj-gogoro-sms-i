

#import "TransferRentVC.h"
#import "ConsentVC.h"
#import "TransferConfirmDocumentListVC.h"

@interface TransferRentVC () <UITextViewDelegate> {
    NSMutableArray *arrayArticle;
    NSInteger selectedIndex;
    
    NSInteger OriginalOwnerIsAgent; //原車主親辦(0: 親辦, 1: 授權委託人)
    NSInteger NewOwnerIsAgent; //新車主親辦(0: 親辦, 1: 授權委託人)
    NSString *ArticleNo; //電馳方案(租賃項目) 代碼
    NSString *Memo; //案件備註
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UIView *ViewCommand;
@property (nonatomic,weak) IBOutlet SubsidyRootView *ViewOwnerOriginal; //原車主
@property (nonatomic,weak) IBOutlet SubsidyRootView *ViewOwnerNew; //新車主
@property (nonatomic,weak) IBOutlet SubsidyRootView *ViewComment; //備註
@property (nonatomic,weak) IBOutlet UITextView *TextComment;
@property (nonatomic,weak) IBOutlet UILabel *LabelEffectiveDate; //過戶生效日
@property (nonatomic,weak) IBOutlet UILabel *LabelIndetityOriginal; //車主身份：原車主
@property (nonatomic,weak) IBOutlet UIButton *ButtonJobOriginalSelf; //辦理業務：原車主：親辦
@property (nonatomic,weak) IBOutlet UIButton *ButtonJobOriginalOther; //辦理業務：原車主：委託
@property (nonatomic,weak) IBOutlet UILabel *LabelRentOriginal; //電馳服務租賃項目：原車主
@property (nonatomic,weak) IBOutlet UILabel *LabelIndetityNew; //車主身份：新車主
@property (nonatomic,weak) IBOutlet UIButton *ButtonJobNewSelf; //辦理業務：新車主：親辦
@property (nonatomic,weak) IBOutlet UIButton *ButtonJobNewOther; //辦理業務：新車主：委託
@property (nonatomic,weak) IBOutlet UIButton *ButtonRentNew; ////電馳服務租賃項目：新車主
@property (nonatomic,weak) IBOutlet UILabel *LabelRentNew; ////電馳服務租賃項目：新車主
@property (nonatomic,weak) IBOutlet UILabel *LabelValueAddNew; //性能加值服務：新車主
@end

@implementation TransferRentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (_didFromTransferList) { //從6.1案件資料查詢過來
        [self setupNavigationItemTitle:@"輸入租約轉讓資料" Image:nil];
    }
    else {
        [self setupNavigationItemTitle:@"輸入租約轉讓資料(步驟4/4：執行文件收集)" Image:nil];
    }
    [self LeftButtonBack];
    
    //[self registerForKeyboardNotifications];
    
    arrayArticle = [[NSMutableArray alloc] init];
    
    //過戶生效日
    _LabelEffectiveDate.text = [_dicTranfer objectForKey:@"EffectiveDate"];
    
    //原車主
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        //車主身份
        _LabelIndetityOriginal.text = [OriginalOwner objectForKey:@"IdentityType"];
        
        //原車主辦理業務：車主親辦(0: 親辦, 1: 授權委託人)
        BOOL IsAgent = [[OriginalOwner objectForKey:@"IsAgent"] boolValue];
        if (IsAgent) { //授權委託人
            [self pressJobOriginalOther:nil];
        }
        else { //親辦
            [self pressJobOriginalSelf:nil];
        }
        
        //電馳服務租賃項目：原車主
        _LabelRentOriginal.text = [OriginalOwner objectForKey:@"ArticleName"];
    }
    
    //新車主
    NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
    if (![NewOwner isKindOfClass:[NSNull class]]) {
        //車主身份
        _LabelIndetityNew.text = [NewOwner objectForKey:@"IdentityType"];
        
        //新主辦理業務：車主親辦(0: 親辦, 1: 授權委託人)
        BOOL IsAgent = [[NewOwner objectForKey:@"IsAgent"] boolValue];
        if (IsAgent) { //授權委託人
            [self pressJobNewOther:nil];
        }
        else { //親辦
            [self pressJobNewSelf:nil];
        }
        
        //電馳服務租賃項目：新車主
        _LabelRentNew.text = [NewOwner objectForKey:@"ArticleName"];
        _LabelRentNew.hidden = NO;
        [_ButtonRentNew setTitle:@"" forState:UIControlStateNormal];
    }
    
    //備註
    _TextComment.text = [_dicTranfer objectForKey:@"Memo"];

    //UI
    [_ScrollView addSubview:_ViewOwnerOriginal];
    [_ScrollView addSubview:_ViewOwnerNew];
    [_ScrollView addSubview:_ViewComment];
    [_ScrollView addSubview:_ViewCommand];
    CGFloat gap = 10;
    _ViewOwnerOriginal.center = CGPointMake(_ScrollView.frame.size.width/2, _ViewOwnerOriginal.frame.size.height/2);
    _ViewOwnerNew.center = CGPointMake(_ScrollView.frame.size.width/2,  _ViewOwnerOriginal.frame.origin.y + _ViewOwnerOriginal.frame.size.height + gap + _ViewOwnerNew.frame.size.height/2);
    _ViewComment.center = CGPointMake(_ScrollView.frame.size.width/2,  _ViewOwnerNew.frame.origin.y + _ViewOwnerNew.frame.size.height + gap + _ViewComment.frame.size.height/2);
    _ViewCommand.center = CGPointMake(_ScrollView.frame.size.width/2,  _ViewComment.frame.origin.y + _ViewComment.frame.size.height + gap + _ViewCommand.frame.size.height/2);
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, _ViewCommand.frame.origin.y + _ViewCommand.frame.size.height)];
    
    //取得電馳方案列表
    [self GetArticleList];
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
#pragma mark - Call API
#pragma mark [609] 取得電馳方案列表
- (void) GetArticleList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [GatewayManager callTransferGetArticleList:parameter Source:self];
}
- (void) callBacTransferGetArticleList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSArray *Data = [parameter objectForKey:@"Data"];
        [arrayArticle removeAllObjects];
        [arrayArticle addObjectsFromArray:Data];
        
        //如果已有資料，要確認選到方案的是哪一個index
        //新車主
        NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
        if (![NewOwner isKindOfClass:[NSNull class]]) {
            //電馳服務租賃項目：新車主
            ArticleNo = [NewOwner objectForKey:@"ArticleNo"];
            
            for (NSInteger i = 0; i < [arrayArticle count]; i++) {
                NSDictionary *_dic = [arrayArticle objectAtIndex:i];
                NSString *No = [_dic objectForKey:@"No"];
                if ([ArticleNo isEqualToString:No]) {
                    selectedIndex = i;
                    break;
                }
            }
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [604] 新增租約轉讓資料(租約轉讓資料選擇完後呼叫)
- (void) callAdd {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //車號
    NSString *Plate = [_dicTranfer objectForKey:@"Plate"];
    [parameter setObject:Plate forKey:@"Plate"];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        //原車主身份證字號
        NSString *OwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        [parameter setObject:OwnerProfileCode forKey:@"OwnerProfileCode"];
        
        //原車主id(略過DMS檢查時，帶回該車牌號碼目前車主id)
        NSString *OwnerProfileId = [OriginalOwner objectForKey:@"OwnerProfileId"];
        if ([OwnerProfileId length] > 0) {
            [parameter setObject:OwnerProfileId forKey:@"OwnerProfileId"];
        }
        else {
            [parameter setObject:@"" forKey:@"OwnerProfileId"];
        }
    }
    else {
        [parameter setObject:@"" forKey:@"OwnerProfileCode"];
    }
    
    //原車主親辦(0: 親辦, 1: 授權委託人)
    [parameter setObject:[NSString stringWithFormat:@"%ld",OriginalOwnerIsAgent] forKey:@"OriginalOwnerIsAgent"];
    
    //新車主身份證字號 2017.02.23取消
    /*NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
    if (![NewOwner isKindOfClass:[NSNull class]]) {
        NSString *NewOwnerProfileCode = [OriginalOwner objectForKey:@"ProfileCode"];
        [parameter setObject:NewOwnerProfileCode forKey:@"NewOwnerProfileCode"];
    }
    else {
        [parameter setObject:@"" forKey:@"NewOwnerProfileCode"];
    }*/
    
    //新車主親辦(0: 親辦, 1: 授權委託人)
    [parameter setObject:[NSString stringWithFormat:@"%ld",NewOwnerIsAgent] forKey:@"NewOwnerIsAgent"];
    
    //電馳方案(租賃項目) 代碼
    [parameter setObject:ArticleNo forKey:@"ArticleNo"];
    
    //案件備註
    Memo = _TextComment.text;
    [parameter setObject:Memo forKey:@"Memo"];
    
    //草稿:0, 處理中:1, 已結案:2
    NSString *State = [_dicTranfer objectForKey:@"State"];
    if ([State isEqualToString:@"草稿"]) {
        State = @"0";
    }
    else if ([State isEqualToString:@"處理中"]) {
        State = @"1";
    }
    else if ([State isEqualToString:@"已結案"]) {
        State = @"2";
    }
    [parameter setObject:State forKey:@"State"];
    
    [GatewayManager callTransferAdd:parameter Source:self];
}
- (void) callBacTransferAdd:(NSDictionary *)parameter {
    //[ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        if (_didFromTransferList) { //從6.1案件資料查詢過來
            [ObjectManager hideHUDForView:self.view];
            
            //6.1.8 確認文件清單
            [self GotoConfirmDocumentList];
        }
        else {
            //[605] 取得租約轉讓需簽名同意書及順序
            //之後到6.1.6 電子文件簽核
            [self callGetTransferContractList];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [605] 取得租約轉讓需簽名同意書及順序
-(void)callGetTransferContractList {
    //[ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
    }
    
    [parameter setObject:[_dicTranfer objectForKey:@"Plate"] forKey:@"Plate"];
    [GatewayManager callTransferGetContractList:parameter Source:self];
}
-(void)callBacTransferGetContractList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSArray *data = [parameter objectForKey:@"Data"];
        NSMutableArray *_contractArray = [NSMutableArray arrayWithArray:data];
        
        //改成依照ContractArray來判斷目前該簽的文件
        if ([_contractArray count] > 0) {
            int _nowSeq = 0;
            NSInteger nowAttachType = [[[_contractArray objectAtIndex:_nowSeq] objectForKey:@"AttachType"] integerValue];
            
            //6.1.6 電子文件簽核
            ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
            destVC.enumCONSENT = CONSENT_CONTRACT_NEED_SIGNATURE;
            //destVC.OrderNo = _OrderNo;
            //destVC.CustomerName = _CustomerName;
            //destVC.dicSubsidy = _dicSubsidy;
            destVC.nowSeq = _nowSeq; //1
            destVC.contractArray = _contractArray;
            destVC.newAttachType = nowAttachType;
            destVC.dicTranfer = _dicTranfer;
            destVC.stringCarNumber = _stringCarNumber;
            destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
    else{
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - 辦理業務選擇
-(IBAction)pressJobOriginalSelf:(id)sender { //原車主辦理業務：親辦
    OriginalOwnerIsAgent = 0;
    
    [_ViewOwnerOriginal SetButtonOnWithSender:_ButtonJobOriginalSelf];
    [_ViewOwnerOriginal SetButtonOffWithSender:_ButtonJobOriginalOther];
}
-(IBAction)pressJobOriginalOther:(id)sender { //原車主辦理業務：委託
    OriginalOwnerIsAgent = 1;
    
    [_ViewOwnerOriginal SetButtonOnWithSender:_ButtonJobOriginalOther];
    [_ViewOwnerOriginal SetButtonOffWithSender:_ButtonJobOriginalSelf];
}
-(IBAction)pressJobNewSelf:(id)sender { //新車主辦理業務：親辦
    NewOwnerIsAgent = 0;
    
    [_ViewOwnerNew SetButtonOnWithSender:_ButtonJobNewSelf];
    [_ViewOwnerNew SetButtonOffWithSender:_ButtonJobNewOther];
}
-(IBAction)pressJobNewOther:(id)sender { //新車主辦理業務：委託
    NewOwnerIsAgent = 1;
    
    [_ViewOwnerNew SetButtonOnWithSender:_ButtonJobNewOther];
    [_ViewOwnerNew SetButtonOffWithSender:_ButtonJobNewSelf];
}
#pragma mark - Button Action
-(IBAction)pressPreviousStep:(id)sender { //上一步
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)pressNextStep:(id)sender { //下一步
    //除案件備註外均為必填
    if ([ArticleNo length] == 0) {
        [self showAlertControllerWithMessage:@"請確認除案件備註外均已填寫完畢"];
        
        return;
    }
    
    //呼叫新增，更新或建立新的過戶案件
    [self callAdd];
}
#pragma mark - UIAlertController Handle
-(IBAction) ShowArticleSelect { //電馳方案選擇
    NSString *message = [NSString stringWithFormat:@"請選擇"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayArticle count]; i++) {
        NSDictionary *_dic = [arrayArticle objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"Name"];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonRentNew;
    popPresenter.sourceRect = _ButtonRentNew.bounds;
    
    UIViewController *sourceVC = self;
    [sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    NSDictionary *_dic = [arrayArticle objectAtIndex:buttonIndex];
    
    NSString *title = [_dic objectForKey:@"Name"];
    
    selectedIndex = buttonIndex;
    _LabelRentNew.text = title;
    _LabelRentNew.hidden = NO;
    [_ButtonRentNew setTitle:@"" forState:UIControlStateNormal];
    
    ArticleNo = [[arrayArticle objectAtIndex:selectedIndex] objectForKey:@"No"];
}
/*#pragma mark 草稿/申請
-(IBAction) pressDraftBtn:(id)sender { //草稿儲存
    if ([arrayModifiedAttachedID count] == 0) { //防呆
        return;
    }
    
    didApply = NO;
    
    //申請下一階段(0:無動作,僅更新附件、1: 申請領牌、2:申請補助、3:地方政府申 請、4:TES政府申請、5:地方及TES政府申 請)
    [self UpdateOrderAttachList:@"0"];
}*/
#pragma mark - UITextViewDelegate
-(void) textViewDidBeginEditing:(UITextView *)textView {
    [_ScrollView setContentOffset:CGPointMake(0, 400) animated:YES];
}
-(void) textViewDidEndEditing:(UITextView *)textView {
    [_ScrollView setContentOffset:CGPointZero animated:YES];
}
#pragma mark - 換頁
-(void) GotoConfirmDocumentList { //6.1.8 確認文件清單
    TransferConfirmDocumentListVC *destVC = [[TransferConfirmDocumentListVC alloc] initWithNibName:@"TransferConfirmDocumentListVC" bundle:nil];
    destVC.dicTranfer = _dicTranfer;
    destVC.stringCarNumber = _stringCarNumber;
    //destVC.stringCarNumberOld = _stringCarNumberOld;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
