

#import <UIKit/UIKit.h>
#import "SubsidyRootView.h"

@interface TransferRentVC : TemplateVC
@property (nonatomic, strong) NSDictionary *dicTranfer; //過戶移轉的資料
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
//@property (strong, nonatomic) NSString *stringCarNumberOld; //原車牌號碼
@property BOOL didFromTransferList; //是不是從6.1案件資料查詢過來
@end
