
#import <UIKit/UIKit.h>

@interface TransferRegisterVC : TemplateVC
@property (nonatomic,strong) NSString *stringEmail;
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
@property (strong, nonatomic) NSString *IdentityType; //身份
@end
