//

#import "TransferRegisterVC.h"
#import "TransferUploadIDVC.h"

@interface TransferRegisterVC () {
}
@property (nonatomic,weak) IBOutlet UIWebView *WebView;
@end

@implementation TransferRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //[self setupNavigationItemTitle:@"註冊gogoro帳號(步驟2/4：確認新車主)" Image:nil];
    //[self LeftButtonBack]; //左側按鈕變成返回
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_WebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://my.gogoro.com/tw/contract"]]];
}
#pragma mark - Call API
#pragma mark [603] 車主email 是否已註冊
- (void) CheckEmail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:_stringEmail forKey:@"Email"];
    [parameter setObject:_IdentityType forKey:@"IdentityType"];
    [GatewayManager callTransferCheckEmail:parameter Source:self];
}
- (void) callBacTransferCheckEmail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        BOOL didRegister = [[parameter objectForKey:@"Data"] boolValue];
        
        //是否已註冊(0:否, 1:是)
        //didRegister = NO;
        
        NSString *message = nil;
        if (didRegister) { //已註冊
            message = @"新車主email 已註冊！將進行個人資料收集！";
        }
        else { //未註冊
            message = @"請於【新車主email】填入官網註冊使用者帳號！";
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (didRegister) { //已註冊
                //6.1.3-1 上傳新車主證件
                [self GotoTransferUploadIDVC];
            }
            else { //未註冊
                //回前頁填Email
                [self pressBackBtn:nil];
            }
        }];
        [alertController addAction:submitAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - Button Action
-(IBAction)pressBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)pressFinishBtn:(id)sender {
    [self CheckEmail];
}
#pragma mark - 換頁
-(void) GotoTransferUploadIDVC { //6.1.3-1 上傳新車主證件
    TransferUploadIDVC *destVC = [[TransferUploadIDVC alloc] initWithNibName:@"TransferUploadIDVC" bundle:nil];
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
