//

#import "TransferConfirmDMSVC.h"
#import "TransferRentVC.h"

@interface TransferConfirmDMSVC () {
    NSDictionary *dicTransfer; //過戶移轉資料
}
@property (nonatomic,weak) IBOutlet UIView *ViewCarData; //車籍資料
@property (nonatomic,weak) IBOutlet UIView *ViewOwner; //原車主與新車主
@property (nonatomic,weak) IBOutlet UIButton *ButtonCarData;
@property (nonatomic,weak) IBOutlet UIButton *ButtonOwner;
@property (nonatomic,weak) IBOutlet UIView *ViewCommandOwner;
@property (nonatomic,weak) IBOutlet UIView *ViewData; //車籍資料裡面的Subview
@property (nonatomic,weak) IBOutlet UIView *ViewOwnerOriginal; //原車主
@property (nonatomic,weak) IBOutlet UIView *ViewOwnerNew; //新車主
@property (nonatomic,weak) IBOutlet UILabel *LabelCarNumber; //車牌
@property (nonatomic,weak) IBOutlet UILabel *LabelLicenseDate; //領牌日期
@property (nonatomic,weak) IBOutlet UILabel *LabelVin; //VIN
@property (nonatomic,weak) IBOutlet UILabel *LabelSpec; //規格
@property (nonatomic,weak) IBOutlet UILabel *LabelColor; //車色
@property (nonatomic,weak) IBOutlet UILabel *LabelWarrantyNew; //新車保固
@property (nonatomic,weak) IBOutlet UILabel *LabelWarrantyValueAdd; //加值保固
@property (nonatomic,weak) IBOutlet UILabel *LabelOriginalName; //原車主姓名
@property (nonatomic,weak) IBOutlet UILabel *LabelOriginalID; //原車主身分證/統編
@property (nonatomic,weak) IBOutlet UILabel *LabelOriginalEmail; //原車主Email
@property (nonatomic,weak) IBOutlet UILabel *LabelOriginalAddress; //原車主聯絡地址
@property (nonatomic,weak) IBOutlet UILabel *LabelOriginalBirthday; //原車主生日
@property (nonatomic,weak) IBOutlet UILabel *LabelOriginalAddressInovice; //原車主發票地址
@property (nonatomic,weak) IBOutlet UILabel *LabelNewName; //新車主姓名
@property (nonatomic,weak) IBOutlet UILabel *LabelNewID; //新車主身分證/統編
@property (nonatomic,weak) IBOutlet UILabel *LabelNewEmail; //新車主Email
@property (nonatomic,weak) IBOutlet UILabel *LabelNewAddress; //新車主聯絡地址
@property (nonatomic,weak) IBOutlet UILabel *LabelNewAddressInovice; //新車主發票地址
@property (nonatomic,weak) IBOutlet UILabel *LabelNewBirthday; //新車主生日
@end

@implementation TransferConfirmDMSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"DMS過戶說明文件(步驟3/4：確認DMS資料)" Image:nil];
    [self LeftButtonBack];
    
    //車籍資料UI處理
    [_ViewCarData addSubview:_ViewData];
    _ViewData.center = CGPointMake(_ViewCarData.frame.size.width/2, _ViewData.frame.size.height/2);
    
    //車主資料UI處理
    [_ViewOwner addSubview:_ViewOwnerOriginal];
    [_ViewOwner addSubview:_ViewOwnerNew];
    _ViewOwnerOriginal.center = CGPointMake(_ViewOwner.frame.size.width/2, _ViewOwnerOriginal.frame.size.height/2);
    _ViewOwnerNew.center = CGPointMake(_ViewOwner.frame.size.width/2,  _ViewOwnerNew.frame.origin.y + _ViewOwnerOriginal.frame.size.height + 10 + _ViewOwnerNew.frame.size.height/2);
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self TransferGet];
}
#pragma mark - Call API
#pragma mark [602] 取得過戶移轉案件
- (void) TransferGet {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    [GatewayManager callTransferGet:parameter Source:self];
}
- (void) callBacTransferGet:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        dicTransfer = [[NSDictionary alloc] initWithDictionary:Data];
        
        //UI清空
        //車輛資料
        _LabelCarNumber.text = @""; //車牌
        _LabelLicenseDate.text = @""; //領牌日期
        _LabelVin.text = @""; //VIN
        _LabelSpec.text = @""; //規格
        _LabelColor.text = @""; //車色
        _LabelWarrantyNew.text = @""; //新車保固
        _LabelWarrantyValueAdd.text = @""; //加值保固
        
        //原車主資料
        _LabelOriginalName.text = @""; //原車主姓名
        _LabelOriginalID.text = @""; //原車主身分證/統編
        _LabelOriginalEmail.text = @""; //原車主Email
        _LabelOriginalAddress.text = @""; //原車主聯絡地址
        _LabelOriginalAddressInovice.text = @""; //原車主發票地址
        _LabelOriginalBirthday.text = @""; //原車主生日
        
        //新車主資料
        _LabelNewName.text = @""; //新車主姓名
        _LabelNewID.text = @""; //新車主身分證/統編
        _LabelNewEmail.text = @""; //新車主Email
        _LabelNewAddress.text = @""; //新車主聯絡地址
        _LabelNewAddressInovice.text = @""; //新車主發票地址
        _LabelNewBirthday.text = @""; //新車主生日
    
        
        //更新UI
        //車輛資料
        _LabelCarNumber.text = [dicTransfer objectForKey:@"Plate"]; //車牌
        _LabelLicenseDate.text = [dicTransfer objectForKey:@"RegisteredDate"]; //領牌日期
        _LabelVin.text = [dicTransfer objectForKey:@"VinNo"]; //VIN
        _LabelSpec.text = [dicTransfer objectForKey:@"ModelName"]; //規格
        _LabelColor.text = [dicTransfer objectForKey:@"ScooterColor"]; //車色
        _LabelWarrantyNew.text = [dicTransfer objectForKey:@"WarrantyDate"]; //新車保固
        _LabelWarrantyValueAdd.text = [dicTransfer objectForKey:@"ExtendDate"]; //加值保固
        
        //原車主資料
        NSDictionary *OriginalOwner = [dicTransfer objectForKey:@"OriginalOwner"];
        if (![OriginalOwner isKindOfClass:[NSNull class]]) {
            _LabelOriginalName.text = [OriginalOwner objectForKey:@"Name"]; //原車主姓名
            _LabelOriginalID.text = [OriginalOwner objectForKey:@"ProfileCode"]; //原車主身分證/統編
            _LabelOriginalEmail.text = [OriginalOwner objectForKey:@"Email"]; //原車主Email
            _LabelOriginalAddress.text = [OriginalOwner objectForKey:@"ContactAddress"]; //原車主聯絡地址
            _LabelOriginalAddressInovice.text = [OriginalOwner objectForKey:@"InvoiceAddress"]; //原車主發票地址
            _LabelOriginalBirthday.text = [OriginalOwner objectForKey:@"Birthday"]; //原車主生日
        }
        
        //新車主資料
        NSDictionary *NewOwner = [dicTransfer
                                  objectForKey:@"NewOwner"];
        if (![NewOwner isKindOfClass:[NSNull class]]) {
            _LabelNewName.text = [NewOwner objectForKey:@"Name"]; //新車主姓名
            _LabelNewID.text = [NewOwner objectForKey:@"ProfileCode"]; //新車主身分證/統編
            _LabelNewEmail.text = [NewOwner objectForKey:@"Email"]; //新車主Email
            _LabelNewAddress.text = [NewOwner objectForKey:@"ContactAddress"]; //新車主聯絡地址
            _LabelNewAddressInovice.text = [NewOwner objectForKey:@"InvoiceAddress"]; //新車主發票地址
            _LabelNewBirthday.text = [NewOwner objectForKey:@"Birthday"]; //新車主生日
        }
        
        //預設顯示車籍資料
        [self pressCarDataBtn:nil];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - Call API
#pragma mark [603] 車主email 是否已註冊
- (void) CheckEmail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //新車主Email
    NSString *Email = @"";
    NSDictionary *NewOwner = [dicTransfer
                              objectForKey:@"NewOwner"];
    if (![NewOwner isKindOfClass:[NSNull class]]) {
        Email = [NewOwner objectForKey:@"Email"];
        
        //身份
        NSString *IdentityType = [NewOwner objectForKey:@"IdentityType"] ;
        if ([IdentityType isEqualToString:@"個人"]) { //個人
            [parameter setObject:@"0" forKey:@"IdentityType"];
        }
        else { //法人
            [parameter setObject:@"1" forKey:@"IdentityType"];
        }
    }

    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:Email forKey:@"Email"];
    [GatewayManager callTransferCheckEmail:parameter Source:self];
}
- (void) callBacTransferCheckEmail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        BOOL didRegister = [[parameter objectForKey:@"Data"] boolValue];
        //是否已註冊(0:否, 1:是)
        
        //didRegister = NO;
        
        if (didRegister) { //已註冊
            //6.1.5 輸入租約轉讓資料
            [self GotoTransferRentVC];
        }
        else { //未註冊
            NSString *message = nil;
            message = @"DMS：@DMS新車主email\nGo Support：@Go Support新車主email\n不相同！請重新確認DMS資訊";

            //新車主Email
            NSString *NewEmail = @"";
            NSDictionary *NewOwner = [dicTransfer
                                      objectForKey:@"NewOwner"];
            if (![NewOwner isKindOfClass:[NSNull class]]) {
                NewEmail = [NewOwner objectForKey:@"Email"];
            }
            
            
            [self showAlertControllerWithMessage:message];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - Button Action
-(IBAction) pressCarDataBtn:(id)sender { //車籍資料
    [_ButtonCarData setBackgroundImage:[UIImage imageNamed:@"btn_carinfo_on.png"] forState:UIControlStateNormal];
    [_ButtonOwner setBackgroundImage:[UIImage imageNamed:@"btn_oldnewuser_off.png"] forState:UIControlStateNormal];
    _ViewCarData.hidden = NO;
    _ViewOwner.hidden = YES;
    
    //調整命令列的位置
    _ViewCommandOwner.hidden = NO;
    _ViewCommandOwner.center = CGPointMake(self.view.frame.size.width/2, _ViewCarData.frame.origin.y + _ViewData.frame.size.height + 50);
    [self.view bringSubviewToFront:_ViewCommandOwner];
}
-(IBAction) pressOwnerBtn:(id)sender { //原車主與新車主
    [_ButtonCarData setBackgroundImage:[UIImage imageNamed:@"btn_carinfo_off.png"] forState:UIControlStateNormal];
    [_ButtonOwner setBackgroundImage:[UIImage imageNamed:@"btn_oldnewuser_on.png"] forState:UIControlStateNormal];
    _ViewCarData.hidden = YES;
    _ViewOwner.hidden = NO;
    
    //調整命令列的位置
    _ViewCommandOwner.hidden = NO;
    _ViewCommandOwner.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - _ViewCommandOwner.frame.size.height/2);
    [self.view bringSubviewToFront:_ViewCommandOwner];
}
-(IBAction)pressPreviousStep:(id)sender { //上一步
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)pressRefreshBtn { //重取過戶資料
    [self TransferGet];
}
-(IBAction)pressNextStep:(id)sender { //下一步
    [self CheckEmail]; //確認新車主的Email有沒有註冊
}
#pragma mark - 換頁
-(void) GotoTransferRentVC  { //6.1.5 輸入租約轉讓資料
    TransferRentVC *destVC = [[TransferRentVC alloc] initWithNibName:@"TransferRentVC" bundle:nil];
    destVC.dicTranfer = dicTransfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
