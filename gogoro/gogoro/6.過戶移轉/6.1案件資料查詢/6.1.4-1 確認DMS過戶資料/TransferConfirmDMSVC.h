
#import <UIKit/UIKit.h>

@interface TransferConfirmDMSVC : TemplateVC
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
//@property (strong, nonatomic) NSString *stringCarNumberOld; //原車牌號碼
@end
