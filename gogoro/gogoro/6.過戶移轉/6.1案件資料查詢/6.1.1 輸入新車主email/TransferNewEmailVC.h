
#import <UIKit/UIKit.h>

@interface TransferNewEmailVC : TemplateVC
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
@property (strong, nonatomic) NSString *stringNewOwnerEmail; //新車主Email
@end
