//

#import "TransferNewEmailVC.h"
#import "TransferRegisterVC.h"
#import "TransferUploadIDVC.h"

@interface TransferNewEmailVC () {
    BOOL didRegister;
    NSMutableArray *arrayIdentity;
    NSInteger selectedIndex_Identity;
}
@property (nonatomic,weak) IBOutlet UITextField *TextNewOwnerEmail;
@property (nonatomic,weak) IBOutlet UIButton *ButtonIdentity;
@end

@implementation TransferNewEmailVC
@synthesize stringNewOwnerEmail;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNavigationItemTitle:@"輸入新車主Email(步驟2/4：確認新車主)" Image:nil];
    [self LeftButtonBack]; //左側按鈕變成返回
    
    arrayIdentity = [[NSMutableArray alloc] initWithObjects:@"個人",@"法人", nil];
    selectedIndex_Identity = 0;
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    _TextNewOwnerEmail.text = @"";
    
#if TARGET_IPHONE_SIMULATOR
    _TextNewOwnerEmail.text = @"howard.gdcrm@gmail.com";
#endif
    
    if ([stringNewOwnerEmail length] > 0) {
        _TextNewOwnerEmail.text = stringNewOwnerEmail;
    }
}
#pragma mark - Call API
#pragma mark [603] 車主email 是否已註冊
- (void) CheckEmail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:_TextNewOwnerEmail.text forKey:@"Email"];
    
    //身份
    NSString *title = [arrayIdentity objectAtIndex:selectedIndex_Identity];
    if ([title isEqualToString:@"個人"]) { //個人
        [parameter setObject:@"0" forKey:@"IdentityType"];
    }
    else { //法人
        [parameter setObject:@"1" forKey:@"IdentityType"];
    }
    
    [GatewayManager callTransferCheckEmail:parameter Source:self];
}
- (void) callBacTransferCheckEmail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        didRegister = [[parameter objectForKey:@"Data"] boolValue];
        //是否已註冊(0:否, 1:是)
        
        //didRegister = YES;
        
        NSString *message = nil;
        if (didRegister) { //已註冊
            message = @"新車主email 已註冊！將進行個人資料收集！";
        }
        else { //未註冊
            message = @"新車主email 未註冊！將導往官網進行註冊！";
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (didRegister) { //已註冊
                //6.1.3-1 上傳新車主證件
                [self GotoTransferUploadIDVC];
            }
            else { //未註冊
                //6.1.2 註冊gogoro帳號
                [self GotoRegisterVC];
            }
        }];
        [alertController addAction:submitAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (Syscode == kStatuscode_Sucess_But_No_Data) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Sysmsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //未註冊
            //6.1.2 註冊gogoro帳號
            [self GotoRegisterVC];
            
        }];
        [alertController addAction:submitAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - Button Action
-(IBAction)pressSelectIdentityBtn:(id)sender { //選身份
    [self ShowIdentitySelect];
}
-(IBAction)pressSendBtn:(id)sender { //送出
    NSString *newOwnerEmail = _TextNewOwnerEmail.text;
    
    //確認有填寫
    if ([newOwnerEmail length] == 0) {
        [self showAlertControllerWithMessage:@"請確認已填寫新車主email！"];
    }
    else {
        [_TextNewOwnerEmail resignFirstResponder];
        
        [self CheckEmail];
    }
}
-(IBAction)pressCopyBtn:(id)sender { //複製
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:_TextNewOwnerEmail.text];
    
    [self showAlertControllerWithMessage:@"新車主email複製成功！"];
}
#pragma mark - 選擇門市與人員
-(void) ShowIdentitySelect { //選擇門市
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"請選擇身份" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [arrayIdentity count]; i++) {
        NSString *title = [arrayIdentity objectAtIndex:i];
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    /*[alertController addAction:[UIAlertAction actionWithTitle:@"取消"
     style:UIAlertActionStyleDestructive
     handler:^(UIAlertAction *action) {
     }]];*/
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonIdentity;
    popPresenter.sourceRect = _ButtonIdentity.bounds;
    
    [self presentViewController:alertController animated:YES completion:NULL];
}
#pragma mark - UIAlertController Handle
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex {
    selectedIndex_Identity = buttonIndex;
    
    NSString *title = [arrayIdentity objectAtIndex:selectedIndex_Identity];
    [_ButtonIdentity setTitle:title forState:UIControlStateNormal];
}
#pragma mark - 換頁
-(void) GotoRegisterVC  { //6.1.2 註冊gogoro帳號
    TransferRegisterVC *destVC = [[TransferRegisterVC alloc] initWithNibName:@"TransferRegisterVC" bundle:nil];
    destVC.stringEmail = _TextNewOwnerEmail.text;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    
    //身份
    NSString *title = [arrayIdentity objectAtIndex:selectedIndex_Identity];
    if ([title isEqualToString:@"個人"]) { //個人
        destVC.IdentityType = @"0";
    }
    else { //法人
        destVC.IdentityType = @"1";
    }
    
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTransferUploadIDVC { //6.1.3-1 上傳新車主證件
    TransferUploadIDVC *destVC = [[TransferUploadIDVC alloc] initWithNibName:@"TransferUploadIDVC" bundle:nil];
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
