
#import <UIKit/UIKit.h>
#import "TransferUploadIDVCView.h"

@interface TransferUploadIDVC : TemplateVC <TransferUploadIDVCViewDelegate>
@property (strong, nonatomic) NSString *stringOriginalOwnerID; //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber; //車牌號碼
@end
