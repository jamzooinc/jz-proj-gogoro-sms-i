
#import <UIKit/UIKit.h>

@protocol TransferUploadIDVCViewDelegate <NSObject>
@optional
-(void) TransferUploadIDVCViewCheckAtIndex:(NSInteger)index; //勾選
-(void) TransferUploadIDVCViewUnCheckAtIndex:(NSInteger)index; //取消勾選
-(void) TransferUploadIDVCViewUploadAtIndex:(NSInteger)index; //上傳
-(void) TransferUploadIDVCViewReTemplateAtIndex:(NSInteger)index; //重新套版
-(void) TransferUploadIDVCViewFileUrldAtIndex:(NSInteger)index; //fileUrl
-(void) TransferUploadIDVCViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl; //預覽列印
@end

@interface TransferUploadIDVCView : UIView {
    __weak id <TransferUploadIDVCViewDelegate> delegate;
    BOOL IsChecked; //文件是否已勾選
    NSMutableArray *arrayPreviewPrint; //預覽列印的文件
}
@property (weak) id delegate;
@property BOOL didFromECDocumentListVC; //從5.1.1電子文件清單（功能按鈕不需作用）
@property (nonatomic,weak) IBOutlet UIImageView *ImageCamera;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCamera;
@property (nonatomic,weak) IBOutlet UIButton *ButtonUpload;
@property (nonatomic,weak) IBOutlet UIButton *ButtonCheck;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPreviewPrint; //預覽列印
@property (nonatomic,weak) IBOutlet UIImageView *ImageCheckBox;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIButton *ButtonReSign; //重簽
-(void) UpdateUIWithData:(NSDictionary *)data;
@property BOOL didDocumentList; //YES:6.3電子文件清單 NO:6.1.8確認文件清單
@end
