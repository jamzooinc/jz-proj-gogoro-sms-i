//

#import "TransferUploadIDVC.h"
#import "UploadFileVC.h"
#import "PhotoViewerVC.h"
#import "ConsentVC.h"
#import "TransferConfirmDMSVC.h"
#import "TransferNewEmailVC.h"

@interface TransferUploadIDVC () {
    NSDictionary *dicTransfer;
    NSInteger indetityType; //身分別 0個人 1法人
    //BOOL didGetDetail; //是否取得過戶移轉案件
    
    NSMutableArray *arrayData;
    NSInteger selectedIndex;
    NSMutableArray *arrayModifiedAttachedID; //有異動的附件ID
    CGFloat totalHeight,gapBetweenView;
    NSString *ownerName,*buyerName;
}
@property (nonatomic,weak) IBOutlet UIScrollView *ScrollView;
@property (nonatomic,weak) IBOutlet UILabel *LabelStore;
@property (nonatomic,weak) IBOutlet UILabel *LabelPerson;
@property (nonatomic,weak) IBOutlet UILabel *LabelVin;
@property (nonatomic,weak) IBOutlet UILabel *LabelCarNumber;
@property (nonatomic,weak) IBOutlet UILabel *LabelEmail;
@property (nonatomic,weak) IBOutlet UILabel *LabelIdentity;
@property (nonatomic,weak) IBOutlet UILabel *LabelUploadtitle1;
@end

@implementation TransferUploadIDVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    arrayData = [[NSMutableArray alloc] init];
    arrayModifiedAttachedID = [[NSMutableArray alloc] init];
    
    [self setupNavigationItemTitle:@"上傳新車主證件(步驟2/4：確認新車主)" Image:nil];
    [self LeftButtonBack]; //左側按鈕變成返回
}
-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    /*if (!didGetDetail) {
        //[602] 取得過戶移轉案件
        [self TransferGet];
    }
    else {
        //[608] 取得執行文件清單
        [self GetAttachList];
    }*/
    
    //[608] 取得執行文件清單
    [self GetAttachList];
}
#pragma mark - Call API
#pragma mark [602] 取得過戶移轉案件
- (void) TransferGet {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    [GatewayManager callTransferGet:parameter Source:self];
}
- (void) callBacTransferGet:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        if (![Data isKindOfClass:[NSNull class]]) {
            dicTransfer = nil;
            dicTransfer = [[NSDictionary alloc] initWithDictionary:Data];
            
            //6.1.3-2 DMS過戶說明文件
            [self GotoProcessDocument];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [608] 取得執行文件清單
- (void) GetAttachList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    [parameter setObject:_stringCarNumber forKey:@"Plate"];
    
    //只取得新車主需上傳證件（0:no,1:yes）
    [parameter setObject:@"1" forKey:@"NewOwnerCredential"];
    [GatewayManager callTransferGetAttachList:parameter Source:self];
}
- (void) callBacTransferGetAttachList:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //更新UI
        dicTransfer = [[NSDictionary alloc] initWithDictionary:Data];
        
        //服務門市
        _LabelStore.text = [dicTransfer objectForKey:@"StoreName"];
        
        //服務人員
        _LabelPerson.text = [dicTransfer objectForKey:@"SalesAccountName"];
        
        //車牌號碼
        _LabelCarNumber.text = [dicTransfer objectForKey:@"Plate"];
        
        //VIN
        _LabelVin.text = [dicTransfer objectForKey:@"VinNo"];
        
        //新車主Email
        _LabelEmail.text = [dicTransfer objectForKey:@"NewOwnerEmail"];

        //新車主身份
        NSString *NewOwnerIdentityType = [NSString stringWithFormat:@"%@",[dicTransfer objectForKey:@"NewOwnerIdentityType"]];
        if ([NewOwnerIdentityType isEqualToString:@"0"]) {
            _LabelIdentity.text = @"個人";
        }
        else {
            _LabelIdentity.text = @"法人";
        }
        
        //執行文件
        NSArray *Attachs = [Data objectForKey:@"Attachs"];
        
        [arrayData removeAllObjects];
        [arrayData addObjectsFromArray:Attachs];
        
        for (NSDictionary *_dic in arrayData) {
            NSLog(@"Name:%@",[_dic objectForKey:@"AttachTypeName"]);
            NSLog(@"_dic:%@",_dic);
            
            //是否已勾
            BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
            if (IsChecked) { //已勾的文件，記起來等上傳API的時候使用
                NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
                [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
            }
        }
        
        //產生項目
        [self GenerateSubview];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - UI產生
-(void) GenerateSubview {
    gapBetweenView = 15;
    totalHeight = 20;
    
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *dic = [arrayData objectAtIndex:i];
        
        NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"TransferUploadIDVCView" owner:self options:nil];
        TransferUploadIDVCView *view = [toplevels objectAtIndex:0];
        view.delegate = self;
        view.tag = i;
        view.frame = CGRectMake(0, totalHeight, _ScrollView.frame.size.width, 49);
        [_ScrollView addSubview:view];
        [view UpdateUIWithData:dic];
        
        totalHeight = view.frame.origin.y + view.frame.size.height + gapBetweenView;
    }
    
    [_ScrollView setContentSize:CGSizeMake(_ScrollView.frame.size.width, totalHeight)];
}
#pragma mark - 勾選/取消勾選
-(void) ConfirmInfoViewCheckAtIndex:(NSInteger)index { //勾選
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID addObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}
-(void) ConfirmInfoViewUnCheckAtIndex:(NSInteger)index { //取消勾選
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    [arrayModifiedAttachedID removeObject:[NSString stringWithFormat:@"%ld",AttachTypeId]];
    NSLog(@"有異動的文件:%@",arrayModifiedAttachedID);
}
#pragma mark - 上傳
-(void) TransferUploadIDVCViewUploadAtIndex:(NSInteger)index { //上傳
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    NSString *memo = [_dic objectForKey:@"Memo"];
    [self GotoUploadFileVCWithType:AttachTypeId AttachName:AttachTypeName Memo:memo];
}
#pragma mark - FileUrl
-(void) TransferUploadIDVCViewFileUrldAtIndex:(NSInteger)index { //fileUrl
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    
    if ([FileUrl length] == 0) {
        [self showAlertControllerWithMessage:@"請先上傳電子檔！"];
        return;
    }
    
    FileUrl = [NSString stringWithFormat:@"%@",FileUrl];
    
    //測試
    //FileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    NSLog(@"FileUrl:%@",FileUrl);
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([FileUrl rangeOfString:@".jpg"].length > 0 || [FileUrl rangeOfString:@".png"].length > 0 || [FileUrl rangeOfString:@".JPG"].length > 0 || [FileUrl rangeOfString:@".PNG"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:FileUrl Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:FileUrl Name:AttachTypeName FromTempUrl:NO];
    }
}
#pragma mark - 預覽列印
-(void) TransferUploadIDVCViewPreviewPrintAtIndex:(NSInteger)index Rect:(CGRect)rect UrlString:(NSString *)urlString FromTempUrl:(BOOL)fromTempUrl { //預覽列印
    selectedIndex = index;
    NSDictionary *_dic = [arrayData objectAtIndex:index];
    if ([urlString length] == 0) {
        return;
    }
    
    //測試
    //TemplateFileUrl = @"http://allsit.cloudapp.net/gogoro/UploadFiles/PDF/訂購合約1.pdf";
    //NSLog(@"TemplateFileUrl:%@",TemplateFileUrl);
    
    urlString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,(CFStringRef)urlString,NULL,NULL,kCFStringEncodingUTF8));
    
    NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
    
    //判斷是JPG還是PDF
    if ([urlString rangeOfString:@".jpg"].length > 0) { //jpg
        [self GotoPhotoViewerWithUrl:urlString Name:AttachTypeName];
    }
    else { //pdf
        [self GotoConsentWithUrl:urlString Name:AttachTypeName FromTempUrl:fromTempUrl];
    }
}
#pragma mark - Button Action
-(IBAction)pressSendBtn:(id)sender { //送出
    //NSString *newOwnerEmail = _TextNewOwnerEmail.text;
    
    //確認車牌與身分證皆有填寫
    /*if ([carNo length] == 0 || [ID length] == 0) {
        [self showAlertControllerWithMessage:@"請確認車牌號碼與原車主身分證皆有填寫！"];
    }
    else {
        [self TransferGetDetail];
    }*/
}
-(IBAction)pressPreviousStepBtn:(id)sender { //上一步
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)pressNextStepBtn:(id)sender { //下一步
    //6.1.3-2 DMS過戶說明文件
    //[self GotoProcessDocument];
    //return;
    
    //文件完整（都勾選），取得取得過戶移轉案件詳細資料
    //[self TransferGet];
    //return;
    
    //2016.10.19 全勾才能申請
    NSInteger totalCount = 0;
    NSInteger needCount = [arrayData count];
    NSInteger noCehckIndex = -1; //哪一筆沒有打勾
    for (NSInteger i = 0; i < [arrayData count]; i++) {
        NSDictionary *_dic = [arrayData objectAtIndex:i];
        BOOL IsChecked = [[_dic objectForKey:@"IsChecked"] boolValue];
        if (IsChecked) { //已勾
            totalCount++;
        }
        else { //未勾，判斷有沒有在手動勾選清單裡面
            BOOL didExist = NO;
            NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
            for (NSString *_attachID in arrayModifiedAttachedID) {
                NSInteger attachID = [_attachID integerValue];
                if (AttachTypeId == attachID) {
                    didExist = YES;
                    totalCount++;
                }
            }
            if (!didExist) {
                noCehckIndex = i;
                break;
            }
        }
    }
    
    if (totalCount != needCount) {
        if (noCehckIndex >= 0) { //防呆
            NSDictionary *_dic = [arrayData objectAtIndex:noCehckIndex];
            NSString *AttachTypeName = [_dic objectForKey:@"AttachTypeName"];
            NSString *message = [NSString stringWithFormat:@"請確認「%@」已勾選！",AttachTypeName];
            [self showAlertControllerWithMessage:message];
        }
        
        return;
    }
    
    //文件完整（都勾選），取得取得過戶移轉案件詳細資料
    //[self TransferGet];
    
    //6.1.3-2 DMS過戶說明文件
    [self GotoProcessDocument];
}
#pragma mark - 換頁
-(void) GotoUploadFileVCWithType:(NSInteger)attachType AttachName:(NSString *)attachName Memo:(NSString *)memo { //檔案上傳
    UploadFileVC *destVC = [[UploadFileVC alloc] initWithNibName:@"UploadFileVC" bundle:nil];
    destVC.dicTransfer = dicTransfer;
    destVC.type = attachType;
    destVC.attachName = attachName;
    destVC.didFromTransfer = YES;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    destVC.memo = memo;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoPhotoViewerWithUrl:(NSString *)urlString Name:(NSString *)name  { //瀏覽照片
    PhotoViewerVC *destVC = [[PhotoViewerVC alloc] initWithNibName:@"PhotoViewerVC" bundle:nil];
    destVC.urlString = urlString;
    destVC.stringTitle = name;
    destVC.showPrintButton = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConsentWithUrl:(NSString *)urlString Name:(NSString *)name FromTempUrl:(BOOL)fromTempUrl { //瀏覽PDF
    //2016.12.21 判斷選到的文件是已簽或空白
    NSDictionary *_dic = [arrayData objectAtIndex:selectedIndex];
    //API 309 有 Fileurl （有簽名）>> API 105 content type 帶 0
    //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
    //PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
    //NSString *TemplateFileUrl = [_dic objectForKey:@"TemplateFileUrl"];
    //BOOL hasTemplateFileUrl = [TemplateFileUrl length] > 0;
    //NSString *FileUrl = [_dic objectForKey:@"FileUrl"];
    //BOOL hasFileUrl = [FileUrl length] > 0;
    
    NSInteger AttachTypeId = [[_dic objectForKey:@"AttachTypeId"] integerValue];
    
    //組成車主與購買人資料
    //NSDictionary *dicCustomerDetail = [dicOrder objectForKey:@"CustomerDetail"];
    //NSString *CustomerName = [dicCustomerDetail objectForKey:@"CustomerName"];
    //NSDictionary *dicOrderDetail = [dicOrder objectForKey:@"OrderDetail"];
    //NSString *BuyName = [dicOrderDetail objectForKey:@"BuyName"];
    NSString *CustomerName = ownerName;
    NSString *BuyName = buyerName;
    
    NSMutableArray *arrayBuyerNames = [[NSMutableArray alloc] init];
    //車主
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:CustomerName,@"name",[NSNumber numberWithInteger:0],@"mailTarget" ,nil];
    [arrayBuyerNames addObject:dic1];
    
    //購買人（車主與購買人不同時才需要）
    if (![CustomerName isEqualToString:BuyName]) {
        NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:BuyName,@"name",[NSNumber numberWithInteger:1],@"mailTarget" ,nil];
        [arrayBuyerNames addObject:dic2];
    }
    
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = name;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = YES;
    destVC.hideEmailButton = YES;
    destVC.FromTempUrl = fromTempUrl;
    destVC.arrayBuyerNames = arrayBuyerNames;
    //destVC.OrderNo = _OrderNo;
    destVC.newAttachType = AttachTypeId;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoProcessDocument { //6.1.3-2 DMS過戶說明文件
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_TRANSFER_DOC;
    destVC.dicTranfer = dicTransfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoTransferConfirmDMSVC { //6.1.4確認DMS過戶資料
    TransferConfirmDMSVC *destVC = [[TransferConfirmDMSVC alloc] initWithNibName:@"TransferConfirmDMSVC" bundle:nil];
    destVC.stringCarNumber = _stringCarNumber;
    //destVC.stringCarNumberOld = _stringCarNumberOld;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(IBAction) GotoTransferNewEmailVC { //退回6.1.1 輸入新車主Email
    //到補助選擇
    NSArray *array = self.navigationController.viewControllers;
    TransferNewEmailVC *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[TransferNewEmailVC class]]) {
            destVC = (TransferNewEmailVC *)vc;
            
        }
    }
    NSLog(@"destVC:%@",destVC);
    if (destVC) { //退回6.1.5 輸入租約轉讓資料
        destVC.stringNewOwnerEmail = _LabelEmail.text;
        [self.navigationController popToViewController:destVC animated:NO];
    }
    else { //6.1.5 輸入租約轉讓資料
        TransferNewEmailVC *destVC = [[TransferNewEmailVC alloc] initWithNibName:@"TransferNewEmailVC" bundle:nil];
        destVC.stringCarNumber = _stringCarNumber;
        destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
        destVC.stringNewOwnerEmail = _LabelEmail.text;
        [self.navigationController pushViewController:destVC animated:YES];
    }
}
#pragma mark - 雜項
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
