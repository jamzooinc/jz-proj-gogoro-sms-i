
#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

-(UIImage *) fixOrientation;
-(UIImage *) rotate:(UIImage *)src Orientation:(UIImageOrientation) orientation;

@end
