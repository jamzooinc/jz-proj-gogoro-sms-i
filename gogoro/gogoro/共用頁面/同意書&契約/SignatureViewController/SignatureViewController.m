//
//  SignatureViewController.m
//  MeetGoal
//
//  Created by Charles Wang on 2015/12/17.
//  Copyright © 2015年 User. All rights reserved.
//

#import "SignatureViewController.h"
#define LOCALIZATION(text) text

@interface SignatureViewController ()
{
    CGPoint lastPoint;
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat brush;
    CGFloat opacity;
    BOOL mouseSwiped;
    BOOL didSentButtonShow;
    BOOL didSign; //是否已簽名
}
@end

@implementation SignatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    red = 0.0/255.0;
    green = 0.0/255.0;
    blue = 0.0/255.0;
    brush = 7.5; //2016.11.15加寬原為3.0
    opacity = 1.0;
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated {
    //self.mainImageView.backgroundColor = [UIColor whiteColor];
    if (_preSignatureImage) {
        self.mainImageView.image = _preSignatureImage;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    //[_saveButton setTitle:LOCALIZATION(@"Done") forState:UIControlStateNormal];
    //[_resetButton setTitle:LOCALIZATION(@"Reset") forState:UIControlStateNormal];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    mouseSwiped = NO;
    UITouch *touch = [touches anyObject];
    lastPoint = [touch locationInView:self.mainImageView];
    NSLog(@"Touch begin");
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    mouseSwiped = YES;
    UITouch *touch = [touches anyObject];
    CGPoint currentPoint = [touch locationInView:self.mainImageView];
    
    UIGraphicsBeginImageContext(_mainImageView.frame.size);
    
    /* mainImage上設定可畫範圍 */
    [self.mainImageView.image drawInRect:CGRectMake(0, 0, _mainImageView.frame.size.width, _mainImageView.frame.size.height)];
    
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
    /* Draw from lastPoint to currentPoint */
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    
    /* 畫圓角 */
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    
    /* 線寬 */
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush );
    
    /* 顏色 */
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, 1.0);
    CGContextSetBlendMode(UIGraphicsGetCurrentContext(),kCGBlendModeNormal);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
    [self.mainImageView setAlpha:1.0];
    UIGraphicsEndImageContext();
    
    lastPoint = currentPoint;
}

/* 單純點擊時所需 */
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    if (!mouseSwiped) {
        NSLog(@"沒滑動");
        /* 沒滑動 */
        UIGraphicsBeginImageContext(_mainImageView.frame.size);
        [self.mainImageView.image drawInRect:CGRectMake(0, 0, _mainImageView.frame.size.width, _mainImageView.frame.size.height)];
        CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
        CGContextSetLineWidth(UIGraphicsGetCurrentContext(), brush);
        CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue, opacity);
        CGContextMoveToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), lastPoint.x, lastPoint.y);
        CGContextStrokePath(UIGraphicsGetCurrentContext());
        CGContextFlush(UIGraphicsGetCurrentContext());
        self.mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    else {
        NSLog(@"有滑動");
        //當簽名檔內有資料時，打勾按鈕換色
        /*if (!didSentButtonShow) {
            didSentButtonShow = YES;
            [self.delegate didSignatureSigned:YES];
        }*/
        
        didSign = YES;
        
        //[self saveButtonPressed:nil];
    }
    
    UIGraphicsBeginImageContext(self.mainImageView.frame.size);
    [self.mainImageView.image drawInRect:CGRectMake(0, 0, _mainImageView.frame.size.width, _mainImageView.frame.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    self.mainImageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

#pragma mark - Button handle
- (IBAction)saveButtonPressed:(id)sender {
    if (!didSign && !_isLawSign) { //畫板上沒簽名，不作動（法定代理人例外，可以不簽）
        return;
    }
    
    [_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes.png"] forState:UIControlStateNormal];
    
    UIImage *image = self.mainImageView.image;
    
    if (_enumCONSENT == CONSENT_TEST_RIDE ||
        _enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT
        ) {
        if ([self.delegate respondsToSelector:@selector(saveSignatureImage:)]) {
            //顯示送出按鈕
            if (!didSentButtonShow) {
                didSentButtonShow = YES;
                //[_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes.png"] forState:UIControlStateNormal];
                [self.delegate didSignatureSigned:YES];
            }
            
            [self.delegate saveSignatureImage:image];
        }
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE ||
             _enumCONSENT == CONSENT_ORDER_CUSTOMER ||  //購買契約書
             _enumCONSENT == CONSENT_PERSONAL_Data ||
             _enumCONSENT == CONSENT_CONTRACT_RESIGN ||
             _enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) //合約用_個資同意書
    {
        [self.delegate saveSignatureImage:image];
        [self.delegate didSignatureSigned:YES];
    }
}
- (IBAction)restButtonPressed:(id)sender {
    didSign = NO;
    self.mainImageView.image = nil;
    
    [_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes_grey.png"] forState:UIControlStateNormal];
    
    if (_enumCONSENT == CONSENT_TEST_RIDE ||
        _enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT
        ) {
        if (didSentButtonShow) {
            didSentButtonShow = NO;
            //[_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes_grey.png"] forState:UIControlStateNormal];
            [self.delegate didSignatureSigned:NO];
        }
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE ||
             _enumCONSENT == CONSENT_ORDER_CUSTOMER ||  //購買契約書
             _enumCONSENT == CONSENT_PERSONAL_Data ||
             _enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) //合約用_個資同意書
    {
        //[_saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes_grey.png"] forState:UIControlStateNormal];
        [self.delegate didSignatureSigned:NO];
    }
    else {
        [self.delegate didSignatureSigned:NO];
    }
}
@end
