//
//  SignatureViewController.h
//  MeetGoal
//
//  Created by Charles Wang on 2015/12/17.
//  Copyright © 2015年 User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DocumentManager.h"

@protocol SignatureVCdelegate<NSObject>
@optional
- (void)saveSignatureImage:(UIImage *)signatureImage;
- (void)didSignatureSigned:(BOOL)didSigned;
@end


@interface SignatureViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *ImageBG;
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) UIImage *preSignatureImage;
@property (weak, nonatomic) id<SignatureVCdelegate>delegate;
@property (nonatomic) enumCONSENT enumCONSENT;
@property BOOL isLawSign; //是不是法定代理人簽名
@end
