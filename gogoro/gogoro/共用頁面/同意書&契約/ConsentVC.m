//
//  ConsentVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//
#define tag_ScrollView_StartTag 100

#import "ConsentVC.h"
#import "SignatureViewController.h"
#import "GetOrderVC.h"
#import "SelectSubsidyVC.h"
#import "ConfirmInfoVC.h"
#import "TransferConfirmDMSVC.h"
#import "TransferConfirmDocumentListVC.h"
#import "CustomDocumentListVC.h"
#import "CustomerDocumentSignVC.h"
#import "CustomDocumentVC.h"

#import "PDF.h"
#import "PDFFormContainer.h"

@class PDFView;
@class PDFDocument;

#define SignatureHeight 528

static int END_SIGNING = -1; //簽名序列結束

@interface ConsentVC ()<SignatureVCdelegate>
{
    NSData *dataForPrint;
    NSInteger attachType;           //後台定義的文件類型 101個資同意書 102試乘個資同意書 36未套版訂購契約書
    NSString *stringApiVersion;
    UIImage *imageMySign;           //同意書本人簽名
    UIImage *imageLawSign;          //同意書法定代理人簽名 或 銷售契約代理人簽名
    UIImage *imageStoreOwnerSign;   //銷售契約店長簽名
    UIImage *imageSalesSign;        //銷售契約業務簽名
    UIImage *imageBuyerSign;        //銷售契約買方簽名
    
    BOOL didTapSwitchButton;        //是否點擊切換簽名身分（點擊時，不視為已存簽名）
    BOOL isMySign;                  //YES:正在簽立書同意人
    BOOL isLawSign;                 //YES:正在簽法定代理人
    BOOL isStoreOwnerSign;          //YES:正在簽銷售契約店長
    BOOL isSalesSign;               //YES:正在簽銷售契約業務
    BOOL isBuyerSign;               //YES:正在簽銷售契約買方
    BOOL didNextButtonShow;         //YES:下一步按鈕已顯示
    BOOL didSendButtonShow;         //YES:送出按鈕已顯示
    
    BOOL didMySigned;               //YES:已簽立書同意人
    BOOL didLawSigned;              //YES:已簽法定代理人
    BOOL didStoreOwnerSigned;       //YES:已簽銷售契約店長
    BOOL didSalesSigned;            //YES:已簽銷售契約業務
    BOOL didBuyerSigned;            //YES:已簽銷售契約買方
    
    NSDictionary *dicDeductContact; //[307] 取得補助內扣款切結書的回傳資料
    
    BOOL isCorporate;               //YES:法人
    
    int MasterSign;
    int SalesSign;
    int BuySign;
    int AgentSign;
    int CustomerSign;
    
    NSInteger intMailTarget;        //收件對象(0:車主, 1:購買人)
    
    //過戶移轉
    int OwnerSign;                  //原車主簽名(0:無，1:非必填，2: 必填)
    int NewOwnerSign;               //新車主簽名(0:無，1:非必填，2: 必填)
    int OwnerDelegateSign;          //原車主委託人簽名(0:無，1:非必填，2: 必填)
    int NewOwnerDelegateSign;       //新車主委託人簽名(0:無，1:非必填，2: 必填)
    BOOL isOwnerSign;               //YES:正在簽原車主
    BOOL isNewOwnerSign;            //YES:正在簽新車主
    BOOL isOwnerDelegateSign;       //YES:正在原車主委託人
    BOOL isNewOwnerDelegateSign;    //YES:正在新車主委託人
    BOOL didOwnerSigned;            //YES:已簽原車主
    BOOL didNewOwnerSigned;         //YES:已簽新車主
    BOOL didOwnerDelegateSigned;    //YES:已簽原車主委託人
    BOOL didNewOwnerDelegateSigned; //YES:已簽新車主委託人
}

@property (weak, nonatomic) IBOutlet UIWebView *consentWebView;
@property (weak, nonatomic) IBOutlet UIButton *signatureButton;                             //簽名
@property (strong, nonatomic) IBOutlet UISegmentedControl *signerSegment;
@property (strong, nonatomic) IBOutlet UIButton *sentButton;                                //送出
@property (strong, nonatomic) IBOutlet UIButton *MySignButton;                              //立書同意人
@property (strong, nonatomic) IBOutlet UIButton *LawSignButton;                             //法定代理人
@property (strong, nonatomic) IBOutlet UIButton *BuyerSignButton;                           //買方姓名
@property (strong, nonatomic) IBOutlet UIButton *StoreOwnerSignButton;                      //店長
@property (strong, nonatomic) IBOutlet UIButton *SalesSignButton;                           //銷售人員
@property (strong, nonatomic) IBOutlet UIButton *SkipButton;                                //Skip
@property (strong, nonatomic) IBOutlet UIButton *NextButton;                                //下一步
@property (strong, nonatomic) SignatureViewController *signVC;
@property (strong, nonatomic) IBOutlet UIView *ViewPreviousNext;
@property (nonatomic,weak) IBOutlet UIButton *ButtonPrint;
@property (nonatomic,weak) IBOutlet UIButton *ButtonEmail;
@property (nonatomic, strong) PDFDocument *document;
@property (strong, nonatomic) IBOutlet UIButton *SignatureSkipButton;                       //跳過
@property (strong, nonatomic) IBOutlet UIButton *TransferOriginalOwnerSignButton;           //過戶移轉原車主
@property (strong, nonatomic) IBOutlet UIButton *TransferOriginalEntrustSignButton;         //過戶移轉原車主委託
@property (strong, nonatomic) IBOutlet UIButton *TransferNewOwnerSignButton;                //過戶移轉新車主
@property (strong, nonatomic) IBOutlet UIButton *TransferNewEntrustSignButton;              //過戶移轉新車主委託
@property (nonatomic, strong) PDFView *pdfView;

@end


@implementation ConsentVC
@synthesize signVC,signatureButton;

#pragma mark - [Life cycle]

-(void)viewDidLoad
{
    [super viewDidLoad];
    //功能與文件順序說明
    //#1
    //#2
    //#3 訂購單-->(取得訂單跟填寫補助資料)-->過場文件-->訂購合約-->合約用_個資同意書-->補助/放棄補助切結書
    
    _consentWebView.scrollView.delegate= self;
    
    //判斷是不是法人(IdentityType 身份 個人0 法人1)
    isCorporate = NO;
    if (_dicSubsidy) {
        NSInteger IdentityType = [[_dicSubsidy objectForKey:@"IdentityType"] integerValue];
        if (IdentityType == 0) { //個人0
            
        }
        else if (IdentityType == 1) { //法人1
            isCorporate = YES;
        }
    }
    
    [self initUIWithStatus];
}
//訂購Gogoro第一份顯示的文件
-(void)initUIWithStatus{
    //不同文件，有不同的介面跟功能
    switch (_enumCONSENT) { //訂購Gogoro所需簽名文件
        case CONSENT_CONTRACT_NEED_SIGNATURE:
            attachType = _newAttachType;
            
            //此處folder要改
            if (_dicTranfer) { //過戶移轉
                _stringFolder = @"TempTransferContract";
                [self setupNavigationItemTitle:@"電子簽核(步驟4/4：執行文件收集)" Image:nil];
                [self LeftButtonBack]; //左側按鈕變成返回
            }
            else {
                _stringFolder = @"TempOrderCustomer";
                [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            }
            
            //要判斷stringfolder
            [self UpdateUI_Sign];
            [self UpdateUI_Send];
            [self UpdateUI_SkipSign];
            [self UpdateUI_PreviousAndNext];
            
            if (_dicTranfer) { //過戶移轉（哪些人需要簽名，前一頁已取得）
                [self ProcessTransferDocumentData];
            }
            else { //透過API取得哪些人需要簽名
                [self callGetContract];
            }
            break;
        case CONSENT_CONTRACT_RESIGN: //重簽
            attachType = _newAttachType;
            
            //此處folder要改
            if (_dicTranfer) { //過戶移轉
                _stringFolder = @"TempTransferContract";
            }
            else {
                _stringFolder = @"TempOrderCustomer";
            }
            
            [self UpdateUI_Sign];
            [self UpdateUI_Send];
            [self setupNavigationItemTitle:@"重簽" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self LeftButtonBack]; //左側按鈕變成返回
            
            if (_dicTranfer) { //過戶
                [self ProcessTransferDocumentData];
            }
            else {
                [self callGetContract];
            }
            break;
        case CONSENT_CONTRACT_REVIEW_SIGNED:
            attachType = _newAttachType;
            
            [self UpdateUI_PreviousAndNext];
            
            if (_dicTranfer) { //過戶移轉
                _stringFolder = @"TempTransferContract";
                [self setupNavigationItemTitle:@"電子簽核(步驟4/4：執行文件收集)" Image:nil];
                
                [self ProcessTransferDocumentData_Review_Signed];
            }
            else {
                _stringFolder = @"TempOrderCustomer";
                [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
                [self callGetContract];
            }
            
            break;
        case CONSENT_PERSONAL_Data: //個資同意書
            attachType = _newAttachType;
            _stringFolder = @"TempPersonalData"; //PersonalData
            [self setupNavigationItemTitle:_stringTitle Image:nil];
            [self LeftButtonBack]; //左側按鈕變成返回
            
            [self UpdateUI_Send];
            [self UpdateUI_Sign];
            [self UpdateUI_SignSwitch];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            
            break;
        case CONSENT_TEST_RIDE: //試乘同意書
            attachType = 102;
            _stringFolder = @"TempTestRide"; //TestRide
            [self setupNavigationItemTitle:@"預約試乘" Image:[UIImage imageNamed:@"icn_res.png"]];
            [self LeftButtonBack]; //左側按鈕變成返回
            
            [self UpdateUI_Send];
            [self UpdateUI_Sign];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            break;
        case CONSENT_ORDER_PERSONAL_Data: //訂購Gogoro第一份顯示的文件
            attachType = 36;
            _stringFolder = @"OrderPersonalData";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self LeftButtonBack]; //左側按鈕變成返回
            [self UpdateUI_SkipAndNext]; //右下角Skip button
            
            //跟後台取得檔案的版本跟網址
            [self callGetContractVersion];
            break;
        case CONSENT_TRANSITION_DOC: //151: 過場文件
            attachType = 151;
            _stringFolder = @"TransitionDocument";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得檔案的版本跟網址
            [self callGetContractVersion];
            break;
        case CONSENT_TRANSFER_DOC: //1101: 過戶說明文件
            attachType = 1101;
            _stringFolder = @"TransferDocument";
            [self setupNavigationItemTitle:@"DMS過戶說明文件(步驟3/4：確認DMS資料)" Image:nil];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得檔案的版本跟網址
            [self callGetContractVersion];
            break;
        case CONSENT_ORDER_CUSTOMER: //訂購契約
            attachType = 36;
            _stringFolder = @"TempOrderCustomer";
             [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self LeftButtonBack]; //左側按鈕變成返回
            
            [self UpdateUI_Send];
            [self UpdateUI_Sign];
            [self UpdateUI_SignSwitch4];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            break;
        case CONSENT_ORDER_CONTRACT_PERSONAL: //合約用_個資同意書
            attachType = 103;
            _stringFolder = @"TempOrderCustomerContract";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self UpdateUI_Send];
            [self UpdateUI_Sign];
            [self UpdateUI_SignSwitch];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            break;
        case CONSENT_ORDER_DEDUCT_CONTRACT: { //補助內扣款切結書
            //attachType = -1; //不需要
            NSInteger SubsidyType = [[_dicSubsidy objectForKey:@"SubsidyType"] integerValue];
            if (SubsidyType == 2) { //補助
                attachType = 28; //28: 補助說明切結書 29: 補助放棄切結書
            }
            else { //放棄補助
                attachType = 29; //28: 補助說明切結書 29: 補助放棄切結書
            }
            
            _stringFolder = @"TempOrderDeductContract";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self UpdateUI_Send];
            [self UpdateUI_Sign];
            
            //跟後台取得補助內扣款切結書
            //[self callGetPreDeductContract];
            //2016.05.19 改成直接開啟文件（補助內扣款切結書）
            [self LoadPersonDataFileStart];
            break;
        }
        case CONSENT_URL_ONLY: //只給Url，直接下載檔案打開，支援Air Print
            attachType = -1; //不需要
            _stringFolder = @"TempPdfFile";
            [self setupNavigationItemTitle:_stringTitle Image:nil];
            [self LeftButtonBack]; //左側按鈕變成返回
            
            if (_showFinishButton) { //1.5預覽已簽核文件
                [self setupNavigationItemTitle:@"文件預覽" Image:nil];
                [self LeftButtonBack]; //左側按鈕變成返回
                [self UpdateUI_SkipAndNext];
                
                //不需跳過按鈕
                [_SkipButton removeFromSuperview];
                
                //下一步按鈕圖改成完成
                //[_NextButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            }
            
            //下載檔案
            [self LoadPersonDataFileStart];
            break;
        case CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL: //已簽名的個資同意書
            attachType = 103;
            _stringFolder = @"TempOrderSignedCustomerContract";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            break;
        case CONSENT_SIGNED_ORDER_CUSTOMER: //顯示已簽名的訂購單
            attachType = 36;
            _stringFolder = @"TempOrderCustomerSigned";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            break;
        /*case CONSENT_TRANSFER_CONTRACT: //過戶轉讓需簽名同意書及順序
            _stringFolder = @"TempTransferContract";
            [self setupNavigationItemTitle:@"電子簽核(步驟4/4：執行文件收集)" Image:nil];
            [self LeftButtonBack]; //左側按鈕變成返回
            
            [self UpdateUI_Send];
            [self UpdateUI_Sign];
            
            //跟後台取得套版的檔案
            [self callGetTransferContractList];
            break;*/
        case CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT: { //顯示已簽名的(放棄)補助切結書
            NSInteger SubsidyType = [[_dicSubsidy objectForKey:@"SubsidyType"] integerValue];
            if (SubsidyType == 2) { //補助
                attachType = 28; //28: 補助說明切結書 29: 補助放棄切結書
            }
            else { //放棄補助
                attachType = 29; //28: 補助說明切結書 29: 補助放棄切結書
            }
            _stringFolder = @"TempOrderSignedDeductContract";
            [self setupNavigationItemTitle:@"訂購Gogoro" Image:[UIImage imageNamed:@"icn_order.png"]];
            [self UpdateUI_PreviousAndNext];
            
            //跟後台取得套版的檔案
            [self callGetContract];
            break;
        }
            
        
        default:
            break;
    }
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //刪除暫存檔
    [self DeleAllTempImageFile];
}
#pragma mark - 過戶移轉文件順序資料處理
-(void) ProcessTransferDocumentData {
    NSDictionary *Data = [_contractArray objectAtIndex:_nowSeq];
    BOOL Signed = [[Data objectForKey:@"Signed"] boolValue];
    
    OwnerSign = [[Data objectForKey:@"OwnerSign"] intValue];
    NewOwnerSign = [[Data objectForKey:@"NewOwnerSign"]intValue];
    OwnerDelegateSign = [[Data objectForKey:@"OwnerAgentSign"]intValue]; //OwnerDelegateSign
    NewOwnerDelegateSign = [[Data objectForKey:@"NewOwnerAgentSign"]intValue]; //NewOwnerDelegateSign
    //AgentSign = [[Data objectForKey:@"NewOwnerAgentSign"] intValue];
    AgentSign = 0;
    
    //UI update
    [self UpdateUI_SignSwitch4sdf];
    
    /*if (Signed) {
     self.consentUrlStr = [Data objectForKey:@"SignFileUrl"]; //取得此筆訂單已簽文件  SignFileUrl 是簽完之後，預覽用的
     NSLog(@"已簽 網址:%@",self.consentUrlStr);
     } else {
     self.consentUrlStr = [Data objectForKey:@"Url"]; //空白文件 Url是show給user看的
     NSLog(@"空白 網址:%@",self.consentUrlStr);
     }*/
    self.consentUrlStr = [Data objectForKey:@"Url"]; //取得文件
    NSLog(@"文件網址:%@",self.consentUrlStr);
    
    //下載
    [self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
}
-(void) ProcessTransferDocumentData_Review_Signed {
    NSDictionary *Data = [_contractArray objectAtIndex:_nowSeq];
    BOOL Signed = [[Data objectForKey:@"Signed"] boolValue];
    
    if (Signed) {
        self.consentUrlStr = [Data objectForKey:@"SignFileUrl"]; //取得此筆訂單已簽文件  SignFileUrl 是簽完之後，預覽用的
        NSLog(@"已簽 網址:%@",self.consentUrlStr);
    }
    else {
        self.consentUrlStr = [Data objectForKey:@"Url"]; //空白文件 Url是show給user看的
        NSLog(@"空白 網址:%@",self.consentUrlStr);
    }
    //self.consentUrlStr = [Data objectForKey:@"Url"]; //取得文件
    NSLog(@"文件網址:%@",self.consentUrlStr);
    
    //下載
    [self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
}
#pragma mark - UI元件建置
-(void) UpdateUI_Send { //設定送出按鈕（先隱藏在螢幕下方）
    self.sentButton.frame = CGRectMake(0, ScreenHeight, 768, 71);
    [self.view addSubview: self.sentButton];
}
-(void) UpdateUI_Sign { //簽名畫面按鈕
    isMySign = YES;
    self.signatureButton.frame = CGRectMake(ScreenWidth - 89, ScreenHeight - 63 - 8, 89, 63);
    [self.view addSubview: self.signatureButton];
    self.signatureButton.hidden = YES;
}
-(void) UpdateUI_SkipSign {
    isMySign = YES;
    self.SignatureSkipButton.frame = CGRectMake(ScreenWidth - 89, ScreenHeight - 63*2 - 8*2 , 89, 63);
    [self.view addSubview: self.SignatureSkipButton];
    self.SignatureSkipButton.hidden = YES;
}
-(void) UpdateUI_SignSwitch { //加入同意人/法定代理人切換按鈕
    //預設簽同意人
    isMySign = YES;
    isLawSign = NO;

    _MySignButton.frame = CGRectMake(0,0,85,85);
    _MySignButton.center = CGPointMake(5+85/2, self.signatureButton.center.y);
    [self.view addSubview: _MySignButton];
    
    _LawSignButton.frame = CGRectMake(0,0,85,85);
    _LawSignButton.center = CGPointMake(100 + 85/2, _MySignButton.center.y);
    [self.view addSubview: _LawSignButton];
    
    _MySignButton.hidden = YES;
    _LawSignButton.hidden = YES;
}
-(void) UpdateUI_SignSwitch4 { //加入店長/銷售人員/買方/法定代理人切換按鈕
    //預設簽買方
    isMySign = NO;
    isBuyerSign = YES;
    
    //店長
    _StoreOwnerSignButton.frame = CGRectMake(0,0,85,85);
    _StoreOwnerSignButton.center = CGPointMake(5+85/2, self.signatureButton.center.y);
    [self.view addSubview: _StoreOwnerSignButton];
    
    //銷售人員
    _SalesSignButton.frame = CGRectMake(0,0,85,85);
    _SalesSignButton.center = CGPointMake(_StoreOwnerSignButton.frame.origin.x + _StoreOwnerSignButton.frame.size.width + 15 + 85/2, _StoreOwnerSignButton.center.y);
    [self.view addSubview: _SalesSignButton];
    
    //買方
    _BuyerSignButton.frame = CGRectMake(0,0,85,85);
    _BuyerSignButton.center = CGPointMake(_SalesSignButton.frame.origin.x + _SalesSignButton.frame.size.width + 15 + 85/2, _StoreOwnerSignButton.center.y);
    [self.view addSubview: _BuyerSignButton];
    
    //法定代理人
    _LawSignButton.frame = CGRectMake(0,0,85,85);
    _LawSignButton.center = CGPointMake(_BuyerSignButton.frame.origin.x + _BuyerSignButton.frame.size.width + 15 + 85/2, _StoreOwnerSignButton.center.y);
    [self.view addSubview: _LawSignButton];
    
    _StoreOwnerSignButton.hidden = YES;
    _SalesSignButton.hidden = YES;
    _BuyerSignButton.hidden = YES;
    _LawSignButton.hidden = YES;
}
-(void) UpdateUI_SkipAndNext { //加入跳過/下一步按鈕
    _SkipButton.frame = CGRectMake(ScreenWidth - 89, ScreenHeight - 63 - 90, 89, 63);
    [self.view addSubview: _SkipButton];
    
    _NextButton.frame = CGRectMake(0, ScreenHeight, 768, 71);
    [self.view addSubview: _NextButton];
}
-(void) UpdateUI_PreviousAndNext { //下一步按鈕/下一步按鈕
    if (_enumCONSENT == CONSENT_TRANSITION_DOC || //過場文件
        _enumCONSENT == CONSENT_TRANSFER_DOC || //過戶說明文件
        _enumCONSENT == CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL || //已簽名的個資同意書
        _enumCONSENT == CONSENT_SIGNED_ORDER_CUSTOMER || //顯示已簽名的訂購單
        _enumCONSENT == CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT || //顯示已簽名的(放棄)補助切結書
        _enumCONSENT == CONSENT_CONTRACT_REVIEW_SIGNED //訂購Gogoro所需簽名文件
        ) {
        _ViewPreviousNext.frame = CGRectMake(0, ScreenHeight - 71, ScreenWidth, 71);
        [self.view addSubview: _ViewPreviousNext];
        
        //_consentWebView.frame = CGRectMake(_consentWebView.frame.origin.x, _consentWebView.frame.origin.y, _consentWebView.frame.size.width, _consentWebView.frame.size.height - _ViewPreviousNext.frame.size.height);
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) {
        _ViewPreviousNext.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 71);
        [self.view addSubview: _ViewPreviousNext];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) { //合約用_個資同意書
        _ViewPreviousNext.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 71);
        [self.view addSubview: _ViewPreviousNext];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CUSTOMER) { //36 訂購合約
        _ViewPreviousNext.frame = CGRectMake(0, ScreenHeight, ScreenWidth, 71);
        [self.view addSubview: _ViewPreviousNext];
    }
}


-(void) UpdateUI_SignSwitch4sdf { //加入店長/銷售人員/買方/法定代理人切換按鈕
    //記住已加入幾個按鈕
    int didAddButtonCount = 0;
    
    if (_dicTranfer) { //過戶移轉
        //判斷預設正在簽哪一個
        isMySign = NO;
        isBuyerSign = NO;
        
        //根據各文件所需簽署人，加入按鈕
        BOOL didGeneratedFirstSignBtn = NO; //判斷是不是第二個以後的按鈕
        
        if (OwnerSign == 1 || OwnerSign == 2) { //原車主
            isOwnerSign = YES;
        }
        else if (NewOwnerSign == 1 || NewOwnerSign == 2) { //新車主
            isNewOwnerSign = YES;
        }
        else if (OwnerDelegateSign == 1 || OwnerDelegateSign == 2) { //原車主委託
            isOwnerDelegateSign = YES;
        }
        else if (NewOwnerDelegateSign == 1 || NewOwnerDelegateSign == 2) { //新車主委託
            isNewOwnerDelegateSign = YES;
        }
        /*else if (AgentSign == 1 || AgentSign == 2) { //法定代理人
            isLawSign = YES;
            [_LawSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_fadin_b.png"] forState:UIControlStateNormal];
        }*/
        
        //根據各文件所需簽署人，加入按鈕
        if (OwnerSign == 1 || OwnerSign == 2) {
            //原車主
            [_TransferOriginalOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_f.png"] forState:UIControlStateNormal];
            didGeneratedFirstSignBtn = YES;
            
            [self adjustSignatureUIWirh:_TransferOriginalOwnerSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (NewOwnerSign == 1 || NewOwnerSign == 2) {
            //新車主
            if (!didGeneratedFirstSignBtn) {
                [_TransferNewOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_f.png"] forState:UIControlStateNormal];
            }
            else {
                [_TransferNewOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar.png"] forState:UIControlStateNormal];
            }
            didGeneratedFirstSignBtn = YES;
            
            [self adjustSignatureUIWirh:_TransferNewOwnerSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (OwnerDelegateSign == 1 || OwnerDelegateSign == 2) {
            //原車主委託或代理
            //必填(2) > 委託人的切圖
            //非必填(1) > 代理人的切圖
            //無(0) > 不顯示任何切圖
            if (OwnerDelegateSign == 2) { //委託人
                if (!didGeneratedFirstSignBtn) {
                    [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_p_f.png"] forState:UIControlStateNormal];
                }
                else {
                    [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_p.png"] forState:UIControlStateNormal];
                }
            }
            else { //代理人
                if (!didGeneratedFirstSignBtn) {
                    [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_agent_f.png"] forState:UIControlStateNormal];
                }
                else {
                    [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_agent.png"] forState:UIControlStateNormal];
                }
            }
            didGeneratedFirstSignBtn = YES;
            
            //原車主委託
            [self adjustSignatureUIWirh:_TransferOriginalEntrustSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (NewOwnerDelegateSign == 1 || NewOwnerDelegateSign == 2) {
            //新車主委託或代理
            //必填(2) > 委託人的切圖
            //非必填(1) > 代理人的切圖
            //無(0) > 不顯示任何切圖
            if (NewOwnerDelegateSign == 2) { //委託人
                if (!didGeneratedFirstSignBtn) {
                    [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_person_f.png"] forState:UIControlStateNormal];
                }
                else {
                    [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_person.png"] forState:UIControlStateNormal];
                }
            }
            else { //代理人
                if (!didGeneratedFirstSignBtn) {
                    [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_agent_f.png"] forState:UIControlStateNormal];
                }
                else {
                    [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_agent.png"] forState:UIControlStateNormal];
                }

                
            }
            didGeneratedFirstSignBtn = YES;
            
            [self adjustSignatureUIWirh:_TransferNewEntrustSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        /*if (AgentSign == 1 || AgentSign == 2) {
            //法定代理人
            [self adjustSignatureUIWirh:_LawSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }*/
        _TransferOriginalOwnerSignButton.hidden = YES;
        _TransferNewOwnerSignButton.hidden = YES;
        _TransferOriginalEntrustSignButton.hidden = YES;
        _TransferNewEntrustSignButton.hidden = YES;
        _LawSignButton.hidden = YES;
        
        //全部都非必填，要直接彈送出
        /*if (NewOwnerSign == 1 && OwnerDelegateSign == 1 && NewOwnerDelegateSign == 1 && AgentSign == 1) {
            [self ShowSendBtn];
        }*/
    }
    else {
        //預設正在簽買方
        isMySign = NO;
        isBuyerSign = YES;
        
        //根據各文件所需簽署人，加入按鈕
        if (MasterSign == 1 || MasterSign == 2) {
            //店長
            [self adjustSignatureUIWirh:_StoreOwnerSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (SalesSign == 1 || SalesSign == 2) {
            //銷售人員
            [self adjustSignatureUIWirh:_SalesSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (BuySign == 1 || BuySign == 2) {
            //買方姓名
            [self adjustSignatureUIWirh:_BuyerSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (AgentSign == 1 || AgentSign == 2) {
            //法定代理人
            [self adjustSignatureUIWirh:_LawSignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        if (CustomerSign == 1 || CustomerSign == 2) {
            isMySign = YES;
            isBuyerSign = NO;
            
            if (attachType == 107)
            {
                [_MySignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_buyername_b.png"] forState:UIControlStateNormal];
            }
            else
            {
                [_MySignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_lishu_b.png"] forState:UIControlStateNormal];
            }
            
            //立書同意人
            [self adjustSignatureUIWirh:_MySignButton andDidAddButtonCount:didAddButtonCount];
            didAddButtonCount++;
        }
        _StoreOwnerSignButton.hidden = YES;
        _SalesSignButton.hidden = YES;
        _BuyerSignButton.hidden = YES;
        _LawSignButton.hidden = YES;
        _MySignButton.hidden = YES;
    }
}

//UpdateUI_SignSwitch4sdf方法的方法
-(void)adjustSignatureUIWirh:(UIButton*)button andDidAddButtonCount:(int)count{
    button.frame = CGRectMake(0,0,85,85);
    button.center = CGPointMake(5+85/2+85*count, self.signatureButton.center.y);
    [self.view addSubview: button];
}



#pragma mark - PDF
-(void) LoadPersonDataFileStart {
    [ObjectManager showTextLoadingHUDinView:self.view text:@"文件更新中"];
     
    //檔案網址
    NSString *urlString = self.consentUrlStr;
    
    //只有補助/放棄補助切結書會有
    if (_enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT) { //補助內扣款切結書
        urlString = self.deductDocumentUrl;
    }
    
    //判斷個資同意書是否存在
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:_stringFolder];
    
    //資料夾如果不存在就建立
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) { //資料夾不存在
        //建立資料夾
        [[NSFileManager defaultManager] createDirectoryAtPath:filePath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    else { //資料夾已存在
    }
    
    //判斷檔案是否存在
    filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",_stringFolder]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
    }
    else { //檔案存在，移除
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    //下載
    NSURL *url = [NSURL URLWithString:urlString];
    if (urlString) {
        [GatewayManager DownloadFileWithURL:url FileName:_stringFolder Folder:_stringFolder Source:self];
    }
    else {
        NSError *error = [NSError errorWithDomain:@"Error" code:0 userInfo:nil];
        [self callBackDownloadFile:error];
    }
}

- (void)callBackDownloadFile:(NSError *)error {
    [self performSelector:@selector(HideLoadingView) withObject:nil afterDelay:2.0f];
    //[ObjectManager hideHUDForView:self.view];
    
    if (error) {
        self.signatureButton.hidden = YES;
        return;
    }
    
    self.signatureButton.hidden = NO;
    self.SignatureSkipButton.hidden = NO;
    
    //把版本跟網址記起來（如果有的話）
    if ([stringApiVersion length] > 0) {
        [[DocumentManager shareInstance] SetDocumentUrlWithType:_enumCONSENT Url:self.consentUrlStr];
        [[DocumentManager shareInstance] SetDocumentVersionWithType:_enumCONSENT Version:stringApiVersion];
    }
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:_stringFolder];
    filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",_stringFolder]];
    NSLog(@"filePath:%@",filePath);
    
    //為了讀到pdf form field properties格式，改用3rd patry library
    //網址 https://github.com/derekblair/Chibuku
    _document = [[PDFDocument alloc] initWithPath:filePath];
    
    /*for (PDFForm *form in _document.forms){
        // Get
        NSString *formValue = form.value;
        NSString *formName = form.name; // Fully qualified field name.
        // Set
        NSLog(@"%@:%@:%ld",formName,formValue,form.formType);
        // If the form is visible on screen it will updated automatically.
    }*/
    
    //CheckBox-->PDFFormTypeButton
    NSArray *aa = [_document.forms formsWithType:PDFFormTypeButton];
    for (PDFForm *form in aa){
        NSString *formValue = form.value;
        NSString *formName = form.name; // Fully qualified field name.
        NSString *exportValue = form.exportValue;
        
        NSLog(@"按鈕-->%@:%@:%@",formName,formValue,exportValue);
        // If the form is visible on screen it will updated automatically.
        //form.value = @"V";
    }
    
    id pass = (_document.documentPath ? _document.documentPath:_document.documentData);
    CGPoint margins = [self getMargins];
    //NSLog(@"margins:%@",NSStringFromCGPoint(margins));
    
    //NSLog(@"self:%@",NSStringFromCGRect(self.view.bounds));
    
    NSArray *additionViews = [_document.forms createWidgetAnnotationViewsForSuperviewWithWidth:self.view.bounds.size.width margin:margins.x hMargin:margins.y];
    
    _pdfView = [[PDFView alloc] initWithFrame:CGRectMake(0, 82, self.view.bounds.size.width, self.view.bounds.size.height-82) dataOrPath:pass additionViews:additionViews];
    //NSLog(@"_pdfView:%@",_pdfView);
    
    //[self.view insertSubview:_pdfView aboveSubview:self.consentWebView];
    [self.view insertSubview:_pdfView belowSubview:_ButtonPrint];
    //_pdfView.pdfView.delegate = self;
    _pdfView.pdfView.scrollView.delegate = self; //為了觸發捲到最後一頁的處理
    //NSLog(@"size:%@",NSStringFromCGSize(_pdfView.pdfView.scrollView.contentSize));
    
    if (//_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE ||
        _enumCONSENT == CONSENT_TRANSITION_DOC || //過場文件
        _enumCONSENT == CONSENT_TRANSFER_DOC || //過戶說明文件
         _enumCONSENT == CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL || //已簽名的個資同意書
         _enumCONSENT == CONSENT_SIGNED_ORDER_CUSTOMER || //顯示已簽名的訂購單
         _enumCONSENT == CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT) { //顯示已簽名的(放棄)補助切結書
        _pdfView.frame = CGRectMake(_pdfView.frame.origin.x, _pdfView.frame.origin.y, _pdfView.frame.size.width, _pdfView.frame.size.height - _ViewPreviousNext.frame.size.height);
    }

    
    //會讀不到pdf form field properties格式
    /*NSURL *url = [NSURL fileURLWithPath:filePath];
    NSURLRequest *pdfRequest = [NSURLRequest requestWithURL:url];
    [self.consentWebView loadRequest:pdfRequest];*/
    
    //視需要顯示列印按鈕
    if (_showPrintButton) {
        _ButtonPrint.hidden = NO;
        
        if (_dicTranfer || _hideEmailButton) { //過戶移轉不用列印
            
        }
        else {
           _ButtonEmail.hidden = NO;
        }
        
        dataForPrint = [[NSData alloc] initWithContentsOfFile:filePath];
    }
    
    //法人->三份電子簽核文件皆不須簽名，即可送出
    //法人可以不用簽名，直接顯示送出按鈕
    if (_enumCONSENT == CONSENT_ORDER_CUSTOMER ||
        _enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL ||
        _enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT) {
        if (isCorporate) {
            //顯示送出按鈕
            [self ShowSendBtn];
            
            //隱藏簽名按鈕
            self.signatureButton.hidden = YES;
        }
    }
}

- (CGPoint)getMargins {
    
    static const float PDFLandscapePadWMargin = 13.0f;
    static const float PDFLandscapePadHMargin = 7.25f;
    static const float PDFPortraitPadWMargin = 9.0f;
    static const float PDFPortraitPadHMargin = 6.10f;
    static const float PDFPortraitPhoneWMargin = 3.5f;
    static const float PDFPortraitPhoneHMargin = 6.7f;
    static const float PDFLandscapePhoneWMargin = 6.8f;
    static const float PDFLandscapePhoneHMargin = 6.5f;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))return CGPointMake(PDFPortraitPadWMargin,PDFPortraitPadHMargin);
        else return CGPointMake(PDFLandscapePadWMargin,PDFLandscapePadHMargin);
    } else {
        if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))return CGPointMake(PDFPortraitPhoneWMargin,PDFPortraitPhoneHMargin);
        else return CGPointMake(PDFLandscapePhoneWMargin,PDFLandscapePhoneHMargin);
    }
}

#pragma mark - Call API
#pragma mark [306] 取得已套版非附件類表單URL
- (void)callGetContract {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    
    if (_customerID) {
        [parameter setObject:_customerID forKey:@"customer"];
    }
    
    if (_OrderNo) {
        [parameter setObject:_OrderNo forKey:@"orderNo"]; //訂單編號 (訂購契約書才有)
    }

    [GatewayManager callGetContract:parameter Source:self];
}
- (void)callBackGetContract:(NSDictionary *)parameter
{
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess)
    {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        BOOL Signed = [[Data objectForKey:@"Signed"] boolValue];
        
        MasterSign = [[Data objectForKey:@"MasterSign"] intValue];
        SalesSign = [[Data objectForKey:@"SalesSign"]intValue];
        BuySign = [[Data objectForKey:@"BuySign"]intValue];
        AgentSign = [[Data objectForKey:@"AgentSign"]intValue];
        CustomerSign = [[Data objectForKey:@"CustomerSign"]intValue];

        //UI update
        [self UpdateUI_SignSwitch4sdf];
        
        if (_enumCONSENT == CONSENT_CONTRACT_REVIEW_SIGNED)
        {
            if (Signed)
            {
                self.consentUrlStr = [Data objectForKey:@"SignFileUrl"]; //取得此筆訂單已簽文件
                NSLog(@"已簽 網址:%@",self.consentUrlStr);
            }
            else
            {
                self.consentUrlStr = [Data objectForKey:@"Url"]; //空白文件
                NSLog(@"空白 網址:%@",self.consentUrlStr);
            }
        }
        else
        {
            self.consentUrlStr = [Data objectForKey:@"Url"]; //空白文件
            NSLog(@"空白 網址:%@",self.consentUrlStr);
        }
 
        if (_isReSign)
        {
            if (_enumCONSENT == CONSENT_ORDER_CUSTOMER || _enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) {
                self.consentUrlStr = [Data objectForKey:@"Url"];
            }
        }
        
        //下載
        [self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
    }
    else
    {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [101] 取得非附件類表單目前版號及位址
- (void)callGetContractVersion {
    [ObjectManager showLodingHUDinView:self.view];

    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callGetContractVersion:parameter Source:self];
}
- (void)callBackGetContractVersion:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSString *stringCurrentVersion = [[DocumentManager shareInstance] GetDocumentVersionWithType:_enumCONSENT];
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        
        //下載網址與版本
        stringApiVersion = [[Data objectForKey:@"Version"] stringByReplacingOccurrencesOfString:@"V" withString:@""];
        self.consentUrlStr = [Data objectForKey:@"Url"];
        
        
        //self.consentUrlStr = [self.consentUrlStr stringByReplacingOccurrencesOfString:@"//" withString:@"/"];
        NSLog(@"網址:%@",self.consentUrlStr);
        
        //比較本地端文件版本
        CGFloat currentVersion = [stringCurrentVersion integerValue];
        CGFloat apiVersion = [stringApiVersion integerValue];
        if (apiVersion > currentVersion) { //要更新
            NSLog(@"文件要更新");
            [self LoadPersonDataFileStart];
        }
        else { //不用更新
            NSLog(@"文件不要更新");
            [self callBackDownloadFile:nil];
        }
        
        /*//判斷檔案是否存在
        NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *filePath = [documentsPath stringByAppendingPathComponent:_stringFolder];
        filePath = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.pdf",_stringFolder]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            NSLog(@"文件不存在，要重新下載");
            [self LoadPersonDataFileStart];
        }
        else { //檔案存在
            
        }*/
    }
    else {
        [ObjectManager hideHUDForView:self.view];
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [102] 上傳同意書簽名
- (void)callUploadContractSignWithImage:(UIImage *)image {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if (_customerID) {
        [parameter setObject:_customerID forKey:@"customer"];
    }
    
    if (_OrderNo) {
        [parameter setObject:_OrderNo forKey:@"orderNo"]; //訂單編號 (訂購契約書才有)
    }
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callUploadContractSign:parameter Source:self];
}
- (void)callBackUploadContractSign:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"文件已送出" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //[self.navigationController popToRootViewControllerAnimated:YES];
            
            //1.5預覽已簽核文件
            NSString *urlString = [parameter objectForKey:@"Data"];
            [self GotoSignedDocumentWithUrlString:urlString];
        }];
        
        [alertController addAction:actionConfirm];
        [self presentViewController:alertController animated:YES completion:^(void){
        }];
        
        //刪除暫存檔
        [self DeleAllTempImageFile];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}

//justin
#pragma mark [104] 訂購簽名文件及順序
-(void)callGetOrderContractList{
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"OrderNo"];
    [GatewayManager callGetOrderContractList:parameter Source:self];
}
-(void)callBackGetOrderContractList:(NSDictionary *)parameter {
    
    [ObjectManager hideHUDForView:self.view];
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    NSDictionary *data = [parameter objectForKey:@"Data"];
    //_contractArray = data[@"Contracts"];
    _contractArray = [NSMutableArray arrayWithArray:data[@"Contracts"]];

    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        dicDeductContact = [[NSDictionary alloc] initWithDictionary:Data];
        
        //依據不同的回覆資料，顯示不同的選項
//        NSDictionary *Attach = [dicDeductContact objectForKey:@"Attach"];
//        self.consentUrlStr = [Attach objectForKey:@"FileUrl"];
//        self.deductDocumentUrl = self.consentUrlStr;
//        NSLog(@"網址:%@",self.consentUrlStr);
        
        //[308] 上傳訂購契約or補助內扣款切結書簽名
        //atachtype 28: 補助說明切結書 29: 補助放棄切結書
        //contractSeq = @[contractsArray[0][@"AttachType"]
        
//        NSInteger Type = [[Attach objectForKey:@"Type"] integerValue];
//        attachType = Type;
        
        NSDictionary *mPosData = [dicDeductContact objectForKey:@"mPosData"];
        NSString *Msg = [mPosData objectForKey:@"Msg"];
        NSInteger State = [[mPosData objectForKey:@"State"] integerValue];
        //State可分為
        //0: 資料不同 (不可繼續)
        //1: 資料相同
        //2: 資料不同 (可繼續)
        
        //資料相同，且無訊息-->直接顯示補助放棄切結書
        if (State == 1 && [Msg length] == 0) {
            //下載
            //[self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
            //2016.05.19 改成：把文件網址記起來，到訂購契約
            //[self GotoOrderContract];
            
            //改成依照ContractArray來判斷目前該簽的文件
            _nowSeq = 0; //1
            int nowAttachType = [self checkNowContractWithSeq:_nowSeq];
            ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
            destVC.enumCONSENT = CONSENT_CONTRACT_NEED_SIGNATURE;
            destVC.OrderNo = _OrderNo;
            destVC.CustomerName = _CustomerName;
            destVC.dicSubsidy = _dicSubsidy;
            destVC.nowSeq = _nowSeq; //1
            destVC.contractArray = _contractArray;
            destVC.newAttachType = nowAttachType;
            [self.navigationController pushViewController:destVC animated:YES];
        }
        else { //其他狀況一定會有訊息
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Msg preferredStyle:UIAlertControllerStyleAlert];
            
            //取消-->退回補助選擇（此選項一定有）
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self GotoSelectSubsidyVC];
            }];
            [alertController addAction:actionCancel];
            
            if (State == 1) { //1: 資料相同
                //確認-->顯示PDF
                UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //下載
                    //[self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
                    //2016.05.19 改成：把文件網址記起來，到訂購契約
                    //[self GotoOrderContract];
                    
                    //改成依照ContractArray來判斷目前該簽的文件
                    _nowSeq = 0; //1
                    int nowAttachType = [self checkNowContractWithSeq:_nowSeq];
                    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
                    destVC.enumCONSENT = CONSENT_CONTRACT_NEED_SIGNATURE;
                    destVC.OrderNo = _OrderNo;
                    destVC.CustomerName = _CustomerName;
                    destVC.dicSubsidy = _dicSubsidy;
                    destVC.nowSeq = _nowSeq; //1
                    destVC.contractArray = _contractArray;
                    destVC.newAttachType = nowAttachType;
                    [self.navigationController pushViewController:destVC animated:YES];
                }];
                [alertController addAction:actionConfirm];
            }
            else { //資料不同，一定有重試的選項
                //重試-->再抓一次資料
                UIAlertAction *actionRetry = [UIAlertAction actionWithTitle:@"重試" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [self callGetOrderContractList];
                    //[self callGetPreDeductContract];
                    //改104
                }];
                [alertController addAction:actionRetry];
                
                if (State == 2) { //2: 資料不同 (可繼續)
                    //繼續-->不管警告，顯示PDF
                    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"繼續" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        //下載
                        //[self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
                        //2016.05.19 改成：把文件網址記起來，到訂購契約
                        //[self GotoOrderContract];
                        
                        //改成依照ContractArray來判斷目前該簽的文件
                        _nowSeq = 0; //1
                        int nowAttachType = [self checkNowContractWithSeq:_nowSeq];
                        ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
                        destVC.enumCONSENT = CONSENT_CONTRACT_NEED_SIGNATURE;
                        destVC.OrderNo = _OrderNo;
                        destVC.CustomerName = _CustomerName;
                        destVC.dicSubsidy = _dicSubsidy;
                        destVC.nowSeq = _nowSeq; //1
                        destVC.contractArray = _contractArray;
                        destVC.newAttachType = nowAttachType;
                        [self.navigationController pushViewController:destVC animated:YES];
                    }];
                    [alertController addAction:actionConfirm];
                }
            }
            [self presentViewController:alertController animated:YES completion:^(void){
            }];
        }
    }else{
        [self showAlertControllerWithMessage:Sysmsg];
    }
}

#pragma mark [308] 上傳訂購契約or補助內扣款切結書簽名
- (void)callUploadOrderContractSign {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    [parameter setObject:_OrderNo forKey:@"orderNo"];
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callUploadOrderContractSign:parameter Source:self];
}
- (void)callBackUploadOrderContractSign:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    //justin
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"文件已送出" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (_isReSign) { //重簽，回上頁
                //2016.11.02 重簽完呼叫105之後回上頁
                [self callSendOrderContractMail];
                //[self.navigationController popViewControllerAnimated:YES];
            }
            else {
                //取得要簽名的第二份文件
                int nowAttachType = [self checkNowContractWithSeq:_nowSeq+1];
                if (nowAttachType == END_SIGNING) {
                    if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) {
                        //2016.11.02 ，在最後一份要簽名的文件簽完送出 / 或是跳過的時候，跳出一個內建的提示，問他要不要寄信嗎，是call這支帶null否就繼
                        [self PromptAlertBeforeGotoConfirmInfoVC];
                        
                        /*NSInteger firstAttachType = -1;
                        if ([_contractArray count] > 0) {
                            NSDictionary *_dic = [_contractArray objectAtIndex:0];
                            firstAttachType = [[_dic objectForKey:@"AttachType"] integerValue];
                        }
                        
                        ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
                        //destVC.newAttachType = [self checkNowContractWithSeq:1];
                        destVC.newAttachType = firstAttachType;
                        destVC.enumCONSENT = CONSENT_CONTRACT_REVIEW_SIGNED;
                        destVC.OrderNo = _OrderNo;
                        destVC.CustomerName = _CustomerName;
                        destVC.dicSubsidy = _dicSubsidy;
                        destVC.nowSeq = 1;
                        destVC.contractArray = _contractArray;
                        [self.navigationController pushViewController:destVC animated:YES];*/
                    }                                
                    
                    //法人->不須預覽三份已簽名文件
                    /*
                    if (!isCorporate) { //個人
                        //到已簽名的訂購單
                        [self GotoSignedOrderCustomer];
                    }
                    else { //法人
                        [self GotoConfirmInfoVC]; //到3.6確認資訊
                    }
                     */
                } else {
                    [self GotoNextContractWithAttachType:nowAttachType andEnumCONSENT:_enumCONSENT];
                }
                
//                if (_enumCONSENT == CONSENT_ORDER_CUSTOMER) { //訂購合約
//                    //要求合約用_個資同意書
//                    [self GotoOrderPersonal];
//                }
//                else if (_enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) { //合約用_個資同意書
//                    //要求補助/不補助切結書
//                    [self GotoDeductContract];
//                }
//                else if (_enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT) { //補助/放棄補助切結書
//                    //法人->不須預覽三份已簽名文件
//                    if (!isCorporate) { //個人
//                        //到已簽名的訂購單
//                        [self GotoSignedOrderCustomer];
//                    }
//                    else { //法人
//                        [self GotoConfirmInfoVC]; //到3.6確認資訊
//                    }
//                }
                
            }
        }];
        
        [alertController addAction:actionConfirm];
        [self presentViewController:alertController animated:YES completion:^(void){
        }];
        
        //刪除暫存檔
        [self DeleAllTempImageFile];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}

#pragma mark [307] 取得補助內扣款切結書
- (void)callGetPreDeductContract {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if (_OrderNo) {
        [parameter setObject:_OrderNo forKey:@"orderNo"]; //訂單編號 (訂購契約書才有)
    }
    [GatewayManager callGetPreDeductContract:parameter Source:self];
}
- (void)callBackGetPreDeductContract:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        NSDictionary *Data = [parameter objectForKey:@"Data"];
        dicDeductContact = [[NSDictionary alloc] initWithDictionary:Data];
        
        //依據不同的回覆資料，顯示不同的選項
        NSDictionary *Attach = [dicDeductContact objectForKey:@"Attach"];
        self.consentUrlStr = [Attach objectForKey:@"FileUrl"];
        self.deductDocumentUrl = self.consentUrlStr;
        NSLog(@"網址:%@",self.consentUrlStr);
        
        //[308] 上傳訂購契約or補助內扣款切結書簽名
        //atachtype 28: 補助說明切結書 29: 補助放棄切結書
        NSInteger Type = [[Attach objectForKey:@"Type"] integerValue];
        attachType = Type;
        
        NSDictionary *mPosData = [dicDeductContact objectForKey:@"mPosData"];
        NSString *Msg = [mPosData objectForKey:@"Msg"];
        NSInteger State = [[mPosData objectForKey:@"State"] integerValue];
        //State可分為
        //0: 資料不同 (不可繼續)
        //1: 資料相同
        //2: 資料不同 (可繼續)
        
        //資料相同，且無訊息-->直接顯示補助放棄切結書
        if (State == 1 && [Msg length] == 0) {
            //下載
            //[self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
            //2016.05.19 改成：把文件網址記起來，到訂購契約
            [self GotoOrderContract];
        }
        else { //其他狀況一定會有訊息
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:Msg preferredStyle:UIAlertControllerStyleAlert];
            
            //取消-->退回補助選擇（此選項一定有）
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [self GotoSelectSubsidyVC];
            }];
            [alertController addAction:actionCancel];
            
            if (State == 1) { //1: 資料相同
                //確認-->顯示PDF
                UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //下載
                    //[self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
                    //2016.05.19 改成：把文件網址記起來，到訂購契約
                    [self GotoOrderContract];
                }];
                [alertController addAction:actionConfirm];
            }
            else { //資料不同，一定有重試的選項
                //重試-->再抓一次資料
                UIAlertAction *actionRetry = [UIAlertAction actionWithTitle:@"重試" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    //[self callGetPreDeductContract];
                    //改104
                }];
                [alertController addAction:actionRetry];
                
                if (State == 2) { //2: 資料不同 (可繼續)
                    //繼續-->不管警告，顯示PDF
                    UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"繼續" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                        //下載
                        //[self performSelector:@selector(LoadPersonDataFileStart) withObject:nil afterDelay:0.5f];
                        //2016.05.19 改成：把文件網址記起來，到訂購契約
                        [self GotoOrderContract];
                    }];
                    [alertController addAction:actionConfirm];
                }
            }

            [self presentViewController:alertController animated:YES completion:^(void){
            }];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}

#pragma mark [105] 訂單同意書發信
- (void)callSendOrderContractMail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    if (_OrderNo) {
        [parameter setObject:_OrderNo forKey:@"orderNo"]; //訂單編號
    }
    
    //重簽要加入
    if (_isReSign) {
       [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    }
    else if (_showPrintButton) { //2016.12.21 新增「Email」按鈕，可以寄出簽好的文件，到購買人/車主信箱
        [parameter setObject:[NSString stringWithFormat:@"%ld",_newAttachType] forKey:@"attachType"];
        
        //mailTarget 收件對象(0:車主, 1:購買人)
        [parameter setObject:[NSString stringWithFormat:@"%ld",intMailTarget] forKey:@"mailTarget"];
        
        //contentType 內容類型(0: 簽好的,1: 空白)
        //API 309 有Fileurl （有簽名）>> API 105 content type 帶 0
        //API 309 是 templatefileurl（空白） >> API 105 content type 帶 1
        if (!_FromTempUrl) {
            [parameter setObject:@"0" forKey:@"contentType"];
        }
        else {
            //如果這個PDF是從TemplateFileUrl來的，表示他是空白文件，ContentType要寫1
            [parameter setObject:@"1" forKey:@"contentType"];
        }
    }
    else {
        [parameter setObject:@"" forKey:@"attachType"];
    }
    
    [GatewayManager callSendOrderContractMail:parameter Source:self];
}
- (void)callBackSendOrderContractMail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        if (_showPrintButton) { //2016.12.21 新增「Email」按鈕，可以寄出簽好的文件，到購買人/車主信箱
            [self showAlertControllerWithMessage:@"寄信成功"];
            return;
        }
    }
    else {
    }
    
    if (_isReSign) { //重簽，回上頁
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        //都簽完，到已簽文件
        [self ShowSignedDocument];
    }
}
#pragma mark [610] 是否已於DMS完成過戶
- (void) callCheckDMSTransfer {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    //車牌號碼
    [parameter setObject:[_dicTranfer objectForKey:@"Plate"] forKey:@"Plate"];
    
    //原車主
    /*NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (OriginalOwner && ![OriginalOwner isKindOfClass:[NSNull class]]) {
        //OwnerProfileCode 原車主身份證字號
        [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
    }
    
    //新車主
    NSDictionary *NewOwner = [_dicTranfer objectForKey:@"NewOwner"];
    if (NewOwner && ![NewOwner isKindOfClass:[NSNull class]]) {
        //新車主eMail
        [parameter setObject:[NewOwner objectForKey:@"Email"] forKey:@"Email"];
    }*/
    
    //OwnerProfileCode 原車主身份證字號
    [parameter setObject:_stringOriginalOwnerID forKey:@"OwnerProfileCode"];
    
    //新車主Email
    [parameter setObject:[_dicTranfer objectForKey:@"NewOwnerEmail"] forKey:@"Email"];
    
    [GatewayManager callTransferCheckDMSTransfer:parameter Source:self];
}
- (void)callBacTransferCheckDMSTransfer:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
    
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        //是否已過戶(0:否, 1:是)
        NSInteger Data = [[parameter objectForKey:@"Data"] integerValue];
        Data = 1;
        if (Data == 1) { //已過戶-->6.1.4-1 確認DMS過戶資料(車籍)
            //6.1.4-1 確認DMS過戶資料(車籍)
            [self GotoTransferConfirmDMSVC];
        }
        else {
            [self showAlertControllerWithMessage:@"請先於DMS車籍執行過戶程序"];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [606] 上傳租約轉讓相關同意書簽名
- (void)callTransferUploadContractSign {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
    [parameter setObject:[_dicTranfer objectForKey:@"Plate"] forKey:@"Plate"];
    
    [parameter setObject:[NSString stringWithFormat:@"%ld",attachType] forKey:@"attachType"];
    [GatewayManager callTransferUploadContractSign:parameter Source:self];
}
- (void)callBacTransferUploadContractSign:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
  
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"文件已送出" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (_isReSign) { //重簽，回上頁
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                //取得要簽名的第二份文件
                int nowAttachType = [self checkNowContractWithSeq:_nowSeq+1];
                if (nowAttachType == END_SIGNING) {
                    if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) {
                        //2016.11.02 ，在最後一份要簽名的文件簽完送出 / 或是跳過的時候，跳出一個內建的提示，問他要不要寄信嗎，是call這支帶null否就繼
                        //[self PromptAlertBeforeGotoConfirmInfoVC];
                        
                        //2017.04.11 [612] 過戶文件財務發信
                        [self callTransferSendMail];
                    }
                }
                else {
                    [self GotoNextContractWithAttachType:nowAttachType andEnumCONSENT:_enumCONSENT];
                }
            }
        }];
        
        [alertController addAction:actionConfirm];
        [self presentViewController:alertController animated:YES completion:^(void){
        }];
        
        //刪除暫存檔
        [self DeleAllTempImageFile];
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark [612] 過戶文件財務發信
- (void)callTransferSendMail {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
    [parameter setObject:[_dicTranfer objectForKey:@"Plate"] forKey:@"Plate"];
    [GatewayManager callTransferSendMail:parameter Source:self];
}
- (void)callBackTransferSendMail:(NSDictionary *)parameter {
    [ObjectManager hideHUDForView:self.view];
   
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    if (Syscode == kStatuscode_Sucess) {
        
    }
    else {
        //[self showAlertControllerWithMessage:Sysmsg];
    }
    
    //都簽完，到已簽文件
    [self ShowSignedDocument];
}
#pragma mark [605] 取得租約轉讓需簽名同意書及順序
-(void)callGetTransferContractList {
    [ObjectManager showLodingHUDinView:self.view];
    
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc] init];
    
    NSDictionary *OriginalOwner = [_dicTranfer objectForKey:@"OriginalOwner"];
    if (![OriginalOwner isKindOfClass:[NSNull class]]) {
        [parameter setObject:[OriginalOwner objectForKey:@"ProfileCode"] forKey:@"OwnerProfileCode"];
    }
    
    [parameter setObject:[_dicTranfer objectForKey:@"Plate"] forKey:@"Plate"];
    [GatewayManager callTransferGetContractList:parameter Source:self];
}
-(void)callBacTransferGetContractList:(NSDictionary *)parameter {
    
    [ObjectManager hideHUDForView:self.view];
    NSInteger Syscode = [[parameter objectForKey:@"SysCode"] integerValue];
    NSString *Sysmsg = [parameter objectForKey:@"SysMsg"];
    
    if (Syscode == kStatuscode_Sucess) {
        NSArray *data = [parameter objectForKey:@"Data"];
        _contractArray = nil;
        _contractArray = [NSMutableArray arrayWithArray:data];
        
        //改成依照ContractArray來判斷目前該簽的文件
        if ([_contractArray count] > 0) {
            
            _nowSeq = -1;
            int firstAttachType = -1;
            if ([_contractArray count] > 0) {
                NSDictionary *_dic = [_contractArray objectAtIndex:0];
                firstAttachType = [[_dic objectForKey:@"AttachType"] intValue];
            }
            
            [self GotoNextContractWithAttachType:firstAttachType andEnumCONSENT:CONSENT_CONTRACT_REVIEW_SIGNED];
        }
    }
    else {
        [self showAlertControllerWithMessage:Sysmsg];
    }
}
#pragma mark - WebView Delegate
-(void) webViewDidStartLoad:(UIWebView *)webView {
    [ObjectManager showLodingHUDinView:self.view];
}
-(void) webViewDidFinishLoad:(UIWebView *)webView {
    [ObjectManager hideHUDForView:self.view];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView { //第一份文件滑到底部show 下一步button
    //NSLog(@"size:%@",NSStringFromCGSize(_pdfView.pdfView.scrollView.contentSize));
    
    //_showFinishButton 1.5已簽核文件
    if (_enumCONSENT == CONSENT_ORDER_PERSONAL_Data || _showFinishButton) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= (scrollView.contentSize.height - 100) ) { //滑動到最底端時，有下一步按鈕
            NSLog(@"滑動到最底，顯示下一步按鈕");
            [self ShowOrHideNextButton:YES];
            
            //調整PFDView
            _pdfView.frame = CGRectMake(_pdfView.frame.origin.x, _pdfView.frame.origin.y, _pdfView.frame.size.width, ScreenHeight - _pdfView.frame.origin.y - _NextButton.frame.size.height);
            
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            [scrollView setContentOffset:bottomOffset animated:NO];
        }
        else {
            NSLog(@"非滑動到最底，隱藏下一步按鈕");
            [self ShowOrHideNextButton:NO];
            
            _pdfView.frame = CGRectMake(_pdfView.frame.origin.x, _pdfView.frame.origin.y, _pdfView.frame.size.width, ScreenHeight - _pdfView.frame.origin.y);
        }
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_REVIEW_SIGNED) {
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= (scrollView.contentSize.height - 100) ) { //滑動到最底端時，有下一步按鈕
            //調整PFDView
            _pdfView.frame = CGRectMake(_pdfView.frame.origin.x, _pdfView.frame.origin.y, _pdfView.frame.size.width, ScreenHeight - _pdfView.frame.origin.y - _NextButton.frame.size.height);
            
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            [scrollView setContentOffset:bottomOffset animated:NO];
        }
        else {
            _pdfView.frame = CGRectMake(_pdfView.frame.origin.x, _pdfView.frame.origin.y, _pdfView.frame.size.width, ScreenHeight - _pdfView.frame.origin.y);
        }
    }
}

#pragma mark - [Button Action]
-(IBAction) pressSendBtn:(id)sender {
    //把目前的圖存起來
    [self saveSignatureImage:signVC.mainImageView.image];
    
    if (_enumCONSENT == CONSENT_TEST_RIDE || //102 預約試乘個資同意書
        _enumCONSENT == CONSENT_PERSONAL_Data) {
        //呼叫API
        [self callUploadContractSignWithImage:nil];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CUSTOMER) { //36 訂購契約書
        //呼叫API
        [self callUploadOrderContractSign];
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) {
        if (_dicTranfer) { //過戶移轉
            //呼叫API
            [self callTransferUploadContractSign];
        }
        else {
            //呼叫API
            [self callUploadOrderContractSign];
        }
    }
    else if (_enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) { //合約用_個資同意書
        //呼叫API
        [self callUploadOrderContractSign];
    }
    else if (_enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT) { //補助內扣款切結書
        //呼叫API
        [self callUploadOrderContractSign];
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) { //訂購Gogoro所需簽名文件
        //呼叫API
        [self callUploadOrderContractSign];
    }
    else {
        if (_dicTranfer) { //過戶移轉
            //呼叫API
            [self callTransferUploadContractSign];
        }
        else {
            //呼叫API
            [self callUploadOrderContractSign];
        }
    }
}
#pragma mark 簽名Button
- (IBAction)pressSignatureSkipBtn:(id)sender { //跳過簽名
    //移除跳過的文件
    int currentAttachType = [self checkNowContractWithSeq:_nowSeq];
    NSInteger indexToDelete = -1;
    for (int i = 0; i < _contractArray.count; i++) {
        NSDictionary *dic = _contractArray[i];
        if ([dic[@"AttachType"] intValue] == currentAttachType) {
            indexToDelete = i;
        }
    }
    if (indexToDelete >= 0) {
        _nowSeq--;
        [_contractArray removeObjectAtIndex:indexToDelete];
    }
    
    //下一份文件的AttachType
    int nextAttachType = [self checkNowContractWithSeq:_nowSeq+1];
    
    if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) {
        if (nextAttachType == END_SIGNING) { //已簽完名
            if (_dicTranfer) { //過戶移轉
                //2017.04.11 [612] 過戶文件財務發信
                [self callTransferSendMail];
            }
            else { //訂購
                //2016.11.02 ，在最後一份要簽名的文件簽完送出 / 或是跳過的時候，跳出一個內建的提示，問他要不要寄信嗎，是call這支帶null否就繼
                [self PromptAlertBeforeGotoConfirmInfoVC];
            }
        }
        else { //到下一份待簽名文件
            [self GotoNextContractWithAttachType:nextAttachType andEnumCONSENT:_enumCONSENT];
        }
    }
}

- (IBAction)signatureButtonOnClick:(id)sender {
    [self showHiddenSignatureView];
}
- (IBAction)pressSkipBtn:(id)sender { //3.1 skip跳過
    [self GotoOrder];
}
- (IBAction)pressNextBtn:(id)sender { //下一步
    if (_showFinishButton) { //1.5預覽已簽核文件完畢後返回1.1客戶資料比對或1.3選擇簽核文件
        [self GotoCustomerDocumentListOrSignVC];
        return;
    }
    
    [self GotoOrder];
}
- (IBAction)pressPreviousStepBtn:(id)sender { //上一步
    if (_enumCONSENT == CONSENT_TRANSITION_DOC || //過場文件
        _enumCONSENT == CONSENT_TRANSFER_DOC || //過戶說明文件
        _enumCONSENT == CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL || //已簽名的個資同意書
        _enumCONSENT == CONSENT_SIGNED_ORDER_CUSTOMER || //顯示已簽名的訂購單
        _enumCONSENT == CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT ||
        _enumCONSENT == CONSENT_CONTRACT_REVIEW_SIGNED ||
        _enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) { //顯示已簽名的(放棄)補助切結書
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) { //合約用_個資同意書
        [self GotoSelectSubsidyVC];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CUSTOMER) { //36 訂購契約書
        [self GotoSelectSubsidyVC];
    }
}
- (IBAction)pressNextStepBtn:(id)sender { //下一步
    if (_enumCONSENT == CONSENT_TRANSITION_DOC) { //過場文件
        
        //[self GotoOrderContract]; //到訂購契約
        //2016.05.19 改成跟MPOS比對
        
        //改call104
        //[self callGetPreDeductContract];
        [self callGetOrderContractList];
    }
    else if (_enumCONSENT == CONSENT_TRANSFER_DOC) { //過戶說明文件
        //跟後台確認是否已於DMS過戶
        [self callCheckDMSTransfer];
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) { //簽名文件
        [self pressSendBtn:nil];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) { //合約用_個資同意書
        [self pressSendBtn:nil];
    }
    else if (_enumCONSENT == CONSENT_ORDER_CUSTOMER) { //36 訂購契約書
        [self pressSendBtn:nil];
    }
    else if (_enumCONSENT == CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL) { //已簽名的個資同意書
        [self GotoSignedOrderDeductContract]; //到已簽名的(放棄)補助切結書
    }
    else if (_enumCONSENT == CONSENT_SIGNED_ORDER_CUSTOMER) { //顯示已簽名的訂購單
        [self GotoSignedOrderContractPersonal]; //到已簽名的個資同意書
    }
    else if (_enumCONSENT == CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT) { //顯示已簽名的(放棄)補助切結書
        [self GotoConfirmInfoVC]; //到3.6確認資訊
    } else if (_enumCONSENT == CONSENT_CONTRACT_REVIEW_SIGNED) { //檢視已簽名文件
        int nowAttachType = [self checkNowContractWithSeq:_nowSeq+1];
        //int nowAttachType = [self GetNextAttachTypeWithSeq:_nowSeq];
        if (nowAttachType == END_SIGNING) {
            if (_dicTranfer) {
                //到6.1.8 確認文件清單
                [self GotoConfirmDocumentList];
            }
            else {
                [self GotoConfirmInfoVC]; //到3.6確認資訊
            }
            
        } else {
            [self GotoNextContractWithAttachType:nowAttachType andEnumCONSENT:_enumCONSENT];
          
        }
    }
}
-(IBAction) pressPrintBtn {
    [self ShowPrintVCWithData:dataForPrint Rect:_ButtonPrint.frame];
}
-(IBAction) pressEmailBtn {
    //2016.12.21 新增「Email」按鈕，可以寄出簽好的文件，到購買人/車主信箱
    [self ShowBuyerNameSelect]; //選車主還是購買人
}

#pragma mark - Signature
- (void)showHiddenSignatureView
{
    if (!signVC)
    {
        signVC = [[SignatureViewController alloc] initWithNibName:@"SignatureViewController" bundle:nil];
        signVC.delegate = self;
        signVC.enumCONSENT = _enumCONSENT;
        signVC.view.frame = CGRectMake(0 , ScreenHeight , ScreenWidth , SignatureHeight);
    }

    if (signVC.view.superview != self.view)
    {
        //顯示
        [self addChildViewController:signVC];
        [self.view addSubview:signVC.view];
        [signVC didMoveToParentViewController:self];

        [UIView animateWithDuration:1 animations:^{
            signVC.view.frame = CGRectOffset(signVC.view.frame, 0, -SignatureHeight);
            _SignatureSkipButton.frame = CGRectOffset(_SignatureSkipButton.frame, 0, -SignatureHeight);
            signatureButton.frame = CGRectOffset(signatureButton.frame, 0, -SignatureHeight);
            _MySignButton.frame = CGRectOffset(_MySignButton.frame, 0, -SignatureHeight);
            _LawSignButton.frame = CGRectOffset(_LawSignButton.frame, 0, -SignatureHeight);
            _StoreOwnerSignButton.frame = CGRectOffset(_StoreOwnerSignButton.frame, 0, -SignatureHeight);
            _BuyerSignButton.frame = CGRectOffset(_BuyerSignButton.frame, 0, -SignatureHeight);
            _SalesSignButton.frame = CGRectOffset(_SalesSignButton.frame, 0, -SignatureHeight);
            _TransferOriginalOwnerSignButton.frame = CGRectOffset(_TransferOriginalOwnerSignButton.frame, 0, -SignatureHeight);
            _TransferOriginalEntrustSignButton.frame = CGRectOffset(_TransferOriginalEntrustSignButton.frame, 0, -SignatureHeight);
            _TransferNewOwnerSignButton.frame = CGRectOffset(_TransferNewOwnerSignButton.frame, 0, -SignatureHeight);
            _TransferNewEntrustSignButton.frame = CGRectOffset(_TransferNewEntrustSignButton.frame, 0, -SignatureHeight);
        }];
        
        if (MasterSign == 1 || MasterSign == 2) {
            _StoreOwnerSignButton.hidden = NO;
        }
        if (SalesSign == 1 || SalesSign == 2) {
            _SalesSignButton.hidden = NO;
        }
        if (BuySign == 1 || BuySign == 2) {
            _BuyerSignButton.hidden = NO;
        }
        if (AgentSign == 1 || AgentSign == 2) {
            _LawSignButton.hidden = NO;
        }
        if (CustomerSign == 1 || CustomerSign == 2) {
            _MySignButton.hidden = NO;
        }
        if (OwnerSign == 1 || OwnerSign == 2) {
            _TransferOriginalOwnerSignButton.hidden = NO;
        }
        if (OwnerDelegateSign == 1 || OwnerDelegateSign == 2) {
            _TransferOriginalEntrustSignButton.hidden = NO;
        }
        if (NewOwnerSign == 1 || NewOwnerSign == 2) {
            _TransferNewOwnerSignButton.hidden = NO;
        }
        if (NewOwnerDelegateSign == 1 || NewOwnerDelegateSign == 2) {
            _TransferNewEntrustSignButton.hidden = NO;
        }
    }
    else
    {
        //隱藏
        [UIView animateWithDuration:1 animations:^{
            signVC.view.frame = CGRectOffset(signVC.view.frame, 0, SignatureHeight);
            signatureButton.frame = CGRectOffset(signatureButton.frame, 0, SignatureHeight);
            _SignatureSkipButton.frame = CGRectOffset(_SignatureSkipButton.frame, 0, SignatureHeight);
            _MySignButton.frame = CGRectOffset(_MySignButton.frame, 0, SignatureHeight);
            _LawSignButton.frame = CGRectOffset(_LawSignButton.frame, 0, SignatureHeight);
            _StoreOwnerSignButton.frame = CGRectOffset(_StoreOwnerSignButton.frame, 0, SignatureHeight);
            _BuyerSignButton.frame = CGRectOffset(_BuyerSignButton.frame, 0, SignatureHeight);
            _SalesSignButton.frame = CGRectOffset(_SalesSignButton.frame, 0, SignatureHeight);
            _TransferOriginalOwnerSignButton.frame = CGRectOffset(_TransferOriginalOwnerSignButton.frame, 0, SignatureHeight);
            _TransferOriginalEntrustSignButton.frame = CGRectOffset(_TransferOriginalEntrustSignButton.frame, 0, SignatureHeight);
            _TransferNewOwnerSignButton.frame = CGRectOffset(_TransferNewOwnerSignButton.frame, 0, SignatureHeight);
            _TransferNewEntrustSignButton.frame = CGRectOffset(_TransferNewEntrustSignButton.frame, 0, SignatureHeight);
        } completion:^(BOOL finished) {
            [signVC.view removeFromSuperview];
            [signVC removeFromParentViewController];
            
            _StoreOwnerSignButton.hidden = YES;
            _SalesSignButton.hidden = YES;
            _BuyerSignButton.hidden = YES;
            _LawSignButton.hidden = YES;
            _MySignButton.hidden = YES;
            _TransferOriginalOwnerSignButton.hidden = YES;
            _TransferOriginalEntrustSignButton.hidden = YES;
            _TransferNewOwnerSignButton.hidden = YES;
            _TransferNewEntrustSignButton.hidden = YES;

        }];
    }
}

#pragma mark - [Delegate]
#pragma mark 簽名 Signature Delegate
- (void)saveSignatureImage:(UIImage *)signatureImage {
    //存到暫存位置
    if (isMySign) { //本人
        [self SaveMysignTempWithImage:signatureImage];
    }
    else if (isStoreOwnerSign) { //店長
        [self SaveStoreOwnersignTempWithImage:signatureImage];
    }
    else if (isBuyerSign) { //買方
        [self SaveBuyersignTempWithImage:signatureImage];
    }
    else if (isSalesSign) { //業務
        [self SaveSalessignTempWithImage:signatureImage];
    }
    else if (isLawSign) { //法定代理人
        [self SaveLawsignTempWithImage:signatureImage];
    }
    else if (isOwnerSign) { //原車主
        [self SaveOwnerSignTempWithImage:signatureImage];
    }
    else if (isNewOwnerSign) { //新車主
        [self SaveNewOwnerSignTempWithImage:signatureImage];
    }
    else if (isOwnerDelegateSign) { //原車主委託人
        [self SaveOwnerDelegateSignTempWithImage:signatureImage];
    }
    else if (isNewOwnerDelegateSign) { //新車主委託人
        [self SaveNewOwnerDelegateSignTempWithImage:signatureImage];
    }
    
    //如果不需簽名，或為非必填將狀態設為已簽
    if (MasterSign == 0 || MasterSign == 1) {
        didStoreOwnerSigned = YES;
    }
    if (SalesSign == 0 || SalesSign == 1) {
        didSalesSigned = YES;
    }
    if (BuySign == 0 || BuySign == 1) {
        didBuyerSigned = YES;
    }
    if (AgentSign == 0 || AgentSign == 1) {
        didLawSigned = YES;
    }
    if (CustomerSign == 0 || CustomerSign == 1) {
        didMySigned = YES;
    }
    if (OwnerSign == 0 || OwnerSign == 1) {
        didOwnerSigned = YES;
    }
    if (NewOwnerSign == 0 || NewOwnerSign == 1) {
        didNewOwnerSigned = YES;
    }
    if (OwnerDelegateSign == 0 || OwnerDelegateSign == 1) {
        didOwnerDelegateSigned = YES;
    }
    if (NewOwnerDelegateSign == 0 || NewOwnerDelegateSign == 1) {
        didNewOwnerDelegateSigned = YES;
    }
    
    //判斷是不是都已簽名或打勾
    if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE ||
        _enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL ||
        _enumCONSENT == CONSENT_ORDER_CUSTOMER ||
        _enumCONSENT == CONSENT_PERSONAL_Data) {
        if (_dicTranfer)
        {
            //過戶管理
            if (!didSendButtonShow && didOwnerSigned && didNewOwnerSigned && didOwnerDelegateSigned && didNewOwnerDelegateSigned && didLawSigned) {
                [self ShowSendBtn];
            }
        }
        else
        {
            if (!didSendButtonShow && didStoreOwnerSigned && didSalesSigned && didBuyerSigned && didLawSigned && didMySigned) {
                [self ShowSendBtn];
            }
        }
    }
}

- (void)didSignatureSigned:(BOOL)didSigned {
    if (_enumCONSENT == CONSENT_TEST_RIDE ||  //試駕
        _enumCONSENT == CONSENT_ORDER_DEDUCT_CONTRACT) {
        if (didSigned) { //按下打勾
            //顯示送出按鈕
            [self ShowSendBtn];
        }
        else { //按下x
            [self DeleteMysignTemp]; //刪除暫存
            [self HideSendBtn];
        }
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE || //訂購Gororo所需簽訂文件
             _enumCONSENT == CONSENT_ORDER_CUSTOMER || //購買契約書
             _enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL || //合約用_個資同意書
             _enumCONSENT == CONSENT_PERSONAL_Data) { //個資同意書
        if (didSigned) { //按下打勾
            if (isLawSign) { //法定代理人可以不簽名就視為已確認
                didLawSigned = YES;
            }
        }
        else { //按下x
            //刪除目前的簽名暫存檔並隱藏送出按鈕
            [self DeleteCurrentSign];
        }
    }
    else if (_enumCONSENT == CONSENT_CONTRACT_RESIGN) {
        if (didSigned) { //按下打勾
            //動態判斷簽名狀況是否已完成
            NSInteger countShouldSign = 0;
            if (MasterSign == 2) { //店長 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (SalesSign == 2) { //銷售人員 簽名 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (BuySign == 2) { //買方 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (AgentSign == 2) { //法定代理人 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (CustomerSign == 2) { //立同意書人 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (OwnerSign == 2) { //原車主 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (NewOwnerSign == 2) { //新車主 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (OwnerDelegateSign == 2) { //原車主委託人 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            if (NewOwnerDelegateSign == 2) { //新車主委託人 0:無,1:非必填,2: 必填
                countShouldSign++;
            }
            NSLog(@"應有的簽名數:%ld",countShouldSign);
            
            NSInteger didShouldSign = 0;
            if (didStoreOwnerSigned && MasterSign == 2) { //店長
                didShouldSign++;
            }
            if (didSalesSigned && SalesSign == 2) { //銷售人員
                didShouldSign++;
            }
            if (didBuyerSigned && CustomerSign == 2) { //立同意書人
                didShouldSign++;
            }
            if (didLawSigned && AgentSign == 2) { //法定代理人
                didShouldSign++;
            }
            if (didMySigned && BuySign == 2) { //立同意書人
                didShouldSign++;
            }
            
            if (didOwnerSigned && OwnerSign == 2) { //原車主
                didShouldSign++;
            }
            if (didNewOwnerSigned && NewOwnerSign == 2) { //新車主
                didShouldSign++;
            }
            if (didOwnerDelegateSigned && OwnerDelegateSign == 2) { //原車主委託人
                didShouldSign++;
            }
            if (didNewOwnerDelegateSigned && NewOwnerDelegateSign == 2) { //新車主委託人
                didShouldSign++;
            }
            NSLog(@"已簽的簽名數:%ld",didShouldSign);
            
            if (countShouldSign > 0 && (countShouldSign == didShouldSign)) {
                [self ShowSendBtn];
            }
        }
        else { //按下x
            //刪除目前的簽名暫存檔並隱藏送出按鈕
            [self DeleteCurrentSign];
        }
    }
}
-(void) ShowSendBtn { //下方確認按鈕呈現
    if (didSendButtonShow) { //防呆
        return;
    }
    
    didSendButtonShow = YES;
    
    [UIView animateWithDuration:1 animations:^{
        signVC.view.frame = CGRectOffset(signVC.view.frame, 0, -_sentButton.frame.size.height);
        signatureButton.frame = CGRectOffset(signatureButton.frame, 0, -_sentButton.frame.size.height);
        _SignatureSkipButton.frame = CGRectOffset(signatureButton.frame, 0, -_sentButton.frame.size.height);
        _sentButton.frame = CGRectOffset(_sentButton.frame, 0, -_sentButton.frame.size.height);
        _MySignButton.frame = CGRectOffset(_MySignButton.frame, 0, -_sentButton.frame.size.height);
        _LawSignButton.frame = CGRectOffset(_LawSignButton.frame, 0, -_sentButton.frame.size.height);
        _StoreOwnerSignButton.frame = CGRectOffset(_StoreOwnerSignButton.frame, 0, -_sentButton.frame.size.height);
        _BuyerSignButton.frame = CGRectOffset(_BuyerSignButton.frame, 0, -_sentButton.frame.size.height);
        _SalesSignButton.frame = CGRectOffset(_SalesSignButton.frame, 0, -_sentButton.frame.size.height);
        _TransferOriginalOwnerSignButton.frame = CGRectOffset(_TransferOriginalOwnerSignButton.frame, 0, -_sentButton.frame.size.height);
        _TransferOriginalEntrustSignButton.frame = CGRectOffset(_TransferOriginalEntrustSignButton.frame, 0, -_sentButton.frame.size.height);
        _TransferNewOwnerSignButton.frame = CGRectOffset(_TransferNewOwnerSignButton.frame, 0, -_sentButton.frame.size.height);
        _TransferNewEntrustSignButton.frame = CGRectOffset(_TransferNewEntrustSignButton.frame, 0, -_sentButton.frame.size.height);
        
        if (_enumCONSENT == CONSENT_CONTRACT_NEED_SIGNATURE) { //
            _ViewPreviousNext.frame = CGRectOffset(_ViewPreviousNext.frame, 0, -_ViewPreviousNext.frame.size.height);
        }
    }];
}
-(void) HideSendBtn { //隱藏送出按鈕
    if (!didSendButtonShow) { //防呆
        return;
    }
    
    didSendButtonShow = NO;
    
    [UIView animateWithDuration:1 animations:^{
        signVC.view.frame = CGRectOffset(signVC.view.frame, 0, _sentButton.frame.size.height);
        signatureButton.frame = CGRectOffset(signatureButton.frame, 0, _sentButton.frame.size.height);
        _sentButton.frame = CGRectOffset(_sentButton.frame, 0, _sentButton.frame.size.height);
        _MySignButton.frame = CGRectOffset(_MySignButton.frame, 0, _sentButton.frame.size.height);
        _LawSignButton.frame = CGRectOffset(_LawSignButton.frame, 0, _sentButton.frame.size.height);
        _StoreOwnerSignButton.frame = CGRectOffset(_StoreOwnerSignButton.frame, 0, _sentButton.frame.size.height);
        _BuyerSignButton.frame = CGRectOffset(_BuyerSignButton.frame, 0, _sentButton.frame.size.height);
        _SalesSignButton.frame = CGRectOffset(_SalesSignButton.frame, 0, _sentButton.frame.size.height);
        _TransferOriginalOwnerSignButton.frame = CGRectOffset(_TransferOriginalOwnerSignButton.frame, 0, _sentButton.frame.size.height);
        _TransferOriginalEntrustSignButton.frame = CGRectOffset(_TransferOriginalEntrustSignButton.frame, 0, _sentButton.frame.size.height);
        _TransferNewOwnerSignButton.frame = CGRectOffset(_TransferNewOwnerSignButton.frame, 0, _sentButton.frame.size.height);
        _TransferNewEntrustSignButton.frame = CGRectOffset(_TransferNewEntrustSignButton.frame, 0, _sentButton.frame.size.height);
        
        if (_enumCONSENT == CONSENT_ORDER_CONTRACT_PERSONAL) { //合約用_個資同意書
            _ViewPreviousNext.frame = CGRectOffset(_ViewPreviousNext.frame, 0, _ViewPreviousNext.frame.size.height);
        }
        else if (_enumCONSENT == CONSENT_ORDER_CUSTOMER) { //36 訂購契約
            _ViewPreviousNext.frame = CGRectOffset(_ViewPreviousNext.frame, 0, _ViewPreviousNext.frame.size.height);
        }
    }];
}
#pragma mark - 下一步按鈕顯示/隱藏
- (void) ShowOrHideNextButton:(BOOL)didShow {
    if (didShow && !didNextButtonShow) { //下一步按鈕呈現
        didNextButtonShow = YES;
        _SkipButton.hidden = YES;
        [UIView animateWithDuration:1 animations:^{
            _NextButton.frame = CGRectOffset(_NextButton.frame, 0, -_NextButton.frame.size.height);
        }];
    }
    else if (!didShow && didNextButtonShow) { //隱藏下一步按鈕
        didNextButtonShow = NO;
        _SkipButton.hidden = NO;
        [UIView animateWithDuration:1 animations:^{
            _NextButton.frame = CGRectOffset(_NextButton.frame, 0, _NextButton.frame.size.height);
        }];
    }
}
#pragma mark - 暫存檔處理
-(void) DeleAllTempImageFile {
    [self DeleteMysignTemp];
    [self DeleteLawsignTemp];
    [self DeleteDriverLicenseTemp];
    [self DeleteBuyersignTemp];
    [self DeleteStoreOwnersignTemp];
    [self DeleteSalessignTemp];
    [self DeleteOwnerSignTemp];
    [self DeleteNewOwnerSignTemp];
    [self DeleteOwnerDelegateSignTemp];
    [self DeleteNewOwnerDelegateSignTemp];
}
-(void) SaveCurrentSign { //把目前的簽名存起來
    [self saveSignatureImage:signVC.mainImageView.image];
}
-(void) DeleteCurrentSign { //刪除目前的簽名
    if (isMySign) { //本人
        [self DeleteMysignTemp];
    }
    else if (isStoreOwnerSign) { //店長
        [self DeleteStoreOwnersignTemp];
    }
    else if (isBuyerSign) { //買方
        [self DeleteBuyersignTemp];
    }
    else if (isSalesSign) { //業務
        [self DeleteSalessignTemp];
    }
    else if (isLawSign) { //法定代理人
        [self DeleteLawsignTemp];
    }
    else if (isOwnerSign) { //原車主
        [self DeleteOwnerSignTemp];
    }
    else if (isNewOwnerSign) { //新車主
        [self DeleteNewOwnerSignTemp];
    }
    else if (isOwnerDelegateSign) { //原車主委託人
        [self DeleteOwnerDelegateSignTemp];
    }
    else if (isNewOwnerDelegateSign) { //新車主委託人
        [self DeleteNewOwnerDelegateSignTemp];
    }
    
    if (didSendButtonShow) {
        [self HideSendBtn];
    }
}
-(void) LoadTempImageWithString:(NSString *)string { //載入暫存的簽名檔
    //存目前的簽名
    [self SaveCurrentSign];
    
    //清空目前畫板上的簽名
    signVC.mainImageView.image = nil;
    
    //載入個人簽名的暫存（如果有的話）
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", string]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        UIImage *img = [UIImage imageWithContentsOfFile:imagePath];
        signVC.mainImageView.image = img;
        
        //打勾按鈕
        [signVC.saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes.png"] forState:UIControlStateNormal];
    }
    else {
        //打勾按鈕
        [signVC.saveButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_yes_grey.png"] forState:UIControlStateNormal];
    }
}
-(void) DeleteMysignTemp { //刪除暫存檔
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempMySign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存簽名檔失敗:%@",error);
        }
        else {
            didMySigned = NO;
            NSLog(@"刪除暫存簽名檔成功");
        }
    }
}
-(void) SaveMysignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempMySign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存個人簽名檔圖片成功:" : @"儲存個人簽名檔圖片失敗:"),imagePath);
    if (result) {
        didMySigned = YES;
    }
}
-(void) DeleteLawsignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempLawSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存法定代理簽名檔失敗:%@",error);
        }
        else {
            didLawSigned = NO;
            NSLog(@"刪除暫存法定代理簽名檔成功");
        }
    }
}
-(void) SaveLawsignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempLawSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存法定代理簽名檔圖片成功:" : @"儲存法定代理簽名檔圖片失敗:"),imagePath);
    //if (result) {
        didLawSigned = YES;
    //}
    //else {
    //}
}
-(void) DeleteStoreOwnersignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempStoreOwnerSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存店長簽名檔失敗:%@",error);
        }
        else {
            didStoreOwnerSigned = NO;
            NSLog(@"刪除暫存店長簽名檔成功");
        }
    }
}
-(void) SaveStoreOwnersignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempStoreOwnerSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存店長簽名檔圖片成功:" : @"儲存店長簽名檔圖片失敗:"),imagePath);
    if (result) {
        didStoreOwnerSigned = YES;
    }
}
-(void) DeleteBuyersignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempBuyerSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存買方簽名檔失敗:%@",error);
        }
        else {
            didBuyerSigned = NO;
            NSLog(@"刪除暫存買方簽名檔成功");
        }
    }
}
-(void) SaveBuyersignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempBuyerSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存買方簽名檔圖片成功:" : @"儲存買方簽名檔圖片失敗:"),imagePath);
    if (result) {
        didBuyerSigned = YES;
    }
}
-(void) DeleteSalessignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempSalesSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存業務簽名檔失敗:%@",error);
        }
        else {
            didSalesSigned = NO;
            NSLog(@"刪除暫存業務簽名檔成功");
        }
    }
}
-(void) SaveSalessignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempSalesSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存業務簽名檔圖片成功:" : @"儲存業務簽名檔圖片失敗:"),imagePath);
    if (result) {
        didSalesSigned = YES;
    }
}
-(void) DeleteDriverLicenseTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempDriveLicense"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存駕照檔失敗:%@",error);
        }
        else {
            NSLog(@"刪除暫存駕照檔成功");
        }
    }
}
-(void) DeleteOwnerSignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempOwnerSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存原車主簽名檔失敗:%@",error);
        }
        else {
            didOwnerSigned = NO;
            NSLog(@"刪除暫存原車主簽名檔成功");
        }
    }
}
-(void) SaveOwnerSignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempOwnerSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存原車主簽名檔圖片成功:" : @"儲存原車主簽名檔圖片失敗:"),imagePath);
    if (result) {
        didOwnerSigned = YES;
    }
}
-(void) DeleteNewOwnerSignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempNewOwnerSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存新車主簽名檔失敗:%@",error);
        }
        else {
            didNewOwnerSigned = NO;
            NSLog(@"刪除暫存新車主簽名檔成功");
        }
    }
}
-(void) SaveNewOwnerSignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempNewOwnerSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存原車主簽名檔圖片成功:" : @"儲存原車主簽名檔圖片失敗:"),imagePath);
    if (result) {
        didNewOwnerSigned = YES;
    }
}
-(void) DeleteOwnerDelegateSignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempOwnerDelegateSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存原車主委託人簽名檔失敗:%@",error);
        }
        else {
            didOwnerDelegateSigned = NO;
            NSLog(@"刪除暫存原車主委託人簽名檔成功");
        }
    }
}
-(void) SaveOwnerDelegateSignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempOwnerDelegateSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存原車主委託人簽名檔圖片成功:" : @"儲存原車主委託人簽名檔圖片失敗:"),imagePath);
    if (result) {
        didOwnerDelegateSigned = YES;
    }
}
-(void) DeleteNewOwnerDelegateSignTemp {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempNewOwnerDelegateSign"]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:imagePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:imagePath error:&error];
        if (error) {
            NSLog(@"刪除暫存新車主委託人簽名檔失敗:%@",error);
        }
        else {
            didNewOwnerDelegateSigned = NO;
            NSLog(@"刪除暫存新車主委託人簽名檔成功");
        }
    }
}
-(void) SaveNewOwnerDelegateSignTempWithImage:(UIImage *)image {
    //存到暫存位置
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", @"TempNewOwnerDelegateSign"]];
    NSData* data = UIImageJPEGRepresentation(image, 1.0f);
    BOOL result = [data writeToFile:imagePath atomically:YES];
    NSLog(@"%@ %@", (result? @"儲存新車主委託人簽名檔圖片成功:" : @"儲存新車主委託人簽名檔圖片失敗:"),imagePath);
    if (result) {
        didNewOwnerDelegateSigned = YES;
    }
}

#pragma mark - 簽名身分切換
-(IBAction) pressMySignBtn:(id)sender { //個人簽名
    if (isMySign) { //目前個人簽名，不作動
        return;
    }
    //存入圖片
    [self LoadTempImageWithString:@"TempMySign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressLawSignBtn:(id)sender { //法定代理人簽名
    if (isLawSign) { //目前法定代理人簽名，不作動
        return;
    }
    
    [self LoadTempImageWithString:@"TempLawSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressStoreOwnerSignBtn:(id)sender { //店長簽名
    if (isStoreOwnerSign) { //同個簽名，不作動
        return;
    }
    
    [self LoadTempImageWithString:@"TempStoreOwnerSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressBuyerSignBtn:(id)sender { //買方簽名
    if (isBuyerSign) { //同個簽名，不作動
        return;
    }
    
    [self LoadTempImageWithString:@"TempBuyerSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressSalesSignBtn:(id)sender { //業務簽名
    if (isSalesSign) { //同個簽名，不作動
        return;
    }
    
    [self LoadTempImageWithString:@"TempSalesSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressOwnerSignBtn:(id)sender { //原車主簽名
    if (isOwnerSign) { //目前原車主簽名，不作動
        return;
    }
    //存入圖片
    [self LoadTempImageWithString:@"TempOwnerSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressNewOwnerSignBtn:(id)sender { //新車主簽名
    if (isNewOwnerSign) { //目前新車主簽名，不作動
        return;
    }
    //存入圖片
    [self LoadTempImageWithString:@"TempNewOwnerSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressOwnerDelegateSignBtn:(id)sender { //原車主委託人簽名
    if (isOwnerDelegateSign) { //目前個人簽名，不作動
        return;
    }
    //存入圖片
    [self LoadTempImageWithString:@"TempOwnerDelegateSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(IBAction) pressNewOwnerDelegateSignBtn:(id)sender { //新車主委託人簽名
    if (isNewOwnerDelegateSign) { //目前新車主委託人簽名，不作動
        return;
    }
    //存入圖片
    [self LoadTempImageWithString:@"TempNewOwnerDelegateSign"];
    [self signatureButtonPressedChangeUIAndStatus:sender];
    signVC.isLawSign = isLawSign;
}
-(void)signatureButtonPressedChangeUIAndStatus:(id)pressedButton
{
    //設定一次為選擇的狀態
    isMySign = NO;
    isLawSign = NO;
    isStoreOwnerSign = NO;
    isBuyerSign = NO;
    isSalesSign = NO;
    
    isOwnerSign = NO;
    isNewOwnerSign = NO;
    isOwnerDelegateSign = NO;
    isNewOwnerDelegateSign = NO;

    //生成一變未按下的狀態
    if (attachType == 107)
    {
        [_MySignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_buyername.png"] forState:UIControlStateNormal];
    }
    else
    {
        [_MySignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_lishu.png"] forState:UIControlStateNormal];
    }
    [_LawSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_fadin.png"] forState:UIControlStateNormal];
    [_StoreOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_manager.png"] forState:UIControlStateNormal];
    [_BuyerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_buyername.png"] forState:UIControlStateNormal];
    [_SalesSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_sales.png"] forState:UIControlStateNormal];
    
    [_TransferOriginalOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar.png"] forState:UIControlStateNormal];
    
    //必填(2) > 委託人的切圖
    //非必填(1) > 代理人的切圖
    //無(0) > 不顯示任何切圖
    if (OwnerDelegateSign == 2) { //委託人
        [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_p.png"] forState:UIControlStateNormal];
    }
    else { //代理人
        [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_agent.png"] forState:UIControlStateNormal];
    }
    
    [_TransferNewOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar.png"] forState:UIControlStateNormal];
    
    //必填(2) > 委託人的切圖
    //非必填(1) > 代理人的切圖
    //無(0) > 不顯示任何切圖
    if (NewOwnerDelegateSign == 2) { //委託人
        [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_person.png"] forState:UIControlStateNormal];
    }
    else { //代理人
        [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_agent.png"] forState:UIControlStateNormal];
    }
    
    
    //更改按下後圖片
    if (pressedButton == _MySignButton)
    {
        isMySign = YES;
        if (attachType == 107)
        {
            [_MySignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_buyername_b.png"] forState:UIControlStateNormal];
        }
        else
        {
            [_MySignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_lishu_b.png"] forState:UIControlStateNormal];
        }
    }
    if (pressedButton == _LawSignButton) {
        isLawSign = YES;
        [_LawSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_fadin_b.png"] forState:UIControlStateNormal];
    }
    if (pressedButton == _StoreOwnerSignButton) {
        isStoreOwnerSign = YES;
        [_StoreOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_manager_b.png"] forState:UIControlStateNormal];
    }
    if (pressedButton == _BuyerSignButton) {
        isBuyerSign = YES;
        [_BuyerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_buyername_b.png"] forState:UIControlStateNormal];
    }
    if (pressedButton == _SalesSignButton) {
        isSalesSign = YES;
        [_SalesSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_sales_b.png"] forState:UIControlStateNormal];
    }
    
    if (pressedButton == _TransferOriginalOwnerSignButton) {
        isOwnerSign = YES;
        [_TransferOriginalOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_f.png"] forState:UIControlStateNormal];
    }
    if (pressedButton == _TransferOriginalEntrustSignButton) {
        isOwnerDelegateSign = YES;
        
        //必填(2) > 委託人的切圖
        //非必填(1) > 代理人的切圖
        //無(0) > 不顯示任何切圖
        if (OwnerDelegateSign == 2) { //委託人
            [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_p_f.png"] forState:UIControlStateNormal];
        }
        else { //代理人
            [_TransferOriginalEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_originalcar_agent_f.png"] forState:UIControlStateNormal];
        }
    }
    if (pressedButton == _TransferNewOwnerSignButton) {
        isNewOwnerSign = YES;
        [_TransferNewOwnerSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_f.png"] forState:UIControlStateNormal];
    }
    if (pressedButton == _TransferNewEntrustSignButton) {
        isNewOwnerDelegateSign = YES;
        //必填(2) > 委託人的切圖
        //非必填(1) > 代理人的切圖
        //無(0) > 不顯示任何切圖
        if (NewOwnerDelegateSign == 2) { //委託人
            [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_person_f.png"] forState:UIControlStateNormal];
        }
        else { //代理人
            [_TransferNewEntrustSignButton setBackgroundImage:[UIImage imageNamed:@"btn_sign_newcar_agent_f.png"] forState:UIControlStateNormal];
        }
    }
}
#pragma mark - 換頁
-(void) GotoNextContractWithAttachType:(int)nowAttachType andEnumCONSENT:(int)enumCONSENT {
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.newAttachType = nowAttachType;
    destVC.enumCONSENT = enumCONSENT;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    destVC.nowSeq = _nowSeq+1;
    destVC.contractArray = _contractArray;
    destVC.dicTranfer = _dicTranfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoOrder { //3.2 撈取訂單
    GetOrderVC *destVC = [[GetOrderVC alloc] initWithNibName:@"GetOrderVC" bundle:nil];
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoOrderContract { //3.4到訂購契約
    //到補助選擇
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_ORDER_CUSTOMER;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    destVC.deductDocumentUrl = self.deductDocumentUrl;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoOrderPersonal { //3.4訂購契約 //合約用_個資同意書
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_ORDER_CONTRACT_PERSONAL;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    destVC.deductDocumentUrl = self.deductDocumentUrl;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoDeductContract { //3.6訂購契約 //補助內扣款切結書
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_ORDER_DEDUCT_CONTRACT;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    destVC.deductDocumentUrl = self.deductDocumentUrl;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoSelectSubsidyVC { //退回補助選擇
    //到補助選擇
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[SelectSubsidyVC class]]) {
            destVC = (SelectSubsidyVC *)vc;
        }
    }
    NSLog(@"destVC:%@",destVC);
    [self.navigationController popToViewController:destVC animated:NO];
}
-(void) GotoSignedOrderContractPersonal { //到已簽名的個資同意書
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_SIGNED_ORDER_CONTRACT_PERSONAL;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoSignedOrderCustomer { //到已簽名的已簽名的訂購單
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_SIGNED_ORDER_CUSTOMER;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoSignedOrderDeductContract{ //到已簽名的(放棄)補助切結書
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.enumCONSENT = CONSENT_SIGNED_ORDER_DEDUCT_CONTRACT;
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    destVC.dicSubsidy = _dicSubsidy;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoConfirmInfoVC { //3.6確認資訊
    ConfirmInfoVC *destVC = [[ConfirmInfoVC alloc] initWithNibName:@"ConfirmInfoVC" bundle:nil];
    destVC.OrderNo = _OrderNo;
    destVC.CustomerName = _CustomerName;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) PromptAlertBeforeGotoConfirmInfoVC {
    //2016.11.02 ，在最後一份要簽名的文件簽完送出 / 或是跳過的時候，跳出一個內建的提示，問他要不要寄信嗎，是call這支帶null否就繼
    NSString *message = @"本次簽核文件：";
    for (NSInteger i = 0; i < [_contractArray count]; i++) {
        NSDictionary *_dic = [_contractArray objectAtIndex:i];
        message = [message stringByAppendingFormat:@"\n%ld. %@",i+1,[_dic objectForKey:@"AttachName"]];
    }
    message = [message stringByAppendingString:@"\n請確認是否要寄出系統上所有最新版本、已簽核的電子文件？(含過去簽署過的文件)"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定"style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self callSendOrderContractMail];//呼叫105後台發信
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消"style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        [self ShowSignedDocument]; //都簽完，看已簽文件
    }];
    [alertController addAction:submitAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
-(void) ShowSignedDocument { //都簽完，看已簽文件
    if ([_contractArray count] == 0) { //所有文件都跳過
        if (_dicTranfer) { //過戶管理
            //到6.1.8 確認文件清單
            [self GotoConfirmDocumentList];
        }
        else {
            [self GotoConfirmInfoVC]; //到3.6確認資訊
        }
        
    }
    else { //從第一份已簽的文件開始看
        if (_dicTranfer) { //過戶管理
            //在呼叫一次[605] 取得租約轉讓需簽名同意書及順序
            //取得已簽文件網址
            [self callGetTransferContractList];
        }
        else {
            _nowSeq = -1;
            int firstAttachType = -1;
            if ([_contractArray count] > 0) {
                NSDictionary *_dic = [_contractArray objectAtIndex:0];
                firstAttachType = [[_dic objectForKey:@"AttachType"] intValue];
            }
            
            [self GotoNextContractWithAttachType:firstAttachType andEnumCONSENT:CONSENT_CONTRACT_REVIEW_SIGNED];
        }
    }
}
#pragma mark 簽名文件順序邏輯
- (int)checkNowContractWithSeq:(int)sequence {
    if (sequence >= _contractArray.count) {
        return END_SIGNING;
    }
    else {
        NSDictionary *dic = [_contractArray objectAtIndex:sequence];
        int AttachType = [[dic objectForKey:@"AttachType"] intValue];
        return AttachType;
    }
    
    return END_SIGNING;
    /*for (int i = 0; i < _contractArray.count; i++) {
        NSDictionary *dic = _contractArray[i];
        if ([dic[@"Seq"] intValue] == sequence) {
            return [dic[@"AttachType"] intValue];
        }
    }
    
    if (sequence > _contractArray.count) {
        return END_SIGNING;
    }
    
    return END_SIGNING;*/
}
-(int) GetNextAttachTypeWithSeq:(int)sequence { //取得下一個文件的AttachType
    int currentAttachType = [self checkNowContractWithSeq:_nowSeq];
    NSInteger index = -1;
    for (int i = 0; i < _contractArray.count; i++) {
        NSDictionary *dic = _contractArray[i];
        if ([dic[@"AttachType"] intValue] == currentAttachType) {
            index = i;
        }
    }
    
    //找到目前的
    if (index >= 0) {
        //有下一個合約
        if (_contractArray.count > index + 1) {
            NSDictionary *_dic = [_contractArray objectAtIndex:index + 1];
            return [[_dic objectForKey:@"AttachType"] intValue];
        }
        else { //沒有下一個合約
            return END_SIGNING;
        }
    }
    
    return END_SIGNING;
}
-(void) GotoTransferConfirmDMSVC { //6.1.4-1 確認DMS過戶資料(車籍)
    TransferConfirmDMSVC *destVC = [[TransferConfirmDMSVC alloc] initWithNibName:@"TransferConfirmDMSVC" bundle:nil];
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    destVC.stringCarNumber = _stringCarNumber;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark 過戶管理
-(void) GotoConfirmDocumentList { //6.1.8 確認文件清單
    TransferConfirmDocumentListVC *destVC = [[TransferConfirmDocumentListVC alloc] initWithNibName:@"TransferConfirmDocumentListVC" bundle:nil];
    destVC.dicTranfer = _dicTranfer;
    destVC.stringCarNumber = _stringCarNumber;
    destVC.stringOriginalOwnerID = _stringOriginalOwnerID;
    [self.navigationController pushViewController:destVC animated:YES];
}
#pragma mark 自訂文件
-(void) GotoSignedDocumentWithUrlString:(NSString *)urlString { //1.5預覽已簽核文件
    ConsentVC *destVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
    destVC.consentUrlStr = urlString;
    destVC.stringTitle = _stringTitle;
    destVC.enumCONSENT = CONSENT_URL_ONLY;
    destVC.showPrintButton = NO;
    destVC.showFinishButton = YES;
    destVC.FromTempUrl = NO;
    destVC.arrayBuyerNames = nil;
    destVC.newAttachType = attachType;
    destVC.customerData = _customerData;
    [self.navigationController pushViewController:destVC animated:YES];
}
-(void) GotoCustomerDocumentListOrSignVC { //1.5預覽已簽核文件完畢後返回1.1客戶資料比對或1.3選擇簽核文件
    NSArray *array = self.navigationController.viewControllers;
    UIViewController *destVC = nil;
    
    CustomDocumentVC *sourceVC = nil;
    
    for (id vc in array) {
        NSLog(@"vc:%@",vc);
        if ([vc isKindOfClass:[CustomDocumentVC class]]) {
            sourceVC = vc;
        }
        if ([vc isKindOfClass:[CustomDocumentListVC class]] ||
            [vc isKindOfClass:[CustomerDocumentSignVC class]]) {
            destVC = vc;
        }
    }
    NSLog(@"destVC:%@",destVC);
    if (!destVC) {
        //表示是新增客戶（從1.0或2.0來的)，回去之後到下個頁面
        sourceVC.customerDataAfterCreateCustomer = _customerData;
        sourceVC.shouldGotoNextFunctionAfterCreateCustomer = YES;
        [self.navigationController popToViewController:sourceVC animated:NO];
    }
    else {
        //從1.1或1.3來的
        [self.navigationController popToViewController:destVC animated:NO];
    }
}
#pragma mark - UIAlertController Handle
-(void) ShowBuyerNameSelect {
    NSString *message = [NSString stringWithFormat:@"請選擇"];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    for (int i = 0; i < [_arrayBuyerNames count]; i++) {
        NSDictionary *_dic = [_arrayBuyerNames objectAtIndex:i];
        NSString *title = [_dic objectForKey:@"name"];
        
        [alertController addAction:
         [UIAlertAction actionWithTitle:title
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action) {
                                    [self alertControllerclickedButtonAtIndex:i];
                                }]];
    }
    
    
    //iPad only
    //[alertController setModalPresentationStyle:UIModalPresentationPopover];
    UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
    popPresenter.sourceView = _ButtonEmail;
    popPresenter.sourceRect = _ButtonEmail.bounds;
    
    UIViewController *sourceVC = self;
    [sourceVC presentViewController:alertController animated:YES completion:NULL];
}
-(void) alertControllerclickedButtonAtIndex:(NSInteger)buttonIndex
{
    //NSLog(@"buttonIndex:%ld",(long)buttonIndex);
    
    NSDictionary *_dic = [_arrayBuyerNames objectAtIndex:buttonIndex];
    
    intMailTarget = [[_dic objectForKey:@"mailTarget"] integerValue];
    
    [self callSendOrderContractMail];
}
#pragma mark - 雜項
-(void) HideLoadingView
{
    [ObjectManager hideHUDForView:self.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
