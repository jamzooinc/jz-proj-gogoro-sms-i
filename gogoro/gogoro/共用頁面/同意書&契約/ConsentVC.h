//
//  ConsentVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DocumentManager.h"

@interface ConsentVC : TemplateVC <UIWebViewDelegate,UIScrollViewDelegate>

@property (nonatomic,strong) NSString* consentUrlStr;
@property (nonatomic,strong) NSString* deductDocumentUrl;               //切結書網址（過場文件時呼叫API307時取得，傳遞到切結書文件時顯示）
@property (nonatomic,strong) NSString *transferResignDocumentUrl;       //移轉重簽文件網址
@property (nonatomic) enumCONSENT enumCONSENT;
@property (nonatomic,strong) NSString *stringFolder;
@property (nonatomic,strong) NSString *customerID;
@property (nonatomic,strong) NSMutableDictionary *customerData;
@property (nonatomic,strong) NSString *OrderNo;
@property (nonatomic,strong) NSString *CustomerName;
@property (nonatomic,strong) NSMutableDictionary *dicSubsidy;           //傳給後台的補助選擇資料
@property (nonatomic,strong) NSString *stringTitle;                     //標題
@property BOOL showPrintButton;                                         //是否要顯示列印按鈕
@property BOOL hideEmailButton;                                         //是否要隱藏列印按鈕
@property BOOL showFinishButton;                                        //是否要顯示完成按鈕(1.5已簽核文件)
@property BOOL isReSign;                                                //YES:重簽
@property BOOL FromTempUrl;                                             //用來判斷API 105的content type要怎麼帶
@property (nonatomic,strong) NSArray *arrayBuyerNames;
@property (nonatomic, assign) int nowSeq;                               //目前文件順序
@property (nonatomic, strong) NSMutableArray *contractArray;            //簽名文件
@property (nonatomic, assign) NSInteger newAttachType;
@property (nonatomic, strong) NSDictionary *dicTranfer;                 //過戶移轉的資料
@property (strong, nonatomic) NSString *stringOriginalOwnerID;          //原車主身份證字號
@property (strong, nonatomic) NSString *stringCarNumber;                //車牌號碼
@end


