//
//  TemplateVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemplateVCTitleView.h"

@interface TemplateVC : UIViewController <TemplateVCTitleViewDelegate,UIPrintInteractionControllerDelegate>

- (void)setupNavigationItemTitle:(NSString *)title Image:(UIImage *)image;
-(IBAction) PressBackBtn; //回上頁
-(void) ShowPrintVCWithData:(NSData *)data Rect:(CGRect)rect; //顯示原生Air Print介面
- (void)showAlertControllerWithMessage:(NSString *)message;
-(void) LeftButtonBack; //左側按鈕變成返回
- (UIImage*) ImageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
@end
