


#import "TemplateVCTitleView.h"

@implementation TemplateVCTitleView


@synthesize delegate;
#pragma mark - Delegate
-(void) TemplateVCTitleViewPressMenuBtn {
    if ([[self delegate] respondsToSelector:@selector(TemplateVCTitleViewPressMenuBtn)]) {
        [[self delegate] TemplateVCTitleViewPressMenuBtn];
    }
}
-(void) TemplateVCTitleViewPressHomeBtn {
    if ([[self delegate] respondsToSelector:@selector(TemplateVCTitleViewPressHomeBtn)]) {
        [[self delegate] TemplateVCTitleViewPressHomeBtn];
    }
}
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
    }
    return self;
}
-(void) UpdateUI {
    
    
}
-(void) LeftButtonBack { //左側按鈕變成返回
    _ImageLeft.image = [UIImage imageNamed:@"btn_back.png"];
    _ImageLeft.frame = CGRectMake(26, 24, 21, 34);
    
    TemplateVC *vc = (TemplateVC *)[ObjectManager viewController:self];
 
    [_ButtonLeft removeTarget:self action:@selector(PressMenuBtn) forControlEvents:UIControlEventTouchUpInside];
    [_ButtonLeft addTarget:vc  action:@selector(PressBackBtn) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - Button
-(IBAction) pressHomeBtn {
    [self TemplateVCTitleViewPressHomeBtn];
}
-(IBAction) PressMenuBtn {
    [self TemplateVCTitleViewPressMenuBtn];
}
@end
