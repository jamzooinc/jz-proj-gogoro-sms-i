
#import <UIKit/UIKit.h>

@protocol TemplateVCTitleViewDelegate <NSObject>
@optional
-(void) TemplateVCTitleViewPressMenuBtn;
-(void) TemplateVCTitleViewPressHomeBtn;
@end

@interface TemplateVCTitleView : UIView  {
    __weak id <TemplateVCTitleViewDelegate> delegate;
}
@property (weak) id delegate;
@property (nonatomic,weak) IBOutlet UILabel *LabelTitle;
@property (nonatomic,weak) IBOutlet UIImageView *ImageTitle;
@property (nonatomic,weak) IBOutlet UIButton *ButtonFinish;
@property (nonatomic,weak) IBOutlet UIImageView *ImageLeft;
@property (nonatomic,weak) IBOutlet UIButton *ButtonLeft;
-(void) UpdateUI;
-(void) LeftButtonBack; //左側按鈕變成返回
@end
