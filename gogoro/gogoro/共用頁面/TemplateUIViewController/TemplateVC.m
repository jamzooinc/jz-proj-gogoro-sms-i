//
//  TemplateVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "TemplateVC.h"

@interface TemplateVC () {

}
@property(strong,nonatomic)UILabel* titleLabel;
@property(strong,nonatomic)UIImageView* titleImageView;
@end

@implementation TemplateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self AddNotif];
    
    /* 隱藏/顯示NavigationBar */
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    /* NavigationBar */
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_titlebar"]]];


    /* 登入頁 & 首頁不顯示NavigationBar */
    if ([viewControllers count] <= 1) {
        self.navigationController.navigationBarHidden = YES;
    } else{
        //self.navigationController.navigationBarHidden = NO;
        self.navigationController.navigationBarHidden = YES;
    }
    
    /* 隱藏Back */
    if ([viewControllers count] <= 2) {
        self.navigationItem.hidesBackButton = YES;
    } else{
        self.navigationItem.hidesBackButton = NO;
    }


    /* Navigation Title*/
    /*UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-44*2, 44)];

    self.titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, titleView.frame.size.width-33, 20)];
    self.titleImageView.contentMode = UIViewContentModeCenter;
    self.titleImageView.image = [UIImage imageNamed:@"icn_first.png"];
    [titleView addSubview:self.titleImageView];

    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, titleView.frame.size.width-33, 24)];
    //self.titleLabel.font = [UIFont fontWithName:kFontFamilyBaseBold size:kFontLargeSize];
    self.titleLabel.text = @"首次參觀";
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [titleView addSubview:self.titleLabel];

    self.navigationItem.titleView = titleView;

    //側邊選單
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_slidemenu.png"] style:UIBarButtonItemStylePlain target:self action:@selector(leftDrawerButtonPress:)];
    leftDrawerButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = leftDrawerButton;
    
    //回首頁
    UIBarButtonItem* rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icn_home.png"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonClick:)];
    rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;*/

    //點擊收鍵盤
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];

    /* */
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_all"]];
}
-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self RemoveNotif];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Title View
-(void) TemplateVCTitleViewPressMenuBtn {
    [self leftDrawerButtonPress:nil];
}
-(void) TemplateVCTitleViewPressHomeBtn {
    [self rightBarButtonClick:nil];
}

#pragma mark - Status Bar
-(BOOL)prefersStatusBarHidden{
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark - Button Action

- (void)rightBarButtonClick:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)leftDrawerButtonPress:(id)leftDrawerButtonPress {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (void)setupNavigationItemTitle:(NSString *)title Image:(UIImage *)image {
    //Title
    NSArray *toplevels = [[NSBundle mainBundle] loadNibNamed:@"TemplateVCTitleView" owner:self options:nil];
    TemplateVCTitleView *titleView = [toplevels objectAtIndex:0];
    [titleView UpdateUI];
    titleView.delegate = self;
    titleView.LabelTitle.text = title;
    titleView.ImageTitle.image = image;
    [self.view addSubview:titleView];
    
    //沒圖檔的話，文字高度置中
    if (!image) {
        titleView.LabelTitle.center = CGPointMake(titleView.LabelTitle.center.x, titleView.frame.size.height/2);
    }
    
    /*CGFloat naviItemTitlelabelWidth = self.navigationController.navigationBar.frame.size.width / 2;
    CGFloat naviItemTitlelabelHeight = self.navigationController.navigationBar.frame.size.height;
   
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, naviItemTitlelabelWidth, naviItemTitlelabelHeight)];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, naviItemTitlelabelHeight / 2 - 4, naviItemTitlelabelWidth, 26)];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    
    self.navigationItem.titleView = view;*/
}
-(IBAction) PressBackBtn {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 左側按鈕變成返回
 */
-(void) LeftButtonBack { 
    
    for (id _subView in self.view.subviews) {
        if ([_subView isKindOfClass:[TemplateVCTitleView class]]) {
            TemplateVCTitleView *myView = (TemplateVCTitleView *)_subView;
            [myView LeftButtonBack];
        }
    }
}
#pragma mark - 收鍵盤
- (void)dismissKeyboard:(UITapGestureRecognizer *)tap{
    [self.view endEditing:YES];
}
#pragma mark - 顯示UIAlertController
- (void)showAlertControllerWithMessage:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知"
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"確定"
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
    [alertController addAction:submitAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - Air Print 相關
-(void) ShowPrintVCWithData:(NSData *)data Rect:(CGRect)rect { //顯示原生Air Print介面
    
    //參考資料
    //https://developer.apple.com/library/ios/documentation/2DDrawing/Conceptual/DrawingPrintingiOS/Printing/Printing.html#//apple_ref/doc/uid/TP40010156-CH12-SW7
    //If the current user-interface idiom is iPad, present the printing interface to the user by calling presentFromBarButtonItem:animated:completionHandler: or presentFromRect:inView:animated:completionHandler:; if the idiom is iPhone or iPod touch, call presentAnimated:completionHandler:. Alternatively, you can embed the printing UI into your existing UI by implementing a printInteractionControllerParentViewController: delegate method. If your app uses an activity sheet (in iOS 6.0 and later), you can also add a printing activity item.
    
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    if  (pic && [UIPrintInteractionController canPrintData: data] ) {
        pic.delegate = self;
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        //printInfo.jobName = [self.path lastPathComponent];
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = data;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            //self.content = nil;
            if (!completed && error)
                NSLog(@"FAILED! due to error in domain %@ with error code %ld",
                      error.domain, (long)error.code);
        };
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [pic presentFromRect:rect inView:self.view animated:YES completionHandler:completionHandler];
        }
        else {
            [pic presentAnimated:YES completionHandler:completionHandler];
        }
    }
}
- (void)printInteractionControllerDidFinishJob:(UIPrintInteractionController *)printInteractionController {
    NSLog(@"列印結束");
}
#pragma mark - Logout 
-(void) Logout {
    [self leftDrawerButtonPress:nil];
    [self rightBarButtonClick:nil];
}
#pragma mark - 圖檔處理
#pragma mark 【圖片大小調整】
- (UIImage*) ImageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize {
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}
#pragma mark - 雜項
-(void) AddNotif {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Logout) name:@"kNotification_Logout" object:nil];
}
-(void) RemoveNotif {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kNotification_Logout" object:nil];
}
@end
