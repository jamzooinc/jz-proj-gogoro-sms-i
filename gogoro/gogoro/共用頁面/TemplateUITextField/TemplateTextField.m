//
//  TemplateTextField.m
//  gogoro
//
//  Created by User on 2016/3/4.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "TemplateTextField.h"

@implementation TemplateTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect)textRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 20, 0);
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    return CGRectInset(bounds, 20, 0);
}

@end
