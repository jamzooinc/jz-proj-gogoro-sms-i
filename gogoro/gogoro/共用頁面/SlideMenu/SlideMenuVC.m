//
//  SlideMenuVC.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "SlideMenuVC.h"
#import "SlideMenuCell.h"
#import "WriteFirstDataVC.h"
#import "WriteRideDataVC.h"
#import "ConsentVC.h"
#import "SingleSupplementVC.h"
#import "TrackOrderVC.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "LoginVC.h"
#import "AccountInfoFooterView.h"
#import "UploadFileVC.h"
#import "TransferVC.h"
#import "CustomDocumentVC.h"
#import "SingleMainVC.h"
#import "UndoneOrderVC.h"

@interface SlideMenuVC () <UITableViewDataSource,UITableViewDelegate>
@end

@implementation SlideMenuVC
@synthesize mainTableView;
@synthesize slideMenuArray;
@synthesize imageArray;
@synthesize highlightImageArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initData];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [mainTableView reloadData];
}

#pragma mark - 初始化
#pragma init data
- (void)initData {
    slideMenuArray = @[@"首度參觀",
                       @"預約試乘",
                       @"訂購gogoro",
                       @"補件上傳",
                       @"我的訂單",
                       @"過戶移轉",
                       @"登出"],
    
    imageArray = [NSArray arrayWithObjects:
                  @"btn_slidemenu_first.png",
                  @"btn_slidemenu_res.png",
                  @"btn_slidemenu_order.png",
                  @"btn_slidemenu_buj.png",
                  @"btn_slidemenu_track.png",
                  @"btn_slidemenu_change.png",
                  @"btn_slidemenu_logout.png", nil];
    
    highlightImageArray = [NSArray arrayWithObjects:
                  @"btn_slidemenu_first_fb.png",
                  @"btn_slidemenu_res_fb.png",
                  @"btn_slidemenu_order_fb.png",
                  @"btn_slidemenu_buj_fb.png",
                  @"btn_slidemenu_track_fb.png",
                  @"btn_slidemenu_change_fb.png",
                  @"btn_slidemenu_logout_fb.png", nil];
}

- (void)initView {

}



#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return slideMenuArray.count;
}

/*-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth/2, ScreenHeight)];
    UIButton *headerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth/2, ScreenHeight)];
    [headerButton addTarget:self action:@selector(headerButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:headerButton];
    return headerView;
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"SlideMenuCell";
    SlideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SlideMenuCell" owner:nil options:nil] objectAtIndex: 0];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.titleLabel.text = slideMenuArray[indexPath.row];
    //NSLog(@"cell.titleLabel = %@",slideMenuArray[indexPath.row]);
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.ImageTitle.image = [UIImage imageNamed:imageArray[indexPath.row]];
    cell.normalImage = [UIImage imageNamed:imageArray[indexPath.row]];
    cell.highlightedImage = [UIImage imageNamed:highlightImageArray[indexPath.row]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if (![self CheckLogin]) { //檢查登入
        return;
    }*/
    
    if (indexPath.row == 0) {
        /* 1.1填寫個資表單 */
        /*WriteFirstDataVC *writeFirstDataVC = [[WriteFirstDataVC alloc] initWithNibName:@"WriteFirstDataVC" bundle:nil];
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:writeFirstDataVC animated:NO];*/
        CustomDocumentVC *destVC = [[CustomDocumentVC alloc] initWithNibName:@"CustomDocumentVC" bundle:nil];
        destVC.attachTypeId = 101; //（首次參觀:101, 預約試乘: 102）
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:destVC animated:NO];
    } else if (indexPath.row == 1) {
        /* 2.1填寫個資表單 */
        /*WriteRideDataVC *writeRideDataVC = [[WriteRideDataVC alloc] initWithNibName:@"WriteRideDataVC" bundle:nil];
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:writeRideDataVC animated:NO];*/
        CustomDocumentVC *destVC = [[CustomDocumentVC alloc] initWithNibName:@"CustomDocumentVC" bundle:nil];
        destVC.attachTypeId = 102; //（首次參觀:101, 預約試乘: 102）
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:destVC animated:NO];
    } else if (indexPath.row == 2) {
        /* 3.1觀看訂購契約書 */
        ConsentVC *consentVC = [[ConsentVC alloc] initWithNibName:@"ConsentVC" bundle:nil];
        consentVC.enumCONSENT = CONSENT_ORDER_PERSONAL_Data;
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:consentVC animated:NO];
    } else if (indexPath.row == 3) {
        /* 4.0補單畫面 */
        SingleMainVC *destVC = [[SingleMainVC alloc] initWithNibName:@"SingleMainVC" bundle:nil];
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:destVC animated:NO];
        //SingleSupplementVC *singleSupplementVC = [[SingleSupplementVC alloc] initWithNibName:@"SingleSupplementVC" bundle:nil];
        //[(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:singleSupplementVC animated:NO];
    } else if (indexPath.row == 4) {
        /* 5.0追蹤訂單 */
        UndoneOrderVC *destVC = [[UndoneOrderVC alloc] initWithNibName:@"UndoneOrderVC" bundle:nil];
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:destVC animated:NO];
        
        //TrackOrderVC *trackOrderVC = [[TrackOrderVC alloc] initWithNibName:@"TrackOrderVC" bundle:nil];
        //[(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:trackOrderVC animated:NO];
    } else if (indexPath.row == 5) {
        /* 6.0過戶移轉 */
        TransferVC *transferVC = [[TransferVC alloc] initWithNibName:@"TransferVC" bundle:nil];
        [(UINavigationController*)self.mm_drawerController.centerViewController pushViewController:transferVC animated:NO];
    } else {
        /* 登出 */
        [[UserManager shareInstance] clearAllUserData];
        //[self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kNotification_Logout" object:nil];
     
        //清除Google Drive憑證
        [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
    }

    //假設點擊前6個按鈕 導頁後收SlideMenu
    if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2|| indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5) {
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }

}

//Add new UI in 2.0 version by Justin in 2016/9/12
- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    AccountInfoFooterView *view = [[AccountInfoFooterView alloc]init];
    
    //TODO:Get data Info to show
    view.storeNameLabel.text = [UserManager shareInstance].storeName;
    view.staffAcountLabel.text = [UserManager shareInstance].account;
    view.staffNameLabel.text = [UserManager shareInstance].staffName;
    view.backgroundColor = [UIColor clearColor];
    return view ;
}

/*-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 150;
    
}*/

//-(BOOL) CheckLogin {
//    if ([UserManager shareInstance].isLogin) {
//       return YES;
//    }
//    else {
//        //提示要先登入
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"系統通知" message:@"請先登入Gogoro" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *actionConfirm = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            [self GotoLoginVC];
//        }];
//        
//        [alertController addAction:actionConfirm];
//        [self presentViewController:alertController animated:YES completion:^(void){
//        }];
//        
//        return NO;
//    }
//}
#pragma mark - Customed Method
- (IBAction)headerButtonClick:(id)sender {
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    [(UINavigationController *)self.mm_drawerController.centerViewController popToRootViewControllerAnimated:YES];
}
#pragma mark - 換頁
-(void) GotoLoginVC {
    LoginVC *destVC = [[LoginVC alloc] initWithNibName:@"LoginVC" bundle:nil];
    [self presentViewController:destVC animated:NO completion:nil];
}
#pragma mark - Status Bar
-(BOOL)prefersStatusBarHidden{
    return YES;
}
@end
