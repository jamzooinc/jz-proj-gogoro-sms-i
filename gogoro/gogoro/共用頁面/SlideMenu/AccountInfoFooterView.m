//
//  AccountInfoFooterView.m
//  gogoro
//
//  Created by 張啟裕 on 2016/9/13.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "AccountInfoFooterView.h"

@implementation AccountInfoFooterView

-(id)init{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"AccountInfoFooterView" owner:self options:nil];
    id mainView = [subviewArray objectAtIndex:0];
    
    AccountInfoFooterView * view = (AccountInfoFooterView*)mainView;
    CGRect frame = view.frame ;
    view.frame = frame ;
    
    [view needsUpdateConstraints];
    
    return mainView ;
}


@end
