//
//  SlideMenuVC.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (strong, nonatomic) NSArray *slideMenuArray;
@property (strong, nonatomic) NSArray *imageArray;
@property (strong, nonatomic) NSArray *highlightImageArray;
@end
