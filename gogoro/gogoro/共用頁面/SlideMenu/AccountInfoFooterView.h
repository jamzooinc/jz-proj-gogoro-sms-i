//
//  AccountInfoFooterView.h
//  gogoro
//
//  Created by 張啟裕 on 2016/9/13.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountInfoFooterView : UIView
@property (weak, nonatomic) IBOutlet UILabel *storeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffAcountLabel;
@property (weak, nonatomic) IBOutlet UILabel *staffNameLabel;

@end
