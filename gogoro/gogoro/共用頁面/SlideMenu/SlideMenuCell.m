//
//  SlideMenuCell.m
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import "SlideMenuCell.h"

@implementation SlideMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    
    [super setHighlighted:highlighted animated:animated];
    //NSLog(@"highlightedImage:%@",self.highlightedImage);
    //NSLog(@"normalImage:%@",self.normalImage);
    
    if (highlighted) {
        self.ImageTitle.image = self.highlightedImage;
    } else {
        self.ImageTitle.image = self.normalImage;
    }
}
@end
