//
//  SlideMenuCell.h
//  gogoro
//
//  Created by William on 2016/3/2.
//  Copyright © 2016年 JamZoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ImageTitle;
@property (strong, nonatomic) UIImage *normalImage;
@property (strong, nonatomic) UIImage *highlightedImage;
@end
